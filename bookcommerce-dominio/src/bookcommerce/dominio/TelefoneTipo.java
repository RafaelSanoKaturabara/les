package bookcommerce.dominio;

public class TelefoneTipo extends EntidadeDominio {
	private String descricao;

	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}	
}