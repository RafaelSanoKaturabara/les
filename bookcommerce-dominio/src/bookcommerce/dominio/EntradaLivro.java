package bookcommerce.dominio;

public class EntradaLivro extends EntidadeDominio {
	private Entrada entrada;
	private Livro livro;
	private float precoPago;
	private int quantidade;
	
	public Entrada getEntrada() {
		return entrada;
	}
	public void setEntrada(Entrada entrada) {
		this.entrada = entrada;
	}
	public Livro getLivro() {
		return livro;
	}
	public void setLivro(Livro livro) {
		this.livro = livro;
	}
	public float getPrecoPago() {
		return precoPago;
	}
	public void setPrecoPago(float precoPago) {
		this.precoPago = precoPago;
	}
	public int getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}	
}