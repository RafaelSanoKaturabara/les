package bookcommerce.dominio;

public class PessoaCartaoCredito extends EntidadeDominio {
	private String apelido;
	private String numero;
	private String nomeImpresso;
	private String codigoSeguranca;
	private CartaoCreditoTipo cartaoCreditoTipo;
	private int idPessoa;
	private Pessoa pessoa;
	
	public String getApelido() {
		return apelido;
	}
	public void setApelido(String apelido) {
		this.apelido = apelido;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getNomeImpresso() {
		return nomeImpresso;
	}
	public void setNomeImpresso(String nomeImpresso) {
		this.nomeImpresso = nomeImpresso;
	}
	public String getCodigoSeguranca() {
		return codigoSeguranca;
	}
	public void setCodigoSeguranca(String codigoSeguranca) {
		this.codigoSeguranca = codigoSeguranca;
	}
	public CartaoCreditoTipo getCartaoCreditoTipo() {
		return cartaoCreditoTipo;
	}
	public void setCartaoCreditoTipo(CartaoCreditoTipo cartaoCreditoTipo) {
		this.cartaoCreditoTipo = cartaoCreditoTipo;
	}
	public int getIdPessoa() {
		return idPessoa;
	}
	public void setIdPessoa(int idPessoa) {
		this.idPessoa = idPessoa;
	}
	public Pessoa getPessoa() {
		return pessoa;
	}
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
}
