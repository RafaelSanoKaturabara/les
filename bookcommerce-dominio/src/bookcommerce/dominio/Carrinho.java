package bookcommerce.dominio;

import java.util.Date;
import java.util.List;

public class Carrinho extends EntidadeDominio {
	private Pessoa pessoa;
	private Date dataUltimaAlteracao; 
	private float precoFrete;
	private CarrinhoStatus carrinhoStatus;
	private Endereco endereco;
	private List<CarrinhoItem> carrinhoItemList;
	private List<CarrinhoCupom> carrinhoCupomList;
	private List<CarrinhoPagamento> carrinhoPagamentoList;
	private Cupom cupomDevolucao;
	
	public void setDataUltimaAlteracao(Date dataUltimaAlteracao) {
		this.dataUltimaAlteracao = dataUltimaAlteracao;
	}
	public void setPrecoFrete(float precoFrete) {
		this.precoFrete = precoFrete;
	}
	public Pessoa getPessoa() {
		return pessoa;
	}
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	public List<CarrinhoItem> getCarrinhoItemList() {
		return carrinhoItemList;
	}
	public void setCarrinhoItemList(List<CarrinhoItem> carrinhoItemList) {
		this.carrinhoItemList = carrinhoItemList;
	}
	public Date getDataUltimaAlteracao() {
		return dataUltimaAlteracao;
	}
	public float getPrecoFrete() {
		return precoFrete;
	}
	public List<CarrinhoCupom> getCarrinhoCupomList() {
		return carrinhoCupomList;
	}
	public void setCarrinhoCupomList(List<CarrinhoCupom> carrinhoCupomList) {
		this.carrinhoCupomList = carrinhoCupomList;
	}
	public List<CarrinhoPagamento> getCarrinhoPagamentoList() {
		return carrinhoPagamentoList;
	}
	public void setCarrinhoPagamentoList(List<CarrinhoPagamento> carrinhoPagamentoList) {
		this.carrinhoPagamentoList = carrinhoPagamentoList;
	}
	public CarrinhoStatus getCarrinhoStatus() {
		return carrinhoStatus;
	}
	public void setCarrinhoStatus(CarrinhoStatus carrinhoStatus) {
		this.carrinhoStatus = carrinhoStatus;
	}
	public Endereco getEndereco() {
		return endereco;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	public Cupom getCupomDevolucao() {
		return cupomDevolucao;
	}
	public void setCupomDevolucao(Cupom cupomDevolucao) {
		this.cupomDevolucao = cupomDevolucao;
	}
}
