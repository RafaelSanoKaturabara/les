package bookcommerce.dominio;

public class LivroMotivoDesativacao extends EntidadeDominio {
	private int idLivro;
	private String motivo;
	public int getIdLivro() {
		return idLivro;
	}
	public void setIdLivro(int idLivro) {
		this.idLivro = idLivro;
	}
	public String getMotivo() {
		return motivo;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
}
