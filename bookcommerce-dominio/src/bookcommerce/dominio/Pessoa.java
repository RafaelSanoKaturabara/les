package bookcommerce.dominio;

import java.util.Date;
import java.util.List;

public class Pessoa extends EntidadeDominio {
	private String nome;
	private String sobrenome;
	private Date dataNascimento;
	private String cpf;
	private String email;
	private String senha;
	private String repitaSenha;
	private Telefone telefone;
	private List<PessoaEndereco> enderecoList;
	private List<PessoaCartaoCredito> cartaoCreditoList;
	private PessoaEndereco enderecoPadrao;
	private PessoaCartaoCredito cartaoCreditoPadrao;
	private PessoaGenero genero;
	private String observacao;
	private PessoaTipo pessoaTipo;
	private Carrinho carrinho;
	private Boolean Ativo;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSobrenome() {
		return sobrenome;
	}
	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}
	public Date getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}	
	public Telefone getTelefone() {
		return telefone;
	}
	public void setTelefone(Telefone telefone) {
		this.telefone = telefone;
	}
	public List<PessoaCartaoCredito> getCartaoCreditoList() {
		return cartaoCreditoList;
	}
	public void setCartaoCreditoList(List<PessoaCartaoCredito> cartaoCreditoList) {
		this.cartaoCreditoList = cartaoCreditoList;
	}
	public List<PessoaEndereco> getEnderecoList() {
		return enderecoList;
	}
	public void setEnderecoList(List<PessoaEndereco> enderecoList) {
		this.enderecoList = enderecoList;
	}
	public PessoaEndereco getEnderecoPadrao() {
		return enderecoPadrao;
	}
	public void setEnderecoPadrao(PessoaEndereco enderecoPadrao) {
		this.enderecoPadrao = enderecoPadrao;
	}
	public PessoaCartaoCredito getCartaoCreditoPadrao() {
		return cartaoCreditoPadrao;
	}
	public void setCartaoCreditoPadrao(PessoaCartaoCredito cartaoCreditoPadrao) {
		this.cartaoCreditoPadrao = cartaoCreditoPadrao;
	}
	public PessoaGenero getGenero() {
		return genero;
	}
	public void setGenero(PessoaGenero genero) {
		this.genero = genero;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public String getRepitaSenha() {
		return repitaSenha;
	}
	public void setRepitaSenha(String repitaSenha) {
		this.repitaSenha = repitaSenha;
	}
	public PessoaTipo getPessoaTipo() {
		return pessoaTipo;
	}
	public void setPessoaTipo(PessoaTipo pessoaTipo) {
		this.pessoaTipo = pessoaTipo;
	}
	public Boolean getAtivo() {
		return Ativo;
	}
	public void setAtivo(Boolean ativo) {
		Ativo = ativo;
	}
	public Carrinho getCarrinho() {
		return carrinho;
	}
	public void setCarrinho(Carrinho carrinho) {
		this.carrinho = carrinho;
	}	
}
