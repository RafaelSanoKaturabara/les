package bookcommerce.dominio;
public class GrupoPrecificacao extends EntidadeDominio{

	private String nome;

	private float margemVenda;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public float getMargemVenda() {
		return margemVenda;
	}

	public void setMargemVenda(float margemVenda) {
		this.margemVenda = margemVenda;
	}

}
