package bookcommerce.dominio;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Log extends EntidadeDominio {
	private String ip;
	private String acao;
	private String jsonEntrada;
	private	String jsonSaida;
	private String chave;
	private String dataEHoraStr;
	private Date dataEHora;
	private Pessoa pessoa;
	
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getAcao() {
		return acao;
	}
	public void setAcao(String acao) {
		this.acao = acao;
	}
	public String getJsonEntrada() {
		return jsonEntrada;
	}
	public void setJsonEntrada(String jsonEntrada) {
		this.jsonEntrada = jsonEntrada;
	}
	public String getJsonSaida() {
		return jsonSaida;
	}
	public void setJsonSaida(String jsonSaida) {
		this.jsonSaida = jsonSaida;
	}
	public Date getDataEHora() {
		return dataEHora;
	}
	public void setDataEHora(Date dataEHora) {
		this.dataEHora = dataEHora;
	}
	public Pessoa getPessoa() {
		return pessoa;
	}
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	public String getChave() {
		return chave;
	}
	public void setChave(String chave) {
		this.chave = chave;
	}
	public String getDataEHoraStr() {
		return dataEHoraStr;
	}
	public void setDataEHoraStr(Date dataEHora) {
		SimpleDateFormat formatBra = new SimpleDateFormat("dd/MM/yyyy HH:mm");		  	     
		this.dataEHoraStr = (formatBra.format(dataEHora)); 
	}	
}
