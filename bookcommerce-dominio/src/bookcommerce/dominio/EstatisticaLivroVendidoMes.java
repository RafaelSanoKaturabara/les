package bookcommerce.dominio;

import java.util.Date;

public class EstatisticaLivroVendidoMes extends EntidadeDominio {
	private Livro livroVendido;
	private int quantidade;
	private String ano;
	private String mes;
	private Date dataInicio;
	private Date dataFim;
	private int qtdeTop;
	
	public String getAno() {
		return ano;
	}
	public void setAno(String ano) {
		this.ano = ano;
	}
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public Livro getLivroVendido() {
		return livroVendido;
	}
	public void setLivroVendido(Livro livroVendido) {
		this.livroVendido = livroVendido;
	}
	public int getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
	public Date getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}
	public Date getDataFim() {
		return dataFim;
	}
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
	public int getQtdeTop() {
		return qtdeTop;
	}
	public void setQtdeTop(int qtdeTop) {
		this.qtdeTop = qtdeTop;
	}
}
