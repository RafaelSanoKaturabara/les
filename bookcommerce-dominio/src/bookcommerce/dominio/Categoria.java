package bookcommerce.dominio;
public class Categoria extends EntidadeDominio{

	private String nome;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}
