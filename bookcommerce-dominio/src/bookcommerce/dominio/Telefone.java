package bookcommerce.dominio;

public class Telefone extends EntidadeDominio {
	private String numeroCompleto;
	private TelefoneTipo telefoneTipo;
	public String getNumeroCompleto() {
		return numeroCompleto;
	}
	public void setNumeroCompleto(String numeroCompleto) {
		this.numeroCompleto = numeroCompleto;
	}
	public TelefoneTipo getTelefoneTipo() {
		return telefoneTipo;
	}
	public void setTelefoneTipo(TelefoneTipo telefoneTipo) {
		this.telefoneTipo = telefoneTipo;
	}
}
