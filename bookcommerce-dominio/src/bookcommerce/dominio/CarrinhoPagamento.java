package bookcommerce.dominio;

public class CarrinhoPagamento extends EntidadeDominio {
	private float valor;
	private String codigoPagamentoExterno;
	private Carrinho carrinho;
	private PessoaCartaoCredito pessoaCartaoCredito;
	private CarrinhoPagamentoStatus carrinhoPagamentoStatus;
	
	public float getValor() {
		return valor;
	}
	public void setValor(float valor) {
		this.valor = valor;
	}
	public String getCodigoPagamentoExterno() {
		return codigoPagamentoExterno;
	}
	public void setCodigoPagamentoExterno(String codigoPagamentoExterno) {
		this.codigoPagamentoExterno = codigoPagamentoExterno;
	}
	public Carrinho getCarrinho() {
		return carrinho;
	}
	public void setCarrinho(Carrinho carrinho) {
		this.carrinho = carrinho;
	}
	public CarrinhoPagamentoStatus getCarrinhoPagamentoStatus() {
		return carrinhoPagamentoStatus;
	}
	public void setCarrinhoPagamentoStatus(CarrinhoPagamentoStatus carrinhoPagamentoStatus) {
		this.carrinhoPagamentoStatus = carrinhoPagamentoStatus;
	}
	public PessoaCartaoCredito getPessoaCartaoCredito() {
		return pessoaCartaoCredito;
	}
	public void setPessoaCartaoCredito(PessoaCartaoCredito pessoaCartaoCredito) {
		this.pessoaCartaoCredito = pessoaCartaoCredito;
	}
}
