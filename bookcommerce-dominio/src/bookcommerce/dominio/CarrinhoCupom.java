package bookcommerce.dominio;

public class CarrinhoCupom extends EntidadeDominio {
	private Carrinho carrinho;
	private Cupom cupom;
	
	public Carrinho getCarrinho() {
		return carrinho;
	}
	public void setCarrinho(Carrinho carrinho) {
		this.carrinho = carrinho;
	}
	public Cupom getCupom() {
		return cupom;
	}
	public void setCupom(Cupom cupom) {
		this.cupom = cupom;
	}	
}
