package bookcommerce.dominio;

public class CarrinhoItem extends EntidadeDominio {
	private Livro livro;
	private float precoItem;
	private int quantidade;
	private Boolean status;
	private Carrinho carrinho;
	
	public Livro getLivro() {
		return livro;
	}
	public void setLivro(Livro livro) {
		this.livro = livro;
	}
	public float getPrecoItem() {
		return precoItem;
	}
	public void setPrecoItem(float precoItem) {
		this.precoItem = precoItem;
	}
	public int getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public Carrinho getCarrinho() {
		return carrinho;
	}
	public void setCarrinho(Carrinho carrinho) {
		this.carrinho = carrinho;
	}
}
