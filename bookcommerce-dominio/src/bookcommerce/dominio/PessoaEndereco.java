package bookcommerce.dominio;

public class PessoaEndereco extends EntidadeDominio {
	private String apelido;
	private String logradouro;
	private String numero;
	private String cep;
	private String complemento;
	private String bairro;
	private int idPessoa;
	private Cidade cidade;
	private ResidenciaTipo residenciaTipo;
	private LogradouroTipo logradouroTipo;
	private Pessoa pessoa;
	
	public String getApelido() {
		return apelido;
	}
	public void setApelido(String apelido) {
		this.apelido = apelido;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public String getNumero() {
		return numero;
	}
	public Pessoa getPessoa() {
		return pessoa;
	}
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplento(String complemento) {
		this.complemento = complemento;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}	
	public int getIdPessoa() {
		return idPessoa;
	}
	public void setIdPessoa(int idPessoa) {
		this.idPessoa = idPessoa;
	}
	public Cidade getCidade() {
		return cidade;
	}
	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}
	public ResidenciaTipo getResidenciaTipo() {
		return residenciaTipo;
	}
	public void setResidenciaTipo(ResidenciaTipo residenciaTipo) {
		this.residenciaTipo = residenciaTipo;
	}
	public LogradouroTipo getLogradouroTipo() {
		return logradouroTipo;
	}
	public void setLogradouroTipo(LogradouroTipo logradouroTipo) {
		this.logradouroTipo = logradouroTipo;
	}		
}
