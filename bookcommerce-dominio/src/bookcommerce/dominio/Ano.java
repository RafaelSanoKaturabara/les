package bookcommerce.dominio;

public class Ano extends EntidadeDominio{

	private int ano;
	
	public int getAno() {
		return ano;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}
}
