package bookcommerce.dominio;

import java.util.Date;

public class Cupom extends EntidadeDominio {
	private String codigo;
	private CupomTipo cupomTipo;
	private CupomDescontoTipo cupomDescontoTipo;
	private float valorDesconto;
	private Date validade;
	private boolean status;
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public CupomTipo getCupomTipo() {
		return cupomTipo;
	}
	public void setCupomTipo(CupomTipo cupomTipo) {
		this.cupomTipo = cupomTipo;
	}
	public CupomDescontoTipo getCupomDescontoTipo() {
		return cupomDescontoTipo;
	}
	public void setCupomDescontoTipo(CupomDescontoTipo cupomDescontoTipo) {
		this.cupomDescontoTipo = cupomDescontoTipo;
	}
	public float getValorDesconto() {
		return valorDesconto;
	}
	public void setValorDesconto(float valorDesconto) {
		this.valorDesconto = valorDesconto;
	}
	public Date getValidade() {
		return validade;
	}
	public void setValidade(Date validade) {
		this.validade = validade;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
}
