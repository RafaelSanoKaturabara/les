package bookcommerce.dominio;

import java.util.List;

public class PesquisaLivro extends EntidadeDominio {	
	private String procurar;
	private List<String> paramList;
	private List<Livro> livroList;
	
	public String getProcurar() {
		return procurar;
	}
	public void setProcurar(String procurar) {
		this.procurar = procurar;
	}
	public List<Livro> getLivroList() {
		return livroList;
	}
	public void setLivroList(List<Livro> livroList) {
		this.livroList = livroList;
	}
	public List<String> getParamList() {
		return paramList;
	}
	public void setParamList(List<String> paramList) {
		this.paramList = paramList;
	}		
}
