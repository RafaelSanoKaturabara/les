package bookcommerce.controle.web.login;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import bookcommerce.core.impl.dao.PessoaDAO;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.Pessoa;

public class LoginController {
	HttpServletRequest request;
	HttpServletResponse response;
	public LoginController(HttpServletRequest request, HttpServletResponse response) {
		this.request = request;
		this.response = response;
	}
	public Pessoa getPessoaLogada() throws ServletException, IOException {
		// verificar se o cast funciona com pessoaLogada null
		Pessoa pessoaLogada = (Pessoa) request.getSession().getAttribute("pessoaLogada");
		Boolean flgManterLogado = false;
		if(pessoaLogada == null) {
			// Recupera cookie
			pessoaLogada = new Pessoa();
	        for (Cookie cookie : request.getCookies()) {
	            if (cookie.getName().equals("manterLogado")) {
	            	//if(cookie.getValue().equals("true")) { 
	            		flgManterLogado = true;
	            	//}	            	
	            } else if(cookie.getName().equals("emailLogado")) {
        			pessoaLogada.setEmail(cookie.getValue());
        		} else if(cookie.getName().equals("senhaLogado")) {
        			pessoaLogada.setSenha(cookie.getValue());
        		} 
	        }
	        if(flgManterLogado) {
	        	// buscar no dao 
	        	PessoaDAO pDAO = new PessoaDAO();
	        	List<EntidadeDominio> entidadePessoaList = null;
	        	try {
					entidadePessoaList = pDAO.consultar(pessoaLogada);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        	if(entidadePessoaList != null && entidadePessoaList.size() == 1) {
	        		pessoaLogada = (Pessoa) entidadePessoaList.get(0);
	        		setPessoaLogada(pessoaLogada);
	        		return pessoaLogada;
	        	}
	        }
	        return null;
		}
		
		return pessoaLogada;		
		//return (Pessoa) request.getSession().getAttribute("pessoaLogada");
	}
	
	public void setPessoaLogada(Pessoa pessoaLogada) throws ServletException, IOException {
		// verificar se o cast funciona com pessoaLogada null

		request.getSession().setAttribute("pessoaLogada", pessoaLogada);
		// setando cookie
		response.addCookie(new Cookie("emailLogado", pessoaLogada.getEmail()));
		response.addCookie(new Cookie("manterLogado", "true"));
		
		String json = new Gson().toJson(pessoaLogada);
		response.setContentType("application/json"); 
		response.setCharacterEncoding("utf-8"); 
		request.getSession().setAttribute("pessoaLogadaJson", json);
	}
	
	public void logoffPessoa() {
		response.addCookie(new Cookie("emailLogado", ""));
		response.addCookie(new Cookie("senhaLogado", "")); // veio a senha?
		response.addCookie(new Cookie("manterLogado", "false"));
		request.getSession().invalidate();
	}
}
