package bookcommerce.controle.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import bookcommerce.controle.web.login.LoginController;
import bookcommerce.core.aplicacao.RespostaWebService;
import bookcommerce.dominio.Pessoa;

public class ServletLogged extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ServletLogged() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		verifyLogged(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		verifyLogged(request, response);
	}
	
	protected void verifyLogged(HttpServletRequest request, 
			HttpServletResponse response) throws ServletException, IOException {
		LoginController loginController = new LoginController(request, response);
		// verificar se está logado
		Pessoa pessoaLogada = loginController.getPessoaLogada();
		if(pessoaLogada == null) {
//			RequestDispatcher requestDispatcher = request
//			        .getRequestDispatcher("/bookcommerce-web/login.jsp");
//			requestDispatcher.forward(request,response);
			loginController.logoffPessoa();
			RespostaWebService resp = new RespostaWebService(275, "error", "Usuário não está logado", false);
			String json = new Gson().toJson(resp);
			response.setContentType("application/json"); 
			response.setCharacterEncoding("utf-8"); 
			response.getWriter().write(json);
		} else {
			// se estiver logado, envia para o controller
			new ControllerWeb().doProcessRequest(request, response);			
		}
	}
}
