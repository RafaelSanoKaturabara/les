
package bookcommerce.controle.web.command.impl;

import bookcommerce.controle.web.command.ICommand;
import bookcommerce.core.IFachada;
import bookcommerce.core.impl.controle.Fachada;



public abstract class AbstractCommand implements ICommand {

	protected IFachada fachada = new Fachada();

}
