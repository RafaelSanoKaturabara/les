
package bookcommerce.controle.web.command.impl;

import bookcommerce.core.aplicacao.Resultado;
import bookcommerce.dominio.EntidadeDominio;


public class ConsultarCommand extends AbstractCommand{

	
	public Resultado execute(EntidadeDominio entidade) {
		
		return fachada.consultar(entidade);
	}

}
