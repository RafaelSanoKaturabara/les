
package bookcommerce.controle.web.command.impl;

import javax.servlet.http.HttpServletRequest;

import bookcommerce.core.aplicacao.Resultado;
import bookcommerce.dominio.EntidadeDominio;


public class ExcluirCommand extends AbstractCommand{

	
	public Resultado execute(EntidadeDominio entidade) {
		
		return fachada.excluir(entidade);
	}

}
