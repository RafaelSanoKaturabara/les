
package bookcommerce.controle.web.command;

import bookcommerce.core.aplicacao.Resultado;
import bookcommerce.dominio.EntidadeDominio;


public interface ICommand {

	public Resultado execute(EntidadeDominio entidade);
	
}
