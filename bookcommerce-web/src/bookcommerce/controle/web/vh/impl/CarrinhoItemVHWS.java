package bookcommerce.controle.web.vh.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import bookcommerce.controle.web.login.LoginController;
import bookcommerce.controle.web.vh.IViewHelper;
import bookcommerce.core.aplicacao.RespostaWebService;
import bookcommerce.core.aplicacao.Resultado;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.Pessoa;
import bookcommerce.dominio.Carrinho;
import bookcommerce.dominio.CarrinhoItem;

public class CarrinhoItemVHWS implements IViewHelper {

	private void putPessoaIntoCarrinhoItem(Pessoa pessoa, CarrinhoItem carrinhoItem) {
		if(carrinhoItem.getCarrinho() == null)
			carrinhoItem.setCarrinho(new Carrinho());
		carrinhoItem.getCarrinho().setPessoa(pessoa);
	}
	
	public EntidadeDominio getEntidade(HttpServletRequest request) {
		CarrinhoItem carrinhoItem = new CarrinhoItem();
		try {
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
			String json = "";
			if(bufferedReader != null) {
				json = bufferedReader.readLine();
			}
			
			Pessoa pessoa = new Pessoa();
			try {
				pessoa = new LoginController(request, null).getPessoaLogada();
			} catch (ServletException se){
				se.printStackTrace();
			} catch (IOException oi) {
				oi.printStackTrace();
			}			
			
			if(json == null) { 
				if(pessoa != null)
					putPessoaIntoCarrinhoItem(pessoa, carrinhoItem);
				return carrinhoItem;
			}
			
			ObjectMapper objectMapper = new ObjectMapper();
			carrinhoItem = objectMapper.readValue(json, CarrinhoItem.class);			
			if(pessoa != null)
				putPessoaIntoCarrinhoItem(pessoa, carrinhoItem);
			
			if(carrinhoItem.getCarrinho().getDataUltimaAlteracao() == null)
				carrinhoItem.getCarrinho().setDataUltimaAlteracao(new Date());
			return carrinhoItem;
		} catch (IOException ioe){			
			ioe.printStackTrace();
			System.out.println(ioe.getMessage());
		}		
		return carrinhoItem;
	}

	public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		RespostaWebService resp;
		if(resultado.getMsg() != null && resultado.getMsg() != "")
			resp = new RespostaWebService(250, "Erro", resultado.getMsg(), false, resultado);	
		else
			resp = new RespostaWebService(200, "Sucesso", true, resultado);		
		String json = new Gson().toJson(resp);
		response.setContentType("application/json"); 
		response.setCharacterEncoding("utf-8"); 
		response.getWriter().write(json);		
	}
}
