package bookcommerce.controle.web.vh.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import bookcommerce.controle.web.vh.IViewHelper;
import bookcommerce.core.aplicacao.RespostaWebService;
import bookcommerce.core.aplicacao.Resultado;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.PesquisaLivro;

public class PesquisarLivroVHWS implements IViewHelper {

	public EntidadeDominio getEntidade(HttpServletRequest request) {
		PesquisaLivro pesquisaLivro = new PesquisaLivro();
		try {
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
			String json = "";
			if(bufferedReader != null) {
				json = bufferedReader.readLine();
			}
			
			if(json == null) { 
				return pesquisaLivro;
			}
			
			ObjectMapper objectMapper = new ObjectMapper();
			pesquisaLivro = objectMapper.readValue(json, PesquisaLivro.class);					
			pesquisaLivro.setProcurar(pesquisaLivro.getProcurar().replaceAll("%20", " ").toLowerCase());			
			String[] paramArray = pesquisaLivro.getProcurar().split(" ");
			
			if(paramArray != null && paramArray.length > 0) { // tem parametro?
				List<String> parametroList = new ArrayList<String>(); 				
				for(String paramWork : paramArray) {
					parametroList.add(paramWork);
				}
				pesquisaLivro.setParamList(parametroList);
			}
			
			return pesquisaLivro;
		} catch (IOException ioe){			
			ioe.printStackTrace();
			System.out.println(ioe.getMessage());
		}		
		return pesquisaLivro;
	}

	public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		RespostaWebService resp;
		if(resultado.getMsg() != null && resultado.getMsg() != "")
			resp = new RespostaWebService(250, "Erro", resultado.getMsg(), false, resultado);	
		else
			resp = new RespostaWebService(200, "Sucesso", true, resultado);		
		String json = new Gson().toJson(resp);
		response.setContentType("application/json"); 
		response.setCharacterEncoding("utf-8"); 
		response.getWriter().write(json);
	}
}
