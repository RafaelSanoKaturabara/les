package bookcommerce.controle.web.vh.impl;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.google.gson.Gson;

import bookcommerce.controle.web.vh.IViewHelper;
import bookcommerce.core.aplicacao.Resultado;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.GrupoPrecificacao;

public class GrupoPrecificacaoVHWS implements IViewHelper {

	public EntidadeDominio getEntidade(HttpServletRequest request) {
		GrupoPrecificacao grupoPrecificacao = new GrupoPrecificacao();
		int idGrupoPrecificacao;
		String idEstadoString = request.getParameter("id");
		if(idEstadoString != null && idEstadoString != "") {
			idGrupoPrecificacao = Integer.parseInt(request.getParameter("idGrupoPrecificacao"));
			grupoPrecificacao.setId(idGrupoPrecificacao);
		}
		return grupoPrecificacao;
	}

	public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		
		String json = new Gson().toJson(resultado);
		response.setContentType("application/json"); 
		response.setCharacterEncoding("utf-8"); 
		response.getWriter().write(json);		
	}
}
