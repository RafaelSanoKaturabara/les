package bookcommerce.controle.web.vh.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import bookcommerce.controle.web.login.LoginController;
import bookcommerce.controle.web.vh.IViewHelper;
import bookcommerce.core.aplicacao.RespostaWebService;
import bookcommerce.core.aplicacao.Resultado;
import bookcommerce.dominio.Carrinho;
import bookcommerce.dominio.CarrinhoStatus;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.Pessoa;

public class PagarCarrinhoVHWS implements IViewHelper {

	private void putPessoaInCarrinho(Pessoa pessoa, Carrinho carrinho) {
		carrinho.setPessoa(pessoa);
	}
	
	public EntidadeDominio getEntidade(HttpServletRequest request) {
		Carrinho carrinho = new Carrinho();
		try {
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
			String json = "";
			if(bufferedReader != null) {
				json = bufferedReader.readLine();
			}
			
			Pessoa pessoa = new Pessoa();
			try {
				pessoa = new LoginController(request, null).getPessoaLogada();
			} catch (ServletException se){
				se.printStackTrace();
			} catch (IOException oi) {
				oi.printStackTrace();
			}
			
			if(json == null) {
				if(pessoa != null) {
					putPessoaInCarrinho(pessoa, carrinho);
				}
				return carrinho;			
			}
			ObjectMapper objectMapper = new ObjectMapper();
			carrinho = objectMapper.readValue(json, Carrinho.class);
			
			if(pessoa != null) {
				putPessoaInCarrinho(pessoa, carrinho);
			}			
			carrinho.setCarrinhoStatus(new CarrinhoStatus());
			carrinho.getCarrinhoStatus().setId(3);
			carrinho.setDataUltimaAlteracao(new Date());
			return carrinho;
		} catch (IOException ioe){			
			ioe.printStackTrace();
			System.out.println(ioe.getMessage());
		}		
		return carrinho;
	}

	public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		RespostaWebService resp;
		if(resultado.getMsg() != null && resultado.getMsg() != "")
			resp = new RespostaWebService(250, "Erro", resultado.getMsg(), false, resultado);	
		else
			resp = new RespostaWebService(200, "Sucesso", true, resultado);		
		String json = new Gson().toJson(resp);
		response.setContentType("application/json"); 
		response.setCharacterEncoding("utf-8"); 
		response.getWriter().write(json);		
	}
}
