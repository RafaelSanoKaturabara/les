package bookcommerce.controle.web.vh.impl;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.google.gson.Gson;
import bookcommerce.controle.web.vh.IViewHelper;
import bookcommerce.core.aplicacao.Resultado;
import bookcommerce.dominio.Categoria;
import bookcommerce.dominio.EntidadeDominio;

public class CategoriaVHWS implements IViewHelper{

	public EntidadeDominio getEntidade(HttpServletRequest request) {
		Categoria categoria = new Categoria();
		int id;
		String idString = request.getParameter("Id");
		if(idString != null && idString != "") {
			id = Integer.parseInt(request.getParameter("Id"));
			categoria.setId(id);
		}
		return categoria;
	}

	public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		String json = new Gson().toJson(resultado);
		response.setContentType("application/json"); 
		response.setCharacterEncoding("utf-8"); 
		response.getWriter().write(json);	
		
	}

}
