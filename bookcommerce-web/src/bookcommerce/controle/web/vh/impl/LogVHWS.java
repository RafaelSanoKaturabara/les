package bookcommerce.controle.web.vh.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import bookcommerce.controle.web.vh.IViewHelper;
import bookcommerce.core.aplicacao.RespostaWebService;
import bookcommerce.core.aplicacao.Resultado;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.Log;
import bookcommerce.dominio.Pessoa;
public class LogVHWS implements IViewHelper {

	public EntidadeDominio getEntidade(HttpServletRequest request) {
		Log log = new Log();
		try {
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
			String json = "";
			if(bufferedReader != null) {
				json = bufferedReader.readLine();
			}
			
			if(json == null) { 
				return log;
			}
			
			ObjectMapper objectMapper = new ObjectMapper();
			log = objectMapper.readValue(json, Log.class);			
			
			return log;
		} catch (IOException ioe){			
			ioe.printStackTrace();
			System.out.println(ioe.getMessage());
		}		
		return log;
	}

	public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		RespostaWebService resp;
		if(resultado.getMsg() != null && resultado.getMsg() != "")
			resp = new RespostaWebService(250, "Erro", resultado.getMsg(), false, resultado);	
		else
			resp = new RespostaWebService(200, "Sucesso", true, resultado);		
		String json = new Gson().toJson(resp);
		response.setContentType("application/json"); 
		response.setCharacterEncoding("utf-8"); 
		response.getWriter().write(json);
	}
	
	public EntidadeDominio getLog(EntidadeDominio entidade, Resultado resultado, HttpServletRequest request) {
		String operacao = request.getParameter("operacao");
		// Montando objeto de log
		Gson gson = new Gson();
		Log log = new Log();
		log.setJsonEntrada(gson.toJson(entidade));
		log.setIp(getClientIp(request));
		log.setAcao(operacao);
		log.setDataEHora(new Date());
		log.setPessoa((Pessoa) request.getSession().getAttribute("pessoaLogada"));
		log.setChave(request.getRequestURI());		
		
		if(resultado != null && resultado.getEntidades() != null && resultado.getEntidades().size() > 0) // Obteve retorno?
			log.setJsonSaida(gson.toJson(resultado.getEntidades()));	
		else 
			log.setJsonSaida(gson.toJson(resultado.getMsg()));
		return log;
	}

	private String getClientIp(HttpServletRequest request) {
		String ip = request.getHeader("X-Forwarded-For");  
	    if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {  
	        ip = request.getHeader("Proxy-Client-IP");  
	    }  
	    if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {  
	        ip = request.getHeader("WL-Proxy-Client-IP");  
	    }  
	    if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {  
	        ip = request.getHeader("HTTP_X_FORWARDED_FOR");  
	    }  
	    if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {  
	        ip = request.getHeader("HTTP_X_FORWARDED");  
	    }  
	    if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {  
	        ip = request.getHeader("HTTP_X_CLUSTER_CLIENT_IP");  
	    }  
	    if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {  
	        ip = request.getHeader("HTTP_CLIENT_IP");  
	    }  
	    if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {  
	        ip = request.getHeader("HTTP_FORWARDED_FOR");  
	    }  
	    if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {  
	        ip = request.getHeader("HTTP_FORWARDED");  
	    }  
	    if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {  
	        ip = request.getHeader("HTTP_VIA");  
	    }  
	    if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {  
	        ip = request.getHeader("REMOTE_ADDR");  
	    }  
	    if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {  
	        ip = request.getRemoteAddr();  
	    }  
	    return ip;  
    }
}
