package bookcommerce.controle.web.vh.impl;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import bookcommerce.controle.web.vh.IViewHelper;
import bookcommerce.core.aplicacao.Resultado;
import bookcommerce.dominio.Editora;
import bookcommerce.dominio.EntidadeDominio;

public class EditoraVHWS implements IViewHelper {
	
	public EntidadeDominio getEntidade(HttpServletRequest request) {
		Editora editora = new Editora();
		int id;
		String idString = request.getParameter("Id");
		if(idString != null && idString != "") {
			id = Integer.parseInt(request.getParameter("Id"));
			editora.setId(id);
		}
		return editora;
	}

	public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		String json = new Gson().toJson(resultado);
		response.setContentType("application/json"); 
		response.setCharacterEncoding("utf-8"); 
		response.getWriter().write(json);			
	}
}
