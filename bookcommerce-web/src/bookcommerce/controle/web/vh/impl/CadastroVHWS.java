package bookcommerce.controle.web.vh.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import bookcommerce.controle.web.vh.IViewHelper;
import bookcommerce.core.aplicacao.RespostaWebService;
import bookcommerce.core.aplicacao.Resultado;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.Pessoa;

public class CadastroVHWS implements IViewHelper {

	public EntidadeDominio getEntidade(HttpServletRequest request) {
		Pessoa pessoa = null;
		try {
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
			String json = "";
			if(bufferedReader != null) {
				json = bufferedReader.readLine();
			}
			if(json == null)
				return new Pessoa();
			ObjectMapper objectMapper = new ObjectMapper();
			pessoa = objectMapper.readValue(json, Pessoa.class);
			return pessoa;
		} catch (IOException ioe){			
			ioe.printStackTrace();
			System.out.println(ioe.getMessage());
		}
		return pessoa;
		
//		
//		Pessoa pessoa = new Pessoa();
//		pessoa.setTelefone(new Telefone());
//		pessoa.getTelefone().setTelefoneTipo(new TelefoneTipo());
//		pessoa.setEnderecoPadrao(new PessoaEndereco());
//		pessoa.setCartaoCreditoPadrao(new PessoaCartaoCredito());
//		pessoa.setGenero(new PessoaGenero());
//		// pessoa
//		if(request.getParameter("idPessoa") != null && request.getParameter("idPessoa") != "") {
//			pessoa.setId(Integer.parseInt(request.getParameter("idPessoa")));
//		}
//		pessoa.setEmail(request.getParameter("email"));
//		pessoa.setSenha(request.getParameter("senha"));
//		pessoa.setCpf(request.getParameter("cpf"));
//		pessoa.setNome(request.getParameter("nome"));
//		pessoa.setSobrenome(request.getParameter("sobrenome"));
//		if(request.getParameter("idGenero") != null && request.getParameter("idGenero") != "") {			
//			pessoa.getGenero().setId(Integer.parseInt(request.getParameter("idGenero")));
//		}	
//		pessoa.getTelefone().setNumero(request.getParameter("telefone"));
//		if(request.getParameter("formModalPessoaTelefoneTipo") != null && request.getParameter("formModalPessoaTelefoneTipo") != "") {			
//			pessoa.getTelefone().getTelefoneTipo().setId(Integer.parseInt(request.getParameter("formModalPessoaTelefoneTipo")));
//		}
//		if(request.getParameter("dataNascimento") != null && request.getParameter("dataNascimento") != "") {			
//			pessoa.setDataNascimento(ConverteDate.converteStringDate(request.getParameter("dataNascimento")));
//		}
//		pessoa.setObservacao(request.getParameter("obervacao"));
//		if(request.getParameter("idCartaoPadrao") != null && request.getParameter("idCartaoPadrao") != "") {			
//			pessoa.getCartaoCreditoPadrao().setId(Integer.parseInt(request.getParameter("idCartaoPadrao")));
//		}
//		if(request.getParameter("idEnderecoPadrao") != null && request.getParameter("idEnderecoPadrao") != "") {			
//			pessoa.getEnderecoPadrao().setId(Integer.parseInt(request.getParameter("idEnderecoPadrao")));
//		}
//		// PessoaEndereco
//		
//		// PessoaCart�o
//		return pessoa;
	}

	public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		RespostaWebService resp;
		if(resultado.getMsg() != null && resultado.getMsg() != "")
			resp = new RespostaWebService(250, "Erro", resultado.getMsg(), false, resultado);	
		else
			resp = new RespostaWebService(200, "Sucesso", true, resultado);		
		String json = new Gson().toJson(resp);
		response.setContentType("application/json"); 
		response.setCharacterEncoding("utf-8"); 
		response.getWriter().write(json);
	}
}
