package bookcommerce.controle.web.vh.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import bookcommerce.controle.web.vh.IViewHelper;
import bookcommerce.core.aplicacao.RespostaWebService;
import bookcommerce.core.aplicacao.Resultado;
import bookcommerce.dominio.Ano;
import bookcommerce.dominio.Autor;
import bookcommerce.dominio.Categoria;
import bookcommerce.dominio.Editora;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.GrupoPrecificacao;
import bookcommerce.dominio.Livro;

public class LivroVHWS implements IViewHelper{

	public EntidadeDominio getEntidade(HttpServletRequest request) {
		Livro livro = new Livro();
		
		livro.setCodBarras(request.getParameter("codigoBarras"));
		livro.setTitulo(request.getParameter("titulo"));
		livro.setIsbn(request.getParameter("isbn"));
		livro.setSinopse(request.getParameter("sinopse"));
		
		if(request.getParameter("preco") != null && request.getParameter("preco") != "") {
			livro.setPreco(Float.parseFloat(request.getParameter("preco")));
		}
		
		if(request.getParameter("id") != null && request.getParameter("id") != "") {
			livro.setId(Integer.parseInt(request.getParameter("id")));
		}
		
		if(request.getParameter("idAno") != null && request.getParameter("idAno") != "") {
			livro.setAno(new Ano());
			livro.getAno().setId(Integer.parseInt(request.getParameter("idAno")));
		}
		
		if(request.getParameter("idAno") != null && request.getParameter("idAno") != "") {
			livro.setAno(new Ano());
			livro.getAno().setId(Integer.parseInt(request.getParameter("idAno")));
		}
		
		if(request.getParameter("idEditora") != null && request.getParameter("idEditora") != "") {
			livro.setEditora(new Editora());
			livro.getEditora().setId(Integer.parseInt(request.getParameter("idEditora")));
		}
		
		if(request.getParameter("idgrupoPrecificacao") != null && request.getParameter("idgrupoPrecificacao") != "") {
			livro.setGrupoPrecificacao(new GrupoPrecificacao());
			livro.getGrupoPrecificacao().setId(Integer.parseInt(request.getParameter("idgrupoPrecificacao")));
		}
		
		if(request.getParameter("idgrupoPrecificacao") != null && request.getParameter("idgrupoPrecificacao") != "") {
			livro.setGrupoPrecificacao(new GrupoPrecificacao());
			livro.getGrupoPrecificacao().setId(Integer.parseInt(request.getParameter("idgrupoPrecificacao")));
		}
		
		if(request.getParameter("edicao") != null && request.getParameter("edicao") != "")
			livro.setEdicao(Integer.parseInt( request.getParameter("edicao")));
		if(request.getParameter("qtdPaginas") != null && request.getParameter("qtdPaginas") != "")
			livro.setQtdPaginas(Integer.parseInt(request.getParameter("qtdPaginas")));
		if(request.getParameter("altura") != null && request.getParameter("altura") != "")
			livro.setAltura(Float.parseFloat( request.getParameter("altura")));
		if(request.getParameter("largura") != null && request.getParameter("largura") != "")
			livro.setLargura(Float.parseFloat(request.getParameter("largura")));
		if(request.getParameter("peso") != null && request.getParameter("peso") != "")
			livro.setPeso(Float.parseFloat(request.getParameter("peso")));
		if(request.getParameter("profundidade") != null && request.getParameter("profundidade") != "")
			livro.setProfundidade(Float.parseFloat(request.getParameter("profundidade")));
		
		livro.setAtivo(Boolean.parseBoolean(request.getParameter("ativo")));
		
		String idCategoriaArray[] = request.getParameterValues("idCategoriaArray[]");
		if(idCategoriaArray != null) {
			List<Categoria> categoriaList = new ArrayList<Categoria>();
			for(String idCategoria : idCategoriaArray) {
				if(idCategoria != null || idCategoria != "") {
					Categoria categoria = new Categoria();
					categoria.setId(Integer.parseInt(idCategoria));
					categoriaList.add(categoria);
				}
			}
			livro.setCategoriaList(categoriaList);
		}
		
		String[] idAutorArray = request.getParameterValues("idAutorArray[]");
		if(idAutorArray != null) {
			List<Autor> autorList = new ArrayList<Autor>();
			for(String idAutor : idAutorArray) {
				if(idAutor != null || idAutor != "") {
					Autor autor = new Autor();
					autor.setId(Integer.parseInt(idAutor));
					autorList.add(autor);
				}
			}
			livro.setAutorList(autorList);
		}
		
		return livro;
	}

	public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		RespostaWebService resp;
		if(resultado.getMsg() != null && resultado.getMsg() != "")
			resp = new RespostaWebService(250, "error", resultado.getMsg(), false, resultado);	
		else
			resp = new RespostaWebService(200, "success", true, resultado);		
		String json = new Gson().toJson(resp);
		response.setContentType("application/json"); 
		response.setCharacterEncoding("utf-8"); 
		response.getWriter().write(json);
		
	}

}
