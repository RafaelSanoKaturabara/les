package bookcommerce.controle.web.vh.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import bookcommerce.controle.web.login.LoginController;
import bookcommerce.controle.web.vh.IViewHelper;
import bookcommerce.core.aplicacao.RespostaWebService;
import bookcommerce.core.aplicacao.Resultado;
import bookcommerce.dominio.Carrinho;
import bookcommerce.dominio.CarrinhoPagamento;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.Pessoa;

public class CarrinhoPagamentoVHWS implements IViewHelper {

	private void putPessoaNoCarrinhoPessoa(Pessoa pessoa, CarrinhoPagamento carrinhoPagamento) {
		carrinhoPagamento.setCarrinho(new Carrinho());
		carrinhoPagamento.getCarrinho().setPessoa(pessoa);
	}
	
	public EntidadeDominio getEntidade(HttpServletRequest request) {
		CarrinhoPagamento carrinhoPagamento = new CarrinhoPagamento();
		try {
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
			String json = "";
			if(bufferedReader != null) {
				json = bufferedReader.readLine();
			}
			
			Pessoa pessoa = new Pessoa();
			try {
				pessoa = new LoginController(request, null).getPessoaLogada();
			} catch (ServletException se){
				se.printStackTrace();
			} catch (IOException oi) {
				oi.printStackTrace();
			}
			
			if(json == null) {
				if(pessoa != null) {
					putPessoaNoCarrinhoPessoa(pessoa, carrinhoPagamento);
				}
				return carrinhoPagamento;			
			}
			ObjectMapper objectMapper = new ObjectMapper();
			carrinhoPagamento = objectMapper.readValue(json, CarrinhoPagamento.class);
			
			if(pessoa != null) {
				putPessoaNoCarrinhoPessoa(pessoa, carrinhoPagamento);
			}			
			
			return carrinhoPagamento;
		} catch (IOException ioe){			
			ioe.printStackTrace();
			System.out.println(ioe.getMessage());
		}		
		return carrinhoPagamento;
	}

	public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		RespostaWebService resp;
		if(resultado.getMsg() != null && resultado.getMsg() != "")
			resp = new RespostaWebService(250, "Erro", resultado.getMsg(), false, resultado);	
		else
			resp = new RespostaWebService(200, "Sucesso", true, resultado);		
		String json = new Gson().toJson(resp);
		response.setContentType("application/json"); 
		response.setCharacterEncoding("utf-8"); 
		response.getWriter().write(json);
	}
}
