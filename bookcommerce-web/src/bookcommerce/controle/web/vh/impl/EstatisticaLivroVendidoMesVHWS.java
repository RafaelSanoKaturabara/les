package bookcommerce.controle.web.vh.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import bookcommerce.controle.web.vh.IViewHelper;
import bookcommerce.core.aplicacao.RespostaWebService;
import bookcommerce.core.aplicacao.Resultado;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.EstatisticaLivroVendidoMes;

public class EstatisticaLivroVendidoMesVHWS implements IViewHelper {

	public EntidadeDominio getEntidade(HttpServletRequest request) {
		EstatisticaLivroVendidoMes estatisticaLivroVendidoMes = new EstatisticaLivroVendidoMes();
		try {
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
			String json = "";
			if(bufferedReader != null) {
				json = bufferedReader.readLine();
			}
			
			if(json == null) { 
				return estatisticaLivroVendidoMes;
			}
			
			ObjectMapper objectMapper = new ObjectMapper();
			estatisticaLivroVendidoMes = objectMapper.readValue(json, EstatisticaLivroVendidoMes.class);	
			
			return estatisticaLivroVendidoMes;
		} catch (IOException ioe){			
			ioe.printStackTrace();
			System.out.println(ioe.getMessage());
		}		
		return estatisticaLivroVendidoMes;
	}

	public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		RespostaWebService resp;
		if(resultado.getMsg() != null && resultado.getMsg() != "")
			resp = new RespostaWebService(250, "Erro", resultado.getMsg(), false, resultado);	
		else
			resp = new RespostaWebService(200, "Sucesso", true, resultado);		
		String json = new Gson().toJson(resp);
		response.setContentType("application/json"); 
		response.setCharacterEncoding("utf-8"); 
		response.getWriter().write(json);
	}
}
