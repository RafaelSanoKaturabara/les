package bookcommerce.controle.web.vh.impl;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import bookcommerce.controle.web.vh.IViewHelper;
import bookcommerce.core.aplicacao.RespostaWebService;
import bookcommerce.core.aplicacao.Resultado;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.LivroImagem;

public class LivroImagemVHWS implements IViewHelper {

	public EntidadeDominio getEntidade(HttpServletRequest request) {
		LivroImagem livroImagem = new LivroImagem();
		if(request.getParameter("idLivro") != null && request.getParameter("idLivro") != "") {
			livroImagem.setIdLivro(Integer.parseInt(request.getParameter("idLivro")));
		}
		livroImagem.setNome(request.getParameter("nome"));
		livroImagem.setImagem( request.getParameter("imagem"));
		return livroImagem;
	}

	public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		RespostaWebService resp;
		if(resultado.getMsg() != null && resultado.getMsg() != "")
			resp = new RespostaWebService(250, "error", resultado.getMsg(), false, resultado);	
		else
			resp = new RespostaWebService(200, "success", true, resultado);		
		String json = new Gson().toJson(resp);
		response.setContentType("application/json"); 
		response.setCharacterEncoding("utf-8"); 
		response.getWriter().write(json);		
	}
}
