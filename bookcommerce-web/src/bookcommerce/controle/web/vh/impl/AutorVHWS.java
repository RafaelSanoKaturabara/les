package bookcommerce.controle.web.vh.impl;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import bookcommerce.controle.web.vh.IViewHelper;
import bookcommerce.core.aplicacao.Resultado;
import bookcommerce.dominio.Autor;
import bookcommerce.dominio.EntidadeDominio;

public class AutorVHWS implements IViewHelper{

	public EntidadeDominio getEntidade(HttpServletRequest request) {
		Autor autor = new Autor();
		int id;
		String idString = request.getParameter("Id");
		if(idString != null && idString != "") {
			id = Integer.parseInt(request.getParameter("Id"));
			autor.setId(id);
		}
		return autor;
	}

	public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		String json = new Gson().toJson(resultado);
		response.setContentType("application/json"); 
		response.setCharacterEncoding("utf-8"); 
		response.getWriter().write(json);	
		
	}

}
