package bookcommerce.controle.web.vh.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import bookcommerce.controle.web.login.LoginController;
import bookcommerce.controle.web.vh.IViewHelper;
import bookcommerce.core.aplicacao.RespostaWebService;
import bookcommerce.core.aplicacao.Resultado;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.Pessoa;

public class LoginVHWS implements IViewHelper {

	public EntidadeDominio getEntidade(HttpServletRequest request) {
		Pessoa pessoa = null;
		try {
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
			String json = "";
			if(bufferedReader != null) {
				json = bufferedReader.readLine();
			}
			if(json == null)
				return new Pessoa();
			ObjectMapper objectMapper = new ObjectMapper();
			pessoa = objectMapper.readValue(json, Pessoa.class);
			return pessoa;
		} catch (IOException ioe){			
			ioe.printStackTrace();
			System.out.println(ioe.getMessage());
		}
		return pessoa;
	}

	public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		RespostaWebService resp;
		
		if(!resultado.getEntidades().isEmpty()) {
			LoginController loginController = new LoginController(request, response);
			Pessoa pessoa = (Pessoa) resultado.getEntidades().get(0);
			loginController.setPessoaLogada(pessoa);	
			response.addCookie(new Cookie("emailLogado", pessoa.getEmail()));
			response.addCookie(new Cookie("manterLogado", "true"));
			resp = new RespostaWebService(200, "Sucesso", true, resultado);		
		} else
			resp = new RespostaWebService(250, "Erro", resultado.getMsg(), false, resultado);
		String json = new Gson().toJson(resp);
		response.setContentType("application/json"); 
		response.setCharacterEncoding("utf-8"); 
		response.getWriter().write(json);		
	}
}
