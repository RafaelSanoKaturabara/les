package bookcommerce.controle.web.vh.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import bookcommerce.controle.web.login.LoginController;
import bookcommerce.controle.web.vh.IViewHelper;
import bookcommerce.core.aplicacao.RespostaWebService;
import bookcommerce.core.aplicacao.Resultado;
import bookcommerce.dominio.Carrinho;
import bookcommerce.dominio.CarrinhoCupom;
import bookcommerce.dominio.CarrinhoStatus;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.Pessoa;

public class CarrinhoCupomVHWS implements IViewHelper {

	private void putPessoaIntoCarrinhoCupom(Pessoa pessoa, CarrinhoCupom carrinhoCupom) {
		carrinhoCupom.setCarrinho(new Carrinho());
		carrinhoCupom.getCarrinho().setPessoa(pessoa);
	}
	
	public EntidadeDominio getEntidade(HttpServletRequest request) {
		CarrinhoCupom carrinhoCupom = new CarrinhoCupom();
		try {
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
			String json = "";
			if(bufferedReader != null) {
				json = bufferedReader.readLine();
			}
			
			Pessoa pessoa = new Pessoa();
			try {
				pessoa = new LoginController(request, null).getPessoaLogada();
			} catch (ServletException se){
				se.printStackTrace();
			} catch (IOException oi) {
				oi.printStackTrace();
			}			
			
			if(json == null) { 
				if(pessoa != null)
					putPessoaIntoCarrinhoCupom(pessoa, carrinhoCupom);
				return carrinhoCupom;
			}
			
			ObjectMapper objectMapper = new ObjectMapper();
			carrinhoCupom = objectMapper.readValue(json, CarrinhoCupom.class);			
			if(pessoa != null)
				putPessoaIntoCarrinhoCupom(pessoa, carrinhoCupom);
			
			if(carrinhoCupom.getCarrinho().getDataUltimaAlteracao() == null)
				carrinhoCupom.getCarrinho().setDataUltimaAlteracao(new Date());
			if(carrinhoCupom.getCarrinho().getCarrinhoStatus() == null)
				carrinhoCupom.getCarrinho().setCarrinhoStatus(new CarrinhoStatus());
			
			carrinhoCupom.getCarrinho().getCarrinhoStatus().setId(1); // Tipo Carrinho
			
			return carrinhoCupom;
		} catch (IOException ioe){			
			ioe.printStackTrace();
			System.out.println(ioe.getMessage());
		}		
		return carrinhoCupom;
	}

	public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		RespostaWebService resp;
		if(resultado.getMsg() != null && resultado.getMsg() != "")
			resp = new RespostaWebService(250, "Erro", resultado.getMsg(), false, resultado);	
		else
			resp = new RespostaWebService(200, "Sucesso", true, resultado);		
		String json = new Gson().toJson(resp);
		response.setContentType("application/json"); 
		response.setCharacterEncoding("utf-8"); 
		response.getWriter().write(json);
	}
}
