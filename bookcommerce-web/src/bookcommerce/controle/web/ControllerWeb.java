package bookcommerce.controle.web;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import bookcommerce.controle.web.command.ICommand;
import bookcommerce.controle.web.command.impl.AlterarCommand;
import bookcommerce.controle.web.command.impl.ConsultarCommand;
import bookcommerce.controle.web.command.impl.ExcluirCommand;
import bookcommerce.controle.web.command.impl.SalvarCommand;
import bookcommerce.controle.web.vh.IViewHelper;
import bookcommerce.controle.web.vh.impl.*;
import bookcommerce.core.aplicacao.Resultado;
import bookcommerce.dominio.EntidadeDominio;

public class ControllerWeb {

	private static Map<String, ICommand> commands;
	private static Map<String, IViewHelper> vhs;
	
	public ControllerWeb() {		
    	/* Utilizando o command para chamar a fachada e indexando cada command 
    	 * pela opera��o garantimos que esta servelt atender� qualquer opera��o */
    	commands = new HashMap<String, ICommand>(); 	   	    	
    	commands.put("SALVAR", new SalvarCommand());
    	commands.put("EXCLUIR", new ExcluirCommand());
    	commands.put("CONSULTAR", new ConsultarCommand());
    	commands.put("ALTERAR", new AlterarCommand());
    	
    	/* Utilizando o ViewHelper para tratar especifica��es de qualquer tela e indexando 
    	 * cada viewhelper pela url em que esta servlet � chamada no form
    	 * garantimos que esta servelt atender� qualquer entidade */    	
    	vhs = new HashMap<String, IViewHelper>();
    	
    	/*A chave do mapa � o mapeamento da servlet para cada form que 
    	 * est� configurado no web.xml e sendo utilizada no action do html
    	 */
    	vhs.put("/les12015-web/SalvarCliente", new ClienteViewHelper());
    	vhs.put("/les12015-web/SalvarProduto", new ProdutoViewHelper());
    	// a partir daqui LES 1 2018
    	vhs.put("/bookcommerce-web/ListarGrupoPrecificacaoWS", new GrupoPrecificacaoVHWS());
    	vhs.put("/bookcommerce-web/ListarAutorWS", new AutorVHWS());
    	vhs.put("/bookcommerce-web/ListarCategoriaWS", new CategoriaVHWS());
    	vhs.put("/bookcommerce-web/ListarAnoWS", new AnoVHWS());
    	vhs.put("/bookcommerce-web/ListaEditoraWS", new EditoraVHWS());   
    	vhs.put("/bookcommerce-web/LivroWS", new LivroVHWS());
    	vhs.put("/bookcommerce-web/LivroImagemWS", new LivroImagemVHWS());
    	vhs.put("/bookcommerce-web/CadastroWS", new CadastroVHWS());
    	vhs.put("/bookcommerce-web/CadastroPessoaWS", new CadastroVHWS());
    	vhs.put("/bookcommerce-web/LoginWS", new LoginVHWS());
    	vhs.put("/bookcommerce-web/PessoaEnderecoWS", new PessoaEnderecoVHWS()); 
    	vhs.put("/bookcommerce-web/PessoaCartaoWS", new PessoaCartaoCreditoVHWS());
    	vhs.put("/bookcommerce-web/EstadoWS", new EstadoVHWS());     
    	vhs.put("/bookcommerce-web/CidadeWS", new CidadeVHWS());       
    	vhs.put("/bookcommerce-web/SalvarSenhaWS", new SalvarSenhaVHWS());  
    	vhs.put("/bookcommerce-web/CarrinhoWS", new CarrinhoVHWS());	
    	vhs.put("/bookcommerce-web/CarrinhoItemWS", new CarrinhoItemVHWS());
    	vhs.put("/bookcommerce-web/CarrinhoCupomWS", new CarrinhoCupomVHWS());
    	vhs.put("/bookcommerce-web/CarrinhoPagamentoWS", new CarrinhoPagamentoVHWS());
    	vhs.put("/bookcommerce-web/PagarCarrinhoWS", new PagarCarrinhoVHWS());
    	vhs.put("/bookcommerce-web/MinhasComprasWS", new MinhasComprasVHWS());
    	vhs.put("/bookcommerce-web/AdmComprasWS", new AdmComprasVHWS());
    	vhs.put("/bookcommerce-web/EntradaWS", new EntradaVHWS());
    	vhs.put("/bookcommerce-web/EntradaLivroWS", new EntradaLivroVHWS());
    	vhs.put("/bookcommerce-web/FornecedorWS", new FornecedorVHWS());
    	vhs.put("/bookcommerce-web/GetLivroWS", new GetLivroVHWS());
    	vhs.put("/bookcommerce-web/EstatisticaCategoriaWS", new EstatisticaCategoriaVHWS());
    	vhs.put("/bookcommerce-web/EstatisticaLivroVendidoMesWS", new EstatisticaLivroVendidoMesVHWS());
    	vhs.put("/bookcommerce-web/LogWS", new LogVHWS());
    	vhs.put("/bookcommerce-web/PesquisarLivroWS", new PesquisarLivroVHWS());    	
	}
	
	protected void doProcessRequest(HttpServletRequest request, 
			HttpServletResponse response) throws ServletException, IOException {
		try {
			//Obtém a uri que invocou esta servlet (O que foi definido no methdo do form html)

			String uri = request.getRequestURI();
			
			//Obtém a operação executada
			String operacao = request.getParameter("operacao");
			
			//Obtém um viewhelper indexado pela uri que invocou esta servlet
			IViewHelper vh = vhs.get(uri);
			
			//O viewhelper retorna a entidade especifica para a tela que chamou esta servlet
			EntidadeDominio entidade =  vh.getEntidade(request);
	
			//Obtém o command para executar a respectiva operação
			ICommand command = commands.get(operacao);
			
			/*Executa o command que chamara a fachada para executar a opeção requisitada
			 * o retorno é uma instância da classe resultado que pode conter mensagens derro 
			 * ou entidades de retorno
			 */
			Resultado resultado = command.execute(entidade);
			
			// Salvando log
			saveLog(entidade, resultado, request);
			
			/*
			 * Executa o m�todo setView do view helper específico para definir como dever� ser apresentado 
			 * o resultado para o usuário
			 */
			vh.setView(resultado, request, response);
		}
		catch (ServletException e){
			// Pega a mensagem do erro e devolve o JSON
			String json = new Gson().toJson(e.getMessage());
			response.setContentType("application/json"); 
			response.setCharacterEncoding("utf-8"); 
			response.getWriter().write(json);
		}	catch (IOException ioe) {
			String json = new Gson().toJson(ioe.getMessage());
			response.setContentType("application/json"); 
			response.setCharacterEncoding("utf-8"); 
			response.getWriter().write(json);
		}
	}
	
	private void saveLog(EntidadeDominio entidade, Resultado resultado, HttpServletRequest request) {
		String operacao = request.getParameter("operacao");
		if(!operacao.equals("SALVAR") && !operacao.equals("ALTERAR")) // Não é salvar nem alterar?
			return;
		
		LogVHWS logVW = new LogVHWS();
		EntidadeDominio log = logVW.getLog(entidade, resultado, request);
		
		ICommand command = commands.get("SALVAR");
		command.execute(log);
	}	
}
