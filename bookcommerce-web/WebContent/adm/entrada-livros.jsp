<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:import url="../master/header.jsp" />
<c:import url="../modal/modalDevolucaoLivros.jsp" />
<c:import url="../modal/modalOrdemCompra.jsp" />
<!-- Importar todos estes abaixo para o modal de fornecedor -->
<c:import url="../modal/modalEntradaMercadoria.jsp" />
<c:import url="../modal/modalCadastroFornecedor.jsp" />
<!-- Fim dos imports para o modal de fornecedor -->
<style>
	.entradaEmTrocaLine{
		cursor: pointer;
	}
</style>
<script>
$(document).ready(function(){
	$("#tableAdmDevolucoes").DataTable({
        "oLanguage": TraducaoDatatable
    }).page.info().recordsTotal;
	$("#tableAdmEntrada").DataTable({
        "oLanguage": TraducaoDatatable
    }).page.info().recordsTotal;
	
	$("#btnOpenModalEntradaMercadoria").click(function(){
		$("#modalEntradaMercadoria").modal("show");
	});	
	
	$(document).on("click", ".tableAdmDevolucoesLine", function(){
		$("#modalDevolucaoMercadoria").modal("show");
	});
	$(document).on("click", ".tableAdmEntradaLine", function(){
		$("#modalEntradaMercadoria").modal("show");
	});
	$(document).on("click", ".entradaEmTrocaLine", function(){
		$("#modalOrdemCompra").modal("show");
	});	
});
</script>
<div class="container"> 
	<ul class="nav nav-tabs" id="myTab" role="tablist">
	  <li class="nav-item">
	    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#entradaMercadoria" role="tab" aria-controls="home" aria-selected="true">Entrada de Mercadorias</a>
	  </li>
	  <li class="nav-item">
	    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#entradaEmTroca" role="tab" aria-controls="profile" aria-selected="false">Em Troca</a>
	  </li>
	  <li class="nav-item">
	    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#entradaDevolucoes" role="tab" aria-controls="contact" aria-selected="false">Devoluções</a>
	  </li>
	</ul>
	<div class="tab-content" id="myTabContent">
	  <div class="tab-pane fade show active" id="entradaMercadoria" role="tabpanel" aria-labelledby="home-tab">
	  	<h4>Entradas  <button type="button" class="btn btn-primary btn-sm" id="btnOpenModalEntradaMercadoria"><i class="fas fa-plus"></i></button></h4>		
	  	<div class="row">
			<table class="table col-md-12" id="tableAdmEntrada">
			  <thead>
			    <tr>
			      <th scope="col">Cod. Entrada</th>
			      <th scope="col">Nº Nota</th>			      
			      <th scope="col">Fornecedor</th>
			      <th scope="col">Data Nota</th>
			      <th scope="col">Data Entrada</th>
			      <th scope="col">Valor</th>
			      <th scope="col">Status</th>
			    </tr>
			  </thead>
			  <tbody id="tableAdmDevolucoesContainer">
			    <tr class="tableAdmEntradaLine" style="cursor: pointer">
			      <td class="tableAdmDevolucoesOrdemCompra">0001</td>
			      <td class="tableAdmDevolucoesNomeCliente">9879 6546</td>
			      <td class="tableAdmDevolucoesData">Livros FATEC</td>
			      <td class="tableAdmDevolucoesData">01/04/2018 17:55h</td>
			      <td class="tableAdmDevolucoesData">01/04/2018 17:55h</td>
			      <td class="tableAdmDevolucoesValor">125,00</td>
			      <td class="">Completo</td>
			    </tr>
			    <tr class="tableAdmEntradaLine" style="cursor: pointer">
			      <td class="tableAdmDevolucoesOrdemCompra">0002</td>
			      <td class="tableAdmDevolucoesNomeCliente">6546 5456</td>
			      <td class="tableAdmDevolucoesData">São Paulo Distribuidora</td>
			      <td class="tableAdmDevolucoesData">01/04/2018 17:55h</td>
			      <td class="tableAdmDevolucoesData">01/04/2018 17:55h</td>
			      <td class="tableAdmDevolucoesValor">125,00</td>
			      <td class="">Completo</td>
			    </tr>
			    <tr class="tableAdmEntradaLine" style="cursor: pointer">
			      <td class="tableAdmDevolucoesOrdemCompra">0003</td>
			      <td class="tableAdmDevolucoesNomeCliente"></td>
			      <td class="tableAdmDevolucoesData">Sem Distribuidora</td>
			      <td class="tableAdmDevolucoesData">01/04/2018 17:55h</td>
			      <td class="tableAdmDevolucoesData">01/04/2018 17:55h</td>
			      <td class="tableAdmDevolucoesValor">125,00</td>
			      <td class="">Incompleto</td>
			    </tr>
			    <tr class="tableAdmEntradaLine" style="cursor: pointer">
			      <td class="tableAdmDevolucoesOrdemCompra">0004</td>
			      <td class="tableAdmDevolucoesNomeCliente">6541 3215</td>
			      <td class="tableAdmDevolucoesData">Rio de Janeiro Distribuidora</td>
			      <td class="tableAdmDevolucoesData">01/04/2018 17:55h</td>
			      <td class="tableAdmDevolucoesData">01/04/2018 17:55h</td>
			      <td class="tableAdmDevolucoesValor">125,00</td>
			      <td class="">Completo</td>
			    </tr>
			  </tbody>
			</table>
		</div>
	  </div>
	  <div class="tab-pane fade" id="entradaEmTroca" role="tabpanel" aria-labelledby="profile-tab">	 
	  	<h4>Em Troca</h4> 	  	
  		<div class="card entradaEmTrocaLine">
	    	<div class="card-body">
	    		<h5>Ordem de compra: 0001</h5>
		    	<table class="table">
		    		<tbody>
		    			<tr>
		    				<td>Cód. Item: <b>0001 </b></td>
		    				<td>Livro: <b> A Arte da Guerra</b></td>
		    				<td>Quantidade: <b>1</b></td>
		    				<td>Valor Pago: R$ <b>27,80</b></td>
		    			</tr>
		    			<tr>
		    				<td>Cód. Item: <b>0015 </b></td>
		    				<td>Livro: <b> Administração</b></td>
		    				<td>Quantidade: <b>1</b></td>
		    				<td>Valor Pago: R$ <b>30,80</b></td>
		    			</tr>
		    			<tr>
		    				<td>Cód. Item: <b>0021 </b></td>
		    				<td>Livro: <b>Plantação no Futuro</b></td>
		    				<td>Quantidade: <b>1</b></td>
		    				<td>Valor Pago: R$ <b>115,80</b></td>
		    			</tr>
		    		</tbody>
		    	</table>
		    	<div class="row">
		    		<div class="col-md-6">
		    			Valor Possível da Devolução: <b>R$ 185,65</b>
		    		</div>
		    		<div class="col-md-6 text-right">
		    			<button class="btn btn-sm btn-danger"><i class="fas fa-times"></i></button>
		    			<button class="btn btn-sm btn-success"><i class="fas fa-check"></i></button>
		    		</div>
		    	</div>
		    </div>
	  	</div>	  	
	  	
	  	<div class="card entradaEmTrocaLine">
	    	<div class="card-body">	    	
	    		<h5>Ordem de compra: 0001</h5>
		    	<table class="table">
		    		<tbody>
		    			<tr>
		    				<td>Cód. Item: <b>0001 </b></td>
		    				<td>Livro: <b> A Arte da Guerra</b></td>
		    				<td>Quantidade: <b>1</b></td>
		    				<td>Valor Pago: R$ <b>27,80</b></td>
		    			</tr>
		    			<tr>
		    				<td>Cód. Item: <b>0015 </b></td>
		    				<td>Livro: <b> Administração</b></td>
		    				<td>Quantidade: <b>1</b></td>
		    				<td>Valor Pago: R$ <b>30,80</b></td>
		    			</tr>
		    			<tr>
		    				<td>Cód. Item: <b>0021 </b></td>
		    				<td>Livro: <b>Plantação no Futuro</b></td>
		    				<td>Quantidade: <b>1</b></td>
		    				<td>Valor Pago: R$ <b>115,80</b></td>
		    			</tr>
		    		</tbody>
		    	</table>
		    	<div class="row">
		    		<div class="col-md-6">
		    			Valor Possível da Devolução: <b>R$ 185,65</b>
		    		</div>
		    		<div class="col-md-6 text-right">
		    			<button class="btn btn-sm btn-danger"><i class="fas fa-times"></i></button>
		    			<button class="btn btn-sm btn-success"><i class="fas fa-check"></i></button>
		    		</div>
		    	</div>
		    </div>
	  	</div>
	  	
	  	<div class="card entradaEmTrocaLine">
	    	<div class="card-body">
	    		<h5>Ordem de compra: 0001</h5>
		    	<table class="table">
		    		<tbody>
		    			<tr>
		    				<td>Cód. Item: <b>0001 </b></td>
		    				<td>Livro: <b> A Arte da Guerra</b></td>
		    				<td>Quantidade: <b>1</b></td>
		    				<td>Valor Pago: R$ <b>27,80</b></td>
		    			</tr>
		    			<tr>
		    				<td>Cód. Item: <b>0015 </b></td>
		    				<td>Livro: <b> Administração</b></td>
		    				<td>Quantidade: <b>1</b></td>
		    				<td>Valor Pago: R$ <b>30,80</b></td>
		    			</tr>
		    			<tr>
		    				<td>Cód. Item: <b>0021 </b></td>
		    				<td>Livro: <b>Plantação no Futuro</b></td>
		    				<td>Quantidade: <b>1</b></td>
		    				<td>Valor Pago: R$ <b>115,80</b></td>
		    			</tr>
		    		</tbody>
		    	</table>
		    	<div class="row">
		    		<div class="col-md-6">
		    			Valor Possível da Devolução: <b>R$ 185,65</b>
		    		</div>
		    		<div class="col-md-6 text-right">
		    			<button class="btn btn-sm btn-danger"><i class="fas fa-times"></i></button>
		    			<button class="btn btn-sm btn-success"><i class="fas fa-check"></i></button>
		    		</div>
		    	</div>
		    </div>
	  	</div>
	  	
	  	<div class="card entradaEmTrocaLine">
	    	<div class="card-body">
	    		<h5>Ordem de compra: 0001</h5>
		    	<table class="table">
		    		<tbody>
		    			<tr>
		    				<td>Cód. Item: <b>0001 </b></td>
		    				<td>Livro: <b> A Arte da Guerra</b></td>
		    				<td>Quantidade: <b>1</b></td>
		    				<td>Valor Pago: R$ <b>27,80</b></td>
		    			</tr>
		    			<tr>
		    				<td>Cód. Item: <b>0015 </b></td>
		    				<td>Livro: <b> Administração</b></td>
		    				<td>Quantidade: <b>1</b></td>
		    				<td>Valor Pago: R$ <b>30,80</b></td>
		    			</tr>
		    			<tr>
		    				<td>Cód. Item: <b>0021 </b></td>
		    				<td>Livro: <b>Plantação no Futuro</b></td>
		    				<td>Quantidade: <b>1</b></td>
		    				<td>Valor Pago: R$ <b>115,80</b></td>
		    			</tr>
		    		</tbody>
		    	</table>
		    	<div class="row">
		    		<div class="col-md-6">
		    			Valor Possível da Devolução: <b>R$ 185,65</b>
		    		</div>
		    		<div class="col-md-6 text-right">
		    			<button class="btn btn-sm btn-danger"><i class="fas fa-times"></i></button>
		    			<button class="btn btn-sm btn-success"><i class="fas fa-check"></i></button>
		    		</div>
		    	</div>
		    </div>
	  	</div>
	  	
	  	<div class="card entradaEmTrocaLine">
	    	<div class="card-body">
	    		<h5>Ordem de compra: 0001</h5>
		    	<table class="table">
		    		<tbody>
		    			<tr>
		    				<td>Cód. Item: <b>0001 </b></td>
		    				<td>Livro: <b> A Arte da Guerra</b></td>
		    				<td>Quantidade: <b>1</b></td>
		    				<td>Valor Pago: R$ <b>27,80</b></td>
		    			</tr>
		    			<tr>
		    				<td>Cód. Item: <b>0015 </b></td>
		    				<td>Livro: <b> Administração</b></td>
		    				<td>Quantidade: <b>1</b></td>
		    				<td>Valor Pago: R$ <b>30,80</b></td>
		    			</tr>
		    			<tr>
		    				<td>Cód. Item: <b>0021 </b></td>
		    				<td>Livro: <b>Plantação no Futuro</b></td>
		    				<td>Quantidade: <b>1</b></td>
		    				<td>Valor Pago: R$ <b>115,80</b></td>
		    			</tr>
		    		</tbody>
		    	</table>
		    	<div class="row">
		    		<div class="col-md-6">
		    			Valor Possível da Devolução: <b>R$ 185,65</b>
		    		</div>
		    		<div class="col-md-6 text-right">
		    			<button class="btn btn-sm btn-danger"><i class="fas fa-times"></i></button>
		    			<button class="btn btn-sm btn-success"><i class="fas fa-check"></i></button>
		    		</div>
		    	</div>
		    </div>
	  	</div>
	  </div>
	  <div class="tab-pane fade" id="entradaDevolucoes" role="tabpanel" aria-labelledby="contact-tab">
	  	<h4>Devoluções</h4>
  		<div class="row">
			<table class="table col-md-12" id="tableAdmDevolucoes">
			  <thead>
			    <tr>
			      <th scope="col">Ordem de compra</th>
			      <th scope="col">Cliente</th>
			      <th scope="col">Data</th>
			      <th scope="col">Valor</th>
			    </tr>
			  </thead>
			  <tbody id="tableAdmDevolucoesContainer" class="tableAdmDevolucoesLine">
			    <tr id="tableAdmDevolucoesLine" style="cursor: pointer" class="tableAdmDevolucoesLine">
			      <td class="tableAdmDevolucoesOrdemCompra">0023</td>
			      <td class="tableAdmDevolucoesNomeCliente">Rafael Sano</td>
			      <td class="tableAdmDevolucoesData">01/04/2018 17:55h</td>
			      <td class="tableAdmDevolucoesValor">125,00</td>
			    </tr>
			    <tr id="" style="cursor: pointer" class="tableAdmDevolucoesLine">
			      <td class="tableAdmDevolucoesOrdemCompra">0033</td>
			      <td class="tableAdmDevolucoesNomeCliente">Rafael Sano</td>
			      <td class="tableAdmDevolucoesData">01/04/2018 17:55h</td>
			      <td class="tableAdmDevolucoesValor">125,00</td>
			    </tr>
			    <tr id="" style="cursor: pointer" class="tableAdmDevolucoesLine">
			      <td class="tableAdmDevolucoesOrdemCompra">0055</td>
			      <td class="tableAdmDevolucoesNomeCliente">Rafael Sano</td>
			      <td class="tableAdmDevolucoesData">01/04/2018 17:55h</td>
			      <td class="tableAdmDevolucoesValor">125,00</td>
			    </tr>
			    <tr id="" style="cursor: pointer" class="tableAdmDevolucoesLine">
			      <td class="tableAdmDevolucoesOrdemCompra">0066</td>
			      <td class="tableAdmDevolucoesNomeCliente">Rafael Sano</td>
			      <td class="tableAdmDevolucoesData">01/04/2018 17:55h</td>
			      <td class="tableAdmDevolucoesValor">125,00</td>
			    </tr>
			  </tbody>
			</table>
		</div>
	  </div>
	</div>
</div> 	
<c:import url="../master/footer.jsp" />