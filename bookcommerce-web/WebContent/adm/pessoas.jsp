<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:import url="../master/header.jsp" />
<!-- Sempre importar estes 3 para o modal de cadastro de pessoa -->
<c:import url="../modal/modalCadastroPessoa.jsp" />
<c:import url="../modal/modalPessoaCartao.jsp" />
<c:import url="../modal/modalPessoaEndereco.jsp" />
<!-- Final do import para o modal de cadastro de pessoa -->
<script src="../js/PessoaAdmController.js"  charset="utf-8"></script>
<style>
	.tableTableAdmPessoasLine{
		cursor: pointer;
	}
</style>
<script>
$(document).ready(function(){
	var constructorParam = {
		tableAdmPessoas: "#tableAdmPessoas",
		btnCadastroPessoa: "#btnCadastroPessoa"
	}
	var pessoaAdmController = new PessoaAdmController(constructorParam);
	pessoaAdmController.GetAndFillTableAdmPessoas();
	
	$("#btnCadastroPessoa").click(function(){
		openModalCadastroPessoa();
	});	
	$(document).on("click", ".tableTableAdmPessoasLine", function(){
		var pessoa = $(this).data("pessoa");
		openModalCadastroPessoa(pessoa);
	});	
});
</script>
<div class="container">
	<h4>Pessoas  <button id="btnCadastroPessoa" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i></button></h4>
	<table class="table col-md-12" id="tableAdmPessoas">
	  <thead>
	    <tr>
	      <th scope="col">Cod. Pessoa</th>
	      <th scope="col">E-mail</th>	
	      <th scope="col">Nome</th>	
	      <th scope="col">Aniversário</th>	
	      <th scope="col">Cpf</th>
	    </tr>
	  </thead>
	  <tbody id="tableTableAdmPessoasContainer">
	    <tr class="tableTableAdmPessoasLine">
	      <td class="tableTableAdmPessoasId"></td>
	      <td class="tableTableAdmPessoasEmail"></td>
	      <td><span class="tableTableAdmPessoasNome"></span> <span class="tableTableAdmPessoasSobrenome"></span></td>
	      <td class="tableTableAdmPessoasAniversario"></td>
	      <td class="tableTableAdmPessoasCpf"></td>	
	    </tr>	    
	  </tbody>
	</table>
</div>
<c:import url="../master/footer.jsp" />