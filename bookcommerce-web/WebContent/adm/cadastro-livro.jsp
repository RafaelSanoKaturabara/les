<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:import url="../master/header.jsp" />
<c:import url="../modal/modalAdmFormLivro.jsp" />
<script src="../js/formAdmLivroController.js"  charset="utf-8"></script>
<script>
$(document).ready(function(){
	var parametrosConstrutor = {
		tableAdmLivros: "#tableAdmLivros"
	}
	var formLivroController = new FormAdmLivroController(parametrosConstrutor);
	formLivroController.InitListeners();
	formLivroController.FillTableAdmLivros();
	
	$("#modalCadastroLivro").on("hidden.bs.modal", function () {
		formLivroController.FillTableAdmLivros();
	});	
});
</script>
<div class="container">	
	<h4>Cadastro de Livros  <button type="button" class="btn btn-primary btn-sm" id="btnOpenModalCadastrarLivro"><i class="fas fa-plus"></i></button></h4>
	
	<div class="row">
		<table class="table col-md-12" id="tableAdmLivros">
		  <thead>
		    <tr>
		      <th scope="col">Código</th>
		      <th scope="col">Cód.Barras</th>
		      <th scope="col">Titulo</th>
		      <th scope="col">Edição</th>
		      <th scope="col">Grupo</th>
		      <th scope="col">Editora</th>      
		      <th scope="col">Ativo</th>
		    </tr>
		  </thead>
		  <tbody id="tableAdmLivrosContainer">
		    <tr id="tableAdmLivrosLine" style="cursor: pointer">
		      <td class="tableAdmLivrosId"></td>
		      <td class="tableAdmLivrosCodigoBarras"></td>
		      <td class="tableAdmLivrosTitulo"></td>
		      <td class="tableAdmLivrosEdicao"></td>
		      <td class="tableAdmLivrosGrupo"></td>
		      <td class="tableAdmLivrosEditora"></td>
		      <td> <input type="checkbox" disabled class="tableAdmLivrosAtivo"/> </td>
		    </tr>
		  </tbody>
		</table>
	</div>
</div>

<!-- Modal Cadastro Imagem Livro -->
<div class="modal fade" id="modalCadastroLivroImagem" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <b class="modal-title" id="exampleModalLongTitle">Cadastre a imagem do Livro:</b>        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">      	
       	<form id="formLivroImagem" class="form-group">
       		<img id="formLivroImagemCanvas" src="" 
       		class="img-thumbnail" alt="">
       		<div class="form-group 	col-md-12">
	    		<label for="formLivroImagemNome">Dê um nome para a imagem:</label>
		    	<input type="text" id="formLivroImagemNome" class="form-control" placeholder="Escolha um apelido"/>
		  	</div>  
       		<input type="file" class="form-group col-md-12 form-control-file" 
       		id="formLivroImagemImagem" />
		</form>
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
      	<button type="button" id="btnSubmitModalLivroImagem" class="btn btn-primary">Salvar</button>
      </div>
    </div>
  </div>
</div>	
<!-- Fim Modal Cadastro Imagem Livro --> 

<c:import url="../master/footer.jsp" />