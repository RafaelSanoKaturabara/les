<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:import url="../master/header.jsp" />
<script src="/bookcommerce-web/chart/Chart.bundle.js" charset="utf-8"></script>
<script src="/bookcommerce-web/chart/Chart.bundle.min.js" charset="utf-8"></script>
<script src="/bookcommerce-web/chart/utils.js" charset="utf-8"></script>
<script src="/bookcommerce-web/js/estatisticaController.js" charset="utf-8"></script>
<script>

$(document).ready(function(){
	var selectors = {
			estatisticaCategoria: "#estatisticaCategoria",
			estatisticaLivro: "#estatisticaLivro",
			tableLog: "#tableLog",
			modalExibirJson: "#modalExibirJson"
	}
	var topDataInicio = $("#inputTopDataInicio").val() + " 00:00";
	var topDataFim = $("#inputTopDataFim").val() + " 00:00";
	var estatisticaLivroVendidoMes = {
		dataInicio: getDateFromDatepiker($("#inputTopDataInicio").val() + " 00:00"),
		dataFim: getDateFromDatepiker($("#inputTopDataFim").val() + " 00:00"),
		qtdeTop: $("#inputTopPosicoes").val()
	}
	var estatisticaController = new EstatisticaController(selectors);
	estatisticaController.FillChart();
	estatisticaController.FillLogs();
	estatisticaController.FillEstatisticaVendidoMes(estatisticaLivroVendidoMes);
	
	$(document).on("click", ".tableLogIn", function(){
		var log = $(this).parent().parent(".tableLogLine").data("log");
		var objEntrada = JSON.parse(log.jsonEntrada);
		var jsonFormated = JSON.stringify(objEntrada, null, 4);
		estatisticaController.OpenModalExibirJson(jsonFormated);
	});
	
	$(document).on("click", ".tableLogOut", function(){
		var log = $(this).parent().parent(".tableLogLine").data("log");
		var objSaida = JSON.parse(log.jsonSaida);
		var jsonFormated = JSON.stringify(objSaida, null, 2);
		estatisticaController.OpenModalExibirJson(jsonFormated);
	});
	
	$("#btnTopSubmitDate").click(function(e) {
		e.preventDefault();		
		var estatisticaLivroVendidoMes = {
			dataInicio: getDateFromDatepiker($("#inputTopDataInicio").val() + " 00:00"),
			dataFim: getDateFromDatepiker($("#inputTopDataFim").val() + " 00:00"),
			qtdeTop: $("#inputTopPosicoes").val()
		}
		estatisticaController.FillEstatisticaVendidoMes(estatisticaLivroVendidoMes);
	});
	
	$("#inputTopDataInicio").datepicker({dateFormat: "dd/mm/yy"});
	$("#inputTopDataFim").datepicker({dateFormat: "dd/mm/yy"});
	$("#inputTopPosicoes").spinner();
});
</script>
<div class="container">
	<ul class="nav nav-tabs" id="myTab" role="tablist">
	  <li class="nav-item">
	    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#estatistica" role="tab" aria-controls="home" aria-selected="true">Estatísticas</a>
	  </li>
	  <li class="nav-item">
	    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#log" role="tab" aria-controls="profile" aria-selected="false">Logs</a>
	  </li>
	</ul>
	<div class="tab-content" id="myTabContent">
	  	<div class="tab-pane fade show active" id="estatistica" role="tabpanel" aria-labelledby="home-tab">
			<div class="row">
				<canvas id="estatisticaCategoria" class="col-6" width="400" height="400"></canvas>
				<div class="col-6">
					<canvas id="estatisticaLivro" width="400" height="400"></canvas>
					<form class="form">
				 		<div class="form-row">
						    <div class="form-group col-4">
						     	<label for="inputTopDataInicio">Data Inicio</label>
						     	<input type="text" class="form-control" value="01/07/2017" id="inputTopDataInicio">
						    </div>
						    <div class="form-group col-4">
					      		<label for="inputTopDataFim">Data Fim</label>		      
						      	<input type="text" class="form-control" value="30/06/2018" id="inputTopDataFim">
						    </div>
						    <div class="form-group col-2">
						    	<label>Posições</label>
						    	<input type="text" class="form-control" id="inputTopPosicoes" value=10 />
						    </div>
						 	<div class="form-group col-2 align-self-end">		      
						 		<button class="btn btn-primary" id="btnTopSubmitDate">OK</button>
						    </div>
					 	</div>
					</form>					
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="log" role="tabpanel" aria-labelledby="home-tab">
			<h4>Logs</h4>
			<form class="form">
		 		<div class="form-row">
				    <div class="form-group col-md-3">
				      <label for="inputDataInicio">Data Inicio</label>
				      <input type="date" class="form-control" value="2017-06-01" id="inputDataInicio">
				    </div>
				    <div class="form-group col-md-3">
				      <label for="inputDataFim">Data Fim</label>		      
				      <input type="date" class="form-control" value="2018-06-30" id="inputDataFim">
				    </div>
				 	<div class="form-group col-md-6 align-self-end">		      
				 		<button class="btn btn-primary">OK</button>
				    </div>
			 	</div>
			</form>
			<div class="row">
				<table class="table col-md-12" id="tableLog">
				  <thead>
				    <tr>
				      <th scope="col">IdPessoa</th>
				      <th scope="col">Ip</th>
				      <th scope="col">Ação</th>
				      <th scope="col">Chave</th>
				      <th scope="col">Data e Hora</th>   
				      <th scope="col">Json</th>
				    </tr>
				  </thead>
				  <tbody id="tableLogContainer">
				    <tr class="tableLogLine">
				      <td class="tableLogIdPessoa"></td>
				      <td class="tableLogIp"></td>
				      <td class="tableLogAcao"></td>
				      <td class="tableLogChave"></td>
				      <td class="tableLogDataHora"></td>
				      <td> 
					      <button class="tableLogIn btn btn-primary btn-sm" title="Json de entrada"><i class="fas fa-sign-in-alt"></i></button> 
					      <button class="tableLogOut btn btn-primary btn-sm" title="Json de saída"><i class="fas fa-sign-out-alt"></i></button> 
				      </td>
				    </tr>
				  </tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modalExibirJson" tabindex="-1" role="dialog" aria-labelledby="jsonTitle" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <b class="modal-title" id="jsonTitle">Json</b>        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"> 
      	<pre id="modalExibirJsonText">
		  
		</pre>			
      </div>
<!--       <div class="modal-footer"> -->
<!--         <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button> -->
<!--         <button type="button" id="btnModalAlterarSenha" class="btn btn-primary">Alterar</button> -->
<!--       </div> -->
    </div>
  </div>
</div>
<c:import url="../master/footer.jsp" />