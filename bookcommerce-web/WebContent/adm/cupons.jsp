<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:import url="../master/header.jsp" />
<c:import url="../modal/modalFormCupom.jsp" />
<script>
$(document).ready(function(){
	$("#tableAdmCupons").DataTable({
        "oLanguage": TraducaoDatatable
    }).page.info().recordsTotal;
	$("#btnAbrirModalCadastroCupom").click(function(){
		$("#modalCadastroCupom").modal("show");
	});	
});
</script>
<div class="container">
	<h4>Cupons  <button id="btnAbrirModalCadastroCupom" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i></button></h4>
	<form class="form">
 		<div class="form-row">
		    <div class="form-group col-md-3">
		      <label for="inputDataInicio">Data Inicio</label>
		      <input type="date" class="form-control" value="2018-04-01" id="inputDataInicio">
		    </div>
		    <div class="form-group col-md-3">
		      <label for="inputDataFim">Data Fim</label>		      
		      <input type="date" class="form-control" value="2018-05-01"  id="inputDataFim">
		    </div>
		 	<div class="form-group col-md-6 align-self-end">		      
		 		<button class="btn btn-primary">OK</button>
		    </div>
	 	</div>
	</form>
	<table class="table col-md-12" id="tableAdmCupons">
	  <thead>
	    <tr>
	      <th scope="col">Cod. Cupom</th>
	      <th scope="col">Cupom</th>	
	      <th scope="col">Data Criação</th>	
	      <th scope="col">Tipo Cupom</th>
	      <th scope="col">Tipo Desconto</th>
	      <th scope="col">Valor</th>
	      <th scope="col">Utilizado</th>
	      <th scope="col">Validade</th>
	      <th scope="col">Status</th>
	    </tr>
	  </thead>
	  <tbody id="tableAdmDevolucoesContainer">
	    <tr id="tableAdmDevolucoesLine" title="Cupom não pode ser reativado. Crie outro cupom!">
	      <td class="tableAdmDevolucoesOrdemCompra">0001</td>
	      <td class="tableAdmDevolucoesNomeCliente">R5GR56FD</td>
	      <td class="tableAdmDevolucoesData">01/04/2018</td>
	      <td class="tableAdmDevolucoesData">Devolução</td>
	      <td class="tableAdmDevolucoesData">Valor</td>
	      <td class="tableAdmDevolucoesData">R$ 125,35</td>
	      <td class="tableAdmDevolucoesData">Utilizado</td>
	      <td class="tableAdmDevolucoesData">01/08/2018</td>
	      <td class="tableAdmDevolucoesData text-center"><input disabled type="checkbox"/></td>
	    </tr>
	   <tr id="tableAdmDevolucoesLine" >
	      <td class="tableAdmDevolucoesOrdemCompra">0002</td>
	      <td class="tableAdmDevolucoesNomeCliente">R5GR56FD</td>
	      <td class="tableAdmDevolucoesData">01/04/2018</td>
	      <td class="tableAdmDevolucoesData">Promoção</td>
	      <td class="tableAdmDevolucoesData">Porcentagem</td>
	      <td class="tableAdmDevolucoesData">10%</td>
	      <td class="tableAdmDevolucoesData">Não utilizado</td>
	      <td class="tableAdmDevolucoesData">01/08/2018</td>
	      <td class="tableAdmDevolucoesData text-center"><input checked type="checkbox"/></td>
	    </tr>
	    <tr id="tableAdmDevolucoesLine" title="Cupom não pode ser reativado. Crie outro cupom!">
	      <td class="tableAdmDevolucoesOrdemCompra">0003</td>
	      <td class="tableAdmDevolucoesNomeCliente">R5GR56FD</td>
	      <td class="tableAdmDevolucoesData">01/04/2018</td>
	      <td class="tableAdmDevolucoesData">Devolução</td>
	      <td class="tableAdmDevolucoesData">Valor</td>
	      <td class="tableAdmDevolucoesData">R$ 125,35</td>
	      <td class="tableAdmDevolucoesData">Utilizado</td>
	      <td class="tableAdmDevolucoesData">01/08/2018</td>
	      <td class="tableAdmDevolucoesData text-center"><input disabled type="checkbox"/></td>
	    </tr>
	    <tr id="tableAdmDevolucoesLine" title="Cupom não pode ser reativado. Crie outro cupom!">
	      <td class="tableAdmDevolucoesOrdemCompra">0004</td>
	      <td class="tableAdmDevolucoesNomeCliente">R5GR56FD</td>
	      <td class="tableAdmDevolucoesData">01/04/2018</td>
	      <td class="tableAdmDevolucoesData">Devolução</td>
	      <td class="tableAdmDevolucoesData">Valor</td>
	      <td class="tableAdmDevolucoesData">R$ 125,35</td>
	      <td class="tableAdmDevolucoesData">Utilizado</td>
	      <td class="tableAdmDevolucoesData">01/08/2018</td>
	      <td class="tableAdmDevolucoesData text-center"><input disabled type="checkbox"/></td>
	    </tr>
	    <tr id="tableAdmDevolucoesLine" title="Cupom não pode ser reativado. Crie outro cupom!">
	      <td class="tableAdmDevolucoesOrdemCompra">0005</td>
	      <td class="tableAdmDevolucoesNomeCliente">R5GR56FD</td>
	      <td class="tableAdmDevolucoesData">01/04/2018</td>
	      <td class="tableAdmDevolucoesData">Promoção</td>
	      <td class="tableAdmDevolucoesData">Valor</td>
	      <td class="tableAdmDevolucoesData">R$ 125,35</td>
	      <td class="tableAdmDevolucoesData">Utilizado</td>
	      <td class="tableAdmDevolucoesData">01/08/2018</td>
	      <td class="tableAdmDevolucoesData text-center"><input disabled type="checkbox"/></td>
	    </tr>
	    
	  </tbody>
	</table>
</div>
<c:import url="../master/footer.jsp" />