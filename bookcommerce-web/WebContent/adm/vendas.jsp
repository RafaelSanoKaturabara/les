<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:import url="../master/header.jsp" />
<c:import url="../modal/modalOrdemCompra.jsp" />
<script src="/bookcommerce-web/js/admVendasController.js" charset="utf-8"></script>
<script>
$(document).ready(function(){
	var selectors = {
		tableAdmVendas: "#tableAdmVendas"
	}
	
	var admVendasController = new AdmVendasController(selectors);
	admVendasController.PrintAdmVendasTable();
		
	$(document).on("click", ".tableAdmDevolucoesLine", function(){
		$("#modalOrdemCompra").modal("show");
	});
	
	$(document).on("click", ".btnConfirmarEnvio", function(e){
		e.stopPropagation();
		var venda = $(this).parent().parent(".tableAdmDevolucoesLine").data("venda");
		var minhaCompraSend = {
			id: venda.id,
			precoFrete: venda.precoFrete,
			endereco: {
				id: venda.endereco.id		
			},
			carrinhoStatus: {					
				id: 4
			}
		}
		admVendasController.AlterCompraStatus(minhaCompraSend);
	});
	
	$(document).on("click", ".btnAprovarTroca", function(e){
		e.stopPropagation();
		var venda = $(this).parent().parent(".tableAdmDevolucoesLine").data("venda");
		var minhaCompraSend = {
			id: venda.id,
			precoFrete: venda.precoFrete,
			endereco: {
				id: venda.endereco.id		
			},
			carrinhoStatus: {					
				id: 7
			}
		}
		admVendasController.AlterCompraStatus(minhaCompraSend);
	});	
});
</script>
<div class="container">
	<h4>Vendas</h4>
	<form class="form">
 		<div class="form-row">
		    <div class="form-group col-md-3">
		      <label for="inputDataInicio">Data Inicio</label>
		      <input type="date" class="form-control" value="2018-04-01" id="inputDataInicio">
		    </div>
		    <div class="form-group col-md-3">
		      <label for="inputDataFim">Data Fim</label>		      
		      <input type="date" class="form-control" value="2018-05-01"  id="inputDataFim">
		    </div>
		 	<div class="form-group col-md-6 align-self-end">		      
		 		<button class="btn btn-primary">OK</button>
		    </div>
	 	</div>
	</form>
	<table class="table col-md-12" id="tableAdmVendas">
	  <thead>
	    <tr>
	      <th scope="col">Cod. Venda</th>
	      <th scope="col">Cliente</th>	
	      <th scope="col">Data</th>
	      <th scope="col">Status</th>
	    </tr>
	  </thead>
	  <tbody id="tableAdmDevolucoesContainer">
	    <tr class="tableAdmDevolucoesLine" style="cursor: pointer">
	      <td class="tableAdmDevolucoesOrdemCompra">0001</td>
	      <td class="tableAdmDevolucoesNomeCliente">Rafael Sano</td>
	      <td class="tableAdmDevolucoesData">01/04/2018 17:55h</td>
	      <td class="">
		      <b class="tableAdmDevolucoesStatus">Pago</b>
		      <button class="btn btn-sm btn-success btnConfirmarEnvio" title="Clique para confirmar o envio"><i class="fas fa-shipping-fast"></i></button>
		      <button class="btn btn-sm btn-primary btnAprovarTroca" title="Clique para permitir a devolução"><i class="fas fa-undo"></i></button>
	      </td>
	    </tr>
	  </tbody>
	</table>
</div>
<c:import url="../master/footer.jsp" />