<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:import url="master/header.jsp" />
<c:import url="modal/modalCadastroPessoa.jsp" />
	<script src="/bookcommerce-web/js/loginController.js" charset="utf-8"></script>
<style>

</style>
<script>
	$(document).ready(function (){
		var constructorParam = {
			formLogin: "#formLogin"
		}		
		var loginController = new LoginController(constructorParam);
		loginController.InitEventListeners();
		
		$("#btnReenviarSenha").click(function(){
			$("#modalReenvioSenha").modal("show");
		});
		$("#btnModalReenvioSenhaEmail").click(function(){
			$("#modalReenvioSenhaForm").submit();
		});
		$("#btnCadastrarCliente").click(function(){
			$("#modalCadastroPessoa").modal("show");
		});
	});	
</script>
<div class="container">
	<div class="row">
		<div class="card">
		  <div class="card-body">	
		    <h5 class="card-title text-center">Login</h5>
			<form id="formLogin">
			  <div class="form-group">
			    <label for="formLoginEmail">Endereço de e-mail</label>
			    <input type="email" class="form-control" id="formLoginEmail" aria-describedby="emailHelp" placeholder="Enter email">
			    <small id="emailHelp" class="form-text text-muted">Não compartilhe o seu e-mail com outras pessoas.</small>
			  </div>
			  <div class="form-group">
			    <label for="formLoginSenha">Senha</label>
			    <input type="password" class="form-control" id="formLoginSenha" placeholder="Password">
			  </div>
			  <div class="form-row">
				  <div class="form-check col-md-8" style="visibility:hidden">
				    &nbsp &nbsp &nbsp<input type="checkbox" class="form-check-input" id="formLoginCheckboxManterLogado">
				    <label class="form-check-label" for="formLoginCheckboxManterLogado">Mantenha-me conectado</label>
				  </div>
				  <div class="col-md-4 text-right">
				  	<button type="submit" class="btn btn-primary btnLoginEntrar" id="btnLoginEntrar">Entrar</button>	
				  </div>		  
			  </div>
			</form>
			<div class="col-12 text-center">
				<a id="btnReenviarSenha" href="#" class="card-link">Esqueceu a senha?</a><br/>
		    	Não tem cadastro? <a id="btnCadastrarCliente" href="#" class="card-link">Clique aqui para se cadastrar.</a>
			</div>		    
		  </div>
		</div>
	</div>
</div>


<div class="modal fade" id="modalReenvioSenha" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <b class="modal-title" id="exampleModalLongTitle">Reenvio de Senha</b>        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"> 
      	<form id="modalReenvioSenhaForm">
		  <div class="form-group">
		    <label for="modalReenvioSenhaEmail">Email</label>
		    <input type="email" class="form-control" id="modalReenvioSenhaEmail" aria-describedby="emailHelp" placeholder="Digite o seu e-mail de cadastro">
		  </div>
		</form>			
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" id="btnModalReenvioSenhaEmail" class="btn btn-primary">Enviar</button>
      </div>
    </div>
  </div>
</div>	

<c:import url="modal/modalPessoaCartao.jsp" />
<c:import url="modal/modalPessoaEndereco.jsp" />
<c:import url="master/footer.jsp" />