<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:import url="master/header.jsp" />
<c:import url="modal/modalLivro.jsp" />
<style>
	.livroItem {
		cursor: pointer;
	}
</style>
<script src="/bookcommerce-web/js/defaultController.js" charset="utf-8"></script>
<script>
	$(document).ready(function (){
		var selectorParams = {
			livroContainer: "#livroContainer"	
		}			
		var defaultController = new DefaultController(selectorParams);
			
		$(document).on("click",".livroItem", function (){
			var livro = $(this).data("livro");
			modalLivroController.OpenModalLivro(livro);
		});
		
		$(document).on("click",".btnAdicionarCarrinho", function (e){
			e.stopPropagation();
			var livro = $(this).parent().parent().parent().parent().parent(".livroItem").data("livro");				
			defaultController.AdicionarAoCarrinho(livro, 1);
		});		
		
		var strParaPesquisar = $.urlParam("param");
		if(strParaPesquisar == null || strParaPesquisar == "")
			defaultController.PrintDefaulContainer();
		else
			defaultController.PesquisarLivros(strParaPesquisar);
		
		$("#btnPesquisarLivro").click(function(e) {
			e.preventDefault();
			if(window.location.pathname.toLowerCase() == "/bookcommerce-web/default.jsp")
				defaultController.PesquisarLivros($("#inputPesquisaLivro").val());
				
		});
	});	
</script>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-2">		
			<h4>Categorias</h4>
			<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
			  <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Aventura</a>
			  <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Auto Ajuda</a>
			  <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Horror</a>
			  <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Infantil</a>
			  <a class="nav-link" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Biografias</a>
			  <a class="nav-link"  data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Folclore</a>
			  <a class="nav-link"  data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Literatura</a>
			</div>
		</div>
		<div class="col-md-8">
			<h4>Livros</h4>
			<div class="row" id="livroContainer">	
					
				<div class="card text-center col-md-3 livroItem">
		            <div class="thumbnail">
		                <img class="group list-group-image livroImagem" src="/bookcommerce-web/imagens/210x210.png" alt="" />
		                <div class="caption">
		                    <h4 class="group inner list-group-item-heading">
		                        R$ <span class="livroPreco">35,00</span></h4>
		                    <p class="livroTitulo">Livro: A Arte da Guerra</p>
		                    <div class="livroAutorContainer">		                    
		                    	<p class="livroAutorLine">Autor: Sun Tsu</p>
		                    </div>
		                    <div class="row col-form-label">
		                        <div class="col-md-12">
		                            <button class="btn btn-success btn-block btnAdicionarCarrinho" >Adicionar ao Carrinho</button>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>	   
		        		
			</div>					
		</div>
		<div class="col-md-2">
			<h4>Indicações</h4>
			<div class="row"> 
			
			<div class="card text-center col-md-12" id="livroItem">
		            <div class="thumbnail">
		                <img class="group list-group-image" src="/bookcommerce-web/imagens/210x210.png" alt="" />
		                <div class="caption">
		                    <h4 class="group inner list-group-item-heading">
		                        R$ 35,00</h4>
		                    <p>Livro: A Arte da Guerra</p>
		                    <p>Autor: Sun Tsu</p>
		                    <div class="row">
		                        <div class="col-md-12">
		                            <button class="btn btn-success" >Adicionar ao Carrinho</button>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>	
             	<div class="card text-center col-md-12" id="livroItem">
		            <div class="thumbnail">
		                <img class="group list-group-image" src="/bookcommerce-web/imagens/210x210.png" alt="" />
		                <div class="caption">
		                    <h4 class="group inner list-group-item-heading">
		                        R$ 355,00</h4>
		                    <p>Livro: ADMINISTRAÇÃO DE RELACIONAMENTO COM CLIENTE</p>
		                    <p>Autor: Blabla doik fdios</p>
		                    <div class="row">
		                        <div class="col-md-12">
		                            <button class="btn btn-success" >Adicionar ao Carrinho</button>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>	<div class="card text-center col-md-12" id="livroItem">
		            <div class="thumbnail">
		                <img class="group list-group-image" src="/bookcommerce-web/imagens/210x210.png" alt="" />
		                <div class="caption">
		                    <h4 class="group inner list-group-item-heading">
		                        R$ 35,00</h4>
		                    <p>Livro: A Arte da Guerra</p>
		                    <p>Autor: Sun Tsu</p>
		                    <div class="row">
		                        <div class="col-md-12">
		                            <button class="btn btn-success" >Adicionar ao Carrinho</button>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>	
             	<div class="card text-center col-md-12" id="livroItem">
		            <div class="thumbnail">
		                <img class="group list-group-image" src="/bookcommerce-web/imagens/210x210.png" alt="" />
		                <div class="caption">
		                    <h4 class="group inner list-group-item-heading">
		                        R$ 355,00</h4>
		                    <p>Livro: ADMINISTRAÇÃO DE RELACIONAMENTO COM CLIENTE</p>
		                    <p>Autor: Blabla doik fdios</p>
		                    <div class="row">
		                        <div class="col-md-12">
		                            <button class="btn btn-success" >Adicionar ao Carrinho</button>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>			
			
			</div>
		</div>
	</div>	
</div>
<c:import url="master/footer.jsp" />