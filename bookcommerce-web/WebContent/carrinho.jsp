<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:import url="master/header.jsp" />
<c:import url="modal/modalLivro.jsp" />
<style>
/* 	.carrinhoLine{ */
/* 		cursor: pointer; */
/* 	} */
</style>
<script src="/bookcommerce-web/js/carrinhoController.js" charset="utf-8"></script>
<script>
	$(document).ready(function (){
		var selectors = {
			carrinhoItemContainer: "#carrinhoItemContainer",
			carrinhoContainer: "#carrinhoContainer"
		}
		
		var carrinhoController = new CarrinhoController(selectors);
		carrinhoController.PrintCarrinhoContainer();
		
		$("#btnAddCupom").click(function(e) {
			e.preventDefault();
			carrinhoController.AddCupomToCarrinho();
		});
		
// 		$(document).on("click", ".carrinhoLine", function (){
// 			modalLivroController.OpenModalLivro();
// 		});		
		
		$(document).on("click", ".btnRemoverItemCarrinho", function (e){
			e.stopPropagation();
			e.preventDefault();
			var carrinhoItem = $(this).parent().parent().parent(".carrinhoLine").data("carrinhoItem");
			carrinhoController.DeleteCarrinhoItem(carrinhoItem);
		});	
		$(document).on("click", ".stopPropagation", function (e){
			e.stopPropagation();
		});	
		
		$("#btnInputCep").click(function(e) {
			e.stopPropagation();
			e.preventDefault();
			var frete = $("#inputCep").val().replace("-", "") / 1000 * 0.0011;
			var total = $("#carrinhoFrete").data("paraCalculo");
			$("#carrinhoFrete").text(FloatFormat(frete, 2));
			$("#carrinhoTotal").text(FloatFormat(total + frete, 2));
		});
				
// 		$(document).on("change", ".carrinhoItemQuantidade", function() {
// 			console.log($(this).val());
// 		});
		
		
		
		$("#inputCep").mask("00000-000");
	});	
</script>
<div class="container">
	<h4>Carrinho</h4>		
	<div id="carrinhoItemContainer">	
		<div class="card carrinhoLine">
			<div class="card-body row">
				<div class="col-md-2 text-center">
					<img class="carrinhoItemImagem" src="/bookcommerce-web/imagens/100x100.png" class="rounded" alt="...">		
				</div>
				<div class="col-md-3">
					<small class="carrinhoItemTitulo"> A Arte da Guerra </small><br/>	
					<div class="carrinhoItemAutorContainer">
						<small class="carrinhoItemAutorLine"> Shun Tzu </small><br/>
					</div>	
					<small>Editora:</small> <small class="carrinhoItemEditora"> Editora: FATEC </small><br/>						
					<small>ano: <span class="carrinhoItemAno">2015</span></small><br/>
				</div>
				<div class="col-md-3 stopPropagation">
					<form class="form-group row">
					    <label for="carrinhoItemQuantidade" class="col-sm-8 col-form-label">Quantidade</label>
					    <div class="col-sm-5">
					      <input type="number" value="1" class="form-control carrinhoItemQuantidade"/>
					    </div>
				  	</form>				  	
				</div>
				<div class="col-md-4 text-right">				
					<button class="btn btn-default btn-sm btnRemoverItemCarrinho"><i class="fas fa-trash-alt"></i></button><br/>
					<small > Valor unitário: R$ <span class="carrinhoItemPrecoUnit"></span> </small><br/>	
					<small> Valor Total: R$ <span class="carrinhoItemPrecoTotal"></span> </small><br/>	
				</div>
			</div>
		</div>
	</div>			
	<div class="row" id="carrinhoContainer">
		<div class="col-md-5">
			<form class="form-group row" id="formAddCupom">
			    <label for="inputCupom" class="col-sm-4 col-form-label">Cupom</label>
			    <div class="col-sm-6">
			      <input type="text" class="form-control" id="inputCupom" />
			    </div>
			    <div class="col-sm-2">
			      <button id="btnAddCupom" class="btn btn-primary btn-sm">OK</button>
			    </div>
		  	</form> 
		  	<form class="form-group row">
			    <label for="inputCep" class="col-sm-4 col-form-label">CEP</label>
			    <div class="col-sm-6">
			      <input type="text" class="form-control" id="inputCep" placeholder="00000-000" />
			    </div>
			    <div class="col-sm-2">
			      <button id="btnInputCep" class="btn btn-primary btn-sm">OK</button>
			    </div>
			</form>		
		</div>
		<div class="col-md-4 text-right" id="carrinhoCupomContainer">
			<p class="carrinhoCupomLine">
				<small>Cupom: <span class="carrinhoCupomNome"></span> Valor: <span class="carrinhoCupomValor"></span>
				</small>
			</p>
		</div>
		<div class="col-md-3 text-right">
			<p>Total Geral: R$ <span id="carrinhoTotalGeral"></span></p>
			<p>Cupom: R$ -<span id="carrinhoCupom">0,00</span></p>
			<p>Frete: R$ <span id="carrinhoFrete"></span></p>
			<p>Total: R$ <span id="carrinhoTotal"></span></p>		
			<a class="btn btn-success" href="/bookcommerce-web/pagamento.jsp">Finalizar</a>	
		</div>
	</div>
</div>
<c:import url="master/footer.jsp" />