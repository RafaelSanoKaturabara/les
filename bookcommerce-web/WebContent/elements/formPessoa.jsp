<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<form id="formModalPessoa" class="form-group">
	<div class="form-row">
		<span class="col-md-11" >Codigo da Cadastro: <span id="formModalPessoaId"></span></span>
    <div class="checkbox col-md-1" style="float: right;">
	    <label>
	     	<input type="checkbox" id="formModalPessoaAtivo" checked data-toggle="toggle">		    
	    </label>
		</div>				
     		</div>
     		<div class="form-row">
      		<div class="form-group col-md-7">
    		<label for="formModalPessoaEmail">E-mail</label>
	    	<input type="email" class="form-control" id="formModalPessoaEmail">
	  	</div>  
      		<div class="form-group col-md-5">
    		<label for="formModalPessoaSenha">Senha</label>
	    	<input type="password" class="form-control" id="formModalPessoaSenha" />
	  	</div>    		
     		</div>
     		<div class="form-row">
      		<div class="form-group col-md-7">
    		<label for="formModalPessoaCpf">CPF</label>
	    	<input type="email" class="form-control" id="formModalPessoaCpf">
	  	</div>
      		<div class="form-group col-md-5">
    		<label for="formModalPessoaRepitaSenha">Repita sua senha</label>
	    	<input type="password" class="form-control" id="formModalPessoaRepitaSenha" />
	  	</div>    		
     		</div>
     		<div class="form-row">
		<div class="form-group col-md-4">
    		<label for="formModalPessoaNome">Nome</label>
	    	<input type="text" class="form-control" id="formModalPessoaNome" aria-describedby="emailHelp" placeholder="Nome">
	  	</div>
	  	<div class="form-group  col-md-6">
	    	<label for="formModalPessoaSobrenome">Sobrenome</label>
	    	<input type="text" class="form-control" id="formModalPessoaSobrenome" placeholder="Sobrenome"/>
	  	</div>   
	  	<div class="form-group  col-md-2">
	    	<label for="formModalPessoaGenero">Gênero</label>
	    	<select id="formModalPessoaGenero" class="custom-select form-control">
	    		<option class="formModalPessoaGeneroOpt" value="1">M</option>
	    		<option class="modalFormSelectLivroEditoraOpt" value="2">F</option>
	    	</select> 
	  	</div>     		
	</div>
  	<div class="form-row">
      		<div class="form-group col-md-5">
    		<label for="formModalPessoaTelefone">Telefone</label>
	    	<input type="text" class="form-control" id="formModalPessoaTelefone">
	  	</div>  
	  	<div class="form-group col-md-3">
	  		<label for="formModalPessoaTelefoneTipo">Tipo</label>	
		  	<select id="formModalPessoaTelefoneTipo" class="custom-select form-control">
	    		<option class="formModalPessoaTelefoneTipoOpt" value="1">Residêncial</option>
	    		<option class="formModalPessoaTelefoneTipoOpt" value="2">Celular</option>
	    		<option class="formModalPessoaTelefoneTipoOpt" value="3">Comercial</option>
	    	</select> 
	  	</div>	    	
      		<div class="form-group col-md-4">
    		<label for="formModalPessoaDataNascimento">Data de Nascimento</label>
	    	<input type="date" class="form-control" id="formModalPessoaDataNascimento" />
	  	</div>  		
     		</div>
     		<div class="form-row">
		<div class="form-group col-md-6">
    		<label for="formModalPessoaEndereco">Endereço:</label>
	    	<a id="btnAdicionarPessoaEndereco" href="#"><i class="fas fa-plus-square"></i></a>
	    	<select id="formModalPessoaEndereco" class="custom-select form-control">
	    		<option class="formModalPessoaEnderecoOpt" value="">Clique ao lado do endereço para adicionar</option>
	    	</select>
	  	</div>
	  	<div class="form-group 	col-md-6">
	    	<label for="formModalPessoaCartao">Cartão:</label>
	    	<a id="btnAdicionarPessoaCartao" href="#"><i class="fas fa-plus-square"></i></a>
	    	<select id="formModalPessoaCartao" class="custom-select form-control">
	    		<option class="formModalPessoaCartaoOpt" value="">Clique ao lado do cartão para adicionar</option>
	    	</select>
	  	</div>       		
     		</div>		
     		<div class="form-group">
	    <label for="formModalPessoaObservacao">Observações</label>
	    <textarea class="form-control" id="formModalPessoaObservacao" rows="3"></textarea>
	</div>
</form>