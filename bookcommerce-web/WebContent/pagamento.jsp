<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:import url="master/header.jsp" />
<c:import url="modal/modalPessoaCartao.jsp" />
<c:import url="modal/modalPessoaEndereco.jsp" />
<script src="/bookcommerce-web/js/pagamentoController.js" charset="utf-8"></script>
<script>

		var SelectorFormPessoa = "";
		var SelectorFormPessoaCartaoContainerSelector = "";
		var SelectorFormPessoaCartaoOptSelector = "";

	$(document).ready(function (){
		$("#btnAdicionarCartao").click(function (){
			$("#modalCadastroPessoaCartao").modal("show");
		});
		$("#btnAdicionarEndereco").click(function (){
			$("#modalCadastroPessoaEndereco").modal("show");
		});
		
		var selectors = {
			carrinhoItemContainer: "#carrinhoItemContainer",
			formPagamento: "#formPagamento",
		}
		
		var pagamentoController = new PagamentoController(selectors);
		pagamentoController.PrintCarrinhoContainer();
		pagamentoController.GetAndFillCartaoEEndereco();
		
		$("#btnAdicionarCarrinhoPagamento").click(function(e) {
			e.preventDefault();
			pagamentoController.AddCarrinhoPagamento();
		});
		
		$("#selectPagamentoEndereco").change(function() {
			var idEndereco = $(this).val();
			var pessoaEndereco = $(this).find("[value='" + idEndereco + "']").data("pessoaEndereco");
			var frete = pessoaEndereco.cep.replace("-", "") / 1000 * 0.0011;
			var total = $("#carrinhoFrete").data("paraCalculo");
			$("#carrinhoFrete").text(FloatFormat(frete, 2));
			$("#carrinhoTotal").text(FloatFormat(total + frete, 2));
		});
		
		$("#btnPagar").click(function(e) {
			e.preventDefault();
			pagamentoController.PagarCarrinho();
		});
	});
</script>
<div class="container" id="pagamentoContainer">
	<h4>Pagamento</h4>	
	<div class="row">
		<div class="col-sm-5">
			<div class="card">
				<div class="card-body">
					<div class="text-right">						
						<h1><i class="fab fa-cc-amex"></i>  <i class="fab fa-cc-mastercard"></i>  <i class="fab fa-cc-visa"></i></h1>
					</div>
					<form id="formPagamento" >
						<div class="form-row">
					    	<div class="form-group col-md-6">
					      		<label for="selectPagamentoCartao">Cartão de Crédito</label>
					      		<a id="btnAdicionarCartao" href="#"><i class="fas fa-plus-square"></i></a>
						      	<select class="form-control" id="selectPagamentoCartao">
									<option class="selectPagamentoCartaoOpt">Cartão 1 </option>
								</select>
						    </div>
						    <div class="form-group col-md-3">
				     			<label for="valorPagamentoCartao">Valor R$</label>
					      		<input type="text" class="form-control" id="valorPagamentoCartao" placeholder="000,00">
							</div>
							<div class="form-group col-md-3 align-self-end">
				     			<button id="btnAdicionarCarrinhoPagamento" class="btn btn-primary">Adicionar</button>
							</div>
						</div>	
					    <div class="form-group">
					      <label for="selectPagamentoEndereco">Endereço</label>
					      <a id="btnAdicionarEndereco" href="#"><i class="fas fa-plus-square"></i></a>
					      <select class="form-control" id="selectPagamentoEndereco">
							<option class="selectPagamentoEnderecoOpt">Cartão 1</option> 
							</select>
					    </div>
					 	<div class="col-md-12 text-right">
					    	<h5>Valores da compra:</h5>
							<p>Total Geral: R$ <span id="carrinhoTotalGeral"></span></p>
							<div id="carrinhoCupomContainer">
								<p class="carrinhoCupomLine">
									<small>Cupom: <span class="carrinhoCupomNome"></span> Valor: <span class="carrinhoCupomValor"></span>
									</small>
								</p>
							</div>
							<p>Frete: R$ <span id="carrinhoFrete"></span></p>
							<p>Total: R$ <span id="carrinhoTotal"></span></p>		
							<hr>
							<h5>Pagamentos:</h5>
							<div id="carrinhoPagamentoContainer" style="display:none">
								<p class="carrinhoPagamentoLine">
									<span class="carrinhoPagamentoApelido"></span>. Valor: R$
									<span class="carrinhoPagamentoValor"></span>
								</p>							
							</div>
							<button id="btnPagar" class="btn btn-success btn-block">Pagar</button>	
						</div>
					</form>
				</div>
			</div>
		</div>
<!-- 		<div class="col-sm-7"> -->
<!-- 			<div class="card"> -->
<!-- 				<div class="card-body row"> -->
<!-- 					<div class="col-md-3 text-center"> -->
<!-- 						<img src="/bookcommerce-web/imagens/100x100.png" class="rounded" alt="...">		 -->
<!-- 					</div> -->
<!-- 					<div class="col-md-3"> -->
<!-- 						<small> A Arte da Guerra </small><br/>	 -->
<!-- 						<small> Shun Tzu </small><br/>	 -->
<!-- 						<small> Editora: FATEC </small><br/>						 -->
<!-- 						<small>ano: 2015</small><br/> -->
<!-- 					</div> -->
<!-- 					<div class="col-md-3 text-center"> -->
<!-- 						<form class="form-group row"> -->
<!-- 						    <label for="inputPassword" class="col-form-label">Quantidade: 2</label> -->
<!-- 					  	</form>				  	 -->
<!-- 					</div> -->
<!-- 					<div class="col-md-3 text-right">				 -->
<!-- 						<small> Valor unitário: R$ 5,65 </small><br/>	 -->
<!-- 						<small> Valor Total: R$ 5,65 </small><br/>	 -->
<!-- 					</div> -->
<!-- 				</div> -->
<!-- 			</div>	 -->
<!-- 		</div> -->
		<div id="carrinhoItemContainer"  class="col-sm-7">	
			<div class="card carrinhoLine">
				<div class="card-body row">
					<div class="col-md-2 text-center">
						<img class="carrinhoItemImagem" src="/bookcommerce-web/imagens/100x100.png" class="rounded" alt="...">		
					</div>
					<div class="col-md-3">
						<small class="carrinhoItemTitulo"> A Arte da Guerra </small><br/>	
						<div class="carrinhoItemAutorContainer">
							<small class="carrinhoItemAutorLine"> Shun Tzu </small><br/>
						</div>	
						<small class="carrinhoItemEditora"> Editora: FATEC </small><br/>						
						<small>ano: <span class="carrinhoItemAno">2015</span></small><br/>
					</div>
					<div class="col-md-3 stopPropagation">
						<form class="form-group row">
						    <small class="col-sm-8 col-form-label">Qtde</small>
						    <div class="col-sm-5">
						      <span class="carrinhoItemQuantidade">2</span>
						    </div>
					  	</form>				  	
					</div>
					<div class="col-md-4 text-right">		
						<small>Valor unitário: R$ <span class="carrinhoItemPrecoUnit"></span></small><br/>	
						<small>Valor Total: R$ <span class="carrinhoItemPrecoTotal"></span></small><br/>	
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>
<c:import url="master/footer.jsp" />