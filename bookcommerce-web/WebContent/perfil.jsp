<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:import url="master/header.jsp" />
<!-- Sempre importar estes 3 para o modal de cadastro de pessoa -->
<c:import url="modal/modalPessoaCartao.jsp" />
<c:import url="modal/modalPessoaEndereco.jsp" />
<!-- Final do import para o modal de cadastro de pessoa -->
<script src="/bookcommerce-web/js/perfilController.js" charset="utf-8"></script>
<style>
	.tableTableAdmPessoasLine{
		cursor: pointer;
	}
</style>
<script>
//globais
var SelectorFormPessoa;
var SelectorFormPessoaEnderecoContainerSelector;
var SelectorFormPessoaEnderecoOptSelector;
var SelectorFormPessoaCartaoContainerSelector;
var SelectorFormPessoaCartaoOptSelector;
$(document).ready(function(){
	
	var constructorParam = {
		formPessoa: "#formPessoa",
		btnCadastroPessoa: "#btnCadastroPessoa"
	}
	
	var perfilController = new PerfilController(constructorParam);
	perfilController.GetAndFillPerfilForm(PessoaLogada);
	perfilController.InitEventListeners();
	
	SelectorFormPessoa = "#formPessoa";
	SelectorFormPessoaEnderecoContainerSelector = "#formlPessoaEndereco";
	SelectorFormPessoaEnderecoOptSelector = ".formPessoaEnderecoOpt";
	SelectorFormPessoaCartaoContainerSelector = "#formPessoaCartao";
	SelectorFormPessoaCartaoOptSelector = ".formPessoaCartaoOpt";

	$("#formPessoaTelefone").mask(SPMaskTelBehavior, spTelOptions);
	$("#formModalPessoaCpf").mask("000.000.000-00");
	$("#formPessoaDataNascimento").datepicker({dateFormat: "dd/mm/yy"});
});
</script>
<div class="container">
	<h4>Dados do Perfil</h4>
	<form id="formPessoa" class="form-group">
		<div class="form-row">
			<span class="col-md-11" >Codigo do Cadastro: <span id="formPessoaId"></span></span>
	    	<div class="checkbox col-md-1" style="float: right;">
			    <label style="display: none">
			     	<input type="checkbox" id="formPessoaAtivo" checked data-toggle="toggle">		    
			    </label>
			</div>				
   		</div>
   		<div class="form-row">
      		<div class="form-group col-md-6">
	    		<label for="formModalPessoaEmail">E-mail</label>
		    	<input type="email" class="form-control" id="formModalPessoaEmail">
		  	</div>  
	   		<div class="form-group col-md-6">
	    		<label for="formModalPessoaCpf">CPF</label>
		    	<input type="email" class="form-control" id="formModalPessoaCpf" placeholder="000.000.000-00">
		  	</div>   		
   		</div>

   		<div class="form-row">
			<div class="form-group col-md-4">
	    		<label for="formPessoaNome">Nome</label>
		    	<input type="text" class="form-control" id="formPessoaNome" aria-describedby="emailHelp" placeholder="Nome">
		  	</div>
		  	<div class="form-group  col-md-6">
		    	<label for="formPessoaSobrenome">Sobrenome</label>
		    	<input type="text" class="form-control" id="formPessoaSobrenome" placeholder="Sobrenome"/>
		  	</div>   
		  	<div class="form-group  col-md-2">
		    	<label for="formPessoaGenero">Gênero</label>
		    	<select id="formPessoaGenero" class="custom-select form-control">
		    		<option class="formPessoaGeneroOpt" value="1">M</option>
		    		<option class="formPessoaGeneroOpt" value="2">F</option>
		    	</select> 
		  	</div>     		
		</div>
	  	<div class="form-row">
	      		<div class="form-group col-md-5">
	    		<label for="formPessoaTelefone">Telefone</label>
		    	<input type="text" class="form-control" id="formPessoaTelefone">
		  	</div>  
		  	<div class="form-group col-md-3">
		  		<label for="formPessoaTelefoneTipo">Tipo</label>	
			  	<select id="formPessoaTelefoneTipo" class="custom-select form-control">
		    		<option class="formPessoaTelefoneTipoOpt" value="1">Residêncial</option>
		    		<option class="formPessoaTelefoneTipoOpt" value="2">Celular</option>
		    		<option class="formPessoaTelefoneTipoOpt" value="3">Comercial</option>
		    	</select> 
		  	</div>	    	
	      		<div class="form-group col-md-4">
	    		<label for="formPessoaDataNascimento">Data de Nascimento</label>
		    	<input type="text" class="form-control" id="formPessoaDataNascimento" />
		  	</div>  		
	     		</div>
	     		<div class="form-row">
			<div class="form-group col-md-6">
	    		<label for="formlPessoaEndereco">Endereço:</label>
		    	<a id="btnAdicionarPessoaEndereco" href="#"><i class="fas fa-plus-square"></i></a>
		    	<select id="formlPessoaEndereco" class="custom-select form-control">
		    		<option class="formPessoaEnderecoOpt" value="">Clique ao lado do endereço para adicionar</option>
		    	</select>
		  	</div>
		  	<div class="form-group 	col-md-6">
		    	<label for="formPessoaCartao">Cartão:</label>
		    	<a id="btnAdicionarPessoaCartao" href="#"><i class="fas fa-plus-square"></i></a>
		    	<select id="formPessoaCartao" class="custom-select form-control">
		    		<option class="formPessoaCartaoOpt" value="">Clique ao lado do cartão para adicionar</option>
		    	</select>
		  	</div>       		
	     		</div>		
	     		<div class="form-group">
		    <label for="formPessoaObservacao">Observações</label>
		    <textarea class="form-control" id="formPessoaObservacao" rows="3"></textarea>
		</div>
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" id="btnAtualizarPerfil" class="btn btn-primary">Atualizar</button>
	</form>
</div>
<c:import url="master/footer.jsp" />