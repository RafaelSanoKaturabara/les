<!-- Modal Cadastro Imagem Livro -->
<script src="/bookcommerce-web/js/modalPessoaEnderecoController.js" charset="utf-8"></script>
<script>	
	var modalPessoaEnderecoController;
	$(document).ready(function() {
		var constructorParam = {
				modalCadastroPessoaEndereco: "#modalCadastroPessoaEndereco",
				isLoginPage: window.location.pathname.toLowerCase() === "/bookcommerce-web/login.jsp"
		}
		modalPessoaEnderecoController = new ModalPessoaEnderecoController(constructorParam);
		modalPessoaEnderecoController.InitEventListener();
		
		$('#modalCadastroPessoaEndereco').on('hidden.bs.modal', function (e) {
		    $('body').addClass('modal-open');
		});
		$("#modalCadastroPessoaEnderecoCep").mask("00000-000");
	});
</script>
<div class="modal fade" id="modalCadastroPessoaEndereco" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <b class="modal-title" id="exampleModalLongTitle">Cadastro de endere�o</b>        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"> 
  		<form>
  			<div class="form-group">
		     	<label for="modalCadastroPessoaEnderecoApelido">Apelido da Resid�ncia</label>		      
		    	<input type="text" class="form-control" id="modalCadastroPessoaEnderecoApelido" placeholder="Casa da Vov�">
			</div>
  			<div class="form-row">
			    <div class="form-group col-md-6">  
			      <label for="modalCadastroPessoaEnderecoTipoResidencia">Tipo de Resid�ncia</label>	
			      <select id="modalCadastroPessoaEnderecoTipoResidencia" class="form-control">
			        <option selected value="">Escolha...</option>
			        <option value="1">Casa</option>
			        <option value="2">Apartamento</option>
			        <option value="3">Outro</option>
			      </select>
			    </div>
			    <div class="form-group col-md-6">
			      <label for="modalCadastroPessoaEnderecoCep">CEP</label>		      
			      <input type="text" class="form-control" id="modalCadastroPessoaEnderecoCep" placeholder="00000-000">
			    </div>
	   	  	</div>
	   	  <div class="form-row">
		    <div class="form-group col-md-5">  
		      <label for="modalCadastroPessoaEnderecoTipoLogradouro">Tipo de Logradouro</label>	
		      <select id="modalCadastroPessoaEnderecoTipoLogradouro" class="form-control">
		        <option selected value="">Escolha...</option>
		        <option value="1">Avenida</option>
		        <option value="2">Rua</option>
		        <option value="3">Outro</option>
		      </select>
		    </div>
		    <div class="form-group col-md-5">
		      <label for="modalCadastroPessoaEnderecoLogradouro">Logradouro</label>		      
		      <input type="text" class="form-control" id="modalCadastroPessoaEnderecoLogradouro" placeholder="Nome do logradouro">
		    </div>
		    <div class="form-group col-md-2">
		      <label for="modalCadastroPessoaEnderecoNumero">N�mero</label>		      
		      <input type="text" class="form-control" id="modalCadastroPessoaEnderecoNumero" placeholder="N�">
		    </div>
	   	  </div>
		 <div class="form-row">
		    <div class="form-group col-md-3">  
		      <label for="modalCadastroPessoaEnderecoPais">Pais</label>	
		      <select id="modalCadastroPessoaEnderecoPais" class="form-control">
		        <option value="1">Brasil</option>
		      </select>
		    </div>
		    <div class="form-group col-md-4">  
		      <label for="modalCadastroPessoaEnderecoEstado">Estado</label>	
		      <select id="modalCadastroPessoaEnderecoEstado" class="form-control">
		        <option selected value="">Escolha...</option>
		         <option value="1">AC - Acre</option>
				 <option value="2">AL - Alagoas</option>
				 <option value="3">AP - Amap�</option>
				 <option value="4">AM - Amazonas</option>
				 <option value="5">BA - Bahia</option>
				 <option value="6">CE - Cear�</option>
				 <option value="7">DF - Distrito Federal</option>
				 <option value="8">ES - Esp�rito Santo</option>
				 <option value="9">GO - Goi�s</option>
				 <option value="10">MA - Maranh�o</option>
				 <option value="11">MT - Mato Grosso</option>
				 <option value="12">MS - Mato Grosso do Sul</option>
				 <option value="13">MG - Minas Gerais</option>
				 <option value="14">PA - Par�</option>
				 <option value="15">PB - Para�ba</option>
				 <option value="16">PR - Paran�</option>
				 <option value="17">PE - Pernambuco</option>
				 <option value="18">PI - Piau�</option>
				 <option value="19">RJ - Rio de Janeiro</option>
				 <option value="20">RN - Rio Grande do Norte</option>
				 <option value="21">RS - Rio Grande do Sul</option>
				 <option value="22">RO - Rond�nia</option>
				 <option value="23">RR - Roraima</option>
				 <option value="24">SC - Santa Catarina</option>
				 <option value="25">SP - S�o Paulo</option>
				 <option value="26">SE - Sergipe</option>
				 <option value="27">TO - Tocantins</option>
		      </select>
		    </div>
		    <div class="form-group col-md-5">  
		      <label for="modalCadastroPessoaEnderecoCidade">Cidade</label>	
		      <select id="modalCadastroPessoaEnderecoCidade" class="form-control">
		        <option class="modalCadastroPessoaEnderecoCidadeOpt" value="">Escolha...</option>
		      </select>
		    </div>
	   	  </div>
	   	  <div class="form-group">
		     <label for="modalCadastroPessoaEnderecoObservacoes">Observa��es</label>		      
		     <textarea class="form-control" id="modalCadastroPessoaEnderecoObservacoes" placeholder="(Opcional)"></textarea>
		  </div>
		</form>	
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
      	<button type="button" id="btnSubmitModalCadastroPessoaEndereco" class="btn btn-primary">Salvar</button>
      </div>
    </div>
  </div>
</div>	
<!-- Fim Modal Cadastro Imagem Livro --> 