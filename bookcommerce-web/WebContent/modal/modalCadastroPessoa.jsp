<!-- Modal Cadastro de Pessoa -->
<script src="/bookcommerce-web/js/modalCadastroPessoaController.js" charset="utf-8"></script>
<script>	
	// globais
	var modalCadastroPessoaController;
	
	var SelectorFormPessoa;
	var SelectorFormPessoaEnderecoContainerSelector;
	var SelectorFormPessoaEnderecoOptSelector;
	var SelectorFormPessoaCartaoContainerSelector;
	var SelectorFormPessoaCartaoOptSelector;
	// abrir o modal por esta fun��o
	function openModalCadastroPessoa(pessoa){
		modalCadastroPessoaController.OpenModalCadastroPessoa(pessoa);
	}
	
	$(document).ready(function() {
		// definindo, pois estes mesmos seletores possuem nomes diferentes em outro lugar
		SelectorFormPessoaEnderecoContainerSelector = "#formModalPessoaEndereco";
		SelectorFormPessoaEnderecoOptSelector = ".formModalPessoaEnderecoOpt"; 
		SelectorFormPessoa = "#formModalPessoa";		
		SelectorFormPessoaCartaoContainerSelector = "#formModalPessoaCartao";
		SelectorFormPessoaCartaoOptSelector = ".formModalPessoaCartaoOpt";
		
		var constructorParam = {
			formModalPessoa: "#formModalPessoa",
			modalCadastroPessoa: "#modalCadastroPessoa",
			modalCadastroPessoaCartao: "#modalCadastroPessoaCartao",
			modalCadastroPessoaEndereco: "#modalCadastroPessoaEndereco",
			isLoginPage: window.location.pathname.toLowerCase() === "/bookcommerce-web/login.jsp",
		}	
		modalCadastroPessoaController = new ModalCadastroPessoaController(constructorParam);
		modalCadastroPessoaController.InitEventListeners();		
		$("#formModalPessoaTelefone").mask(SPMaskTelBehavior, spTelOptions);
		$("#formModalPessoaCpf").mask("000.000.000-00");
		$("#formModalPessoaDataNascimento").datepicker({dateFormat: "dd/mm/yy"});		
	});
</script>
<div class="modal fade" id="modalCadastroPessoa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <b class="modal-title" id="exampleModalLongTitle">Cadastro</b>        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"> 
   	 	<form id="formModalPessoa" class="form-group">
       		<div class="form-row">
       			<span class="col-md-11" >Codigo da Cadastro: <span id="formModalPessoaId"></span></span>
	   	        <div class="checkbox col-md-1" style="float: right;">
				  <label>
				    <input type="checkbox" id="formModalPessoaAtivo" checked data-toggle="toggle">		    
				  </label>
				</div>				
       		</div>
       		<div class="form-row">
	       		<div class="form-group col-md-7">
		    		<label for="formModalPessoaEmail">E-mail</label>
			    	<input type="email" class="form-control" id="formModalPessoaEmail">
			  	</div>  
	       		<div class="form-group col-md-5">
		    		<label for="formModalPessoaSenha">Senha</label>
			    	<input type="password" class="form-control" id="formModalPessoaSenha" />
			  	</div>    		
       		</div>
       		<div class="form-row">
	       		<div class="form-group col-md-7">
		    		<label for="formModalPessoaCpf">CPF</label>
			    	<input type="email" class="form-control" id="formModalPessoaCpf" placeholder="000.000.000-00">
			  	</div>
	       		<div class="form-group col-md-5">
		    		<label for="formModalPessoaRepitaSenha">Repita sua senha</label>
			    	<input type="password" class="form-control" id="formModalPessoaRepitaSenha" />
			  	</div>    		
       		</div>
       		<div class="form-row">
				<div class="form-group col-md-4">
		    		<label for="formModalPessoaNome">Nome</label>
			    	<input type="text" class="form-control" id="formModalPessoaNome" aria-describedby="emailHelp" placeholder="Nome">
			  	</div>
			  	<div class="form-group  col-md-6">
			    	<label for="formModalPessoaSobrenome">Sobrenome</label>
			    	<input type="text" class="form-control" id="formModalPessoaSobrenome" placeholder="Sobrenome"/>
			  	</div>   
			  	<div class="form-group  col-md-2">
			    	<label for="formModalPessoaGenero">G�nero</label>
			    	<select id="formModalPessoaGenero" class="custom-select form-control">
			    		<option class="formModalPessoaGeneroOpt" value="1">M</option>
			    		<option class="modalFormSelectLivroEditoraOpt" value="2">F</option>
			    	</select> 
			  	</div>     		
       		</div>
		  	<div class="form-row">
	       		<div class="form-group col-md-5">
		    		<label for="formModalPessoaTelefone">Telefone</label>
			    	<input type="text" class="form-control" id="formModalPessoaTelefone" placeholder="(00) 00000-0000">
			  	</div>  
			  	<div class="form-group col-md-3">
			  		<label for="formModalPessoaTelefoneTipo">Tipo</label>	
				  	<select id="formModalPessoaTelefoneTipo" class="custom-select form-control">
			    		<option class="formModalPessoaTelefoneTipoOpt" value="1">Resid�ncial</option>
			    		<option class="formModalPessoaTelefoneTipoOpt" value="2">Celular</option>
			    		<option class="formModalPessoaTelefoneTipoOpt" value="3">Comercial</option>
			    	</select> 
			  	</div>	    	
	       		<div class="form-group col-md-4">
		    		<label for="formModalPessoaDataNascimento">Data de Nascimento</label>
			    	<input type="text" class="form-control" id="formModalPessoaDataNascimento" />
			  	</div>  		
       		</div>
       		<div class="form-row">
				<div class="form-group col-md-6">
		    		<label for="formModalPessoaEndereco">Endere�o:</label>
			    	<a id="btnAdicionarPessoaEndereco" href="#"><i class="fas fa-plus-square"></i></a>
			    	<select id="formModalPessoaEndereco" class="custom-select form-control">
			    		<option class="formModalPessoaEnderecoOpt" value="">Clique ao lado do endere�o para adicionar</option>
			    	</select>
			  	</div>
			  	<div class="form-group 	col-md-6">
			    	<label for="formModalPessoaCartao">Cart�o:</label>
			    	<a id="btnAdicionarPessoaCartao" href="#"><i class="fas fa-plus-square"></i></a>
			    	<select id="formModalPessoaCartao" class="custom-select form-control">
			    		<option class="formModalPessoaCartaoOpt" value="">Clique ao lado do cart�o para adicionar</option>
			    	</select>
			  	</div>       		
       		</div>		
       		<div class="form-group">
			    <label for="formModalPessoaObservacao">Observa��es</label>
			    <textarea class="form-control" id="formModalPessoaObservacao" rows="3"></textarea>
			</div>
		</form>
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
      	<button type="button" id="btnSubmitFormModalPessoa" class="btn btn-primary">Salvar</button>
      </div>
    </div>
  </div>
</div>	
<!-- Fim Modal Cadastro Imagem Livro --> 