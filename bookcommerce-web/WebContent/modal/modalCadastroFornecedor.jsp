<!-- Modal Cadastro Imagem Livro -->
<script>	
	var modalLivroController;
	$(document).ready(function() {
		var constructorParam = {
			modalLivro: "#modalLivro"
		}
		$('#modalCadastroFornecedor').on('hidden.bs.modal', function (e) {
		    $('body').addClass('modal-open');
		});
	});
</script>
<div class="modal fade" id="modalCadastroFornecedor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <b class="modal-title" id="exampleModalLongTitle">Cadastro de Forcededor</b>        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"> 
  		<form>
  			<div class="form-group">
	     		<label for="3123232">Nome do Fornecedor</label>		      
	    		<input type="text" class="form-control" id="3123232" placeholder="Digite o nome do fornecedor">
		  	</div>
		</form>	
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
      	<button type="button" id="btnSubmitModalCadastroFornecedor" class="btn btn-primary">Salvar</button>
      </div>
    </div>
  </div>
</div>	
<!-- Fim Modal Cadastro Imagem Livro --> 