<!-- Modal Cadastro Imagem Livro -->
<script src="/bookcommerce-web/js/modalPessoaCartaoController.js" charset="utf-8"></script>
<script>	
	var modalPessoaCartaoController;
	// abrir o modal por esta fun��o
	function openModalPessoaCartao(pessoaCartaoCredito){
		modalPessoaCartaoController.openModalPessoaCartao(pessoaCartaoCredito);
	}
	$(document).ready(function() {
		var constructorParam = {
				modalCadastroPessoaCartao: "#modalCadastroPessoaCartao",
				isLoginPage: window.location.pathname.toLowerCase() === "/bookcommerce-web/login.jsp"
		}
		modalPessoaCartaoController = new ModalPessoaCartaoController(constructorParam);
		modalPessoaCartaoController.InitEventListener();
		
		$('#modalCadastroPessoaCartao').on('hidden.bs.modal', function (e) {
		    $('body').addClass('modal-open');
		});
		$("#modalCadastroPessoaCartaoNumero").mask("0000 0000 0000 0000");
		$("#modalCadastroPessoaCartaoCodSeguranca").mask("000");
	});
</script>
<div class="modal fade" id="modalCadastroPessoaCartao" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <b class="modal-title" id="exampleModalLongTitle">Cadastro de Cart�o de c�dito</b>        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"> 
      	<form>
      	  <div class="form-group">
	      	<label for="modalCadastroPessoaCartaoApelido" title="">
	      		Apelido Para o Cart�o
	      	</label>
	      	<input type="email" class="form-control" id="modalCadastroPessoaCartaoApelido" placeholder="D� um apelido para o cart�o">
	      </div>
		  <div class="form-row">		   
		    <div class="form-group col-md-7">
		      <label for="modalCadastroPessoaCartaoNumero">N�mero do Cart�o</label>		      
		      <input type="email" class="form-control" id="modalCadastroPessoaCartaoNumero" placeholder="0000 0000 0000 0000">
		    </div>
		    <div class="form-group col-md-5">
		      <label for="modalCadastroPessoaCartaoCodSeguranca">C�digo de Seguran�a</label>		      
		      <input type="email" class="form-control" id="modalCadastroPessoaCartaoCodSeguranca" placeholder="000">
		    </div>
		  </div>	
		  <div class="form-row">
		    <div class="form-group col-md-7">
		      <label for="modalCadastroPessoaCartaoNome">Nome Impresso no Cart�o</label>		      
		      <input type="email" class="form-control" id="modalCadastroPessoaCartaoNome" placeholder="Digite o mesmo nome impresso no cart�o">
		    </div>
		    <div class="form-group col-md-5">  
		      <label for="modalCadastroPessoaCartaoTipo">Bandeira</label>	
		      <select id="modalCadastroPessoaCartaoTipo" class="form-control">
		        <option selected value="">Escolha...</option>
		        <option value="1">Master Card</option>
		        <option value="2">Visa</option>
		        <option value="3">American Express</option>
		      </select>
		    </div>
	   	  </div>
		</form>	
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
      	<button type="button" id="btnSubmitModalCadastroPessoaCartao" class="btn btn-primary">Salvar</button>
      </div>
    </div>
  </div>
</div>	
<!-- Fim Modal Cadastro Imagem Livro --> 