<!-- Modal Cadastro Imagem Livro -->
<script>	
	var modalLivroController;
	$(document).ready(function() {
		var constructorParam = {
			modalLivro: "#modalLivro"
		}
	});
</script>
<div class="modal fade" id="modalDevolucaoMercadoria" tabindex="-1" role="dialog" aria-labelledby="devolucaoMercadoriaTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <b class="modal-title" id="devolucaoMercadoriaTitle">Devolu��es de Livros</b>        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"> 
      	<h4>Ordem de compra: 0458</h4>
      	
      	<div class="card">
	    	<div class="card-body">
		    	<table class="table">
		    		<tbody>
		    			<tr>
		    				<td>C�d. Item: <b>0001 </b></td>
		    				<td>Livro: <b> A Arte da Guerra</b></td>
		    				<td>Quantidade Comprada: <b>1</b></td>
		    				<td>Valor Pago: R$ <b>27,80</b></td>
		    				<td>Quantidade Devolvida: <input value="0" class="form-control" type="number"></td>
		    			</tr>
		    			<tr>
		    				<td>C�d. Item: <b>0015 </b></td>
		    				<td>Livro: <b> Administra��o</b></td>
		    				<td>Quantidade Comprada: <b>1</b></td>
		    				<td>Valor Pago: R$ <b>30,80</b></td>
		    				<td>Quantidade Devolvida: <input value="0" class="form-control" type="number"></td>
		    			</tr>
		    			<tr>
		    				<td>C�d. Item: <b>0021 </b></td>
		    				<td>Livro: <b>Planta��o no Futuro</b></td>
		    				<td>Quantidade Comprada: <b>1</b></td>
		    				<td>Valor Pago: R$ <b>115,80</b></td>
		    				<td>Quantidade Devolvida: <input value="0" class="form-control" type="number"></td>
		    			</tr>
		    		</tbody>
		    	</table>
		    	<div class="row">
		    		<div class="col-md-6">
		    			Valor da Devolu��o: <b>R$ 185,65</b>
		    		</div>
		    	</div>
		    </div>
	  	</div>
      	
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
      	<button type="button" id="btnSubmitModalCadastroPessoa1" class="btn btn-primary">Salvar</button>
      </div>
    </div>
  </div>
</div>	
<!-- Fim Modal Cadastro Imagem Livro --> 