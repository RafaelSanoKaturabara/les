<!-- Modal Cadastro de Livro -->
<!-- JavaScript pr�prio para ficar mais f�cil a separa��o -->
<script src="/bookcommerce-web/js/modalAdmLivroController.js" charset="utf-8"></script>
<script>
	$(document).ready(function(){
		var parametrosConstrutor = {
				modalCadastroLivro: "#modalCadastroLivro",
				formLivro: "#formLivro",
				modalCadastroLivroImagem: "#modalCadastroLivroImagem"
		}
		var modalLivroController = new ModalAdmLivroController(parametrosConstrutor);
		modalLivroController.InitListeners();
		modalLivroController.FillModalLivrosSelectOptions();		
		
		$("#modalFormLivroPreco").mask('000.000.000.000.000,00', {reverse: true});
	});
</script>
<div class="modal fade" id="modalCadastroLivro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <b class="modal-title" id="exampleModalLongTitle">Cadastro de Livros:</b>        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">      	
       	<form id="formLivro" class="form-group">
       		<div class="form-row">
       			<span class="col-md-11" > C�digo do livro: <span id="modalIdLivro"></span></span>
	   	        <div class="checkbox col-md-1" style="float: right;">
				  <label>
				    <input type="checkbox" id="modalFormLivroAtivo" checked data-toggle="toggle">		    
				  </label>
				</div>				
       		</div>
       		<div class="container">
			  	<div id="carouselModalLivro" style="display: none;" class="carousel slide" data-ride="carousel">
			  		<div id="carouselModalLivroContainer" class="carousel-inner">
				    	<div id="carouselModalLivroLine" class="carousel-item">
					      	<img class="carouselModalLivroImagem" class="d-block w-100" src="">
					    </div>				    
				  	</div>
				  	<div id="carouselModalLivroFim">
					  	<a class="carousel-control-prev" href="#carouselModalLivro" role="button" data-slide="prev">
					    	<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					    	<span class="sr-only">Anterior</span>
					  	</a>
					  	<a class="carousel-control-next" href="#carouselModalLivro" role="button" data-slide="next">
						    <span class="carousel-control-next-icon" aria-hidden="true"></span>
						    <span class="sr-only">Pr�xima</span>
					  	</a>
				  	</div>
				</div>   
				<button id="btnAdicionarLivroImagem" type="button" class="btn btn-primary btn-sm" data-toggle="modal" 
				data-target="#modalCadastroLivroImagem">Adicionar Imagem</button>   	
      		</div>
       		<div class="form-row">
				<div class="form-group col-md-8">
		    		<label for="modalFormLivroCodBarras">C�digo de Barras:</label>
			    	<input type="text" class="form-control" id="modalFormLivroCodBarras" aria-describedby="emailHelp" placeholder="C�gido de Barras">
			  	</div>
			  	<div class="form-group  col-md-4">
			    	<label for="modalFormSelectLivroGrupoPrecificacao">Grupo de Precifica��o:</label>
			    	<a id="btnAdicGrupPrec" href="#"><i class="fas fa-plus-square"></i></a>
			    	<select id="modalFormLivroSelectGrupoPrecificacao" class="custom-select">
			    		<option class="modalFormLivroSelectGrupoPrecificacaoOpt" value="">Selecione</option>
			    	</select>
			  	</div>       		
       		</div>
       		<div class="form-row">
	       		<div class="form-group col-md-8">
		    		<label for="modalFormLivroTitulo">Titulo:</label>
			    	<input type="text" class="form-control" id="modalFormLivroTitulo">
			  	</div>
			  	<div class="form-group col-md-4">
		    		<label for="modalFormLivroPreco">Pre�o:</label>
		    		<div class="input-group">
				        <div class="input-group-prepend">
				          <span class="input-group-text" id="inputGroupPrepend">R$</span>
				        </div>
				        <input type="text" class="form-control" id="modalFormLivroPreco" placeholder="0.000,00">
			      	</div>			    	
			  	</div>       		
       		</div>
		  	<div class="form-row">
		  		<div class="form-group col-md-8">
		    		<label for="modalFormLivroIsbn">ISBN:</label>
			    	<input type="text" class="form-control" id="modalFormLivroIsbn">			  	
		  		</div>
		  		<div class="form-group col-md-2">
		    		<label for="modalFormLivroNumeroPagina">N� P�g.</label>
			    	<input type="number" value="300" class="form-control" id="modalFormLivroNumeroPagina">			  	
		  		</div>
		  		<div class="form-group col-md-2">
		    		<label for="modalFormLivroEdicao">Edi��o:</label>
			    	<input type="number" value="1" class="form-control" id="modalFormLivroEdicao">			  	
		  		</div>
		  	</div>
       		<div class="form-row">
				<div class="form-group col-md-8">
		    		<label for="modalFormSelectLivroEditora">Editora:</label>
			    	<a id="btnAdicEditora" href="#"><i class="fas fa-plus-square"></i></a>
			    	<select id="modalFormSelectLivroEditora" class="custom-select form-control">
			    		<option class="modalFormSelectLivroEditoraOpt" value="">Selecione</option>
			    	</select>
			  	</div>
			  	<div class="form-group 	col-md-4">
			    	<label for="modalFormSelectLivroAno">Ano:</label>
			    	<a id="btnAdicAno" href="#"><i class="fas fa-plus-square"></i></a>
			    	<select id="modalFormSelectLivroAno" class="custom-select form-control">
			    		<option class="modalFormSelectLivroAnoOpt" value="">Selecione</option>
			    	</select>
			  	</div>       		
       		</div>
       		<div class="form-row">
       		<div class="form-group col-md-6">
	    		<label for="modalFormSelectLivroAutor">Autor:</label>
			    	<a id="btnAdicAutor" href="#"><i class="fas fa-plus-square"></i></a>
			    	<select id="modalFormSelectLivroAutor" class="custom-select form-control" multiple>
			    		<option class="modalFormSelectLivroAutorOpt" value="">Selecione</option>
			    	</select>
			  	</div>				
			  	<div class="form-group 	col-md-6">
			    	<label for="modalFormSelectLivroCategoria">Categoria: (categoria do shopping)</label>
			    	<a id="btnAdicCategoria" href="#"><i class="fas fa-plus-square"></i></a>
			    	<select id="modalFormSelectLivroCategoria" class="form-control" multiple>
			    		<option class="modalFormSelectLivroCategoriaOpt" value="">Selecione</option>
			    	</select>
			  	</div>       		
       		</div> 	
       		<div class="form-row panel panel-group">
				<div class="form-group col-md-3">	
		    		<label for="modalFormLivroAltura">Altura:</label>
			    	<input type="text" id="modalFormLivroAltura" class="form-control" placeholder="cm"/>
			  	</div>
			  	<div class="form-group 	col-md-3">
		    		<label for="modalFormLivroLargura">Largura:</label>
			    	<input type="text" id="modalFormLivroLargura" class="form-control" placeholder="cm"/>
			  	</div>     
			  	<div class="form-group 	col-md-3">
		    		<label for="modalFormLivroProfundidade">Profundidade:</label>
			    	<input type="text" id="modalFormLivroProfundidade" class="form-control" placeholder="cm"/>
			  	</div> 
			  	<div class="form-group 	col-md-3">
		    		<label for="modalFormLivroPeso">Peso:</label>
			    	<input type="text" id="modalFormLivroPeso" class="form-control" placeholder="mg"/>
			  	</div>   		
       		</div> 
       		<div class="form-group">
			    <label for="modalFormLivroSinopse">Sinopse</label>
			    <textarea class="form-control" id="modalFormLivroSinopse" rows="3"></textarea>
			</div>
		</form>
      </div>
      <div class="modal-footer">
      <button type="button" id="btnExluirLivro" class="btn btn-danger">Excluir Livro</button>      
      <button type="button" id="btnResetModalFormLivro" class="btn btn-secondary">Novo</button>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
      <button type="button" id="btnSubmitModalFormLivro" class="btn btn-primary">Salvar</button>
      </div>
    </div>
  </div>
</div>	
<!-- Fim Modal Cadastro de Livro-->