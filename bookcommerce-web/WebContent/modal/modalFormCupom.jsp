<!-- Modal Cadastro Imagem Livro -->
<script>	
	var modalLivroController;
	$(document).ready(function() {
		var constructorParam = {
			modalLivro: "#modalLivro"
		}
	});
</script>
<div class="modal fade" id="modalCadastroCupom" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <b class="modal-title" id="exampleModalLongTitle">Cria��o de Cupons</b>        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"> 
      	<form>
		  <div class="form-row">
		    <div class="form-group col-md-6">
		      <label for="inputEmail421" title="Caso n�o seja adicionado, o sistema gerar� um c�digo automaticamente">
		      	C�gido do Cupom: (Opcional)
		      </label>
		      <input type="email" class="form-control" id="inputEmail4" Value="C2R0U1D8">
		    </div>
		    <div class="form-group col-md-6">
		      <label for="inputState">Tipo de Cupom</label>
		      <select id="inputState" class="form-control">
		        <option selected>Escolha...</option>
		        <option>Promocional</option>
		        <option>Desconto</option>
		        <option>Devolu��o (N�o deve aparecer aqui)</option>
		      </select>
		    </div>
		  </div>	
		  <div class="form-row">
		    <div class="form-group col-md-6">
		      <label for="inputState">Tipo de Desconto</label>
		      <select id="inputState" class="form-control">
		        <option selected>Escolha...</option>
		        <option>Porcentagem</option>
		        <option>Valor</option>
		      </select>
		    </div>
		    <div class="form-group col-md-6">
		      <label for="inputZip">Valor do desconto</label>
		      <input type="text" class="form-control" id="inputZip">
		    </div>
		  </div>
		  <div class="form-group">
		  	<label for="formDataValidadeCupom">Validade do Cupom</label>
		  		<input class="form-control" type="date" value="2018-04-09"/>		  
		  </div>
		</form>	
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
      	<button type="button" id="btnSubmitModalCadastroPessoa" class="btn btn-primary">Salvar</button>
      </div>
    </div>
  </div>
</div>	
<!-- Fim Modal Cadastro Imagem Livro --> 