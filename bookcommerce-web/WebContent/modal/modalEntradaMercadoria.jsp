<!-- Modal Cadastro Imagem Livro -->
<style>
	.visibilidadeCampos{
		display: none;
	}
	.ui-autocomplete { 
		z-index:2147483647; 
	}
</style>
<script src="/bookcommerce-web/js/modalEntradaMercadoriaController.js" charset="utf-8"></script>
<script>	
	var modalEntradaMercadoriaController;
	$(document).ready(function() {
		var selectors = {
			modalEntradaMercadoria: "#modalEntradaMercadoria"
		}
		modalEntradaMercadoriaController = new ModalEntradaMercadoriaController(selectors);
		modalEntradaMercadoriaController.GetAndFillFornecedor();
		modalEntradaMercadoriaController.GetAndFillLivroMultiselect();
		
		$("#formEntradaMercadoriaNumeroNota").on("focusout", function(){
			modalEntradaMercadoriaController.GetEntrada();
			$(".visibilidadeCampos").show();
		});
		
		$("#soParaOSelenium").click(function(){
			modalEntradaMercadoriaController.GetEntrada();
			$(".visibilidadeCampos").show();
		});

		$("#btnAdicionarFornecedor").click(function(){
			$("#modalCadastroFornecedor").modal("show");
		});
		
		$(document).on("click", ".btnExcluirEntradaLivro", function(e) {
			e.stopPropagation();
			e.preventDefault();
		});
		
		$("#btnSubmitEntrada").click(function() {
			modalEntradaMercadoriaController.SaveEntrada();
		});
		
		$(document).on("focusout", ".tableEntradaLivroQuantidade", function(e) {
			e.stopPropagation();
			e.preventDefault();
			var entradaLivro = $(this).parent().parent(".tableEntradaLivroLine").data("entradaLivro");
			var quantidade = $(this).val();
			var precoPago = $(this).parent().parent(".tableEntradaLivroLine").find(".tableEntradaLivroPrecoPago").val();
			
			entradaLivro.quantidade = quantidade;
			entradaLivro.precoPago = precoPago != null || precoPago !== "" ? precoPago.replace(",",".") : precoPago;
			modalEntradaMercadoriaController.AlterEntradaLivro(entradaLivro);
		});
		
		$(document).on("focusout", ".tableEntradaLivroPrecoPago", function(e) {
			e.stopPropagation();
			e.preventDefault();
			var precoPago = $(this).val();
			var quantidade = $(this).parent().parent(".tableEntradaLivroLine").find(".tableEntradaLivroQuantidade").val();
			var entradaLivro = $(this).parent().parent(".tableEntradaLivroLine").data("entradaLivro");
			entradaLivro.quantidade = quantidade;
			entradaLivro.precoPago = precoPago != null && precoPago != "" ? precoPago.replace(",",".") : precoPago;
			modalEntradaMercadoriaController.AlterEntradaLivro(entradaLivro);
		});
		
		$("#soParaOSeleniumQtde").click(function(e) {
			console.log("clicou");
			e.stopPropagation();
			e.preventDefault();
			var precoPago = $(".tableEntradaLivroLine").find(".tableEntradaLivroPrecoPago").val();
			var quantidade = $(".tableEntradaLivroLine").find(".tableEntradaLivroQuantidade").val();
			var entradaLivro = $(".tableEntradaLivroLine").data("entradaLivro");
			entradaLivro.quantidade = quantidade;
			entradaLivro.precoPago = precoPago != null && precoPago != "" ? precoPago.replace(",",".") : precoPago;
			modalEntradaMercadoriaController.AlterEntradaLivro(entradaLivro);
		});
		
		$(".tableEntradaLivroPrecoPago").mask("000.000,00", {reverse:true});
		
		$("#formEntradaMercadoriaData").datepicker({dateFormat: "dd/mm/yy"});		
	});
</script>
<div class="modal fade" id="modalEntradaMercadoria" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <b class="modal-title" id="modalEntradaMercadoriaTitle">Entrada de Mercadorias</b>        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"> 
      	<form id="formEntradaMercadoria">
		  <div class="form-row">
		    <div class="form-group col-md-6">
		      <label for="formEntradaMercadoriaNumeroNota">N�mero da nota</label>
		    	<a id="soParaOSelenium" href="#"><i class="far fa-question-circle"></i></a>
		      <input type="text" class="form-control" id="formEntradaMercadoriaNumeroNota" placeholder="Digite o n�mero da nota">
		    </div>
	    	<div class="form-group col-md-6 visibilidadeCampos">
		      <label for="formEntradaMercadoriaData">Data da Nota</label>
		      <input type="text" class="form-control" id="formEntradaMercadoriaData" placeholder="DD/MM/AAAA">
		    </div>    
		  </div>
		  <div class="form-row visibilidadeCampos">
		  	<div class="form-group col-md-12">
		    	<label for="formEntradaMercadoriaFornecedorSelect">Fornecedor:</label>
		    	<a id="btnAdicionarFornecedor" href="#"><i class="fas fa-plus-square"></i></a>
		    	<select id="formEntradaMercadoriaFornecedorSelect" class="custom-select">
		    		<option class="formEntradaMercadoriaFornecedorLine" value="">Selecione</option>
		    		<option class="formEntradaMercadoriaFornecedorLine" value="1">Mogi la la al</option>
		    	</select>
		  	</div>  
		  </div>		  
		  <div class="form-row visibilidadeCampos">
		    <div class="form-group col-md-6">
		      <label for="inputModalPesquisaLivro">Encontre o livro</label>
		    	<a id="soParaOSeleniumQtde" href="#"><i class="far fa-question-circle"></i></a>
		      <input type="text" class="form-control" id="inputModalPesquisaLivro" placeholder="C�digo do Livro, C�digo de Barras e Titulo">
		    </div>
		  </div>
		  <table class="table visibilidadeCampos" id="tableEntradaLivro">
		  	<thead>
		  		<tr>
		  			<th scope="col">Cod. Livro</th>
		  			<th scope="col">Cod. Barras</th>
		  			<th scope="col">Titulo</th>
		  			<th scope="col">Quantidade</th>
		  			<th scope="col">Pre�o</th>
		  			<th scope="col">Total</th>		  			
		  			<th scope="col">A��o</th>
		  		</tr>
		  	</thead>
		  	<tbody id="tableEntradaLivroContainer">
		  		<tr class="tableEntradaLivroLine">
		  			<td scope="row" class="tableEntradaLivroIdLivro"></td>
		  			<td class="tableEntradaLivroCodBarras"></td>
		  			<td class="tableEntradaLivroTitulo"></td>
		  			<td><input type="text" class="form-control tableEntradaLivroQuantidade" value="0"/></td>
		  			<td><input type="text" class="form-control tableEntradaLivroPrecoPago" value="0"/></td>
		  			<td class="tableEntradaLivroTotalUnitario">R$ 875,00</td>
		  			<td><button class="btn btn-default btnExcluirEntradaLivro"><i class="far fa-trash-alt"></i></button></td>
		  		</tr>
		  	</tbody>
		  </table>
		  <div class="visibilidadeCampos">			  
			  <div class="row">
			    <div class="col-md-12 text-right">
			      <b>Total da Nota R$ </b><b id="inputModalEntradaLivroTotalNota"></b>
			    </div>		    
			  </div>			  
		  </div>
		</form>	
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
      	<button type="button" id="btnSubmitEntrada" class="btn btn-primary visibilidadeCampos">Salvar</button>
      </div>
    </div>
  </div>
</div>	
<!-- Fim Modal Cadastro Imagem Livro --> 