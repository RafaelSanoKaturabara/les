<!-- Modal Cadastro Imagem Livro -->
<script>	
	var modalLivroController;
	$(document).ready(function() {
		var constructorParam = {
			modalLivro: "#modalLivro"
		}
	});
</script>
<div class="modal fade" id="modalOrdemCompra" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <b class="modal-title" id="exampleModalLongTitle">Ordem de Compra: 0001</b>        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<div class="container">
      		<div class="row">
	      		<div class="col-12">
	      			Comprador: <b>Rafael Sano Katurabara</b> <br/>
	      			E-mail: <b>rafael.katurabara@fatec.com</b> <br/>
	      			Nota Paulista: <b>326.860.148-06</b> <br/>
	      			Endere�o Entrega: <b>Ricardo Vilela, 35 - Centro - Mogi Das Cruzes - SP</b>
	      			
	      		</div>
	      	</div>
	      	<div class="row">
	      		<div class="col-12">
	      			<table class="table">
	      				<thead>
	      					<tr> 
	      						<th>Img</th>
	      						<th>Livro</th>
	      						<th>Qtd.</th>
	      						<th>Valor</th>
	      						<th>Total</th>
	      					</tr>
	      				</thead>
	      				<tbody>
	      					<tr>
	      						<td><img src="/bookcommerce-web/imagens/50x50.png" class="rounded" alt="..."></td>
	      						<td>A Arte da Guerra</td>
	      						<td>1</td>
	      						<td>R$ 35,45</td>
	      						<td>R$ 35,45</td>
	      					</tr>
	      					<tr>
	      						<td><img src="/bookcommerce-web/imagens/50x50.png" class="rounded" alt="..."></td>
	      						<td>A Arte da Guerra</td>
	      						<td>1</td>
	      						<td>R$ 35,45</td>
	      						<td>R$ 35,45</td>
	      					</tr>
	      					<tr>
	      						<td><img src="/bookcommerce-web/imagens/50x50.png" class="rounded" alt="..."></td>
	      						<td>A Arte da Guerra</td>
	      						<td>1</td>
	      						<td>R$ 35,45</td>
	      						<td>R$ 35,45</td>
	      					</tr>
	      					<tr>
	      						<td><img src="/bookcommerce-web/imagens/50x50.png" class="rounded" alt="..."></td>
	      						<td>A Arte da Guerra</td>
	      						<td>1</td>
	      						<td>R$ 35,45</td>
	      						<td>R$ 35,45</td>
	      					</tr>
	      				</tbody>
	      			</table>
	      		</div>
	      	</div>
	      	<div class="row">
	      		<div class="col-12 text-right">
	      			Total Pago: <b>R$ 150,00</b>
	      		</div>
	      	</div>
      	</div>       	
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>	
<!-- Fim Modal Cadastro Imagem Livro --> 