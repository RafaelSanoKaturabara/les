<!-- Modal Cadastro Imagem Livro -->
<script src="/bookcommerce-web/js/modalLivroController.js"  charset="utf-8"></script>
<script>	
	var modalLivroController;
	$(document).ready(function() {
		var constructorParam = {
			modalLivro: "#modalLivro"
		}
		modalLivroController = new ModalLivroController(constructorParam);
	});
</script>
<div class="modal fade" id="modalLivro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <b class="modal-title" id="exampleModalLongTitle">Livro:</b>        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"> 
      	<div class="row">
	   		<div class="col-md-5 item-photo">
	        	<img style="max-width:100%;" src="/bookcommerce-web/imagens/300x300.png" />
	        </div>
	        <div class="col-md-7" style="border:0px solid gray">
	            <!-- Datos del vendedor y titulo del producto -->
	            <h3>O Livro dos Mortos Tibetano</h3>    
	            <h5 style="color:#337ab7">Autor <a href="#">Lama Kazi Dawa Samdup</a> � <small style="color:#337ab7">(5054 vendas)</small></h5>
	            <!-- Precios -->
	            <h6 class="title-price"><small>PRE�O PROMOCIONAL</small></h6>
	            <h3 style="margin-top:0px;">R$ 39,99</h3>
	
	            <!-- Detalles especificos del producto -->
	            <div class="section">
	                <h6 class="title-attr" style="margin-top:15px;" ><small>7� Edi��o</small></h6> 	                
	                <h6 class="title-attr" style="margin-top:15px;" ><small>Editora Hemus</small></h6>                     
	                <div>
	                    <div class="attr" style="width:25px;background:#5a5a5a;"></div>
	                    <div class="attr" style="width:25px;background:white;"></div>
	                </div>
	            </div>   
	            <div class="section" style="padding-bottom:20px;">
	                <h6 class="title-attr"><small>QUANTIDADE</small></h6>                    
	                <div>
	                    <div class="btn-minus"><span class="glyphicon glyphicon-minus"></span></div>
	                    <input class="form-control col-3" type="number" value="1" />
	                    <div class="btn-plus"><span class="glyphicon glyphicon-plus"></span></div>
	                </div>
	            </div>   
	            <!-- Botones de compra -->
	            <div class="section" style="padding-bottom:20px;">
	                <button class="btn btn-success"><i class="fas fa-shopping-cart"></i> Adicionar ao Carrinho</button>
	                <h6 style="display: none"><a href="#"><span class="glyphicon glyphicon-heart-empty" style="cursor:pointer;"></span> Agregar a lista de deseos</a></h6>
	            </div>                                        
	        </div>      	
      	</div>
		<div class="col-md-12">
           <div style="width:100%;border-top:1px solid silver">
               <p style="padding:15px;">
                   <small>
                   Stay connected either on the phone or the Web with the Galaxy S4 I337 from Samsung. With 16 GB of memory and a 4G connection, this phone stores precious photos and video and lets you upload them to a cloud or social network at blinding-fast speed. With a 17-hour operating life from one charge, this phone allows you keep in touch even on the go. 

                   With its built-in photo editor, the Galaxy S4 allows you to edit photos with the touch of a finger, eliminating extraneous background items. Usable with most carriers, this smartphone is the perfect companion for work or entertainment.
                   </small>
               </p>
               <small>
                   <ul>
                       <li>Super AMOLED capacitive touchscreen display with 16M colors</li>
                       <li>Available on GSM, AT T, T-Mobile and other carriers</li>
                       <li>Compatible with GSM 850 / 900 / 1800; HSDPA 850 / 1900 / 2100 LTE; 700 MHz Class 17 / 1700 / 2100 networks</li>
                       <li>MicroUSB and USB connectivity</li>
                       <li>Interfaces with Wi-Fi 802.11 a/b/g/n/ac, dual band and Bluetooth</li>
                       <li>Wi-Fi hotspot to keep other devices online when a connection is not available</li>
                       <li>SMS, MMS, email, Push Mail, IM and RSS messaging</li>
                       <li>Front-facing camera features autofocus, an LED flash, dual video call capability and a sharp 4128 x 3096 pixel picture</li>
                       <li>Features 16 GB memory and 2 GB RAM</li>
                       <li>Upgradeable Jelly Bean v4.2.2 to Jelly Bean v4.3 Android OS</li>
                       <li>17 hours of talk time, 350 hours standby time on one charge</li>
                       <li>Available in white or black</li>
                       <li>Model I337</li>
                       <li>Package includes phone, charger, battery and user manual</li>
                       <li>Phone is 5.38 inches high x 2.75 inches wide x 0.13 inches deep and weighs a mere 4.59 oz </li>
                   </ul>  
               </small>
           </div>
       	</div>	
      </div>
    </div>
  </div>
</div>	
<!-- Fim Modal Cadastro Imagem Livro --> 