function LoginController(constructorParam){
	var $formLogin = $(constructorParam.formLogin);
	
	function readFormLogin(){
		var pessoa = {
			email: $formLogin.find("#formLoginEmail").val(),
			senha: $formLogin.find("#formLoginSenha").val()
		}
		return pessoa;
	} 
	
	function verifyAndLogin(pessoa){
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/LoginWS?operacao=CONSULTAR",
            dataType: "json",
            data: JSON.stringify(pessoa),
            contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
            		$.gritter.add({
                        title: respostaServlet.mensagemResposta,
                        text: "Cadastro efetuado com sucesso!",
                        time: 1500
                    });
            		window.location.href = "/bookcommerce-web/default.jsp";
            	} else if(respostaServlet.codigoResposta == 250){ //erro!            		
            		$.gritter.add({
            			  title: "Login não efetuado",
            			  text: "E-mail ou senha inválidos.",
                          time: 3000
            			});
            	} else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }, 
            complete: function(){
            	//$("#btnSubmitModalFormLivro").prop( "disabled", false );
            }
		});
	}

	function initEventListeners(){
		$formLogin.submit(function(e){
			e.preventDefault();
			verifyAndLogin(readFormLogin());
		});		
	}
	
	this.InitEventListeners = function(){
		initEventListeners();
	}
}