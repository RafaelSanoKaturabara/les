function CarrinhoController(selectors){
	var $carrinhoContainer = $(selectors.carrinhoItemContainer);
	var $carrinhoLine = $carrinhoContainer.find(".carrinhoLine").clone();
	$carrinhoContainer.find(".carrinhoLine").remove();
	var $carrinho = $(selectors.carrinhoContainer);
	var $carrinhoContainerCerto = $(selectors.carrinhoContainer);
	var $carrinhoCupomContainer = $carrinhoContainerCerto.find("#carrinhoCupomContainer");
	var $carrinhoCupomLine = $carrinhoContainerCerto.find(".carrinhoCupomLine").clone();
	var ajaxAlterCarrinhoItem;
	
	
	function alterCarrinhoItem(carrinhoItem){
		if(ajaxAlterCarrinhoItem != null) // já foi instanciado?
			ajaxAlterCarrinhoItem.abort();
		ajaxAlterCarrinhoItem = $.ajax({
			type: "POST",
            url: "/bookcommerce-web/CarrinhoItemWS?operacao=ALTERAR",
            dataType: "json",
            //async: false,
            data: JSON.stringify(carrinhoItem),
            contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	if(ex.statusText !== "abort")
	            	$.gritter.add({
	                    title: "Erro",
	                    text: ex.statusText
	                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
            		var entradaLivroList = respostaServlet.resultado.entidades;       
            		$.gritter.add({
                        title: "Sucesso",
                        text: "Quantidades e preços atualizados",
                        time: 1500
                    });
            	} else if(respostaServlet.codigoResposta == 250){ //erro!            		
            		swal({
            			  type: 'error',
            			  title: "Erro:",
            			  html: respostaServlet.resultado.msg.replace("\n", "<br/>")
        			});
            	} else if(respostaServlet.codigoResposta == 275){ // não logado!            		
            		window.location.href = "/bookcommerce-web/login.jsp";
              	} else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }, 
            complete: function(){
            	//$("#btnSubmitModalFormLivro").prop( "disabled", false );
            }
		});
	}
	
	function fillCarrinhoLine(carrinhoItem, $carrinhoLine){
		$carrinhoLine.data("carrinhoItem", carrinhoItem);
		$carrinhoLine.find(".carrinhoItemTitulo").text(carrinhoItem.livro.titulo);		
		$carrinhoLine.find(".carrinhoItemAutorLine").hide(); //.text(pessoaCarrinhoItem.livro);
		$carrinhoLine.find(".carrinhoItemEditora").text(carrinhoItem.livro.editora.nome);
		$carrinhoLine.find(".carrinhoItemAno").text(carrinhoItem.livro.ano.ano);
		$carrinhoLine.find(".carrinhoItemQuantidade").val(carrinhoItem.quantidade);
		$carrinhoLine.find(".carrinhoItemImagem").attr("src", carrinhoItem.livro.img100 == null ? "/bookcommerce-web/imagens/100x100.png" : carrinhoItem.livro.img100);
		// adicionando spinner do jquey
		$carrinhoLine.find(".carrinhoItemQuantidade").spinner({
		      min: 1,
		      max: 50,
		      spin: function( event, ui ) {
		    	  var carrinhoItem = $(this).parent().parent().parent().parent().parent().parent().data("carrinhoItem");
		    	  var quantidadeNova = ui.value;
		    	  carrinhoItem.quantidade = quantidadeNova;
		    	  alterCarrinhoItem(carrinhoItem);
		      }
		});
		$carrinhoLine.find(".carrinhoItemPrecoUnit").text(FloatFormat(carrinhoItem.precoItem, 2));
		$carrinhoLine.find(".carrinhoItemPrecoTotal").text(FloatFormat(carrinhoItem.precoItem * carrinhoItem.quantidade, 2));
	}
	
	function fillCarrinhoCupomList(carrinhoCupom, $carrinhoCupomLine){
		var valorFormatado;
		if(carrinhoCupom.cupom.cupomDescontoTipo.id == 1){ // tipo porcentagem
			valorFormatado = FloatFormat(carrinhoCupom.cupom.valorDesconto, 2) + "%";
		} else { // valor
			valorFormatado = "R$ " + FloatFormat(carrinhoCupom.cupom.valorDesconto, 2);
		}
		$carrinhoCupomLine.data("carrinhoCupom", carrinhoCupom);
		$carrinhoCupomLine.find(".carrinhoCupomNome").text(carrinhoCupom.cupom.codigo); 
		$carrinhoCupomLine.find(".carrinhoCupomValor").text(valorFormatado);
	}
	
	function fillCarrinhoContainer(carrinho){
		$carrinhoContainer.empty();	
		if(carrinho == null)
			return;
		var totalGeral = getTodalCarrinhoItem(carrinho.carrinhoItemList);
		var totalComDesconto = calcularTotalDescontoCarrinhoCupom(carrinho);
		$("#carrinhoFrete").data("paraCalculo", carrinho.precoFrete + totalComDesconto);
		$("#carrinhoTotalGeral").text(FloatFormat(totalGeral, 2));
		$("#carrinhoCupom").text(FloatFormat(totalGeral - totalComDesconto, 2));
		$("#carrinhoFrete").text(FloatFormat(carrinho.precoFrete, 2));
		$("#carrinhoTotal").text(FloatFormat(carrinho.precoFrete + totalComDesconto, 2));
		$.each(carrinho.carrinhoItemList, function(i, carrinhoItem) {
			var $carrinhoLineClone = $carrinhoLine.clone();
			fillCarrinhoLine.call(this, carrinhoItem, $carrinhoLineClone);
			$carrinhoContainer.append($carrinhoLineClone);			
		});
		
		if(carrinho != null && carrinho.carrinhoCupomList != null){
			$carrinhoCupomContainer.empty();
			$.each(carrinho.carrinhoCupomList, function(i, carrinhoCupom){
				var $carrinhoCupomLineClone = $carrinhoCupomLine.clone();
				fillCarrinhoCupomList.call(this, carrinhoCupom, $carrinhoCupomLineClone);
				$carrinhoCupomContainer.append($carrinhoCupomLineClone);
			});
		}
		
	}
	
	function getAndPrintCarrinhoContainer(){
		var carrinho = {
			carrinhoStatus: {
				id: 1 // tipo carrinho
			}
		}
		
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/CarrinhoWS?operacao=CONSULTAR",
            dataType: "json",
            data: JSON.stringify(carrinho),
            contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
            		var carrinhoList = respostaServlet.resultado.entidades;
            		fillCarrinhoContainer(carrinhoList[0]);
            	} else if(respostaServlet.codigoResposta == 250){ //erro!            		
            		swal({
            			  type: 'error',
            			  title: "Erro ao cadastrar",
            			  html: respostaServlet.resultado.msg.replace("\n", "<br/>")
            			});
            	}else if(respostaServlet.codigoResposta == 275){ // não logado!            		
            		window.location.href = "/bookcommerce-web/login.jsp";
              	} else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }
		});
	}
	
	function deleteCarrinhoItem(carrinhoItem) {
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/CarrinhoItemWS?operacao=EXCLUIR",
            dataType: "json",
            data: JSON.stringify(carrinhoItem),
            contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
            		var carrinhoList = respostaServlet.resultado.entidades;
            		getAndPrintCarrinhoContainer();
            		$.gritter.add({
                        title: "Sucesso",
                        text: "Livro retirado do carrinho",
                        time: 1500
                    });
            	} else if(respostaServlet.codigoResposta == 250){ //erro!            		
            		swal({
            			  type: 'error',
            			  title: "Erro ao cadastrar",
            			  html: respostaServlet.resultado.msg.replace("\n", "<br/>")
            			});
            	}else if(respostaServlet.codigoResposta == 275){ // não logado!            		
            		window.location.href = "/bookcommerce-web/login.jsp";
              	} else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }
		});
	}
	
	function readAndAddCupomToCarrinho(){
		var carrinhoCupom = {
			cupom: {
				codigo: $carrinho.find("#inputCupom").val()
			}
		}
		
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/CarrinhoCupomWS?operacao=SALVAR",
            dataType: "json",
            data: JSON.stringify(carrinhoCupom),
            contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
            		var carrinhoList = respostaServlet.resultado.entidades;
            		getAndPrintCarrinhoContainer();
            		$.gritter.add({
                        title: "Sucesso",
                        text: "Cupom inserido com sucesso",
                        time: 1500
                    });
            	} else if(respostaServlet.codigoResposta == 250){ //erro!            		
            		swal({
            			  type: 'error',
            			  title: "Erro de cupom",
            			  html: respostaServlet.resultado.msg.replace("\n", "<br/>")
            			});
            	}else if(respostaServlet.codigoResposta == 275){ // não logado!            		
            		window.location.href = "/bookcommerce-web/login.jsp";
              	} else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }
		});
	}
	
	this.DeleteCarrinhoItem = function(carrinhoItem) {
		deleteCarrinhoItem(carrinhoItem);
	}
	this.PrintCarrinhoContainer = function() {
		getAndPrintCarrinhoContainer();
	}
	this.AddCupomToCarrinho = function() {
		readAndAddCupomToCarrinho();
	}
}