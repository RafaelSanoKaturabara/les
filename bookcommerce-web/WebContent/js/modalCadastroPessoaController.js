function ModalCadastroPessoaController(constructorParam){
	var $modalCadastroPessoa = $(constructorParam.modalCadastroPessoa);
	var $formModalPessoa = $(constructorParam.formModalPessoa);
//	var $modalCadastroPessoaCartao = $(constructorParam.modalCadastroPessoaCartao);
	var $modalCadastroPessoaEndereco = $(constructorParam.modalCadastroPessoaEndereco);
	var $formModalPessoaEnderecoContainer = $formModalPessoa.find("#formModalPessoaEndereco");
	var $formModalPessoaEnderecoOpt = $formModalPessoa.find(".formModalPessoaEnderecoOpt").clone();
	var $formModalPessoaCartaoContainer = $formModalPessoa.find("#formModalPessoaCartao");
	var $formModalPessoaCartaoOpt = $formModalPessoa.find(".formModalPessoaCartaoOpt").clone();
	
	var isLoginPage = constructorParam.isLoginPage; 
	
	function fillModalCadastroPessoa(pessoa){	
		if(pessoa != null){ // 
			$formModalPessoa.find("#formModalPessoaId").text(pessoa.id);
			$formModalPessoa.find("#formModalPessoaEmail").val(pessoa.email);
			$formModalPessoa.find("#formModalPessoaSenha").val("");
			$formModalPessoa.find("#formModalPessoaCpf").val(pessoa.cpf);
			$formModalPessoa.find("#formModalPessoaRepitaSenha").val("");
			$formModalPessoa.find("#formModalPessoaNome").val(pessoa.nome);
			$formModalPessoa.find("#formModalPessoaSobrenome").val(pessoa.sobrenome);
			$formModalPessoa.find("#formModalPessoaAtivo").prop('checked', pessoa.Ativo);
			$formModalPessoa.find("#formModalPessoaGenero").val(pessoa.genero.id);
			$formModalPessoa.find("#formModalPessoaTelefone").val(pessoa.telefone.numeroCompleto);
			$formModalPessoa.find("#formModalPessoaTelefoneTipo").val(pessoa.telefone.telefoneTipo.id);
			$formModalPessoa.find("#formModalPessoaDataNascimento").val(pessoa.dataNascimento);
			$formModalPessoa.find("#formModalPessoaObservacao").val(pessoa.observacao);
		} else {
			$formModalPessoa.find("#formModalPessoaId").text("");
			$formModalPessoa.find("#formModalPessoaEmail").val("");
			$formModalPessoa.find("#formModalPessoaSenha").val("");
			$formModalPessoa.find("#formModalPessoaCpf").val("");
			$formModalPessoa.find("#formModalPessoaRepitaSenha").val("");
			$formModalPessoa.find("#formModalPessoaNome").val("");
			$formModalPessoa.find("#formModalPessoaSobrenome").val();
			$formModalPessoa.find("#formModalPessoaAtivo").prop('checked', true);
			$formModalPessoa.find("#formModalPessoaGenero").val("");
			$formModalPessoa.find("#formModalPessoaTelefone").val("");
			$formModalPessoa.find("#formModalPessoaTelefoneTipo").val("");
			$formModalPessoa.find("#formModalPessoaDataNascimento").val("");
			$formModalPessoa.find("#formModalPessoaObservacao").val("");
		}
	}
//	
//	function fillModalPessoaEnderecoOpt(pessoaEndereco, $formModalPessoaEnderecoOpt){
//		$formModalPessoaEnderecoOpt.val(pessoaEndereco.id);
//		$formModalPessoaEnderecoOpt.text(pessoaEndereco.apelido);
//	}
	
	function readFormPessoaLogin(){
		var dataNascimentoBr = $formModalPessoa.find("#formModalPessoaDataNascimento").val();
		var dataNascimentoUs =  dataNascimentoBr.substring(6,10) + "-" + dataNascimentoBr.substring(3,5) + "-" + dataNascimentoBr.substring(0,2) + " 00:00";
		var pessoa = {
			id: $formModalPessoa.find("#formModalPessoaId").text(),
			email: $formModalPessoa.find("#formModalPessoaEmail").val(),
			senha: $formModalPessoa.find("#formModalPessoaSenha").val(),
			cpf: $formModalPessoa.find("#formModalPessoaCpf").val(),
			repitaSenha: $formModalPessoa.find("#formModalPessoaRepitaSenha").val(),
			nome: $formModalPessoa.find("#formModalPessoaNome").val(),
			sobrenome: $formModalPessoa.find("#formModalPessoaSobrenome").val(),
			ativo: $formModalPessoa.find("#formModalPessoaAtivo").prop('checked'),
			genero: {
				id: $formModalPessoa.find("#formModalPessoaGenero").val()
				},
			telefone: {
				numeroCompleto: $formModalPessoa.find("#formModalPessoaTelefone").val(),
				telefoneTipo:{
					id: $formModalPessoa.find("#formModalPessoaTelefoneTipo").val()
				}
			},
			dataNascimento: new Date(dataNascimentoUs),
			observacao: $formModalPessoa.find("#formModalPessoaObservacao").val(),
			enderecoPadrao: PessoaEndereco,
			cartaoCreditoPadrao: PessoaCartaoCredito
		}	
		return pessoa;
	}

	function cadastrarPessoaPaginaLogin(){
		var objPost = readFormPessoaLogin();
		//objPost.operacao = "SALVAR";
		
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/CadastroWS?operacao=SALVAR",
            dataType: "json",
            data: JSON.stringify(objPost),
            contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
            		$.gritter.add({
                        title: respostaServlet.mensagemResposta,
                        text: "Cadastro efetuado com sucesso!",
                        time: 1500
                    });
            	} else if(respostaServlet.codigoResposta == 250){ //erro!            		
            		swal({
            			  type: 'error',
            			  title: "Erro ao cadastrar",
            			  html: respostaServlet.resultado.msg.replace("\n", "<br/>")
            			});
            	} else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }, 
            complete: function(){
            	//$("#btnSubmitModalFormLivro").prop( "disabled", false );
            }
		});
	}
	
	function openModalCadastroPessoa(pessoa){
		fillModalCadastroPessoa(pessoa);
		$modalCadastroPessoa.modal("show");	
	}
	
	function savePessoa(formPessoa){
		if(isLoginPage){ // estamos na página de login
			cadastrarPessoaPaginaLogin();
		} else {
			console.log("Colocar código aqui");
			formPessoa = null;
		}		
	}
	
	function initEventListeners(){
		$formModalPessoa.find("#btnAdicionarPessoaCartao").click(function (){
			$("#modalCadastroPessoaCartao").modal("show");
		});
		$formModalPessoa.find("#btnAdicionarPessoaEndereco").click(function (){
			$("#modalCadastroPessoaEndereco").modal("show");
		});		
		$modalCadastroPessoa.find("#btnSubmitFormModalPessoa").click( function(){
			savePessoa();			
		});
	}
	this.OpenModalCadastroPessoa = function (pessoa){
		openModalCadastroPessoa(pessoa);
	}
	this.InitEventListeners = function (){
		initEventListeners();
	}
	this.SavePessoa = function (){
		savePessoa();
	}
}