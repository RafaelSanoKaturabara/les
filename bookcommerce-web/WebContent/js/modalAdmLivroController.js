function ModalAdmLivroController(constructorParam){
	var $modalCadastroLivro = $(constructorParam.modalCadastroLivro);
	var $formLivro = $(constructorParam.formLivro);
	// Select Grupo Precificação
	var $modalFormLivroSelectGrupoPrecificacao = $formLivro.find("#modalFormLivroSelectGrupoPrecificacao");
	var $modalFormLivroSelectGrupoPrecificacaoOpt = $formLivro.find(".modalFormLivroSelectGrupoPrecificacaoOpt").clone().remove();
	// Autor
	var $modalFormSelectLivroAutor = $formLivro.find("#modalFormSelectLivroAutor");
	var $modalFormSelectLivroAutorOpt = $formLivro.find(".modalFormSelectLivroAutorOpt").clone().remove();
	// Ano
	var $modalFormSelectLivroAno = $formLivro.find("#modalFormSelectLivroAno");
	var $modalFormSelectLivroAnoOpt = $formLivro.find(".modalFormSelectLivroAnoOpt").clone().remove();
	// editora
	var $modalFormSelectLivroEditora = $formLivro.find("#modalFormSelectLivroEditora");
	var $modalFormSelectLivroEditoraOpt = $formLivro.find(".modalFormSelectLivroEditoraOpt").clone().remove();
	// Categoria
	var $modalFormSelectLivroCategoria = $formLivro.find("#modalFormSelectLivroCategoria");
	var $modalFormSelectLivroCategoriaOpt = $formLivro.find(".modalFormSelectLivroCategoriaOpt").clone().remove();
	var $modalCadastroLivroImagem = $(constructorParam.modalCadastroLivroImagem);
	var $formLivroImagem = $modalCadastroLivroImagem.find("#formLivroImagem");
	// modal imagens
	var $modalCadastroLivroImagem = $(constructorParam.modalCadastroLivroImagem);
	// carousel imagens	
	var $carouselModalLivro = $formLivro.find("#carouselModalLivro");
	var $carouselModalLivroContainer = $formLivro.find("#carouselModalLivroContainer").clone().remove();
	var $carouselModalLivroLine = $formLivro.find("#carouselModalLivroLine").clone().remove();
	var $carouselModalLivroFim = $formLivro.find("#carouselModalLivroFim").clone().remove();	
	
	function initListeners(){
		
		$(document).on("click", ".tableAdmLivrosLine", function (){
			var livro = $(this).data("livro");
			fillModalLivroForm(livro);
			getLivroImagens(livro);
			$modalCadastroLivro.modal("show");
		});
		$(document).on("click", "#tableAdmLivrosAtivo", function (e){
			e.stopPropagation();
			//fillModalLivroForm($(this).data("livro"));
		});
		$modalCadastroLivro.find("#btnAdicGrupPrec").click(function(){
			console.log("bnt Adicionar Grupo de Precificação");
		});
		$modalCadastroLivro.find("#btnExluirLivro").click(function(){
			excluirLivro(readLivroForm());
		});
		$modalCadastroLivro.find("#btnAdicAutor").click(function(){
			console.log("bnt Adicionar Autor");
		});
		$modalCadastroLivro.find("#btnAdicAno").click(function(){
			console.log("bnt Adicionar Ano");
		});
		$modalCadastroLivro.find("#btnAdicEditora").click(function(){
			console.log("bnt Adicionar Editora");
		});
		$modalCadastroLivro.find("#btnAdicCategoria").click(function(){
			console.log("bnt Adicionar Categoria");
		});
		$modalCadastroLivro.find("#btnSubmitModalFormLivro").click(function (){
			salvarLivro(readLivroForm());
		});		
		$modalCadastroLivro.find("#btnResetModalFormLivro").click(function (){
			cleanModal();
		});	
		$("#btnOpenModalCadastrarLivro").click(function (){
			cleanModal();
			$modalCadastroLivro.modal("show");
		});
		$modalCadastroLivroImagem.find("#btnSubmitModalLivroImagem").click(function (){			
			
			salvarLivroImagem($formLivro.find("#modalIdLivro").text());
			$modalCadastroLivroImagem.modal("hide");
		});
		$modalCadastroLivroImagem.find("#formLivroImagemImagem").on("change", function (){
			var reader = new FileReader();
			reader.readAsDataURL(document.querySelector("input[type=file]").files[0]);
			console.log(reader.result);
			reader.onloadend = function (){
				$("#formLivroImagemCanvas").prop("src", reader.result);
				$formLivroImagem.find("#formLivroImagemImagem").data("image", reader.result);
			}
		});
		
	}
	
	function readLivroForm(){
		var livro = {
				id: $formLivro.find("#modalIdLivro").text(),
				codigoBarras: $formLivro.find("#modalFormLivroCodBarras").val(),
				titulo: $formLivro.find("#modalFormLivroTitulo").val(),
				preco: $formLivro.find("#modalFormLivroPreco").val().replace(".","").replace(",","."),
				edicao: $formLivro.find("#modalFormLivroEdicao").val(),
				isbn: $formLivro.find("#modalFormLivroIsbn").val(),
				qtdPaginas: $formLivro.find("#modalFormLivroNumeroPagina").val(),
				sinopse: $formLivro.find("#modalFormLivroSinopse").val(),
				altura: $formLivro.find("#modalFormLivroAltura").val().replace(",","."),
				largura: $formLivro.find("#modalFormLivroLargura").val().replace(",","."),
				peso: $formLivro.find("#modalFormLivroPeso").val().replace(",","."),
				profundidade: $formLivro.find("#modalFormLivroProfundidade").val().replace(",","."),
				ativo: $formLivro.find("#modalFormLivroAtivo").prop('checked'),
				idgrupoPrecificacao: $formLivro.find("#modalFormLivroSelectGrupoPrecificacao").val(),
				idAutorArray: $formLivro.find("#modalFormSelectLivroAutor").val(),
				idCategoriaArray: $formLivro.find("#modalFormSelectLivroCategoria").val(),
				idAno: $formLivro.find("#modalFormSelectLivroAno").val(),
				idEditora: $formLivro.find("#modalFormSelectLivroEditora").val(),
		}
		return livro;
	}
	
	function fillModalLivroForm(livro){
		$formLivro.find("#modalIdLivro").text(livro.id);
		$formLivro.find("#modalFormLivroCodBarras").val(livro.codBarras);
		$formLivro.find("#modalFormLivroTitulo").val(livro.titulo);
		$formLivro.find("#modalFormLivroPreco").val(FloatFormat(livro.preco, 2));		
		$formLivro.find("#modalFormLivroEdicao").val(livro.edicao);
		$formLivro.find("#modalFormLivroIsbn").val(livro.isbn);
		$formLivro.find("#modalFormLivroNumeroPagina").val(livro.qtdPaginas);
		$formLivro.find("#modalFormLivroSinopse").val(livro.sinopse);
		$formLivro.find("#modalFormLivroAltura").val(FloatFormat(livro.altura, 2));
		$formLivro.find("#modalFormLivroLargura").val(FloatFormat(livro.largura, 2));
		$formLivro.find("#modalFormLivroPeso").val(FloatFormat(livro.peso, 2));
		$formLivro.find("#modalFormLivroProfundidade").val(FloatFormat(livro.profundidade, 2));
		if(livro.ativo)
			$formLivro.find("#modalFormLivroAtivo").bootstrapToggle("on");
		else 
			$formLivro.find("#modalFormLivroAtivo").bootstrapToggle("off");
		$formLivro.find("#modalFormLivroSelectGrupoPrecificacao").val(livro.grupoPrecificacao.id);
		fillChosenMultipleSelect($formLivro.find("#modalFormSelectLivroAutor"), livro.autorList);
		fillChosenMultipleSelect($formLivro.find("#modalFormSelectLivroCategoria"), livro.categoriaList);
		$formLivro.find("#modalFormSelectLivroAno").val(livro.ano.id);
		$formLivro.find("#modalFormSelectLivroEditora").val(livro.editora.id);
	}
	
	function cleanModal(){
		$formLivro[0].reset();		
		fillChosenMultipleSelect($formLivro.find("#modalFormSelectLivroAutor"), []);
		fillChosenMultipleSelect($formLivro.find("#modalFormSelectLivroCategoria"), []);
		$formLivro.find("#modalIdLivro").text("");
		$formLivro.find("#modalFormLivroAtivo").bootstrapToggle("on");
		$carouselModalLivro.hide(); 
	}
	
	function fillChosenMultipleSelect($chosenMultipleSelect, objList){
		destroyChosenMultiselect($chosenMultipleSelect);			
		var idArray = [];
		if(objList != null && objList.length > 0){
			$.each(objList, function(i, objList){
				idArray.push(objList.id);
			});
		}
		$chosenMultipleSelect.val(idArray);
		putChosenMultiselect($chosenMultipleSelect);
	}
	
	function fillGrupoPrecificacaoOpt(grupoPrecificacao, $modalFormLivroSelectGrupoPrecificacaoOpt){
		$modalFormLivroSelectGrupoPrecificacaoOpt.val(grupoPrecificacao.id);
		$modalFormLivroSelectGrupoPrecificacaoOpt.html(grupoPrecificacao.nome);
	}
	
	function fillAutorOpt(autor, $modalFormSelectLivroAutorOpt){
		$modalFormSelectLivroAutorOpt.val(autor.id);
		$modalFormSelectLivroAutorOpt.html(autor.nome);
	}
	
	function fillAnoOpt(Ano, $modalFormSelectLivroAnoOptClone){
		$modalFormSelectLivroAnoOptClone.val(Ano.id);
		$modalFormSelectLivroAnoOptClone.html(Ano.nome);
	}
	
	function fillEditoraOpt(editora, $modalFormSelectLivroEditoraOptClone){
		$modalFormSelectLivroEditoraOptClone.val(editora.id);
		$modalFormSelectLivroEditoraOptClone.html(editora.nome);
	}
	
	function fillCategoriaOpt(categoria, $modalFormSelectLivroCategoriaOptClone){
		$modalFormSelectLivroCategoriaOptClone.val(categoria.id);
		$modalFormSelectLivroCategoriaOptClone.html(categoria.nome);
	}
	
	function fillAutor(autorList){
		//console.log(autorList);
		destroyChosenMultiselect($modalFormSelectLivroAutor);
		$modalFormSelectLivroAutor.empty();
		$.each(autorList, function(i, autor){
			var $modalFormSelectLivroAutorOptClone = $modalFormSelectLivroAutorOpt.clone();
			fillAutorOpt.call(this, autor, $modalFormSelectLivroAutorOptClone);
			$modalFormSelectLivroAutor.append($modalFormSelectLivroAutorOptClone);
		});
		putChosenMultiselect($modalFormSelectLivroAutor);
	}
	
	function fillAno(AnoList){
		//console.log(AnoList);
		$modalFormSelectLivroAno.empty();
		$.each(AnoList, function(i, Ano){
			var $modalFormSelectLivroAnoOptClone = $modalFormSelectLivroAnoOpt.clone();
			fillAnoOpt.call(this, Ano, $modalFormSelectLivroAnoOptClone);
			$modalFormSelectLivroAno.append($modalFormSelectLivroAnoOptClone);
		});
	}
	
	function fillEditora(editoraList){
		//console.log(editoraList);
		$modalFormSelectLivroEditora.empty();
		$.each(editoraList, function(i, editora){
			var $modalFormSelectLivroEditoraOptClone = $modalFormSelectLivroEditoraOpt.clone();
			fillAnoOpt.call(this, editora, $modalFormSelectLivroEditoraOptClone);
			$modalFormSelectLivroEditora.append($modalFormSelectLivroEditoraOptClone);
		});
	}
	
	function fillGrupoPrecificacao(grupoPrecificacaoList){
		//console.log(grupoPrecificacaoList);
		$modalFormLivroSelectGrupoPrecificacao.empty();
		$.each(grupoPrecificacaoList, function(i, grupoPrecificacao){
			var $SelectGrupoPrecificacaoOptClone = $modalFormLivroSelectGrupoPrecificacaoOpt.clone();
			fillGrupoPrecificacaoOpt.call(this, grupoPrecificacao, $SelectGrupoPrecificacaoOptClone);
			$modalFormLivroSelectGrupoPrecificacao.append($SelectGrupoPrecificacaoOptClone);
		});
	}	
	
	function fillModalCaouselLivroImagensLine(livroImagem, $carouselModalLivroLine, indice){
		if(indice == 0)
			$carouselModalLivroLine.addClass("active");
		$carouselModalLivroLine.find(".carouselModalLivroImagem").attr("src", livroImagem.imagem);
	}
	
	function putChosenMultiselect($selector){
		$selector.chosen({
            no_results_text: "Oops, Menu não encontrado!",
            placeholder_text_multiple: "Digite aqui para encontrar",
            width: "100%",
            display_selected_options: false
        });
	}
	
	function destroyChosenMultiselect($selector){
		$selector.chosen("destroy");
	}
	
	function fillCategoriaList(categoriaList){
		//console.log(categoriaList);
		destroyChosenMultiselect($modalFormSelectLivroCategoria);		
		$modalFormSelectLivroCategoria.empty();
		$.each(categoriaList, function(i, categoria){
			var $modalFormSelectLivroCategoriaOptClone = $modalFormSelectLivroCategoriaOpt.clone();
			fillAnoOpt.call(this, categoria, $modalFormSelectLivroCategoriaOptClone);
			$modalFormSelectLivroCategoria.append($modalFormSelectLivroCategoriaOptClone);
		});
		putChosenMultiselect($modalFormSelectLivroCategoria);		
	}
	
	function fillModalCaouselLivroImagens(livroImagemList){
		$carouselModalLivro.empty();
		$carouselModalLivroContainer.empty();
		if(livroImagemList != null && livroImagemList.length > 0)
			$carouselModalLivro.show();
		else{
			$carouselModalLivro.hide();
			//return;
		}
			
		$.each(livroImagemList, function(i, livroImagem){
			var $carouselModalLivroLineClone = $carouselModalLivroLine.clone();
			fillModalCaouselLivroImagensLine.call(this, livroImagem, $carouselModalLivroLineClone, i);
			$carouselModalLivroContainer.append($carouselModalLivroLineClone);
		});
		$carouselModalLivroContainer.append($carouselModalLivroFim);
		$carouselModalLivro.append($carouselModalLivroContainer);
	}
	
	function getAndFillModalLivroGrupoPrecificacao(){
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/ListarGrupoPrecificacaoWS",
            data: "operacao=CONSULTAR",
            aysnc: false,
            //contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro!",
                    text: "Erro ao listar os grupos de precificação!"
                });
                console.log(ex);
            },
            success: function (respostaServlet) {
            	var grupoPrecificacaoList = respostaServlet.entidades;
            	fillGrupoPrecificacao(grupoPrecificacaoList);
            }
		});
	}
	
	function getAndFillModalLivroAutor(){
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/ListarAutorWS",
            data: "operacao=CONSULTAR",
            aysnc: false,
            //contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro!",
                    text: "Erro ao criar a lista de autores!"
                });
                console.log(ex);
            },
            success: function (respostaServlet) {
            	var autorList = respostaServlet.entidades;
            	fillAutor(autorList)
            }
		});
	}
	
	function getAndFillModalLivroCategoria(){
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/ListarCategoriaWS",
            data: "operacao=CONSULTAR",
            aysnc: false,
            //contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro!",
                    text: "Erro ao criar a lista de categorias!"
                });
                console.log(ex);
            },
            success: function (respostaServlet) {
            	var categoriaList = respostaServlet.entidades;
            	fillCategoriaList(categoriaList);
            }
		});
	}
	
	function getAndFillModalLivroAno(){
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/ListarAnoWS",
            data: "operacao=CONSULTAR",
            aysnc: false,
            //contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro!",
                    text: "Erro ao criar a lista de anos!"
                });
                console.log(ex);
            },
            success: function (respostaServlet) {
            	var anoList = respostaServlet.entidades;
            	fillAno(anoList);
            }
		});
	}
	
	function getAndFillModalLivroEditora(){
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/ListaEditoraWS",
            data: "operacao=CONSULTAR",
            aysnc: false,
            //contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro!",
                    text: "Erro ao criar a lista de editoras!"
                });
                console.log(ex);
            },
            success: function (respostaServlet) {
            	var editoraList = respostaServlet.entidades;
            	fillEditora(editoraList);
            }
		});
	}
	
	function getLivroImagens(livro){
		var livroImagem = {
			idLivro: livro.id,
			operacao: "CONSULTAR" 
		}
		if(livro.id == null || livro.id == 0)
			return	
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/LivroImagemWS",
            data: livroImagem,
            //contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro!",
                    text: "Erro ao obter as imagens do produto!"
                });
                console.log(ex);
            },
            success: function (respostaServlet) {
            	var livroImagenList = respostaServlet.resultado.entidades;
            	fillModalCaouselLivroImagens(livroImagenList);            	
            }
		});
	}
	
	function excluirLivro(livro){
		if(livro.id == null || livro.id == 0)
			return			
		livro.operacao = "EXCLUIR";
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/LivroWS",
            data: livro,
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro desconhecido",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
            		$.gritter.add({
                        title: respostaServlet.mensagemResposta,
                        text: "Livro excluído com sucesso",
                        time: 1500
                    });
            		cleanModal();
            	} else if(respostaServlet.codigoResposta == 250){ //erro!
            		$.gritter.add({
                        title: respostaServlet.mensagemResposta,
                        text: respostaServlet.resultado.msg,
                        time: 1500
                    });
            	} else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }
		});
	}
	
	function salvarLivroImagem(idLivro){
		var reader = new FileReader();
		reader.readAsDataURL(document.querySelector("input[type=file]").files[0]);
		var livroImagem = {
				operacao: "SALVAR",
				idLivro: idLivro,
				nome: $formLivroImagem.find("#formLivroImagemNome").val(),
				imagem: $formLivroImagem.find("#formLivroImagemImagem").data("image")		
		}
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/LivroImagemWS",
            data: livroImagem,
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro desconhecido",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
            		$formLivro.find("#modalIdLivro").text(respostaServlet.resultado.entidades[0].id);
            		$.gritter.add({
                        title: respostaServlet.mensagemResposta,
                        text: "Imagem inserida!",
                        time: 1500
                    });
            	} else if(respostaServlet.codigoResposta == 250){ //erro!
            		$.gritter.add({
                        title: respostaServlet.mensagemResposta,
                        text: respostaServlet.resultado.msg,
                        time: 1500
                    });
            	} else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }, 
            complete: function(){
            	$("#btnSubmitModalFormLivro").prop( "disabled", false );
            	$modalCadastroLivroImagem.find("#formLivroImagemCanvas").prop("src","");
            	$modalCadastroLivroImagem.find("#formLivroImagemNome").val("");
            	$modalCadastroLivroImagem.find("#formLivroImagemImagem").val("");
            	$modalCadastroLivro.modal("hide");
            	$modalCadastroLivroImagem.modal("hide");
            }
		});
	}
	
	function salvarLivro(livro){
		var mensagem;
    	$("#btnSubmitModalFormLivro").prop( "disabled", true );
		if(livro.id != null && livro.id > 0){
			livro.operacao = "ALTERAR";
			mensagem = "alterado";
		}
		else {
			livro.operacao = "SALVAR";
			mensagem = "cadastrado";
		}
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/LivroWS",
            data: livro,
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro desconhecido",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
            		$formLivro.find("#modalIdLivro").text(respostaServlet.resultado.entidades[0].id);
            		$.gritter.add({
                        title: respostaServlet.mensagemResposta,
                        text: "Livro " + mensagem + " com sucesso",
                        time: 1500
                    });
            	} else if(respostaServlet.codigoResposta == 250){ //erro!            		
            		swal({
            			  type: 'error',
            			  title: "Erro ao " + mensagem + " o livro",
            			  html: respostaServlet.resultado.msg.replace("\n", "<br/>"),
            			});
            	} else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }, 
            complete: function(){
            	$("#btnSubmitModalFormLivro").prop( "disabled", false );
            }
		});
	}
	
	function fillModalLivrosSelectOptions(){
		getAndFillModalLivroGrupoPrecificacao();
		getAndFillModalLivroAutor();
		getAndFillModalLivroCategoria();
		getAndFillModalLivroAno();
		getAndFillModalLivroEditora();		
	}	
	
	this.FillModalLivro = function(livro){
		fillModalLivro(livro);
	}
	this.FillModalLivrosSelectOptions = function(){
		fillModalLivrosSelectOptions();
	}
	this.InitListeners = function (){
		initListeners();
	}
}