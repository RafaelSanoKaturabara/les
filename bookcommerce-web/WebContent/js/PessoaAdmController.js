function PessoaAdmController(constructorParam){
	var $tableAdmPessoas = $(constructorParam.tableAdmPessoas);
	var $btnCadastroPessoa = $(constructorParam.btnCadastroPessoa);
	var $tableTableAdmPessoasContainer = $tableAdmPessoas.find("#tableTableAdmPessoasContainer");
	var $tableTableAdmPessoasLine =  $tableAdmPessoas.find(".tableTableAdmPessoasLine").clone().remove();
	
	function fillTableAdmPessoaLine(pessoa, $tableTableAdmPessoasLine){
		$tableTableAdmPessoasLine.data("pessoa", pessoa);
		$tableTableAdmPessoasLine.find(".tableTableAdmPessoasId").text(pessoa.id);
		$tableTableAdmPessoasLine.find(".tableTableAdmPessoasEmail").text(pessoa.email);
		$tableTableAdmPessoasLine.find(".tableTableAdmPessoasNome").text(pessoa.nome);
		$tableTableAdmPessoasLine.find(".tableTableAdmPessoasSobrenome").text(pessoa.sobrenome);
		$tableTableAdmPessoasLine.find(".tableTableAdmPessoasAniversario").text(pessoa.dataNascimento);
		$tableTableAdmPessoasLine.find(".tableTableAdmPessoasCpf").text(pessoa.cpf);
	}
	
	function fillTableAdmPessoaContainer(pessoaList){
		if ($tableAdmPessoas.hasClass('initialized')) {
			$tableAdmPessoas.dataTable().fnDestroy();
        }
		
		$tableTableAdmPessoasContainer.empty();
		$.each(pessoaList, function(i, pessoa){
			var $tableTableAdmPessoasLineClone = $tableTableAdmPessoasLine.clone();
			fillTableAdmPessoaLine.call(this, pessoa, $tableTableAdmPessoasLineClone);
			$tableTableAdmPessoasContainer.append($tableTableAdmPessoasLineClone);
		});	
		
		if ($tableAdmPessoas.is('table')) {
            var recordsTotal = $tableAdmPessoas.addClass('initialized').DataTable({
                "oLanguage": TraducaoDatatable
            }).page.info().recordsTotal;
        }
	}
	
	function getAndFillTableAdmPessoas(){
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/CadastroPessoaWS?operacao=CONSULTAR",
            dataType: "json",
            //data: JSON.stringify(objPost),
            contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
            		var pessoaList = respostaServlet.resultado.entidades;
            		fillTableAdmPessoaContainer(pessoaList);
            	} else if(respostaServlet.codigoResposta == 250){ //erro!            		
            		swal({
            			  type: 'error',
            			  title: "Erro ao listar os clientes",
            			  html: respostaServlet.resultado.msg.replace("\n", "<br/>")
            			});
            	} else if(respostaServlet.codigoResposta == 275){ //erro!            		
            		window.location.href = "/bookcommerce-web/login.jsp";
            	} else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }, 
            complete: function(){
            	//$("#btnSubmitModalFormLivro").prop( "disabled", false );
            }
		});
	}
	
	function addEventAndListeners(){
		
	}
	
	this.GetAndFillTableAdmPessoas = function(){
		getAndFillTableAdmPessoas();
	}
	this.AddEventAndListeners = function(){
		addEventAndListeners();
	}
}