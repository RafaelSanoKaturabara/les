function MinhasComprasController(selectors){
	var $minhasComprasContainer = $(selectors.minhasComprasContainer);
	var $minhasComprasLine = $minhasComprasContainer.find(".minhasComprasLine").clone();
	$minhasComprasContainer.find(".minhasComprasLine").remove();
	
	
	function fillMinhasComprasLine(minhasCompras, $minhasComprasLine){
		$minhasComprasLine.data("minhasCompras", minhasCompras);
		$minhasComprasLine.find(".minhasComprasId").text(minhasCompras.id);
		$minhasComprasLine.find(".minhasComprasEnderecoApelido").text(minhasCompras.endereco.apelido);
		// status
		var idStatus = minhasCompras.carrinhoStatus.id;
		var statusClass = "badge badge-";
		if(idStatus === 3 || idStatus === 4 || idStatus === 5)
			statusClass += "success";
		else if (idStatus === 2 || idStatus === 6)
			statusClass += "warning";
		else if( idStatus === 1 )
			statusClass += "danger";
		else 
			statusClass += "primary";
		$minhasComprasLine.find(".minhasComprasStatus").addClass(statusClass);
		$minhasComprasLine.find(".minhasComprasStatus").text(minhasCompras.carrinhoStatus.descricao);
		
		if(idStatus === 5) // Status entregue?
			$minhasComprasLine.find(".btnSolicitarTroca").show();
		else 
			$minhasComprasLine.find(".btnSolicitarTroca").remove();
		// Botão confirmar recebimento
		if(idStatus === 4)
			$minhasComprasLine.find(".btnConfirmarRecebimento").show();
		else 
			$minhasComprasLine.find(".btnConfirmarRecebimento").remove();
		
		$minhasComprasLine.find(".minhasComprasDataCompra").text(formatDate(new Date(minhasCompras.dataUltimaAlteracao)));
		
		//cupom
		if(minhasCompras.cupomDevolucao != null && minhasCompras.cupomDevolucao.id != null
			&& minhasCompras.cupomDevolucao.id > 0){	// tem cupom gerado?
			$minhasComprasLine.find(".minhasComprasCupomVisibilidade").show();
			$minhasComprasLine.find(".minhasComprasCupomCodigo").text(minhasCompras.cupomDevolucao.codigo);
			if(minhasCompras.cupomDevolucao.status)
				$minhasComprasLine.find(".minhasComprasCupomCodigo").addClass("badge badge-pill badge-success");
			else
				$minhasComprasLine.find(".minhasComprasCupomCodigo").addClass("badge badge-pill badge-secondary")
		} else {
			$minhasComprasLine.find(".minhasComprasCupomVisibilidade").remove();
		}
	}
	
	function fillMinhasComprasContainer(minhasComprasList){
		$minhasComprasContainer.empty();
		$.each(minhasComprasList, function(i, minhasCompras) {
			var $minhasComprasLineClone = $minhasComprasLine.clone();
			fillMinhasComprasLine.call(this, minhasCompras, $minhasComprasLineClone);
			$minhasComprasContainer.append($minhasComprasLineClone);
		});
	}
	
	function getAndFillMinhasCompras() {
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/MinhasComprasWS?operacao=CONSULTAR",
            dataType: "json",
            //async: false,
            //data: JSON.stringify(pessoaCartao),
            contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
            		var minhasComprasList = respostaServlet.resultado.entidades;         
            		fillMinhasComprasContainer(minhasComprasList);
            	} else if(respostaServlet.codigoResposta == 250){ //erro!            		
            		swal({
            			  type: 'error',
            			  title: "Consultar as compras",
            			  html: respostaServlet.resultado.msg.replace("\n", "<br/>")
            			});
            	} else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }, 
            complete: function(){
            	//$("#btnSubmitModalFormLivro").prop( "disabled", false );
            }
		});
	}
	
	function solicitarTroca(minhasCompras){
		var minhaCompraSend = {
			id: minhasCompras.id,
			precoFrete: minhasCompras.precoFrete,
			endereco: {
				id: minhasCompras.endereco.id		
			},
			carrinhoStatus: {					
				id: 6
			}
		}
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/MinhasComprasWS?operacao=ALTERAR",
            dataType: "json",
            //async: false,
            data: JSON.stringify(minhaCompraSend),
            contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
            		var minhasComprasList = respostaServlet.resultado.entidades;         
            		getAndFillMinhasCompras();
            	} else if(respostaServlet.codigoResposta == 250){ //erro!            		
            		swal({
            			  type: 'error',
            			  title: "Erro ao solicitar troca",
            			  html: respostaServlet.resultado.msg.replace("\n", "<br/>")
            			});
            	} else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }, 
            complete: function(){
            	//$("#btnSubmitModalFormLivro").prop( "disabled", false );
            }
		});
	}
	
	function confirmarRecebimento(minhasCompras){
		var minhaCompraSend = {
			id: minhasCompras.id,
			precoFrete: minhasCompras.precoFrete,
			endereco: {
				id: minhasCompras.endereco.id		
			},
			carrinhoStatus: {					
				id: 5
			}
		}
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/MinhasComprasWS?operacao=ALTERAR",
            dataType: "json",
            //async: false,
            data: JSON.stringify(minhaCompraSend),
            contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
            		var minhasComprasList = respostaServlet.resultado.entidades;         
            		getAndFillMinhasCompras();
            	} else if(respostaServlet.codigoResposta == 250){ //erro!            		
            		swal({
            			  type: 'error',
            			  title: "Erro ao confirmar o recebimento",
            			  html: respostaServlet.resultado.msg.replace("\n", "<br/>")
            			});
            	} else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }, 
            complete: function(){
            	//$("#btnSubmitModalFormLivro").prop( "disabled", false );
            }
		});
	}
	
	this.SolicitarTroca = function(minhasCompras) {
		solicitarTroca(minhasCompras);
	}
	this.PrintMinhasCompras = function() {
		getAndFillMinhasCompras();
	}
	this.ConfirmarRecebimento = function(minhasCompras) {
		confirmarRecebimento(minhasCompras);
	}
}