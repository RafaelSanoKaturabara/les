function PerfilController(constructorParam){
	var $formPessoa = $(constructorParam.formPessoa);
	
	var $formlPessoaEnderecoContainer = $formPessoa.find("#formlPessoaEndereco");
	var $formPessoaEnderecoOpt = $formPessoa.find(".formPessoaEnderecoOpt").clone();
	
	var $formlPessoaCartaoContainer = $formPessoa.find("#formPessoaCartao");
	var $formPessoaCartaoOpt = $formPessoa.find(".formPessoaCartaoOpt").clone();
	
	function fillPerfilForm(pessoaLogada){
		$formPessoa.find("#formPessoaId").text(pessoaLogada.id);
		$formPessoa.find("#formModalPessoaEmail").val(pessoaLogada.email);
		$formPessoa.find("#formModalPessoaCpf").val(pessoaLogada.cpf);
		$formPessoa.find("#formPessoaNome").val(pessoaLogada.nome);
		$formPessoa.find("#formPessoaSobrenome").val(pessoaLogada.sobrenome);
		$formPessoa.find("#formPessoaGenero").val(pessoaLogada.genero.id);
		$formPessoa.find("#formPessoaTelefone").val(pessoaLogada.telefone.numeroCompleto);
		$formPessoa.find("#formPessoaTelefoneTipo").val(pessoaLogada.telefone.telefoneTipo.id);
		var dataNascimento = new Date(pessoaLogada.dataNascimento);
		$formPessoa.find("#formPessoaDataNascimento").val(dataNascimento.format('dd/mm/yyyy'));
		$formPessoa.find("#formPessoaObservacao").val(pessoaLogada.observacao);
	}	
	
	function readFormPessoaLogin(){
		var dataNascimentoBr = $formPessoa.find("#formPessoaDataNascimento").val();
		var dataNascimentoUs =  dataNascimentoBr.substring(6,10) + "-" + dataNascimentoBr.substring(3,5) + "-" + dataNascimentoBr.substring(0,2) + " 00:00";
		var pessoa = {
			id: $formPessoa.find("#formPessoaId").text(),
			email: $formPessoa.find("#formModalPessoaEmail").val(),
			//senha: $formPessoa.find("#formModalPessoaSenha").val(),
			cpf: $formPessoa.find("#formModalPessoaCpf").val(),
			//repitaSenha: $formPessoa.find("#formModalPessoaRepitaSenha").val(),
			nome: $formPessoa.find("#formPessoaNome").val(),
			sobrenome: $formPessoa.find("#formPessoaSobrenome").val(),
			ativo: true,
			genero: {
				id: $formPessoa.find("#formPessoaGenero").val()
				},
			telefone: {
				numeroCompleto: $formPessoa.find("#formPessoaTelefone").val(),
				telefoneTipo:{
					id: $formPessoa.find("#formPessoaTelefoneTipo").val()
				}
			},
			dataNascimento: new Date(dataNascimentoUs),
			observacao: $formPessoa.find("#formPessoaObservacao").val(),
			enderecoPadrao: {
				id: $formPessoa.find("#formlPessoaEndereco").val()
			},			
			cartaoCreditoPadrao: {
				id: $formPessoa.find("#formPessoaCartao").val()
			}
		}	
		return pessoa;
	}

	function fillPessoaEnderecoLine(pessoaEndereco, $formPessoaEnderecoOpt){
		$formPessoaEnderecoOpt.data("pessoaEndereco", pessoaEndereco);
		$formPessoaEnderecoOpt.val(pessoaEndereco.id);
		$formPessoaEnderecoOpt.text(pessoaEndereco.apelido);
	}
	
	function fillPessoaCartaoLine(pessoaCartao, $formPessoaCartaoOpt){
		$formPessoaCartaoOpt.data("pessoaCartao", pessoaCartao);
		$formPessoaCartaoOpt.val(pessoaCartao.id);
		$formPessoaCartaoOpt.text(pessoaCartao.apelido);
	}
	
	function fillPessoaEnderecoContainer(pessoaEnderecoList){
		$formlPessoaEnderecoContainer.empty();
		$.each(pessoaEnderecoList, function(i, pessoaEndereco){
			var $formPessoaEnderecoOptClone = $formPessoaEnderecoOpt.clone();
			fillPessoaEnderecoLine.call(this, pessoaEndereco, $formPessoaEnderecoOptClone);
			$formlPessoaEnderecoContainer.append($formPessoaEnderecoOptClone);
		});
	}

	function fillPessoaCartaoContainer(pessoaCartaoList){
		$formlPessoaCartaoContainer.empty();
		$.each(pessoaCartaoList, function(i, pessoaCartao){
			var $formPessoaCartaoOptClone = $formPessoaCartaoOpt.clone();
			fillPessoaCartaoLine.call(this, pessoaCartao, $formPessoaCartaoOptClone);
			$formlPessoaCartaoContainer.append($formPessoaCartaoOptClone);
		});
	}
	
	function getEnderecoPessoa(){	
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/PessoaEnderecoWS?operacao=CONSULTAR",
            dataType: "json",
            //data: JSON.stringify(pessoaEndereco),
            async: false,
            contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
            		var pessoaEnderecoList = respostaServlet.resultado.entidades;
            		fillPessoaEnderecoContainer(pessoaEnderecoList);
            	} else if(respostaServlet.codigoResposta == 250){ //erro!            		
            		swal({
            			  type: 'error',
            			  title: "Erro ao cadastrar",
            			  html: respostaServlet.resultado.msg.replace("\n", "<br/>")
            			});
            	} else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }, 
            complete: function(){
            	//$("#btnSubmitModalFormLivro").prop( "disabled", false );
            }
		});
	}
	
		
	
	function getPessoaCartao(){
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/PessoaCartaoWS?operacao=CONSULTAR",
            dataType: "json",
            async: false,
            //data: JSON.stringify(pessoaCartao),
            contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
            		var pessoaCartaoList = respostaServlet.resultado.entidades;            		
            		fillPessoaCartaoContainer(pessoaCartaoList);
            	} else if(respostaServlet.codigoResposta == 250){ //erro!            		
            		swal({
            			  type: 'error',
            			  title: "Erro ao cadastrar",
            			  html: respostaServlet.resultado.msg.replace("\n", "<br/>")
            			});
            	} else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }, 
            complete: function(){
            	//$("#btnSubmitModalFormLivro").prop( "disabled", false );
            }
		});
	}
	
	function updatePerfil(){
		var pessoa = readFormPessoaLogin();
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/CadastroWS?operacao=ALTERAR",
            dataType: "json",
            async: false,
            data: JSON.stringify(pessoa),
            contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
            		var pessoaCartaoList = respostaServlet.resultado.entidades;            		
            		fillPessoaCartaoContainer(pessoaCartaoList);
            		$.gritter.add({
                        title: "Sucesso",
                        text: "Perfil alterado com sucesso!",
                        time: 1500
                    });
            	} else if(respostaServlet.codigoResposta == 250){ //erro!            		
            		swal({
            			  type: 'error',
            			  title: "Erro ao alterar",
            			  html: respostaServlet.resultado.msg.replace("\n", "<br/>")
            			});
            	} else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }, 
            complete: function(){
            	//$("#btnSubmitModalFormLivro").prop( "disabled", false );
            }
		});
	}
	
	function initEventListeners(){
		$formPessoa.find("#btnAtualizarPerfil").click(function(){
			updatePerfil();
			getEnderecoPessoa();
			getPessoaCartao();
		});
		$formPessoa.find("#btnAdicionarPessoaCartao").click(function(){
			$("#modalCadastroPessoaCartao").modal("show");
		});
		$formPessoa.find("#btnAdicionarPessoaEndereco").click(function(){
			$("#modalCadastroPessoaEndereco").modal("show");
		});
		$('.modal').on('hidden.bs.modal', function (e) {
			getEnderecoPessoa();
			getPessoaCartao();
		});
	}
	
	function GetAndFillPerfilForm(pessoaLogada){
		fillPerfilForm(pessoaLogada);
		getEnderecoPessoa();
		getPessoaCartao();
	}
	
	this.GetAndFillPerfilForm = function (pessoaLogada){
		GetAndFillPerfilForm(pessoaLogada);
	}
	this.InitEventListeners = function (){
		initEventListeners();
	}
}