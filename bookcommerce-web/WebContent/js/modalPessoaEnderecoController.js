var PessoaEndereco;
function ModalPessoaEnderecoController(constructorParam){
	var isPagamentoPage = window.location.pathname.toLowerCase() === "/bookcommerce-web/pagamento.jsp";
	var isLoginPage = constructorParam.isLoginPage;  
	
	var $modalCadastroPessoaEndereco = $(constructorParam.modalCadastroPessoaEndereco);
	var $modalCadastroPessoaEnderecoCidadeContainer = $modalCadastroPessoaEndereco.find("#modalCadastroPessoaEnderecoCidade");
	var $modalCadastroPessoaEnderecoCidadeOpt = $modalCadastroPessoaEndereco.find(".modalCadastroPessoaEnderecoCidadeOpt").clone();
	
	// de outra tela
	var $formModalPessoaEnderecoContainer;
	var $formModalPessoaEnderecoOpt;	
	if(!isPagamentoPage){
		$formModalPessoaEnderecoContainer = $(SelectorFormPessoa).find(SelectorFormPessoaEnderecoContainerSelector);
		$formModalPessoaEnderecoOpt = $(SelectorFormPessoa).find(SelectorFormPessoaEnderecoOptSelector).clone();
	}


	function fillModalPessoaEnderecoOpt(pessoaEndereco, $formModalPessoaEnderecoOpt){
		$formModalPessoaEnderecoOpt.val(pessoaEndereco.id);
		$formModalPessoaEnderecoOpt.text(pessoaEndereco.apelido);
	}
	
	function readPessoaEndereco(){
		PessoaEndereco = {
			idPessoa: $("#formPessoaId").text(),
			apelido: $modalCadastroPessoaEndereco.find("#modalCadastroPessoaEnderecoApelido").val(),
			logradouro: $modalCadastroPessoaEndereco.find("#modalCadastroPessoaEnderecoLogradouro").val(),
			numero: $modalCadastroPessoaEndereco.find("#modalCadastroPessoaEnderecoNumero").val(),
			cep: $modalCadastroPessoaEndereco.find("#modalCadastroPessoaEnderecoCep").val(),
			//complento: $modalCadastroPessoaEndereco.find("#").val(),
			//bairro: $modalCadastroPessoaEndereco.find("#").val(),
			cidade: {
				id:	$modalCadastroPessoaEndereco.find("#modalCadastroPessoaEnderecoCidade").val()
			},
			residenciaTipo: {
				id: $modalCadastroPessoaEndereco.find("#modalCadastroPessoaEnderecoTipoResidencia").val()
			},
			logradouroTipo:{
				id: $modalCadastroPessoaEndereco.find("#modalCadastroPessoaEnderecoTipoLogradouro").val()
			}
		}
	}
	
	function savePessoaEnderecoPaginaLogin(){
		readPessoaEndereco();
		
		$formModalPessoaEnderecoContainer.empty();
		var $formModalPessoaEnderecoOptClone = $formModalPessoaEnderecoOpt.clone();
		fillModalPessoaEnderecoOpt.call(this, PessoaEndereco, $formModalPessoaEnderecoOptClone);
		$formModalPessoaEnderecoContainer.append($formModalPessoaEnderecoOptClone);		
		$modalCadastroPessoaEndereco.modal("hide");
		$.gritter.add({
            title: "Ok",
            text: "Endereço cadastrado com sucesso!",
            time: 1500
        });
	}
	
	function fillCidadeLine(cidade, $modalCadastroPessoaEnderecoCidadeOpt) {
		$modalCadastroPessoaEnderecoCidadeOpt.val(cidade.id);
		$modalCadastroPessoaEnderecoCidadeOpt.text(cidade.nome);
	}
	
	function fillCidadesContainer(cidadeList){
		$modalCadastroPessoaEnderecoCidadeContainer.empty(); 
		$modalCadastroPessoaEnderecoCidadeContainer.append($modalCadastroPessoaEnderecoCidadeOpt.clone());
		$.each(cidadeList, function(i, cidade){
			var $modalCadastroPessoaEnderecoCidadeOptClone = $modalCadastroPessoaEnderecoCidadeOpt.clone();
			fillCidadeLine.call(this, cidade, $modalCadastroPessoaEnderecoCidadeOptClone);
			$modalCadastroPessoaEnderecoCidadeContainer.append($modalCadastroPessoaEnderecoCidadeOptClone);
		});
		 
	}
	
	function savePessoaEnderecoPaginaPerfil(){
		readPessoaEndereco();
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/PessoaEnderecoWS?operacao=SALVAR",
            dataType: "json",
            data: JSON.stringify(PessoaEndereco),
            contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
            		var PessoaEnderecoList = respostaServlet.resultado.entidades;            		
            		var $formModalPessoaEnderecoOptClone = $formModalPessoaEnderecoOpt.clone();
            		fillModalPessoaEnderecoOpt.call(this, PessoaEnderecoList[0], $formModalPessoaEnderecoOptClone);
            		$formModalPessoaEnderecoContainer.append($formModalPessoaEnderecoOptClone);		
            		$.gritter.add({
                        title: "Sucesso",
                        text: "Endereço salvo com sucesso",
                        time: 1500
                    });
            		$("#modalCadastroPessoaEndereco").modal("hide");
            	} else if(respostaServlet.codigoResposta == 250){ //erro!            		
            		$.gritter.add({
                        title: "Erro ao listar as cidades",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	} else if(respostaServlet.codigoResposta == 275){ //erro!            		
            		swal({
          			  type: 'error',
          			  title: respostaServlet.mensagemResposta,
          			  html: respostaServlet.resultado.msg.replace("\n", "<br/>")
          			});
            	} else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }, 
            complete: function(){
            	//$("#btnSubmitModalFormLivro").prop( "disabled", false );
            }
		});
	}
	
	function getAndFillCidades(idEstado){
		var cidade = {
			estado:{
				id: idEstado
			}
		}
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/CidadeWS?operacao=CONSULTAR",
            dataType: "json",
            data: JSON.stringify(cidade),
            contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
            		var cidadeList = respostaServlet.resultado.entidades;
            		fillCidadesContainer(cidadeList);
            	} else if(respostaServlet.codigoResposta == 250){ //erro!            		
            		$.gritter.add({
                        title: "Erro ao listar as cidades",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	} else if(respostaServlet.codigoResposta == 275){ //erro!            		
            		swal({
          			  type: 'error',
          			  title: respostaServlet.mensagemResposta,
          			  html: respostaServlet.resultado.msg.replace("\n", "<br/>")
          			});
            	} else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }, 
            complete: function(){
            	//$("#btnSubmitModalFormLivro").prop( "disabled", false );
            }
		});
	}
	
	function savePessoaEndereco(){
		if(isLoginPage){ // estamos na página de login
			savePessoaEnderecoPaginaLogin();
		} else {
			savePessoaEnderecoPaginaPerfil();
		}	
	}		
	
	function initEventListener(){
 		$modalCadastroPessoaEndereco.find("#btnSubmitModalCadastroPessoaEndereco").click(function(){
			savePessoaEndereco();
		});
 		$modalCadastroPessoaEndereco.find("#modalCadastroPessoaEnderecoEstado").on("change", function(){
 			getAndFillCidades($(this).val()); 			
 		})
	}
	
	this.InitEventListener = function (){
		initEventListener();
	}
}