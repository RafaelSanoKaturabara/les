function FormAdmLivroController(constructorParam){	
	var $tableAdmLivros = $(constructorParam.tableAdmLivros);
	var $tableAdmLivrosContainer = $tableAdmLivros.find("#tableAdmLivrosContainer");
	var $tableAdmLivrosLine = $tableAdmLivros.find("#tableAdmLivrosLine").clone().remove();
	
	function initListeners(){
		
	}
	
	function fillTableAdmLivrosLine(livro, $tableAdmLivrosLineClone){
		$tableAdmLivrosLineClone.data("livro", livro);
		$tableAdmLivrosLineClone.prop("id", "idLivro_" + livro.id);
		$tableAdmLivrosLineClone.addClass("tableAdmLivrosLine");
		$tableAdmLivrosLineClone.find(".tableAdmLivrosId").html(livro.id);
		$tableAdmLivrosLineClone.find(".tableAdmLivrosCodigoBarras").html(livro.codBarras);
		$tableAdmLivrosLineClone.find(".tableAdmLivrosTitulo").html(livro.titulo);
		$tableAdmLivrosLineClone.find(".tableAdmLivrosEdicao").html(livro.edicao);
		$tableAdmLivrosLineClone.find(".tableAdmLivrosGrupo").html(livro.grupoPrecificacao.nome);
		$tableAdmLivrosLineClone.find(".tableAdmLivrosEditora").html(livro.editora.nome);
		$tableAdmLivrosLineClone.find(".tableAdmLivrosAtivo").prop('checked', livro.ativo);
	}
	
	function fillTableAdmLivros(livroList){
		if ($tableAdmLivros.hasClass('initialized')) {
			$tableAdmLivros.dataTable().fnDestroy();
        }
		
		$tableAdmLivrosContainer.empty();
		$.each(livroList, function (i, livro){
			var $tableAdmLivrosLineClone = $tableAdmLivrosLine.clone();
			fillTableAdmLivrosLine.call(this, livro, $tableAdmLivrosLineClone);
			$tableAdmLivrosContainer.append($tableAdmLivrosLineClone);
		});
		
		if ($tableAdmLivros.is('table')) {
            var recordsTotal = $tableAdmLivros.addClass('initialized').DataTable({
                "oLanguage": TraducaoDatatable
            }).page.info().recordsTotal;
        }
	}
	
	function getAndFillTableAdmLivros(){
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/LivroWS",
            data: "operacao=CONSULTAR",
            error: function (ex) {
            	$.gritter.add({
                    title: "Houve algum erro não conhecido",
                    text: ex.statusText
                });
                console.log(ex);
            },
            success: function (respostaServlet) {
            	var livroList = respostaServlet.resultado.entidades;
            	fillTableAdmLivros(livroList);
            }
		});
	}
	
	this.FillTableAdmLivros = function() {
		getAndFillTableAdmLivros();
	}
	this.InitListeners = function(){
		initListeners();
	}
}