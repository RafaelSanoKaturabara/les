function AdmVendasController(selectors){
	var $tableAdmVendas = $(selectors.tableAdmVendas);
	var $tableAdmDevolucoesContainer = $tableAdmVendas.find("#tableAdmDevolucoesContainer");
	var $tableAdmDevolucoesLine = $tableAdmVendas.find(".tableAdmDevolucoesLine").clone();
	
	
	function fillAdmVendasLine(venda, $tableAdmDevolucoesLine) {
		$tableAdmDevolucoesLine.data("venda", venda);
		$tableAdmDevolucoesLine.find(".tableAdmDevolucoesOrdemCompra").text(venda.id);
		$tableAdmDevolucoesLine.find(".tableAdmDevolucoesNomeCliente").text(venda.pessoa.nome + " " + venda.pessoa.sobrenome);
		$tableAdmDevolucoesLine.find(".tableAdmDevolucoesData").text(formatDate(new Date(venda.dataUltimaAlteracao)));
		$tableAdmDevolucoesLine.find(".tableAdmDevolucoesStatus").text(venda.carrinhoStatus.descricao);
		
		var idStatus = venda.carrinhoStatus.id;
		var statusClass = "badge badge-";
		if(idStatus === 4 || idStatus === 5 || idStatus === 7 )
			statusClass += "success";
		else if (idStatus === 2 || idStatus === 3 || idStatus === 6)
			statusClass += "warning";
		else if( idStatus === 1 )
			statusClass += "danger";
		else 
			statusClass += "primary";
		$tableAdmDevolucoesLine.find(".tableAdmDevolucoesStatus").addClass(statusClass);
		
		if(idStatus === 3) // Status entregue?
			$tableAdmDevolucoesLine.find(".btnConfirmarEnvio").show();
		else 
			$tableAdmDevolucoesLine.find(".btnConfirmarEnvio").remove();
		
		if(idStatus === 6) // Status entregue?
			$tableAdmDevolucoesLine.find(".btnAprovarTroca").show();
		else 
			$tableAdmDevolucoesLine.find(".btnAprovarTroca").remove();
	}
	
	function fillAdmVendasContainer(vendaList) {
		if ($tableAdmVendas.hasClass('initialized')) {
			$tableAdmVendas.dataTable().fnDestroy();
        }
		
		$tableAdmDevolucoesContainer.empty();
		$.each(vendaList, function (i, venda){
			var $tableAdmDevolucoesLineClone = $tableAdmDevolucoesLine.clone();
			fillAdmVendasLine.call(this, venda, $tableAdmDevolucoesLineClone);
			$tableAdmDevolucoesContainer.append($tableAdmDevolucoesLineClone);
		});
		
		if ($tableAdmVendas.is('table')) {
            var recordsTotal = $tableAdmVendas.addClass('initialized').DataTable({
                "oLanguage": TraducaoDatatable,
                "order": [[ 0, "desc" ]]
            }).page.info().recordsTotal;
        }
	}
	
	function getAndFillAdmVendasTable(){
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/AdmComprasWS?operacao=CONSULTAR",
            dataType: "json",
            //async: false,
            //data: JSON.stringify(pessoaCartao),
            contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
            		var vendaList = respostaServlet.resultado.entidades;   
            		fillAdmVendasContainer(vendaList);
            	} else if(respostaServlet.codigoResposta == 250){ //erro!            		
            		swal({
            			  type: 'error',
            			  title: "Consultar as compras",
            			  html: respostaServlet.resultado.msg.replace("\n", "<br/>")
            			});
            	} else if(respostaServlet.codigoResposta == 275){ // não logado!            		
            		RedirectToLoginPage();
              	} else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }, 
            complete: function(){
            	//$("#btnSubmitModalFormLivro").prop( "disabled", false );
            }
		});
	}
	
	function alterCompraStatus(compra){
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/AdmComprasWS?operacao=ALTERAR",
            dataType: "json",
            //async: false,
            data: JSON.stringify(compra),
            contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
            		var vendaList = respostaServlet.resultado.entidades;       
            		console.log(vendaList);
            		$.gritter.add({
                        title: "Status alterado",
                        text: "O status da compra foi alterado com sucesso",
                        time: 1500
                    });
            		getAndFillAdmVendasTable();
            	} else if(respostaServlet.codigoResposta == 250){ //erro!            		
            		swal({
            			  type: 'error',
            			  title: "Consultar as compras",
            			  html: respostaServlet.resultado.msg.replace("\n", "<br/>")
            			});
            	} else if(respostaServlet.codigoResposta == 275) { // não logado!            		
            		RedirectToLoginPage();
              	} else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }, 
            complete: function(){
            	//$("#btnSubmitModalFormLivro").prop( "disabled", false );
            }
		});
	}
	
	this.AlterCompraStatus = function(compra) {
		alterCompraStatus(compra);
	}
	this.PrintAdmVendasTable = function(){
		getAndFillAdmVendasTable();
	}
}