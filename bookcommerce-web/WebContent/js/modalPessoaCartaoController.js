var PessoaCartaoCredito;
function ModalPessoaCartaoController(constructorParam){
	var isPagamentoPage = window.location.pathname.toLowerCase() === "/bookcommerce-web/pagamento.jsp";
	var isLoginPage = constructorParam.isLoginPage;
	
	var $modalCadastroPessoaCartao = $(constructorParam.modalCadastroPessoaCartao);
	
	// telas anteriores a esta
	var $formModalPessoaCartaoContainer;
	var $formModalPessoaCartaoOpt;
	if(!isPagamentoPage){
		$formModalPessoaCartaoContainer = $(SelectorFormPessoa).find(SelectorFormPessoaCartaoContainerSelector);
		$formModalPessoaCartaoOpt = $(SelectorFormPessoa).find(SelectorFormPessoaCartaoOptSelector).clone();
	}
	
	function fillModalPessoaCartaoOpt(pessoaCartaoCredito, $formModalPessoaCartaoOpt){
		$formModalPessoaCartaoOpt.val(pessoaCartaoCredito.id);
		$formModalPessoaCartaoOpt.text(pessoaCartaoCredito.apelido);
	}
		
	function readPessoaCartaoCredito(){
		PessoaCartaoCredito = {
			idPessoa: $("#formPessoaId").text(),
			apelido: $modalCadastroPessoaCartao.find("#modalCadastroPessoaCartaoApelido").val(),
			numero: $modalCadastroPessoaCartao.find("#modalCadastroPessoaCartaoNumero").val(),
			nomeImpresso: $modalCadastroPessoaCartao.find("#modalCadastroPessoaCartaoNome").val(),
			codigoSeguranca: $modalCadastroPessoaCartao.find("#modalCadastroPessoaCartaoCodSeguranca").val(),
			cartaoCreditoTipo: {
				id: $modalCadastroPessoaCartao.find("#modalCadastroPessoaCartaoTipo").val()
			}
		}
	}
	
	function savePessoaCartaoPaginaLogin(){
		readPessoaCartaoCredito();
		
		$formModalPessoaCartaoContainer.empty();
		var $formModalPessoaCartaoOptClone = $formModalPessoaCartaoOpt.clone();
		fillModalPessoaCartaoOpt.call(this, PessoaCartaoCredito, $formModalPessoaCartaoOptClone);
		$formModalPessoaCartaoContainer.append($formModalPessoaCartaoOptClone);
			
		$modalCadastroPessoaCartao.modal("hide");
		$.gritter.add({
            title: "Ok",
            text: "Cartão cadastrado com sucesso!",
            time: 1500
        });
	}
	
	function savePessoaCartaoPaginaPerfil(){
		readPessoaCartaoCredito();
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/PessoaCartaoWS?operacao=SALVAR",
            dataType: "json",
            data: JSON.stringify(PessoaCartaoCredito),
            contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
            		var pessoaCartaoList = respostaServlet.resultado.entidades;   
            		
            		var $formModalPessoaCartaoOptClone = $formModalPessoaCartaoOpt.clone();
            		fillModalPessoaCartaoOpt.call(this, pessoaCartaoList[0], $formModalPessoaCartaoOptClone);
            		$formModalPessoaCartaoContainer.append($formModalPessoaCartaoOptClone);		
            		$.gritter.add({
                        title: "Sucesso",
                        text: "Cartão salvo com sucesso",
                        time: 1500
                    });
            		$("#modalCadastroPessoaCartao").modal("hide");
            	} else if(respostaServlet.codigoResposta == 250){ //erro!            		
            		$.gritter.add({
                        title: "Erro ao listar as cidades",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	} else if(respostaServlet.codigoResposta == 275){ //erro!            		
            		swal({
          			  type: 'error',
          			  title: respostaServlet.mensagemResposta,
          			  html: respostaServlet.resultado.msg.replace("\n", "<br/>")
          			});
            	} else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }, 
            complete: function(){
            	//$("#btnSubmitModalFormLivro").prop( "disabled", false );
            }
		});
	}
	
	function savePessoaCartao(){
		if(isLoginPage){ // estamos na página de login
			savePessoaCartaoPaginaLogin();
		} else {
			savePessoaCartaoPaginaPerfil();
		}	
	}
	
	function initEventListener(){
		$modalCadastroPessoaCartao.find("#btnSubmitModalCadastroPessoaCartao").click(function(){
			savePessoaCartao();
		});
	}
	
	this.OpenModalPessoaCartao = function (pessoaCartaoCredito){
		openModalPessoaCartao(pessoaCartaoCredito);
	}
	this.InitEventListener = function (){
		initEventListener();
	}
}