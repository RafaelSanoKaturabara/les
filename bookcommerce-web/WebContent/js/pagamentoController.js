function PagamentoController(selectors){
	var $carrinhoContainer = $(selectors.carrinhoItemContainer);
	var $carrinhoLine = $carrinhoContainer.find(".carrinhoLine").clone();
	$carrinhoContainer.find(".carrinhoLine").remove();
	var $formPagamento = $(selectors.formPagamento);
	
	var $formlPessoaEnderecoContainer = $formPagamento.find("#selectPagamentoEndereco");
	var $formPessoaEnderecoOpt = $formPagamento.find(".selectPagamentoEnderecoOpt").clone();
	$formPagamento.find(".selectPagamentoEnderecoOpt").remove();
	
	var $formlPessoaCartaoContainer = $formPagamento.find("#selectPagamentoCartao");
	var $formPessoaCartaoOpt = $formPagamento.find(".selectPagamentoCartaoOpt").clone();
	$formPagamento.find(".selectPagamentoCartaoOpt").remove();
	
	var $carrinhoCupomContainer = $formPagamento.find("#carrinhoCupomContainer");
	var $carrinhoCupomLine = $formPagamento.find(".carrinhoCupomLine").clone();
	
	var $carrinhoPagamentoContainer = $formPagamento.find("#carrinhoPagamentoContainer");
	var $CarrinhoPagamentoLine = $formPagamento.find(".carrinhoPagamentoLine").clone();
	$formPagamento.find(".carrinhoPagamentoLine").remove();
	
	function fillCarrinhoLine(carrinhoItem, $carrinhoLine){
		//$carrinhoLine.find(".carrinhoItemImagem").text(pessoaCarrinhoItem.);
		$carrinhoLine.data("carrinhoItem", carrinhoItem);
		$carrinhoLine.find(".carrinhoItemTitulo").text(carrinhoItem.livro.titulo);		
		$carrinhoLine.find(".carrinhoItemAutorLine").hide(); //.text(pessoaCarrinhoItem.livro);
		$carrinhoLine.find(".carrinhoItemEditora").text(carrinhoItem.livro.editora.nome);
		$carrinhoLine.find(".carrinhoItemAno").text(carrinhoItem.livro.ano.ano);
		$carrinhoLine.find(".carrinhoItemQuantidade").text(carrinhoItem.quantidade);
		$carrinhoLine.find(".carrinhoItemPrecoUnit").text(FloatFormat(carrinhoItem.precoItem, 2));
		$carrinhoLine.find(".carrinhoItemPrecoTotal").text(FloatFormat(carrinhoItem.precoItem * carrinhoItem.quantidade, 2));
		$carrinhoLine.find(".carrinhoItemImagem").attr("src", carrinhoItem.livro.img100 == null ? "/bookcommerce-web/imagens/100x100.png" : carrinhoItem.livro.img100);
	}
	
	function readCarrinhoForm(){
		var carrinhoData = $carrinhoContainer.data("carrinho");		
		var carrinho = {
			id: carrinhoData.id,
			endereco:{ 
				id:	$formPagamento.find("#selectPagamentoEndereco").val()			
			}
		}
		return carrinho;
	}
	
	function fillCarrinhoCupomList(carrinhoCupom, $carrinhoCupomLine){
		var valorFormatado;
		if(carrinhoCupom.cupom.cupomDescontoTipo.id == 1){ // tipo porcentagem
			valorFormatado = FloatFormat(carrinhoCupom.cupom.valorDesconto, 2) + "%";
		} else { // valor
			valorFormatado = "R$ " + FloatFormat(carrinhoCupom.cupom.valorDesconto, 2);
		}
		$carrinhoCupomLine.data("carrinhoCupom", carrinhoCupom);
		$carrinhoCupomLine.find(".carrinhoCupomNome").text(carrinhoCupom.cupom.codigo); 
		$carrinhoCupomLine.find(".carrinhoCupomValor").text(valorFormatado);
	}
	
	function fillCarrinhoPagamentoLine(carrinhoPagamento, $CarrinhoPagamentoLine){
		$CarrinhoPagamentoLine.data("carrinhoPagamento", carrinhoPagamento);
		$CarrinhoPagamentoLine.find(".carrinhoPagamentoApelido").text(carrinhoPagamento.pessoaCartaoCredito.apelido);
		$CarrinhoPagamentoLine.find(".carrinhoPagamentoValor").text(FloatFormat(carrinhoPagamento.valor, 2));
	}
	
	function fillCarrinhoContainer(carrinho){
		$carrinhoContainer.empty();	
		$carrinhoCupomContainer.empty();
		$carrinhoPagamentoContainer.empty();
		if(carrinho == null)
			return;
		var totalGeral = getTodalCarrinhoItem(carrinho.carrinhoItemList);
		var totalComDesconto = calcularTotalDescontoCarrinhoCupom(carrinho);
		$carrinhoContainer.data("carrinho", carrinho);
		$formPagamento.find("#carrinhoTotalGeral").text(FloatFormat(totalGeral, 2));
		$formPagamento.find("#carrinhoCupom").text(FloatFormat(totalGeral - totalComDesconto, 2));
		$formPagamento.find("#carrinhoFrete").text(FloatFormat(carrinho.precoFrete, 2));
		$formPagamento.find("#carrinhoFrete").data("paraCalculo", carrinho.precoFrete + totalComDesconto);
		$formPagamento.find("#carrinhoTotal").text(FloatFormat(carrinho.precoFrete + totalComDesconto, 2));
		$.each(carrinho.carrinhoItemList, function(i, carrinhoItem) {
			var $carrinhoLineClone = $carrinhoLine.clone();
			fillCarrinhoLine.call(this, carrinhoItem, $carrinhoLineClone);
			$carrinhoContainer.append($carrinhoLineClone);			
		});
		
		if(carrinho != null && carrinho.carrinhoCupomList != null){
			$.each(carrinho.carrinhoCupomList, function(i, carrinhoCupom){
				var $carrinhoCupomLineClone = $carrinhoCupomLine.clone();
				fillCarrinhoCupomList.call(this, carrinhoCupom, $carrinhoCupomLineClone);
				$carrinhoCupomContainer.append($carrinhoCupomLineClone);
			});
		}
		
		if(carrinho != null && carrinho.carrinhoPagamentoList != null){
			$carrinhoPagamentoContainer.show();
			$.each(carrinho.carrinhoPagamentoList, function(i, carrinhoPagamento){
				var $CarrinhoPagamentoLineClone = $CarrinhoPagamentoLine.clone();
				fillCarrinhoPagamentoLine.call(this, carrinhoPagamento, $CarrinhoPagamentoLineClone);
				$carrinhoPagamentoContainer.append($CarrinhoPagamentoLineClone);
			});
		}
	}
	
	function fillPessoaEnderecoLine(pessoaEndereco, $formPessoaEnderecoOpt){
		$formPessoaEnderecoOpt.data("pessoaEndereco", pessoaEndereco);
		$formPessoaEnderecoOpt.val(pessoaEndereco.id);
		$formPessoaEnderecoOpt.text(pessoaEndereco.apelido);
	}
	
	function fillPessoaCartaoLine(pessoaCartao, $formPessoaCartaoOpt){
		$formPessoaCartaoOpt.data("pessoaCartao", pessoaCartao);
		$formPessoaCartaoOpt.val(pessoaCartao.id);
		$formPessoaCartaoOpt.text(pessoaCartao.apelido);
	}
	
	function fillPessoaEnderecoContainer(pessoaEnderecoList){
		$formlPessoaEnderecoContainer.empty();
		$.each(pessoaEnderecoList, function(i, pessoaEndereco){
			var $formPessoaEnderecoOptClone = $formPessoaEnderecoOpt.clone();
			fillPessoaEnderecoLine.call(this, pessoaEndereco, $formPessoaEnderecoOptClone);
			$formlPessoaEnderecoContainer.append($formPessoaEnderecoOptClone);
		});
	}

	function fillPessoaCartaoContainer(pessoaCartaoList){
		$formlPessoaCartaoContainer.empty();
		$.each(pessoaCartaoList, function(i, pessoaCartao){
			var $formPessoaCartaoOptClone = $formPessoaCartaoOpt.clone();
			fillPessoaCartaoLine.call(this, pessoaCartao, $formPessoaCartaoOptClone);
			$formlPessoaCartaoContainer.append($formPessoaCartaoOptClone);
		});
	}
	
	function getAndPrintCarrinhoContainer(){
		var carrinho = {
			carrinhoStatus: {
				id: 1 // tipo carrinho
			}
		}
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/CarrinhoWS?operacao=CONSULTAR",
            dataType: "json",
            async: false,
            data: JSON.stringify(carrinho),
            contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
            		var carrinhoList = respostaServlet.resultado.entidades;
            		fillCarrinhoContainer(carrinhoList[0]);
            	} else if(respostaServlet.codigoResposta == 250){ //erro!            		
            		swal({
        			  type: 'error',
        			  title: "Erro ao cadastrar",
        			  html: respostaServlet.resultado.msg.replace("\n", "<br/>")
        			});
            	}else if(respostaServlet.codigoResposta == 275){ // não logado!            		
            		window.location.href = "/bookcommerce-web/login.jsp";
              	} else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }
		});
	}	
	
	function getEnderecoPessoa(){	
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/PessoaEnderecoWS?operacao=CONSULTAR",
            dataType: "json",
            //data: JSON.stringify(pessoaEndereco),
            async: false,
            contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
            		var pessoaEnderecoList = respostaServlet.resultado.entidades;
            		fillPessoaEnderecoContainer(pessoaEnderecoList);
            		$formlPessoaEnderecoContainer.val(pessoaEnderecoList[0].pessoa.enderecoPadrao.id);
            		$formlPessoaEnderecoContainer.change();
            	} else if(respostaServlet.codigoResposta == 250){ //erro!            		
            		swal({
            			  type: 'error',
            			  title: "Erro ao cadastrar",
            			  html: respostaServlet.resultado.msg.replace("\n", "<br/>")
            			});
            	} else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }, 
            complete: function(){
            	//$("#btnSubmitModalFormLivro").prop( "disabled", false );
            }
		});
	}

	function getPessoaCartao(){
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/PessoaCartaoWS?operacao=CONSULTAR",
            dataType: "json",
            async: false,
            //data: JSON.stringify(pessoaCartao),
            contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
            		var pessoaCartaoList = respostaServlet.resultado.entidades;            		
            		fillPessoaCartaoContainer(pessoaCartaoList);
            		$formlPessoaCartaoContainer.val(pessoaCartaoList[0].pessoa.cartaoCreditoPadrao.id);
            	} else if(respostaServlet.codigoResposta == 250){ //erro!            		
            		swal({
            			  type: 'error',
            			  title: "Erro ao cadastrar",
            			  html: respostaServlet.resultado.msg.replace("\n", "<br/>")
            			});
            	} else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }, 
            complete: function(){
            	//$("#btnSubmitModalFormLivro").prop( "disabled", false );
            }
		});
	}
	
	function readCarrinhoPagamentoForm(){
		var carrinhoPagamento = {
			valor: $formPagamento.find("#valorPagamentoCartao").val().replace(",","."),
			pessoaCartaoCredito: {
				id: $formPagamento.find("#selectPagamentoCartao").val()
			}
		}
		return carrinhoPagamento;
	}
	
	function readAndCarrinhoPagamento(){
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/CarrinhoPagamentoWS?operacao=SALVAR",
            dataType: "json",
            async: false,
            data: JSON.stringify(readCarrinhoPagamentoForm()),
            contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
            		var carrinhoList = respostaServlet.resultado.entidades;
            		getAndPrintCarrinhoContainer();
            		$.gritter.add({
                        title: "Sucesso",
                        text: "Pagamento inserido",
                        time: 1500
                    });
            	} else if(respostaServlet.codigoResposta == 250){ //erro!            		
            		swal({
            			  type: 'error',
            			  title: "Erro ao inserir o pagamento",
            			  html: respostaServlet.resultado.msg.replace("\n", "<br/>")
            			});
            	}else if(respostaServlet.codigoResposta == 275){ // não logado!            		
            		window.location.href = "/bookcommerce-web/login.jsp";
              	} else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }
		});
	}
	
	function readFormAndPay() {
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/PagarCarrinhoWS?operacao=SALVAR",
            dataType: "json",
            async: false,
            data: JSON.stringify(readCarrinhoForm()),
            contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
            		var carrinhoList = respostaServlet.resultado.entidades;
            		getAndPrintCarrinhoContainer();
            		swal({
            			  title: 'Pagamento efetuado!',
            			  text: 'Você será redirecionado em 3 segundos',
            			  allowEscapeKey: false,
            			  timer: 3000,
            			  onOpen: () => {
            			    swal.showLoading()
            			  }
            			}).then((result) => {
            				window.location.href = "/bookcommerce-web/minhas-compras.jsp";
            			})
            	} else if(respostaServlet.codigoResposta == 250){ //erro!            		
            		swal({
            			  type: 'error',
            			  title: "Erro ao inserir o pagamento",
            			  html: respostaServlet.resultado.msg.replace("\n", "<br/>")
            			});
            	}else if(respostaServlet.codigoResposta == 275){ // não logado!            		
            		window.location.href = "/bookcommerce-web/login.jsp";
              	} else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }
		});
	}
		
	this.GetAndFillCartaoEEndereco = function() {
		getEnderecoPessoa();
		getPessoaCartao();
	}
	this.PrintCarrinhoContainer = function() {
		getAndPrintCarrinhoContainer();
	}
	this.AddCarrinhoPagamento = function() {
		readAndCarrinhoPagamento();
	}
	this.PagarCarrinho = function() {
		readFormAndPay();
	}
}