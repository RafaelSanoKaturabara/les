function ModalEntradaMercadoriaController(selectors) {
	var $modalEntradaMercadoria = $(selectors.modalEntradaMercadoria);
	var $formEntradaMercadoria = $modalEntradaMercadoria.find("#formEntradaMercadoria");
	var $tableEntradaLivroContainer = $formEntradaMercadoria.find("#tableEntradaLivroContainer");
	var $tableEntradaLivroLine = $formEntradaMercadoria.find(".tableEntradaLivroLine").clone();
	$formEntradaMercadoria.find(".tableEntradaLivroLine").remove();
	
	var $inputModalPesquisaLivro = $formEntradaMercadoria.find("#inputModalPesquisaLivro");
	
	var livroTags = [];
	
	function fillFormEntradaLivroLine(entradaLivro, $tableEntradaLivroLine){
		$tableEntradaLivroLine.data("entradaLivro", entradaLivro);
		$tableEntradaLivroLine.find(".tableEntradaLivroIdLivro").text(entradaLivro.livro.id);
		$tableEntradaLivroLine.find(".tableEntradaLivroCodBarras").text(entradaLivro.livro.codBarras);
		$tableEntradaLivroLine.find(".tableEntradaLivroTitulo").text(entradaLivro.livro.titulo);
		$tableEntradaLivroLine.find(".tableEntradaLivroQuantidade").val(entradaLivro.quantidade);
		$tableEntradaLivroLine.find(".tableEntradaLivroPrecoPago").val(FloatFormat(entradaLivro.precoPago, 2));
		$tableEntradaLivroLine.find(".tableEntradaLivroTotalUnitario").text(FloatFormat(entradaLivro.quantidade * entradaLivro.precoPago, 2));
	}
	
	function fillFormEntradaLivroContainer(entradaLivroList){
		$tableEntradaLivroContainer.empty();
		var totalDaNota = 0;
		$.each(entradaLivroList, function(i, entradaLivro) {
			var $tableEntradaLivroLineClone = $tableEntradaLivroLine.clone();
			totalDaNota += entradaLivro.quantidade * entradaLivro.precoPago;
			fillFormEntradaLivroLine.call(this, entradaLivro, $tableEntradaLivroLineClone);
			$tableEntradaLivroContainer.append($tableEntradaLivroLineClone);
		});
		$formEntradaMercadoria.find("#inputModalEntradaLivroTotalNota").text(FloatFormat(totalDaNota, 2));
	}
	
	function fillFormEntradaLivro(entrada) {
		if(entrada != null){			
			var date = new Date(entrada.dataEntrada);
			$formEntradaMercadoria.data("entrada", entrada);
			//$formEntradaMercadoria.find("#formEntradaMercadoriaNumeroNota").val(entrada.numeroNotaFiscal);
			$formEntradaMercadoria.find("#formEntradaMercadoriaData").val(date.format("dd/mm/yyyy"));
			$formEntradaMercadoria.find("#formEntradaMercadoriaFornecedorSelect").val(entrada.fornecedor.id);
		} else {
			$formEntradaMercadoria.data("entrada", "");
			$tableEntradaLivroContainer.empty();
//			$formEntradaMercadoria.find("#formEntradaMercadoriaNumeroNota").val("");
			$formEntradaMercadoria.find("#formEntradaMercadoriaData").val("");
			$formEntradaMercadoria.find("#formEntradaMercadoriaFornecedorSelect").val("");
		}
		
	}
	
	function fillLivroMultiselect(livroList){
		livroTags = [];
		$.each(livroList, function(i, livro) {
			livroTags.push(livro.id + " " + livro.codBarras + " " + livro.titulo);
		});
		
		$inputModalPesquisaLivro.autocomplete({
		      source: livroTags,
		      select: function(event, ui) {
		    	  var srrayLivroTag = ui.item.value.split(" ");
		    	  var idLivro = srrayLivroTag[0];
		    	  saveEntradaLivro(idLivro);
		      }
	    });
	}
	
	function saveEntradaLivro(idLivro){
		var entrada = $formEntradaMercadoria.data("entrada");
		if(entrada == null || entrada.id == null || entrada.id == 0){
			swal({
  			  type: 'warning',
  			  title: "Entrada não encontrada",
  			  html: "Cadastre a nota fiscal primeiro"
  			});
			return;
		}
		var entradaLivro = {
			entrada: {
				id: entrada.id
			},
			livro: {
				id: idLivro		
			}
		}
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/EntradaLivroWS?operacao=SALVAR",
            dataType: "json",
            //async: false,
            data: JSON.stringify(entradaLivro),
            contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
            		var entradaLivroList = respostaServlet.resultado.entidades;
            		getAndFillFormEntradaLivroContainer(entrada);
            		$.gritter.add({
                        title: "Sucesso",
                        text: "Livro adicionado à entrada com sucesso",
                        time: 1500
                    });
            	} else if(respostaServlet.codigoResposta == 250){ //erro!            		
            		swal({
        			  type: 'Tente novamente',
        			  title: "Não foi possível buscar as entradas",
        			  html: respostaServlet.resultado.msg.replace("\n", "<br/>")
        			});
            	} else if(respostaServlet.codigoResposta == 275){ // não logado!            		
            		window.location.href = "/bookcommerce-web/login.jsp";
              	} else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }, 
            complete: function(){
            	//$("#btnSubmitModalFormLivro").prop( "disabled", false );
            }
		});
	}
	
	function getAndFillFormEntradaLivroContainer(entrada){
		var entradaLivro = {
			entrada: {
				id: entrada.id
			}
		}
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/EntradaLivroWS?operacao=CONSULTAR",
            dataType: "json",
            //async: false,
            data: JSON.stringify(entradaLivro),
            contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
            		var entradaLivroList = respostaServlet.resultado.entidades;       
            		fillFormEntradaLivroContainer(entradaLivroList);
            	} else if(respostaServlet.codigoResposta == 250){ //erro!            		
            		swal({
            			  type: 'Tente novamente',
            			  title: "Não foi possível buscar as entradas",
            			  html: respostaServlet.resultado.msg.replace("\n", "<br/>")
            			});
            	} else if(respostaServlet.codigoResposta == 275){ // não logado!            		
            		window.location.href = "/bookcommerce-web/login.jsp";
              	} else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }, 
            complete: function(){
            	//$("#btnSubmitModalFormLivro").prop( "disabled", false );
            }
		});
	}
	
	function getEntrada(){
		var entrada = {
			numeroNotaFiscal: $formEntradaMercadoria.find("#formEntradaMercadoriaNumeroNota").val()
		}
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/EntradaWS?operacao=CONSULTAR",
            dataType: "json",
            //async: false,
            data: JSON.stringify(entrada),
            contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
            		var entradaList = respostaServlet.resultado.entidades;       
            		if(entradaList.length === 1){
            			getAndFillFormEntradaLivroContainer(entradaList[0]);
            			fillFormEntradaLivro(entradaList[0]);   
            		} else {
            			fillFormEntradaLivro(); 
            			$formEntradaMercadoria.find("#inputModalEntradaLivroTotalNota").text("0");            			
            		}
            	} else if(respostaServlet.codigoResposta == 250){ //erro!            		
            		swal({
            			  type: 'Tente novamente',
            			  title: "Não foi possível buscar as entradas",
            			  html: respostaServlet.resultado.msg.replace("\n", "<br/>")
            			});
            	} else if(respostaServlet.codigoResposta == 275){ // não logado!            		
            		window.location.href = "/bookcommerce-web/login.jsp";
              	} else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }, 
            complete: function(){
            	//$("#btnSubmitModalFormLivro").prop( "disabled", false );
            }
		});
	}
	
	function saveEntrada() {
		var dataEntrada = getDateFromDatepiker($formEntradaMercadoria.find("#formEntradaMercadoriaData").val() + " 00:00");
		var entrada = {
			numeroNotaFiscal: $formEntradaMercadoria.find("#formEntradaMercadoriaNumeroNota").val(),
			dataEntrada: dataEntrada,
			fornecedor: {
				id: $formEntradaMercadoria.find("#formEntradaMercadoriaFornecedorSelect").val()
			}
		}
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/EntradaWS?operacao=SALVAR",
            dataType: "json",
            //async: false,
            data: JSON.stringify(entrada),
            contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
            		var entradaList = respostaServlet.resultado.entidades;  
            		if(entradaList.length === 1){
            			getAndFillFormEntradaLivroContainer(entradaList[0]);
            			//fillFormEntradaLivro(entradaList[0]);
            		}
            		$.gritter.add({
                        title: "Sucesso",
                        text: "Entrada cadastrada com sucesso",
                        time: 1500
                    });
            		fillFormEntradaLivro(entradaList[0]);
            	} else if(respostaServlet.codigoResposta == 250){ //erro!            		
            		swal({
            			  type: 'Tente novamente',
            			  title: "Não foi possível buscar as entradas",
            			  html: respostaServlet.resultado.msg.replace("\n", "<br/>")
            			});
            	} else if(respostaServlet.codigoResposta == 275){ // não logado!            		
            		window.location.href = "/bookcommerce-web/login.jsp";
              	}  else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }, 
            complete: function(){
            	//$("#btnSubmitModalFormLivro").prop( "disabled", false );
            }
		});
	}
	
	function getAndFillFornecedor(){
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/FornecedorWS?operacao=CONSULTAR",
            dataType: "json",
            //async: false,
            //data: JSON.stringify(entrada),
            contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
            		var entradaList = respostaServlet.resultado.entidades;     
            		if(entradaList != null && entradaList.length === 1){
            			getAndFillFormEntradaLivroContainer(entradaList[0]);
            		}
            	} else if(respostaServlet.codigoResposta == 250){ //erro!            		
            		swal({
            			  type: 'Tente novamente',
            			  title: "Não foi possível buscar as entradas",
            			  html: respostaServlet.resultado.msg.replace("\n", "<br/>")
            			});
            	} else if(respostaServlet.codigoResposta == 275){ // não logado!            		
            		window.location.href = "/bookcommerce-web/login.jsp";
              	} else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }, 
            complete: function(){
            	//$("#btnSubmitModalFormLivro").prop( "disabled", false );
            }
		});
	}
	
	function getLivro(livro){
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/GetLivroWS?operacao=CONSULTAR",
            dataType: "json",
            async: false,
            data: JSON.stringify(livro),
            contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
            		var entradaList = respostaServlet.resultado.entidades;    
            		if(entradaList.length === 1){
            			getAndFillFormEntradaLivroContainer(entradaList[0]);
            		} else if(entradaList.length > 1){
            			fillLivroMultiselect(entradaList);
            		}
            	} else if(respostaServlet.codigoResposta == 250){ //erro!            		
            		swal({
            			  type: 'Tente novamente',
            			  title: "Não foi possível buscar as entradas",
            			  html: respostaServlet.resultado.msg.replace("\n", "<br/>")
            			});
            	} else if(respostaServlet.codigoResposta == 275){ // não logado!            		
            		window.location.href = "/bookcommerce-web/login.jsp";
              	} else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }, 
            complete: function(){
            	//$("#btnSubmitModalFormLivro").prop( "disabled", false );
            }
		});
	}
	
	function alterEntradaLivro(entradaLivro) {
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/EntradaLivroWS?operacao=ALTERAR",
            dataType: "json",
            async: false,
            data: JSON.stringify(entradaLivro),
            contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
            		var entradaLivroList = respostaServlet.resultado.entidades;       
            		$.gritter.add({
                        title: "Sucesso",
                        text: "Quantidades e preços atualizados",
                        time: 1500
                    });
            		var entrada = {
            				id: entradaLivro.entrada.id
            		}
            		getAndFillFormEntradaLivroContainer(entrada);
            	} else if(respostaServlet.codigoResposta == 250){ //erro!            		
            		swal({
            			  type: 'Tente novamente',
            			  title: "Não foi possível buscar as entradas",
            			  html: respostaServlet.resultado.msg.replace("\n", "<br/>")
            			});
            	} else if(respostaServlet.codigoResposta == 275){ // não logado!            		
            		window.location.href = "/bookcommerce-web/login.jsp";
              	} else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }, 
            complete: function(){
            	//$("#btnSubmitModalFormLivro").prop( "disabled", false );
            }
		});
	}
	
	function getAndFillLivroMultiselect(){
		getLivro();		
	}
	
	this.GetAndFillLivroMultiselect = function () {
		getAndFillLivroMultiselect();
	}	
	this.GetAndFillFornecedor = function() {
		getAndFillFornecedor();
	}
	this.GetEntrada = function() {
		getEntrada();
	}
	this.SaveEntrada = function() {
		saveEntrada();
	}
	this.AlterEntradaLivro = function(entradaLivro) {
		alterEntradaLivro(entradaLivro);
	}
}