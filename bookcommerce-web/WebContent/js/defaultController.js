function DefaultController(initParams){
	var $livroContainer = $(initParams.livroContainer);	
	var $livroItem = $livroContainer.find(".livroItem").clone();
	$livroContainer.find(".livroItem").remove();
	

	//var $livroAutorContainer = $livroContainer.find(".livroAutorContainer");
	var $livroAutorLine = $livroItem.find(".livroAutorLine").clone();
	
	function fillAutorLine(autor, $livroAutorLine){
		$livroAutorLine.text(autor.Nome);
	}
	
	function fillLivroItem(livro, $livroItem){
		//$livroItem.find(".livroImagem").prop("src", livro....);
		$livroItem.data("livro", livro);
		$livroItem.find(".livroPreco").text(FloatFormat(livro.preco, 2));
		$livroItem.find(".livroTitulo").text(livro.titulo);
		$livroItem.find(".livroImagem").attr("src", livro.img210 == null ? "/bookcommerce-web/imagens/210x210.png" : livro.img210);
		
		$livroItem.find(".livroAutorContainer").empty();
		if(livro.autorList != null){
			$.each(livro.autorList, function(i, autor){
				var $livroAutorLineClone = $livroAutorLine.clone();
				fillAutorLine(autor, $livroAutorLineClone);
				$livroItem.find(".livroAutorContainer").append($livroAutorLineClone);
			});
		} 
		
		
//		
//		$livroItem.find(".livroAutor").text(livro.autorList[0] == null 
//			|| livro.autorList[0].Nome == null ? "" : livro.autorList[0].Nome);
	}
	
	function fillLivroContainer(livroList) {		
		$livroContainer.html("");			
		$.each(livroList, function (i, livro){
			if(livro.ativo){ // o livro está ativo?
				var $livroItemClone = $livroItem.clone();
				fillLivroItem.call(this, livro, $livroItemClone);
				$livroContainer.append($livroItemClone);
			}
		});
	}
	
	function getAndFillLivroContainer() {
		var livro = { // era para buscar somente ativos, por estar enviando aqui. mas foi feito da forma mais fácil
			ativo: true
		}
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/LivroWS?operacao=CONSULTAR",
            data: livro,
            error: function (ex) {
            	$.gritter.add({
                    title: "Houve algum erro não conhecido",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	var livroList = respostaServlet.resultado.entidades;
            	fillLivroContainer(livroList);
            }
		});
	}
	
	function adicionarAoCarrinho(livro, quantidade){
		var objCarrinhoItem = {
			livro: {
				id: livro.id
			},
			precoItem: livro.preco,
			status: true,
			quantidade: quantidade,
			carrinho:{
				dataUltimaAlteracao: new Date(),
				precoFrete: 0
			}			
		}		
		
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/CarrinhoItemWS?operacao=SALVAR",
            dataType: "json",
            async: false,
            data: JSON.stringify(objCarrinhoItem),
            contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
            		$.gritter.add({
                        title: respostaServlet.mensagemResposta,
                        text: "Livro adicionado ao carrinho!",
                        time: 1500
                    });
            	} else if(respostaServlet.codigoResposta == 250){ //erro!            		
            		swal({
            			  type: 'error',
            			  title: "Erro ao adicionar livro ao carrinho",
            			  html: respostaServlet.resultado.msg.replace("\n", "<br/>")
            			});
            	}else if(respostaServlet.codigoResposta == 275){ // não logado!            		
            		window.location.href = "/bookcommerce-web/login.jsp";
              	} else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }
		});
	}
	
	function pesquisarLivros(parametroBusca){
		var objPost = {
			procurar: parametroBusca		
		}	
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/PesquisarLivroWS?operacao=CONSULTAR",
            dataType: "json",
            async: false,
            data: JSON.stringify(objPost),
            contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
//            		$.gritter.add({
//                        title: respostaServlet.mensagemResposta,
//                        text: "Livro adicionado ao carrinho!",
//                        time: 1500
//                    });
            		var livroList = respostaServlet.resultado.entidades;
            		fillLivroContainer(livroList);
            	} else if(respostaServlet.codigoResposta == 250){ //erro!            		
            		swal({
            			  type: 'error',
            			  title: "Erro ao efetuar a busca",
            			  html: respostaServlet.resultado.msg.replace("\n", "<br/>")
            			});
            	}else if(respostaServlet.codigoResposta == 275){ // não logado!            		
            		window.location.href = "/bookcommerce-web/login.jsp";
              	} else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }
		});
	}
	
	this.AdicionarAoCarrinho = function(livro, quantidade){
		adicionarAoCarrinho(livro, quantidade);
	}
	this.PrintDefaulContainer = function(){
		getAndFillLivroContainer();
	}
	this.PesquisarLivros = function(parametroBusca){
		pesquisarLivros(parametroBusca);
	}
}