function ModalLivroController(constructorParam){
	var $modalLivro = $(constructorParam.modalLivro);
	
	function printModalLivro(livro){
		console.log(livro);

		$modalLivro.modal("show");
	}
	
	this.OpenModalLivro = function (livro){
		printModalLivro(livro);
	}
}