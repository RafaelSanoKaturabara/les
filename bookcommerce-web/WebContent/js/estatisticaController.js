function EstatisticaController(selectors) {	
	var $tableLog = $(selectors.tableLog);
	var $tableLogContainer = $tableLog.find("#tableLogContainer");
	var $tableLogLine = $tableLog.find(".tableLogLine").clone();
	var $modalExibirJson = $(selectors.modalExibirJson);
	var datasEstatisticaArray = [];
	var myChart;
	var chartEstatisticaLivro;

	$tableLog.find(".tableLogLine").remove();
	
	function fillTableLogLine(log, $tableLogLine){
		$tableLogLine.data("log", log);
		$tableLogLine.find(".tableLogIdPessoa").text(log.pessoa.id);
		$tableLogLine.find(".tableLogIp").text(log.ip);
		$tableLogLine.find(".tableLogAcao").text(log.acao);
		$tableLogLine.find(".tableLogChave").text(log.chave);
		$tableLogLine.find(".tableLogDataHora").text(log.dataEHoraStr + "h");
	}
	
	function fillTableLogContainer(logList){
		if ($tableLog.hasClass('initialized')) {
			$tableLog.dataTable().fnDestroy();
        }
		
		$tableLogContainer.empty();
		$.each(logList, function(i, log) {
			var $tableLogLineClone = $tableLogLine.clone();
			fillTableLogLine(log, $tableLogLineClone);
			$tableLogContainer.append($tableLogLineClone);
		});		
		
		if ($tableLog.is('table')) {
            var recordsTotal = $tableLog.addClass('initialized').DataTable({
            	columnDefs: { type: 'date-euro', targets: 4},
                "oLanguage": TraducaoDatatable,
                "order": [[ 4, "desc" ]]
            }).page.info().recordsTotal;
        }
	}
	
	function fillEstatisticaCategoriaChart(categoriaNomeArray, categoriaQuantidadeVendidaArray) {
		myChart = new Chart($("#estatisticaCategoria"), {
		    type: 'doughnut',
		    data: {
		        labels: categoriaNomeArray,
		        datasets: [{
		            label: '# de produtos vendidos',
		            data: categoriaQuantidadeVendidaArray,
		            backgroundColor: [
		                'rgba(255, 99, 132, 0.2)',
		                'rgba(54, 162, 235, 0.2)',
		                'rgba(255, 206, 86, 0.2)',
		                'rgba(75, 192, 192, 0.2)',
		                'rgba(153,102, 255, 0.2)',
		                'rgba(255, 159, 64, 0.2)',
		                'rgba(235, 89, 122, 0.2)',
		                'rgba(24, 142, 215, 0.2)',
		                'rgba(215, 176, 56, 0.2)',
		                'rgba(45, 152, 152, 0.2)',
		                'rgba(113, 72, 215, 0.2)',
		                'rgba(235, 129, 44, 0.2)'
		            ],
		            borderColor: [
		                'rgba(255,99,132,1)',
		                'rgba(54, 162, 235, 1)',
		                'rgba(255, 206, 86, 1)',
		                'rgba(75, 192, 192, 1)',
		                'rgba(153,102, 255, 1)',
		                'rgba(255, 159, 64, 1)',
		                'rgba(235, 89, 122, 1)',
		                'rgba(24, 142, 215, 1)',
		                'rgba(215, 176, 56, 1)',
		                'rgba(45, 152, 152, 1)',
		                'rgba(113, 72, 215, 1)',
		                'rgba(235, 129, 44, 1)'
		            ],
		            borderWidth: 1
		        }]
		    },
		    options: {
		    	title: {
		            display: true,
		            text: 'Categorias',
		            fontSize: 16
		        },
		        scales: {
		            yAxes: [{
		                ticks: {
		                    beginAtZero:true
		                }
		            }]
		        }
		    }
		});
	}

	function prepareAndFillEstatisticaCategoriaChart(estatisticaCategoriaList){
		var categoriaNomeArray = [];
		var categoriaQuantidadeVendidaArray = [];
		$.each(estatisticaCategoriaList, function(i, estatisticaCategoria) {
			categoriaNomeArray.push(estatisticaCategoria.categoria.nome);
			categoriaQuantidadeVendidaArray.push(estatisticaCategoria.quantidadeVendida);
		});
		fillEstatisticaCategoriaChart(categoriaNomeArray, categoriaQuantidadeVendidaArray);
	}
	
	function getColor(indice){
		var colorsArray = [
			'rgb(255, 99, 132)',
			'rgb(255, 159, 64)',
			'rgb(255, 205, 86)',
			'rgb(75, 192, 192)',
			'rgb(54, 162, 235)',
			'rgb(153, 102, 255)',
			'rgb(201, 203, 207)',
			'rgb(255, 99, 132)',
			'rgb(255, 159, 64)',
			'rgb(255, 205, 86)',
			'rgb(75, 192, 192)',
			'rgb(54, 162, 235)',
			'rgb(153, 102, 255)',
			'rgb(201, 203, 207)'
		];		
		return colorsArray[indice];
	}
	
	function obterFlgJaFoi(datasetsArray, tituloLivro){
		var flgJaFoi = false;
		$.each(datasetsArray, function(i, dataset){
			if(dataset.label === tituloLivro){
				flgJaFoi = true;				
			}
		});
		return flgJaFoi;
	}
	
	function getArrayQtdVendaLivroPorMes(estatisticaLivroVendidoMesList, idLivro, estatisticaLivroVendidoMes){
		var arrayQuantidades = [];
		var flgEncontrou;
		var dataAtual = new Date(estatisticaLivroVendidoMes.dataFim);
		var qtdeMeses = parseInt(((estatisticaLivroVendidoMes.dataFim - estatisticaLivroVendidoMes.dataInicio) / (31 * 1000 * 3600 * 24)) + 1);
		for(var i = qtdeMeses - 1; i >= 0; i--){
			flgEncontrou = false;
			var intData = dataAtual - (i * 31 * 1000 * 3600 * 24);
			dataParaVerificar = new Date(intData);
			if(datasEstatisticaArray.length < qtdeMeses)
				datasEstatisticaArray.push(parseInt(dataParaVerificar.getMonth() + 1) + "/"+ dataParaVerificar.getFullYear());
			$.each(estatisticaLivroVendidoMesList, function(j, estatisticaLivroVendidoMes){	
				if(idLivro === estatisticaLivroVendidoMes.livroVendido.id 
						&& dataParaVerificar.getFullYear() == estatisticaLivroVendidoMes.ano
						&& parseInt(dataParaVerificar.getMonth() + 1) == estatisticaLivroVendidoMes.mes){ 
					arrayQuantidades.push(estatisticaLivroVendidoMes.quantidade);
					flgEncontrou = true;
					return false; // break
				}  
			});						
			if(!flgEncontrou) // não encontrou a quantidade vendida deste mês			
				arrayQuantidades.push(0);
		}		
		return arrayQuantidades;
	}
	
	function getEstatisticaLivroVendidoArray(estatisticaLivroVendidoMesList, estatisticaLivroVendidoMes){
		var datasetsArray = [];
		var idLivroAtual;	
		for(var i = 0; i < estatisticaLivroVendidoMesList.length; i++){
			var objClone;
			var flgJaFoi = obterFlgJaFoi(datasetsArray, estatisticaLivroVendidoMesList[i].livroVendido.titulo);
			if(!flgJaFoi){
				idLivroAtual = estatisticaLivroVendidoMesList[i].livroVendido.id;
				objClone = {
					label: estatisticaLivroVendidoMesList[i].livroVendido.titulo,
					fill: false,
					backgroundColor: getColor(i),
					borderColor: getColor(i),
					data: getArrayQtdVendaLivroPorMes(estatisticaLivroVendidoMesList, idLivroAtual, estatisticaLivroVendidoMes)
				}
				datasetsArray.push(objClone);
			}			
		}
		return datasetsArray;
	}	
	
	function fillChartEstatisticaLivro(estatisticaLivroVendidoMesList, estatisticaLivroVendidoMes){
		var dataSets = getEstatisticaLivroVendidoArray(estatisticaLivroVendidoMesList, estatisticaLivroVendidoMes);
		var config = {
			type: 'line',
			data: {
				labels: datasEstatisticaArray,
				datasets: dataSets
			},
			options: {
				responsive: true,
				title: {
					display: true,
					text: 'Livros mais vendidos',
					fontSize: 16
				},
				tooltips: {
					mode: 'index',
					intersect: false,
				},
				hover: {
					mode: 'nearest',
					intersect: true
				},
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Mês'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Quantidade de Vendas'
						}
					}]
				}
			}
		};
		if(chartEstatisticaLivro != null)
			chartEstatisticaLivro.destroy();
		chartEstatisticaLivro = new Chart($("#estatisticaLivro"), config);
	}
	
	function getAndFillCategoriaChart(){
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/EstatisticaCategoriaWS?operacao=CONSULTAR",
            dataType: "json",
            //async: false,
            //data: JSON.stringify(pessoaCartao),
            contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
            		var estatisticaCategoriaList = respostaServlet.resultado.entidades;     
            		prepareAndFillEstatisticaCategoriaChart(estatisticaCategoriaList);
            	} else if(respostaServlet.codigoResposta == 250){ //erro!            		
            		swal({
        			  type: 'error',
        			  title: "Erro ao obter as estatísticas",
        			  html: respostaServlet.resultado.msg.replace("\n", "<br/>")
        			});
            	} else if(respostaServlet.codigoResposta == 275){ // não logado!            		
            		RedirectToLoginPage();
              	} else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }, 
            complete: function(){
            	//$("#btnSubmitModalFormLivro").prop( "disabled", false );
            }
		});
	}
	
	function getAndFillLivroVendidoMesChart(){
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/EstatisticaLivroVendidoMesWS?operacao=CONSULTAR",
            dataType: "json",
            //async: false,
            //data: JSON.stringify(pessoaCartao),
            contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
            		var estatisticaLivroVendidoMesList = respostaServlet.resultado.entidades;       
            		fillChartEstatisticaLivro(estatisticaLivroVendidoMesList);
            	} else if(respostaServlet.codigoResposta == 250){ //erro!            		
            		swal({
        			  type: 'error',
        			  title: "Erro ao obter as estatísticas",
        			  html: respostaServlet.resultado.msg.replace("\n", "<br/>")
        			});
            	} else if(respostaServlet.codigoResposta == 275){ // não logado!            		
            		RedirectToLoginPage();
              	} else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }, 
            complete: function(){
            	//$("#btnSubmitModalFormLivro").prop( "disabled", false );
            }
		});
	}
	
	function getAndFillLogs() {
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/LogWS?operacao=CONSULTAR",
            dataType: "json",
            async: false,
            //data: JSON.stringify(pessoaCartao),
            contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
            		var LogList = respostaServlet.resultado.entidades;       
            		fillTableLogContainer(LogList);
            	} else if(respostaServlet.codigoResposta == 250){ //erro!            		
            		swal({
        			  type: 'error',
        			  title: "Erro ao obter as estatísticas",
        			  html: respostaServlet.resultado.msg.replace("\n", "<br/>")
        			});
            	} else if(respostaServlet.codigoResposta == 275){ // não logado!            		
            		RedirectToLoginPage();
              	} else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }, 
            complete: function(){
            	//$("#btnSubmitModalFormLivro").prop( "disabled", false );
            }
		});
	}
	
	function fillEstatisticaVendidoMes(estatisticaLivroVendidoMes){
		datasEstatisticaArray = [];
		$.ajax({
			type: "POST",
            url: "/bookcommerce-web/EstatisticaLivroVendidoMesWS?operacao=CONSULTAR",
            dataType: "json",
            //async: false,
            data: JSON.stringify(estatisticaLivroVendidoMes),
            contentType: "application/json; charset=utf-8",
            error: function (ex) {
            	$.gritter.add({
                    title: "Erro",
                    text: ex.statusText
                });
            },
            success: function (respostaServlet) {
            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
            		var estatisticaLivroVendidoMesList = respostaServlet.resultado.entidades;       
            		fillChartEstatisticaLivro(estatisticaLivroVendidoMesList, estatisticaLivroVendidoMes);
            	} else if(respostaServlet.codigoResposta == 250){ //erro!            		
            		swal({
        			  type: 'error',
        			  title: "Erro ao obter as estatísticas",
        			  html: respostaServlet.resultado.msg.replace("\n", "<br/>")
        			});
            	} else if(respostaServlet.codigoResposta == 275){ // não logado!            		
            		RedirectToLoginPage();
              	} else { //erro!
            		$.gritter.add({
                        title: "Houve algum erro não conhecido",
                        text: "Contate a empresa desenvolvedora",
                        time: 1500
                    });
            	}
            }, 
            complete: function(){
            	//$("#btnSubmitModalFormLivro").prop( "disabled", false );
            }
		});
	}
	
	function openModalExibirJson(jsonFormated) {
		$modalExibirJson.find("#modalExibirJsonText").text(jsonFormated);
		$modalExibirJson.modal("show");
	}
	
	this.OpenModalExibirJson = function(jsonFormated) {
		openModalExibirJson(jsonFormated);
	}
	this.FillChart = function(estatisticaLivroVendidoMes) {
		getAndFillCategoriaChart();
		//fillEstatisticaVendidoMes(estatisticaLivroVendidoMes);
	}
	this.FillEstatisticaVendidoMes = function(estatisticaLivroVendidoMes) {
		fillEstatisticaVendidoMes(estatisticaLivroVendidoMes);
	}
	this.FillLogs = function() {
		getAndFillLogs();
	}
}