var TraducaoDatatable = {
	    "sEmptyTable": "Nenhum registro encontrado",
	    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
	    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
	    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
	    "sInfoPostFix": "",
	    "sInfoThousands": ".",
	    "sLengthMenu": "_MENU_ resultados por página",
	    "sLoadingRecords": "Carregando...",
	    "sProcessing": "Processando...",
	    "sZeroRecords": "Nenhum registro encontrado",
	    "sSearch": "Pesquisar",
	    "oPaginate": {
	        "sNext": "Próximo",
	        "sPrevious": "Anterior",
	        "sFirst": "Primeiro",
	        "sLast": "Último"
	    },
	    "oAria": {
	        "sSortAscending": ": Ordenar colunas de forma ascendente",
	        "sSortDescending": ": Ordenar colunas de forma descendente"
	    }
	};

function ConvertInputToDecimal(inputValue) {
    if (inputValue == null)
        return null;
    inputValue = inputValue.replace(/\D/g, "");
    if (inputValue.length > 2) {
        inputValue = inputValue.substring(0, inputValue.length - 2) + "." + inputValue.substring(inputValue.length - 2, inputValue.length);
    }
    return inputValue;
}

function FloatFormat(num, casasDepoisVirgula) {
    var n = casasDepoisVirgula;
    var x = 3;
    var s = '.';
    var c = ',';
    if (num != null) {
        num = parseFloat(num).toFixed(Math.max(0, ~~n));
        var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')';

        return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
    }
    return null;
};

//
//Adicione aqui funções e variaveis globais uteis!
//Atenção: Este arquivo é usado em diversas paginas evite conflitos. 
//

//Variaveis Globais
//Nomes devem sempre ser precedidos por g_

//Variavel utilizada no mask para atualizar o valor o campo a cada tecla precionada
g_keyPressDate = {
 onKeyPress: function (val, e, field, options) {
     field.mask(SPMaskDateBehavior.apply({}, arguments), options);
 }
};

//Variavel utilizada no mask para atualizar o valor o campo a cada tecla precionada
g_keyPressTel = {
 onKeyPress: function (val, e, field, options) {
     field.mask(SPMaskTelBehavior.apply({}, arguments), options);
 }
};

//Variavel utilizada no mask para atualizar o valor o campo a cada tecla precionada
g_keyPressCnpj = {
 onKeyPress: function (val, e, field, options) {
     field.mask(SPMaskCnpjBehavior.apply({}, arguments), options);
 }
};
//Variavel utilizada no mask para atualizar o valor o campo a cada tecla precionada
g_keyPressCnpjOrCpf = {
 onKeyPress: function (val, e, field, options) {
     field.mask(SPMaskCnpjOrCpfBehavior.apply({}, arguments), options);
 }
};

//Variaveis Locais ser precedidos por privateUtil_
//var privateUtil_


//Funcções Auxiliares 
//Atenção: Sempre descrever sua funcionalidade.

//Mascara custumizada para datas //todo: Pra que serve?
function SPMaskDateBehavior(val) {
 return val.replace(/\D/g, '').length === 8 ? '00/00/0000' : '00/00/0009';
};
//Mascara customizada para CNPJs
function SPMaskCnpjBehavior(val) {
 return val.replace(/\D/g, '').length === 14 ? '00.000.000/0000-00' : '0.000.000/0000-009';
}
//Mascara customizada para Documentos CPF ou CNPJ
function SPMaskCnpjOrCpfBehavior(val) {
 var length = val.replace(/\D/g, '').length;
 var mask;
 if (length === 14) // cnpj ?
     mask = '00.000.000/0000-00';
 else if (length === 13)
     mask = '0.000.000/0000-009';
 else  // cpf ?
     mask = '000.000.000-00999';
 return mask;
}
//Mascara custumizada para telefone fixo ou celular
var SPMaskTelBehavior = function (val) {
 return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
}
var spTelOptions = {
 onKeyPress: function (val, e, field, options) {
     field.mask(SPMaskTelBehavior.apply({}, arguments), options);
 }
};
function getOnlyNumber(text) {
 var numberPattern = /\d+/g;
 return text.match(numberPattern).join([]);
}

//Utilizada para limpar form
//Obs: Para evitar de limpar campos adicione a classe .cleanIgnore
function LimparForm(form) {
 $(':input', form)
		.not(':button, :submit, :reset, :radio, :checkbox, .cleanIgnore')
		.val('')
		.removeAttr('checked')
		.removeAttr('selected');

 $('input[type=radio]', form).prop('checked', false);
 $('input[type=checkbox]', form).prop('checked', false);
 $('select > option:first-child', form).prop('selected', true);
}

//Verifica se o e-mail é valido 
function isEmailValid(email, acceptNull) {

 if (email == '' && acceptNull)
     return true;

 var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
 return re.test(email);
}

function ConvertInputToDecimal(inputValue) {
 if (inputValue == null)
     return null;
 inputValue = inputValue.replace(/\D/g, "");
 if (inputValue.length > 2) {
     inputValue = inputValue.substring(0, inputValue.length - 2) + "." + inputValue.substring(inputValue.length - 2, inputValue.length);
 }

 return inputValue;
}

function DecimalFormat(num) {
 var n = 2;
 var x = 3;
 var s = '.';
 var c = ',';

 if (num != null) {
     num = parseFloat(num).toFixed(Math.max(0, ~~n));
     var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')';

     return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
 }

 return null;
};

function CentesimalFormat(num) {
 var n = 2;
 var x = 3;
 var s = '.';
 var c = ',';

 if (num != null) {
     num = parseFloat(num).toFixed(Math.max(0, ~~n));
     var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')';
     return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
 }

 return null;
};

//Função para exibir mensagem quando houver erros com ajax e redirecionar para chamados

function ShowErrorAlert() {
 swal({
     title: 'Erro desconhecido!',
     text: "Deseja Abrir um chamado?",
     type: 'error',
     showCancelButton: true,
     confirmButtonText: 'Sim!',
     cancelButtonText: 'Não.'
 }).then(function () {
     window.location = "/pages/mensagens/chamados.aspx";
 });
}

//Função para busca
function makeComp(input) {
 input = input.toLowerCase();
 var output = '';
 for (var i = 0; i < input.length; i++) {
     if (input.charAt(i) == 'a')
         output = output + '[aàáâãäåæ]'
     else if (input.charAt(i) == 'c')
         output = output + '[cç]';
     else if (input.charAt(i) == 'e')
         output = output + '[eèéêëæ]';
     else if (input.charAt(i) == 'i')
         output = output + '[iìíîï]';
     else if (input.charAt(i) == 'n')
         output = output + '[nñ]';
     else if (input.charAt(i) == 'o')
         output = output + '[oòóôõöø]';
     else if (input.charAt(i) == 's')
         output = output + '[sß]';
     else if (input.charAt(i) == 'u')
         output = output + '[uùúûü]';
     else if (input.charAt(i) == 'y')
         output = output + '[yÿ]'
     else
         output = output + input.charAt(i);
 }
 return output;
}


//------------------------------------------------------------------
//Utility functions for parsing in getDateFromFormat()
//------------------------------------------------------------------
function _isInteger(val) {
 var digits = "1234567890";
 for (var i = 0; i < val.length; i++) {
     if (digits.indexOf(val.charAt(i)) == -1) { return false; }
 }
 return true;
}
function _getInt(str, i, minlength, maxlength) {
 for (var x = maxlength; x >= minlength; x--) {
     var token = str.substring(i, i + x);
     if (token.length < minlength) { return null; }
     if (_isInteger(token)) { return token; }
 }
 return null;
}

function days_between(date1, date2, dateFormat) {
 // Convert both dates to milliseconds
 var convertedDate1 = getDateFromFormat(date1, dateFormat);
 var convertedDate2 = getDateFromFormat(date2, dateFormat);

 // Calculate the difference in milliseconds
 return Math.floor((convertedDate2 - convertedDate1) / (1000 * 60 * 60 * 24));
}

//------------------------------------------------------------------
//getDateFromFormat( date_string , format_string )
//
//This function takes a date string and a format string. It matches
//If the date string matches the format string, it returns the 
//getTime() of the date. If it does not match, it returns 0.
//------------------------------------------------------------------
function getDateFromFormat(val, format) {
 val = val + "";
 format = format + "";
 var i_val = 0;
 var i_format = 0;
 var c = "";
 var token = "";
 var token2 = "";
 var x, y;
 var now = new Date();
 var year = now.getYear();
 var month = now.getMonth() + 1;
 var date = 1;
 var hh = now.getHours();
 var mm = now.getMinutes();
 var ss = now.getSeconds();
 var ampm = "";

 while (i_format < format.length) {
     // Get next token from format string
     c = format.charAt(i_format);
     token = "";
     while ((format.charAt(i_format) == c) && (i_format < format.length)) {
         token += format.charAt(i_format++);
     }
     // Extract contents of value based on format token
     if (token == "yyyy" || token == "yy" || token == "y") {
         if (token == "yyyy") { x = 4; y = 4; }
         if (token == "yy") { x = 2; y = 2; }
         if (token == "y") { x = 2; y = 4; }
         year = _getInt(val, i_val, x, y);
         if (year == null) { return 0; }
         i_val += year.length;
         if (year.length == 2) {
             if (year > 70) { year = 1900 + (year - 0); }
             else { year = 2000 + (year - 0); }
         }
     }
     else if (token == "MMM" || token == "NNN") {
         month = 0;
         for (var i = 0; i < MONTH_NAMES.length; i++) {
             var month_name = MONTH_NAMES[i];
             if (val.substring(i_val, i_val + month_name.length).toLowerCase() == month_name.toLowerCase()) {
                 if (token == "MMM" || (token == "NNN" && i > 11)) {
                     month = i + 1;
                     if (month > 12) { month -= 12; }
                     i_val += month_name.length;
                     break;
                 }
             }
         }
         if ((month < 1) || (month > 12)) { return 0; }
     }
     else if (token == "EE" || token == "E") {
         for (var i = 0; i < DAY_NAMES.length; i++) {
             var day_name = DAY_NAMES[i];
             if (val.substring(i_val, i_val + day_name.length).toLowerCase() == day_name.toLowerCase()) {
                 i_val += day_name.length;
                 break;
             }
         }
     }
     else if (token == "MM" || token == "M") {
         month = _getInt(val, i_val, token.length, 2);
         if (month == null || (month < 1) || (month > 12)) { return 0; }
         i_val += month.length;
     }
     else if (token == "dd" || token == "d") {
         date = _getInt(val, i_val, token.length, 2);
         if (date == null || (date < 1) || (date > 31)) { return 0; }
         i_val += date.length;
     }
     else if (token == "hh" || token == "h") {
         hh = _getInt(val, i_val, token.length, 2);
         if (hh == null || (hh < 1) || (hh > 12)) { return 0; }
         i_val += hh.length;
     }
     else if (token == "HH" || token == "H") {
         hh = _getInt(val, i_val, token.length, 2);
         if (hh == null || (hh < 0) || (hh > 23)) { return 0; }
         i_val += hh.length;
     }
     else if (token == "KK" || token == "K") {
         hh = _getInt(val, i_val, token.length, 2);
         if (hh == null || (hh < 0) || (hh > 11)) { return 0; }
         i_val += hh.length;
     }
     else if (token == "kk" || token == "k") {
         hh = _getInt(val, i_val, token.length, 2);
         if (hh == null || (hh < 1) || (hh > 24)) { return 0; }
         i_val += hh.length; hh--;
     }
     else if (token == "mm" || token == "m") {
         mm = _getInt(val, i_val, token.length, 2);
         if (mm == null || (mm < 0) || (mm > 59)) { return 0; }
         i_val += mm.length;
     }
     else if (token == "ss" || token == "s") {
         ss = _getInt(val, i_val, token.length, 2);
         if (ss == null || (ss < 0) || (ss > 59)) { return 0; }
         i_val += ss.length;
     }
     else if (token == "a") {
         if (val.substring(i_val, i_val + 2).toLowerCase() == "am") { ampm = "AM"; }
         else if (val.substring(i_val, i_val + 2).toLowerCase() == "pm") { ampm = "PM"; }
         else { return 0; }
         i_val += 2;
     }
     else {
         if (val.substring(i_val, i_val + token.length) != token) { return 0; }
         else { i_val += token.length; }
     }
 }
 // If there are any trailing characters left in the value, it doesn't match
 if (i_val != val.length) { return 0; }
 // Is date valid for month?
 if (month == 2) {
     // Check for leap year
     if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) { // leap year
         if (date > 29) { return 0; }
     }
     else { if (date > 28) { return 0; } }
 }
 if ((month == 4) || (month == 6) || (month == 9) || (month == 11)) {
     if (date > 30) { return 0; }
 }
 // Correct hours value
 if (hh < 12 && ampm == "PM") { hh = hh - 0 + 12; }
 else if (hh > 11 && ampm == "AM") { hh -= 12; }
 var newdate = new Date(year, month - 1, date, hh, mm, ss);
 return newdate;
}


//## Filtro para lista e tabelas
//header is any element, list is an unordered list
function listFilter(header, list, componentType) {
 // $('.filterform').remove();
 // create and add the filter form to the header
 if (header.find('.filterform').length)
     return;

 var form = $("<form>").attr(
     { "class": "filterform", "action": "#" })
     , input = $("<input>").attr({ "class": "filterinput form-control", "type": "text", "placeholder": "Digite para filtrar..." });

 $(form).append(input).appendTo(header);

 $(input).change(function () {
     var filter = $(this).val();
     // alert(filter);
     if (filter) {
         // this finds all links in a list that contain the input,
         // and hide the ones not containing the input while showing the ones that do
         $(list).find(componentType + ":not(:Contains(" + filter + "))").slideUp();
         $(list).find(componentType + ":Contains(" + filter + ")").slideDown();
         $(list).children().find(componentType + ":not(:Contains(" + filter + "))").slideUp();
         $(list).children().find(componentType + ":Contains(" + filter + ")").slideDown();
     } else {
         $(list).find(componentType).slideDown();
     }
     return false;
 }).keyup(function () {
     // fire the above change event after every letter
     $(this).change();
 });
}

//verifica se está usando mobile
//retorno: true - Está usando mobile
//        false - usando pc
function detectarMobile() {
 var check = false; //wrapper no check
 (function (a) { if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true })(navigator.userAgent || navigator.vendor || window.opera);
 return check;
}

//Plugin clickoutside <http://www.stoimen.com/jquery.clickoutside.plugin/> acesso: 02/08/2017
(function (jQuery) {
 jQuery.fn.clickoutside = function (callback) {
     var outside = 1, self = $(this);
     self.cb = callback;
     this.click(function () {
         outside = 0;
     });
     $(document).click(function () {
         outside && self.cb();
         outside = 1;
     });
     return $(this);
 }
})(jQuery);

var dateFormat = function () {
	var	token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
		timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
		timezoneClip = /[^-+\dA-Z]/g,
		pad = function (val, len) {
			val = String(val);
			len = len || 2;
			while (val.length < len) val = "0" + val;
			return val;
		};

	// Regexes and supporting functions are cached through closure
	return function (date, mask, utc) {
		var dF = dateFormat;

		// You can't provide utc if you skip other args (use the "UTC:" mask prefix)
		if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
			mask = date;
			date = undefined;
		}

		// Passing date through Date applies Date.parse, if necessary
		date = date ? new Date(date) : new Date;
		if (isNaN(date)) throw SyntaxError("invalid date");

		mask = String(dF.masks[mask] || mask || dF.masks["default"]);

		// Allow setting the utc argument via the mask
		if (mask.slice(0, 4) == "UTC:") {
			mask = mask.slice(4);
			utc = true;
		}

		var	_ = utc ? "getUTC" : "get",
			d = date[_ + "Date"](),
			D = date[_ + "Day"](),
			m = date[_ + "Month"](),
			y = date[_ + "FullYear"](),
			H = date[_ + "Hours"](),
			M = date[_ + "Minutes"](),
			s = date[_ + "Seconds"](),
			L = date[_ + "Milliseconds"](),
			o = utc ? 0 : date.getTimezoneOffset(),
			flags = {
				d:    d,
				dd:   pad(d),
				ddd:  dF.i18n.dayNames[D],
				dddd: dF.i18n.dayNames[D + 7],
				m:    m + 1,
				mm:   pad(m + 1),
				mmm:  dF.i18n.monthNames[m],
				mmmm: dF.i18n.monthNames[m + 12],
				yy:   String(y).slice(2),
				yyyy: y,
				h:    H % 12 || 12,
				hh:   pad(H % 12 || 12),
				H:    H,
				HH:   pad(H),
				M:    M,
				MM:   pad(M),
				s:    s,
				ss:   pad(s),
				l:    pad(L, 3),
				L:    pad(L > 99 ? Math.round(L / 10) : L),
				t:    H < 12 ? "a"  : "p",
				tt:   H < 12 ? "am" : "pm",
				T:    H < 12 ? "A"  : "P",
				TT:   H < 12 ? "AM" : "PM",
				Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
				o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
				S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
			};

		return mask.replace(token, function ($0) {
			return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
		});
	};
}();

// Some common format strings
dateFormat.masks = {
	"default":      "ddd mmm dd yyyy HH:MM:ss",
	shortDate:      "m/d/yy",
	mediumDate:     "mmm d, yyyy",
	longDate:       "mmmm d, yyyy",
	fullDate:       "dddd, mmmm d, yyyy",
	shortTime:      "h:MM TT",
	mediumTime:     "h:MM:ss TT",
	longTime:       "h:MM:ss TT Z",
	isoDate:        "yyyy-mm-dd",
	isoTime:        "HH:MM:ss",
	isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
	isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
dateFormat.i18n = {
	dayNames: [
		"Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab",
		"Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"
	],
	monthNames: [
		"Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez",
		"Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"
	]
};

// For convenience...
Date.prototype.format = function (mask, utc) {
	return dateFormat(this, mask, utc);
};

function calcularTotalDescontoCarrinhoCupom(carrinho){
	var totalGeral = getTodalCarrinhoItem(carrinho.carrinhoItemList);
	var totalComDescontos = totalGeral;
	var carrinhoCupomList = carrinho.carrinhoCupomList;
	// Adiciona o cupom que não é de devolução
	$.each(carrinhoCupomList, function(i, carrinhoCupom){
		var cupom = carrinhoCupom.cupom;
		if(cupom.cupomTipo.id !== 3){ // não é cupom de devolução? pode apenas 1
			if(cupom.cupomDescontoTipo.id == 1){ // Cupom de porcentagem?
				totalComDescontos = totalGeral * ((100.0 - cupom.valorDesconto) / 100.0);
			} else {
				totalComDescontos -= cupom.valorDesconto;
			}
		}
		return;
	});
	// Adiciona os cupons que são de devolução
	$.each(carrinhoCupomList, function(i, carrinhoCupom){
		var cupom = carrinhoCupom.cupom;
		if(cupom.cupomTipo.id == 3){ // não é cupom de devolução? pode apenas 1
			if(cupom.cupomDescontoTipo.id == 1){ // Cupom de porcentagem?
				totalComDescontos = totalGeral * (100 - cupom.valorDesconto / 100);
			} else {
				totalComDescontos -= cupom.valorDesconto;
			}
		}
	});
	return totalComDescontos;
}

function getTodalCarrinhoItem(carrinhoItemList){
	if(carrinhoItemList == null)
		return;
	var soma = 0;
	$.each(carrinhoItemList, function(i, carrinhoItem) {
		soma += carrinhoItem.precoItem * carrinhoItem.quantidade;
	});
	return soma;
}

function formatDate(date) {
	var hours = date.getHours();
	var minutes = date.getMinutes();
	var ampm = hours >= 12 ? 'pm' : 'am';
	hours = hours % 12;
	hours = hours ? hours : 12; // the hour '0' should be '12'
	minutes = minutes < 10 ? '0'+minutes : minutes;
	var strTime = hours + ':' + minutes + ' ' + ampm;
	var mounth = date.getMonth()+1;
	return date.getDate() + "/" + mounth + "/" + date.getFullYear() + "  " + strTime;
}

function formatOnlyDate(date) {
	var hours = date.getHours();
	var minutes = date.getMinutes();
	var ampm = hours >= 12 ? 'pm' : 'am';
	hours = hours % 12;
	hours = hours ? hours : 12; // the hour '0' should be '12'
	minutes = minutes < 10 ? '0'+minutes : minutes;
	var strTime = hours + ':' + minutes + ' ' + ampm;
	var mounth = date.getMonth()+1;
	return date.getDate() + "/" + mounth + "/" + date.getFullYear();
}

Date.isLeapYear = function (year) { 
    return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0)); 
};

Date.getDaysInMonth = function (year, month) {
    return [31, (Date.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
};

Date.prototype.isLeapYear = function () { 
    return Date.isLeapYear(this.getFullYear()); 
};

Date.prototype.getDaysInMonth = function () { 
    return Date.getDaysInMonth(this.getFullYear(), this.getMonth());
};
Date.prototype.addMonths = function (value) {
    var n = this.getDate();
    this.setDate(1);
    this.setMonth(this.getMonth() + value);
    this.setDate(Math.min(n, this.getDaysInMonth()));
    return this;
};
function RedirectToLoginPage(){
	window.location.href = "/bookcommerce-web/login.jsp";
}


function getDateFromDatepiker(dateBr){
	var dataNascimentoBr = dateBr;
	var dataNascimentoUs =  dataNascimentoBr.substring(6,10) + "-" + dataNascimentoBr.substring(3,5) + "-" + dataNascimentoBr.substring(0,2) + " 00:00";	
	return new Date(dataNascimentoUs);	
}

$.urlParam = function (name) {
    var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
    //decodeURIComponent()
    if (results == null)
        return null;
    return results[1] || 0;
};