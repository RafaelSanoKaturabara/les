<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:import url="master/header.jsp" />
<c:import url="modal/modalOrdemCompra.jsp" />
<style>
	.btnAbrirOrdemCompra{
		cursor: pointer;
	}
</style>
<script src="/bookcommerce-web/js/minhasComprasController.js" charset="utf-8"></script>
<script>
	$(document).ready(function (){
		$(document).on("click", ".btnAbrirOrdemCompra", function(){
			$("#modalOrdemCompra").modal("show");
		});
		
		var selectors = {
			minhasComprasContainer: "#minhasComprasContainer"				
		}		
		var minhasComprasController = new MinhasComprasController(selectors);
		minhasComprasController.PrintMinhasCompras();
		
		$(document).on("click", ".btnSolicitarTroca", function(){
			var minhasCompras = $(this).parent().parent().parent().parent(".minhasComprasLine").data("minhasCompras");
			
			swal({
				  title: 'Realmente deseja devolver?',
				  text: "Apenas clique se tiver certeza!",
				  type: 'warning',
				  showCancelButton: true,
				  reverseButtons: true,
				  confirmButtonClass: 'btn btn-primary',
				  cancelButtonClass: 'btn btn-danger',
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Sim, quero trocar!'
				}).then(function(result){
				  if (result.value) {
					  minhasComprasController.SolicitarTroca(minhasCompras);
				  }
				});
		});
		
		$(document).on("click", ".btnConfirmarRecebimento", function(){
			var minhasCompras = $(this).parent().parent().parent().parent(".minhasComprasLine").data("minhasCompras");
			
			swal({
				  title: 'Confirmar recebimento?',
				  text: "Você já recebeu a encomenda?",
				  type: 'warning',
				  showCancelButton: true,
				  reverseButtons: true,
				  confirmButtonClass: 'btn btn-primary',
				  cancelButtonClass: 'btn btn-danger',
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Sim, recebi!'
				}).then(function(result){
				  if (result.value) {
					  minhasComprasController.ConfirmarRecebimento(minhasCompras);
				  }
				});
		});
	});
</script>
<div class="container">
	<h4>Minhas Compras</h4>
	<div id="minhasComprasContainer">
		<div class="card minhasComprasLine">
		    <div class="card-header btnAbrirOrdemCompra">Ordem de compra: <b class="minhasComprasId">0001</b></div>
		    <div class="card-body">
		    	<div class="row">
		            <div class="col-md-4">
		                <p><small class="text-muted">Status: </small><b class="minhasComprasStatus">badge badge-warning</b></p>	                
		                <p class="erro" style="display: none"><small class="text-muted">Cupom: </small><b class="minhasComprasCupom">G5JIU4W</b></p>
		            </div>
		            <div class="col-md-4">
		                <p><small class="text-muted">Apelido do endereço: </small><b class="minhasComprasEnderecoApelido">4</b></p>
		                <p class="minhasComprasCupomVisibilidade"><small class="text-muted">Cupom de devolução: </small><b class="minhasComprasCupomCodigo">4</b></p>
		            </div>
		            <div class="col-md-4">	               
		                <p><small class="text-muted">Data e hora: </small><b class="minhasComprasDataCompra">09/11/2017 13:51h</b></p>
		            	<button class="btn btn-secondary btnSolicitarTroca" style="display: none">Solicitar Troca</button> 
		            	
		            	<button class="btn btn-primary btnConfirmarRecebimento" style="display: none">Confirmar Recebimento</button> 
		            </div>	    	
		    	</div>  
		    </div>    
		</div>
	</div>
</div>
<c:import url="master/footer.jsp" />