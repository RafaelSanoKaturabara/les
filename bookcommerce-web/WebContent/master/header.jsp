<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="UTF-8"%>
<%@ page import="bookcommerce.dominio.*, java.util.*"%>
<!DOCTYPE html>
<head >	
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="pt-br">
	<link rel="stylesheet" href="/bookcommerce-web/bootstrap/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="/bookcommerce-web/bootstrap/css/bootstrap-chosen.css"/>
	<link rel="stylesheet" href="/bookcommerce-web/bootstrap/css/chosen.min.css"/>
	<link rel="stylesheet" href="/bookcommerce-web/fontsIcons/css/fontawesome-all.css" />
	<link rel="stylesheet" href="/bookcommerce-web/bootstrap/css/bootstrap-toggle.min.css" />
	<link rel="stylesheet" href="/bookcommerce-web/Gritter-master/css/jquery.gritter.css"/>
	<link rel="stylesheet" href="/bookcommerce-web/datatables/datatables.min.css"/>
	<link rel="stylesheet" href="/bookcommerce-web/css/bookcommerce.css"/>
	<link rel="stylesheet" href="/bookcommerce-web/sweetalert2/sweetalert2.min.css">
	<link rel="icon" href="/bookcommerce-web/imagens/favicon.ico" type="image/x-icon"/>
	<link rel="stylesheet" href="/bookcommerce-web/jquery-ui-1.12.1.custom/jquery-ui.min.css">
	<title>LES - Laboratório de Engenharia de Software</title>
</head>
<body>
	<script src="/bookcommerce-web/js/jquery-3.2.1.min.js" charset="utf-8"></script>
	<script src="/bookcommerce-web/js/popper.min.js" charset="utf-8"></script>
	<script src="/bookcommerce-web/js/util.js" charset="utf-8"></script>
	<script src="/bookcommerce-web/bootstrap/js/bootstrap.min.js" charset="utf-8"></script>
	<script src="/bookcommerce-web/bootstrap/js/chosen.jquery.min.js" charset="utf-8"></script>
	<script src="/bookcommerce-web/bootstrap/js/chosen.proto.min.js" charset="utf-8"></script>
	<script src="/bookcommerce-web/bootstrap/js/bootstrap-toggle.min.js" charset="utf-8"></script>
	<script src="/bookcommerce-web/Gritter-master/js/jquery.gritter.min.js"  charset="utf-8"></script>
	<script src="/bookcommerce-web/datatables/datatables.min.js"  charset="utf-8"></script>
	<script src="/bookcommerce-web/sweetalert2/sweetalert2.all.min.js"></script>
	<script src="/bookcommerce-web/sweetalert2/sweetalert2.min.js"></script>
	<script src="/bookcommerce-web/mask/jquery.mask.min.js"></script>
	<script src="/bookcommerce-web/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
	<script>
		var PessoaLogada; // variavel global da pessoa logada
		$(document).ready(function(){
			$("a[href|='" + window.location.pathname + "']").parent().addClass("active").parent(".dropdown").addClass("active");
			
			
			// forma de mostrar a pessoa logada mudou
			var pessoaLogadaJson = '<%= session.getAttribute("pessoaLogadaJson") %>';
			if(pessoaLogadaJson == null || pessoaLogadaJson == 'null'){ // não tem ninguem logado?
				$("#pessoaLogada").text("Efetue o login");		
				$(".flgNaoLogado").show();
				$(".flgLogado").hide();	
				if(window.location.pathname != "/bookcommerce-web/login.jsp" && window.location.pathname != "/bookcommerce-web/default.jsp")
					RedirectToLoginPage();
			} else {
				PessoaLogada = JSON.parse(pessoaLogadaJson);
				$("#pessoaLogada").text(PessoaLogada.nome + " " + PessoaLogada.sobrenome);	
				$(".flgNaoLogado").hide();
				$(".flgLogado").show();
			}
			
			$("#btnPesquisarLivro").click(function(e) {
				e.preventDefault();
				if(window.location.pathname.toLowerCase() != "/bookcommerce-web/default.jsp")
					window.location.href = "/bookcommerce-web/default.jsp?param=" + $("#inputPesquisaLivro").val();
					
			});
			
			$("#btnDeslogar").click(function(){
				$.ajax({
					type: "POST",
		            url: "/bookcommerce-web/ServletLogoff",
// 		            dataType: "json",
// 		            contentType: "application/json; charset=utf-8",
		            error: function (ex) {
		            	$.gritter.add({
		                    title: "Erro",
		                    text: ex.statusText
		                });
		            },
		            success: function (respostaServlet) {
		            		window.location.href = "/bookcommerce-web/login.jsp";
		            }, 
		            complete: function(){
		            	//$("#btnSubmitModalFormLivro").prop( "disabled", false );
		            }
				});
			});
			
			$("#btnAbrirModalAlterarSenha").click(function(){
				$("#modalAlterarSenha").modal("show");			
			});
			
			$("#btnModalAlterarSenha").click(function(){
				alterarSenha();		
			});
			
			// Da para levar para javascript
			function readPessoaSenha(){
				var pessoa = {
					senha: $("#FormModalAlterarSenha").find("#modalAlterarSenha").val(),
					repitaSenha: $("#FormModalAlterarSenha").find("#modalAlterarRepitaSenha").val()
				}				
				return pessoa;
			}
			
			function alterarSenha(){
				var pessoa = readPessoaSenha();
				$.ajax({
					type: "POST",
		            url: "/bookcommerce-web/SalvarSenhaWS?operacao=ALTERAR",
		            dataType: "json",
		            data: JSON.stringify(pessoa),
		            contentType: "application/json; charset=utf-8",
		            error: function (ex) {
		            	$.gritter.add({
		                    title: "Erro",
		                    text: ex.statusText
		                });
		            },
		            success: function (respostaServlet) {
		            	if(respostaServlet.codigoResposta == 200){ // Cadastro sucesso!
		            		$.gritter.add({
		                        title: "Sucesso",
		                        text: "Senha alterada com sucesso",
		                        time: 1500
		                    });            		
		            		$("#modalAlterarSenha").modal("hide");	
		            		$("#FormModalAlterarSenha").find("#modalAlterarSenha").val("");
							$("#FormModalAlterarSenha").find("#modalAlterarRepitaSenha").val("");		            		
		            	} else if(respostaServlet.codigoResposta == 250){ //erro!            		
		            		swal({
			          			  type: 'error',
			          			  title: respostaServlet.mensagemResposta,
			          			  html: respostaServlet.resultado.msg.replace("\n", "<br/>")
		          			});
		            	} else if(respostaServlet.codigoResposta == 275){ //erro!        
		            		window.location.href = "/bookcommerce-web/login.jsp";
		            	} else { //erro!
		            		$.gritter.add({
		                        title: "Houve algum erro não conhecido",
		                        text: "Contate a empresa desenvolvedora",
		                        time: 1500
		                    });
		            	}
		            }, 
		            complete: function(){
		            	//$("#btnSubmitModalFormLivro").prop( "disabled", false );
		            }
				});
			}
			
		});
	</script>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="/bookcommerce-web/default.jsp">BookCommerce</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="/bookcommerce-web/default.jsp">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/bookcommerce-web/carrinho.jsp">Carrinho<i class="fas fa-shopping-cart"></i></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/bookcommerce-web/minhas-compras.jsp">Compras</a>
      </li>     
      <li class="nav-item dropdown navItemAdministrador flgLogado">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Administrador
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown1">
	        <a class="dropdown-item" href="/bookcommerce-web/adm/estatisticas.jsp">Dashboard</a>
	        <a class="dropdown-item" href="/bookcommerce-web/adm/cadastro-livro.jsp">Livros</a>
	        <a class="dropdown-item" href="/bookcommerce-web/adm/vendas.jsp">Vendas</a>
	        <a class="dropdown-item" href="/bookcommerce-web/adm/entrada-livros.jsp">Entradas</a>
	        <a class="dropdown-item" href="/bookcommerce-web/adm/cupons.jsp">Cupons</a>
	        <a class="dropdown-item" href="/bookcommerce-web/adm/pessoas.jsp">Pessoas</a>     
        </div>
      </li>
      <li class="nav-item" style="display: none">
        <a class="nav-link disabled" href="#">Disabled</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
	    <ul class="navbar-nav mr-auto text-right">
	      <li class="nav-item dropdown">
	        <a class="nav-link dropdown-toggle" href="#" id="pessoaLogada" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	          
	        </a>
	        <div class="dropdown-menu" aria-labelledby="pessoaLogada">
	          <a class="dropdown-item flgNaoLogado" href="/bookcommerce-web/login.jsp">Login</a>
	          <a class="dropdown-item flgLogado" href="/bookcommerce-web/perfil.jsp">Editar</a>
	          <a class="dropdown-item flgLogado" href="#" id="btnAbrirModalAlterarSenha">Alterar Senha</a>
	          <div class="dropdown-divider flgLogado"></div>
	          <a class="dropdown-item flgLogado" id="btnDeslogar" href="#">Sair</a>
	        </div>
	      </li>
	    </ul>
      	<input id="inputPesquisaLivro" class="form-control mr-sm-2" type="search" placeholder="Titulo, autor, categoria e etc..." aria-label="Search">
      	<button class="btn btn-outline-success my-2 my-sm-0" id="btnPesquisarLivro">Pesquisar</button>
    </form>
  </div>
</nav>
<div class="modal fade" id="modalAlterarSenha" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <b class="modal-title" id="exampleModalLongTitle">Reenvio de Senha</b>        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"> 
      	<form id="FormModalAlterarSenha">
		  <div class="form-group">
		    <label for="modalAlterarSenha">Digite a sua nova senha</label>
		    <input type="password" class="form-control" id="modalAlterarSenha" aria-describedby="emailHelp" placeholder="Digite a sua nova senha">
		  </div>
		  <div class="form-group">
		    <label for="modalAlterarRepitaSenha">Redigite a sua nova senha</label>
		    <input type="password" class="form-control" id="modalAlterarRepitaSenha" aria-describedby="emailHelp" placeholder="Redigite a sua nova senha">
		  </div>
		</form>			
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" id="btnModalAlterarSenha" class="btn btn-primary">Alterar</button>
      </div>
    </div>
  </div>
</div>	