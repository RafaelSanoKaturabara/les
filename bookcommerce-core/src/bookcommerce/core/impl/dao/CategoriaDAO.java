package bookcommerce.core.impl.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bookcommerce.dominio.Categoria;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.GrupoPrecificacao;

public class CategoriaDAO extends AbstractJdbcDAO{

	public CategoriaDAO() {
		super("BookCommerce.Categoria", "id");
		// TODO Auto-generated constructor stub
	}

	@Override
	public void salvar(EntidadeDominio entidade) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void alterar(EntidadeDominio entidade) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) throws SQLException {
		PreparedStatement pst = null;		
		Categoria categoria = (Categoria)entidade;
		String sql=null;
		
		String Id; 
		if(categoria.getId() != null && categoria.getId() > 0){
			Id = Integer.toString(categoria.getId());
		} else
			Id = "null";
		
		sql = " select * "
				+ " from BookCommerce.Categoria "
				+ " where "+ Id + " is null or Id = " + Id
				+ ";";	
		try {
			openConnection();
			pst = connection.prepareStatement(sql);		
			ResultSet rs = pst.executeQuery();
			List<EntidadeDominio> categoriaList = new ArrayList<EntidadeDominio>();
			while (rs.next()) {
				GrupoPrecificacao c = new GrupoPrecificacao();
				c.setId(rs.getInt("Id"));
				c.setNome(rs.getString("Nome"));
				categoriaList.add(c);
			}
			return categoriaList;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
