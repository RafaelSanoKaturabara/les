package bookcommerce.core.impl.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import bookcommerce.dominio.CartaoCreditoTipo;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.Pessoa;
import bookcommerce.dominio.PessoaCartaoCredito;
import bookcommerce.dominio.PessoaEndereco;

public class PessoaCartaoCreditoDAO extends AbstractJdbcDAO {

	public PessoaCartaoCreditoDAO() {
		super("BookCommerce.PessoaCartaoCredito", "Id");
	}
	public PessoaCartaoCreditoDAO(Connection connection) {
		super(connection, "BookCommerce.PessoaCartaoCredito", "Id");
	}

	@Override
	public void salvar(EntidadeDominio entidade) throws SQLException {
		PreparedStatement pst=null;
		StringBuilder sql = new StringBuilder();
		try {
			if(connection == null){
				openConnection();
			}else {
				ctrlTransaction = false;
			}
			PessoaCartaoCredito pessoaCartao = (PessoaCartaoCredito)entidade;
			
			sql.append(" insert into BookCommerce.PessoaCartaoCredito (Numero, NomeImpresso, CodigoSeguranca, IdPessoa, IdCartaoCreditoTipo, Apelido) ");
			sql.append(" values ( ?, ?, ?, ?, ?, ? ); ");	
			
			connection.setAutoCommit(false);
			pst = connection.prepareStatement(sql.toString(), 
					Statement.RETURN_GENERATED_KEYS);
			
			int i = 1;
			pst.setString(i++, pessoaCartao.getNumero());
			pst.setString(i++, pessoaCartao.getNomeImpresso());
			pst.setString(i++, pessoaCartao.getCodigoSeguranca());
			pst.setInt(i++, pessoaCartao.getIdPessoa());
			pst.setInt(i++, pessoaCartao.getCartaoCreditoTipo().getId());	
			pst.setString(i++, pessoaCartao.getApelido());		
			
			pst.executeUpdate();		
					
			ResultSet rs = pst.getGeneratedKeys();
			int idEnd=0;
			if(rs.next())
				idEnd = rs.getInt(1);
			pessoaCartao.setId(idEnd);
			
			if(ctrlTransaction) // commita apenas se não está na conexão de outra classe
				connection.commit();					
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();	
		}finally{
			if(ctrlTransaction){
				try {
					pst.close();
					if(ctrlTransaction)
						connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}			
		}	
	}

	@Override
	public void alterar(EntidadeDominio entidade) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) throws SQLException {
		PreparedStatement pst = null;

		PessoaCartaoCredito pessoaCartaoCredito = (PessoaCartaoCredito) entidade;
		String sql = null;

		sql = " select pcc.*" + 
				"	, cct.Id 'cct_Id'" + 
				"	, cct.Descricao 'cct_Descricao' " + 
				"	, pe.IdCartaoPadrao 'pe_IdCartaoPadrao'" + 
				"	, pe.IdEnderecoPadrao 'pe_IdEnderecoPadrao'" +
				" from BookCommerce.PessoaCartaoCredito pcc" + 
				"	join BookCommerce.CartaoCreditoTipo cct on pcc.IdCartaoCreditoTipo = cct.Id" + 
				"   join BookCommerce.Pessoa pe on pcc.IdPessoa = pe.Id " +
				" where " + 
				"	( ? is null or pcc.IdPessoa = ?) ;";
		try {
			openConnection();
			pst = connection.prepareStatement(sql);
			int i = 1;
			if (pessoaCartaoCredito.getIdPessoa() == 0) {
				pst.setNull(i++, Types.INTEGER);
				pst.setNull(i++, Types.INTEGER);
			} else {
				pst.setInt(i++, pessoaCartaoCredito.getIdPessoa());
				pst.setInt(i++, pessoaCartaoCredito.getIdPessoa());
			}

			ResultSet rs = pst.executeQuery();
			List<EntidadeDominio> PessoaCartaoCreditoList = new ArrayList<EntidadeDominio>();
			while (rs.next()) {
				PessoaCartaoCredito pe = new PessoaCartaoCredito();

				pe.setId(rs.getInt("Id"));
				pe.setNumero(rs.getString("Numero"));
				pe.setNomeImpresso(rs.getString("NomeImpresso"));
				pe.setCodigoSeguranca(rs.getString("CodigoSeguranca"));
				pe.setApelido(rs.getString("Apelido"));
				pe.setIdPessoa(rs.getInt("IdPessoa"));
				
				pe.setCartaoCreditoTipo(new CartaoCreditoTipo());
				pe.getCartaoCreditoTipo().setId(rs.getInt("cct_Id"));
				pe.getCartaoCreditoTipo().setDescricao(rs.getString("cct_Descricao"));
				
				pe.setPessoa(new Pessoa());
				pe.getPessoa().setCartaoCreditoPadrao(new PessoaCartaoCredito());
				pe.getPessoa().getCartaoCreditoPadrao().setId(rs.getInt("pe_IdCartaoPadrao"));				

				pe.getPessoa().setEnderecoPadrao(new PessoaEndereco());
				pe.getPessoa().getEnderecoPadrao().setId(rs.getInt("pe_IdEnderecoPadrao"));
				
				PessoaCartaoCreditoList.add(pe);
			}
			return PessoaCartaoCreditoList;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
