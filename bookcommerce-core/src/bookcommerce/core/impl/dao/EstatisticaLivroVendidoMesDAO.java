package bookcommerce.core.impl.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bookcommerce.core.util.ConverteDate;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.EstatisticaLivroVendidoMes;
import bookcommerce.dominio.Livro;

public class EstatisticaLivroVendidoMesDAO extends AbstractJdbcDAO {

	public EstatisticaLivroVendidoMesDAO() {
		super("", "");
	}

	@Override
	public void salvar(EntidadeDominio entidade) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void alterar(EntidadeDominio entidade) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) throws SQLException {
		PreparedStatement pst = null;		
		EstatisticaLivroVendidoMes estatisticaLivroVendidoMes = (EstatisticaLivroVendidoMes)entidade;
				
		String sql=null;
		
		sql = " select " + 
				"	year(c.DataUltimaAlteracao) 'ano'" + 
				"	, month( c.DataUltimaAlteracao) 'mes'" + 
				"	, sum(ci.Quantidade) 'quantidade'" + 
				"	, l.Id 'l_Id'" + 
				"	, l.Titulo 'l_Titulo'" + 
				"	from BookCommerce.CarrinhoItem ci" + 
				"	join BookCommerce.carrinho c on ci.IdCarrinho = c.Id" + 
				"	join BookCommerce.livro l on ci.IdLivro = l.Id" + 
				" where " + 
				" c.IdCarrinhoStatus <> 1\r\n" + 
				"	and c.IdCarrinhoStatus <> 7" +
				"	and l.Id in (select @qtdeTop IdLivro  from BookCommerce.CarrinhoItem group by IdLivro order by sum(Quantidade) desc)" + 
				" and c.DataUltimaAlteracao >= ? and c.DataUltimaAlteracao <= ? " +
				" group by" + 
				"	year(c.DataUltimaAlteracao)" + 
				"	, month( c.DataUltimaAlteracao)" + 
				"	, l.Id" + 
				"	, l.Titulo" + 
				" order by " + 
				"	'ano' desc" + 
				"	, 'mes' desc ";
		
		try {
			if(connection == null) {
				openConnection();
				connection.setAutoCommit(false);
			}
			sql = sql.replace("@qtdeTop", "top " + estatisticaLivroVendidoMes.getQtdeTop());
			pst = connection.prepareStatement(sql);
						
			int i = 1;
			pst.setTimestamp(i++, ConverteDate.Convert(estatisticaLivroVendidoMes.getDataInicio()));
			pst.setTimestamp(i++, ConverteDate.Convert(estatisticaLivroVendidoMes.getDataFim()));
			
			ResultSet rs = pst.executeQuery();
			List<EntidadeDominio> estatisticaLivroVendidoMesList = new ArrayList<EntidadeDominio>();
			while (rs.next()) {
				EstatisticaLivroVendidoMes elvm = new EstatisticaLivroVendidoMes();
				elvm.setQuantidade(rs.getInt("quantidade"));
				elvm.setMes(rs.getString("mes"));
				elvm.setAno(rs.getString("ano"));
				
				elvm.setLivroVendido(new Livro());
				elvm.getLivroVendido().setId(rs.getInt("l_Id"));
				elvm.getLivroVendido().setTitulo(rs.getString("l_Titulo"));	
						
				estatisticaLivroVendidoMesList.add(elvm);
			}		
			return estatisticaLivroVendidoMesList;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;	
	}
}
