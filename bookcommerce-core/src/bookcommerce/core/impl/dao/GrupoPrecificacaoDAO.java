package bookcommerce.core.impl.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.GrupoPrecificacao;

public class GrupoPrecificacaoDAO extends AbstractJdbcDAO{

	public GrupoPrecificacaoDAO() {
		super("BookCommerce.GrupoPrecificacao", "id");		
	}

	@Override
	public void salvar(EntidadeDominio entidade) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void alterar(EntidadeDominio entidade) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) throws SQLException {
		PreparedStatement pst = null;		
		GrupoPrecificacao grupoPrecificacao = (GrupoPrecificacao)entidade;
		String sql=null;
		
		String Id; 
		if(grupoPrecificacao.getId() != null && grupoPrecificacao.getId() > 0){
			Id = Integer.toString(grupoPrecificacao.getId());
		} else
			Id = "null";
		
		sql = " select * "
				+ " from BookCommerce.GrupoPrecificacao "
				+ " where "+ Id + " is null or Id = " + Id
				+ ";";	
		try {
			openConnection();
			pst = connection.prepareStatement(sql);		
			ResultSet rs = pst.executeQuery();
			List<EntidadeDominio> grupoPrecificacaoList = new ArrayList<EntidadeDominio>();
			while (rs.next()) {
				GrupoPrecificacao c = new GrupoPrecificacao();
				c.setId(rs.getInt("Id"));
				c.setNome(rs.getString("Nome"));
				c.setMargemVenda(rs.getFloat("MargemVenda"));
				grupoPrecificacaoList.add(c);
			}
			return grupoPrecificacaoList;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
