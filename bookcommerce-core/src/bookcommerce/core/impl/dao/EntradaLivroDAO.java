package bookcommerce.core.impl.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.Entrada;
import bookcommerce.dominio.EntradaLivro;
import bookcommerce.dominio.Livro;

public class EntradaLivroDAO extends AbstractJdbcDAO {

	public EntradaLivroDAO(Connection connection) {
		super(connection, "BookCommerce.EntradaLivro", "Id");
	}
	
	public EntradaLivroDAO() {
		super("BookCommerce.EntradaLivro", "Id");
	}

	@Override
	public void salvar(EntidadeDominio entidade) throws SQLException {
		PreparedStatement pst=null;
		StringBuilder sql = new StringBuilder();
		try {
			if(connection == null){
				openConnection();
			}else {
				ctrlTransaction = false;
			}
			EntradaLivro entradaLivro = (EntradaLivro)entidade;
			
			List<EntidadeDominio> entEntradaLivro = consultar(entidade);
			if(entEntradaLivro != null && entEntradaLivro.size() > 0) { // Já existe este livro? colocar o strategy
				
				return;
			}
			
			sql.append(" insert into BookCommerce.EntradaLivro (IdEntrada, IdLivro, PrecoPago, Quantidade) ");
			sql.append(" values ( ?, ?, ?, ? ); ");	
			
			connection.setAutoCommit(false);
			pst = connection.prepareStatement(sql.toString(), 
					Statement.RETURN_GENERATED_KEYS);
			
			int i = 1;
			pst.setInt(i++, entradaLivro.getEntrada().getId());	
			pst.setInt(i++, entradaLivro.getLivro().getId());	
			pst.setFloat(i++, entradaLivro.getPrecoPago());
			pst.setInt(i++, entradaLivro.getQuantidade());				
			
			pst.executeUpdate();		
					
			ResultSet rs = pst.getGeneratedKeys();
			int idEnd=0;
			if(rs.next())
				idEnd = rs.getInt(1);
			entradaLivro.setId(idEnd);
			
			if(ctrlTransaction) // commita apenas se não está na conexão de outra classe
				connection.commit();					
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();	
		}finally{
			if(ctrlTransaction){
				try {
					pst.close();
					if(ctrlTransaction)
						connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}			
		}				
	}

	@Override
	public void alterar(EntidadeDominio entidade) throws SQLException {
		if(connection == null) {
			openConnection();
			connection.setAutoCommit(false);
		}
		PreparedStatement pst=null;
		EntradaLivro entradaLivro = (EntradaLivro)entidade;		
		if(entradaLivro.getId() == null || entradaLivro.getId() == 0)
			return;
		try {
			connection.setAutoCommit(false);			
			
			String sql = " update BookCommerce.EntradaLivro set " + 
					"	IdLivro = ?" + 
					"	, PrecoPago = ?" + 
					"	, Quantidade = ?" + 
					" where " + 
					"	Id = ?  "; 
			pst = connection.prepareStatement(sql.toString());
			
			int i = 1;
			pst.setInt(i++, entradaLivro.getLivro().getId());
			pst.setFloat(i++, entradaLivro.getPrecoPago());
			pst.setInt(i++, entradaLivro.getQuantidade());			
			pst.setInt(i++, entradaLivro.getId());			
			
			pst.executeUpdate();			
			connection.commit();		
			
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();			
		}finally{
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}			
	}

	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) throws SQLException {
		PreparedStatement pst = null;

		EntradaLivro entradaLivro = (EntradaLivro) entidade;
		String sql = null;

		sql = " select el.* " + 
				"	, l.CodigoBarras 'l_CodigoBarras'" + 
				"	, l.Titulo 'l_Titulo'" + 
				" from BookCommerce.EntradaLivro el" + 
				"	join BookCommerce.Entrada e on el.IdEntrada = e.Id" + 
				"	join BookCommerce.Livro l on el.IdLivro = l.Id" + 
				" where " + 
				"	e.Id = ?  " +
				" 	and (? is null or l.Id = ? )";
		try {
			openConnection();
			pst = connection.prepareStatement(sql);
			int i = 1;
			pst.setInt(i++, entradaLivro.getEntrada().getId());
			
			if(entradaLivro.getLivro() == null || entradaLivro.getLivro().getId() == null
					|| entradaLivro.getLivro().getId() == 0) {
				pst.setNull(i++, Types.INTEGER);
				pst.setNull(i++, Types.INTEGER);
			} else {
				pst.setInt(i++, entradaLivro.getLivro().getId());
				pst.setInt(i++, entradaLivro.getLivro().getId());
			}			

			ResultSet rs = pst.executeQuery();
			List<EntidadeDominio> entradaLivroList = new ArrayList<EntidadeDominio>();
			while (rs.next()) {
				EntradaLivro el = new EntradaLivro();

				el.setId(rs.getInt("Id"));
				el.setPrecoPago(rs.getFloat("PrecoPago"));
				el.setQuantidade(rs.getInt("Quantidade"));
				
				el.setEntrada(new Entrada());
				el.getEntrada().setId(rs.getInt("IdEntrada"));
				
				el.setLivro(new Livro());
				el.getLivro().setId(rs.getInt("IdLivro"));
				el.getLivro().setCodBarras(rs.getString("l_CodigoBarras"));
				el.getLivro().setTitulo(rs.getString("l_Titulo"));
				
				entradaLivroList.add(el);
			}
			return entradaLivroList;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
