package bookcommerce.core.impl.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import bookcommerce.core.util.ConverteDate;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.Log;
import bookcommerce.dominio.Pessoa;

public class LogDAO extends AbstractJdbcDAO {

	public LogDAO(Connection connection) {
		super(connection, "BookCommerce.Log", "id");
	}
	
	public LogDAO() {
		super("BookCommerce.Log", "id");
	}

	@Override
	public void salvar(EntidadeDominio entidade) throws SQLException {
		openConnection();
		PreparedStatement pst=null;	
		Log log = (Log)entidade;

		try {
			connection.setAutoCommit(false);			
					
			StringBuilder sql = new StringBuilder();
			sql.append(" insert into BookCommerce.Log (IdPessoa, IP, Acao, JsonEntrada, JsonSaida, DataEHora, Chave) ");
			sql.append(" values (?, ?, ?, ?, ?, ?, ?); ");			
			
			pst = connection.prepareStatement(sql.toString(), 
					Statement.RETURN_GENERATED_KEYS);
			int i = 1;			
			if(log.getPessoa() != null && log.getPessoa().getId() != null && log.getPessoa().getId() > 0 )
				pst.setInt(i++, log.getPessoa().getId());
			else
				pst.setNull(i++, Types.INTEGER);
			pst.setString(i++, log.getIp());
			pst.setString(i++, log.getAcao());	
			pst.setString(i++, log.getJsonEntrada());
			pst.setString(i++, log.getJsonSaida());
			pst.setTimestamp(i++, ConverteDate.Convert(log.getDataEHora()));
			pst.setString(i++, log.getChave());
						
			pst.executeUpdate();	
			
			ResultSet rs = pst.getGeneratedKeys();
			int id=0;
			if(rs.next())
				id = rs.getInt(1);
			log.setId(id);
			
			connection.commit();		
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();			
		}finally{
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void alterar(EntidadeDominio entidade) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) throws SQLException {
		PreparedStatement pst = null;
		String sql = null;

		sql = " select top 100 l.* \r\n" + 
				"from BookCommerce.log l\r\n" + 
				"order by id desc ";
		try {
			openConnection();
			pst = connection.prepareStatement(sql);
			
			ResultSet rs = pst.executeQuery();
			List<EntidadeDominio> logList = new ArrayList<EntidadeDominio>();
			while (rs.next()) {
				Log en = new Log();
				
				en.setId(rs.getInt("Id"));				
				en.setPessoa(new Pessoa());
				en.getPessoa().setId(rs.getInt("IdPessoa"));
				en.setIp(rs.getString("Ip"));
				en.setAcao(rs.getString("Acao"));
				en.setJsonEntrada(rs.getString("JsonEntrada"));
				en.setJsonSaida(rs.getString("JsonSaida"));				
				en.setDataEHora(ConverteDate.Convert(rs.getTimestamp("DataEHora")));
				en.setDataEHoraStr(ConverteDate.Convert(rs.getTimestamp("DataEHora")));
				en.setChave(rs.getString("Chave"));
				
				logList.add(en);
			}
			return logList;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
