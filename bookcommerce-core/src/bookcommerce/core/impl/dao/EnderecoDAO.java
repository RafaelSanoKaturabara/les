
package bookcommerce.core.impl.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import bookcommerce.dominio.Endereco;
import bookcommerce.dominio.EntidadeDominio;

public class EnderecoDAO extends AbstractJdbcDAO {

	public EnderecoDAO(){
		super("BookCommerce.Endereco", "Id");			
	}
	
	public void salvar(EntidadeDominio entidade) {

		PreparedStatement pst=null;
		StringBuilder sql = new StringBuilder();
		try {
			if(connection == null){
				openConnection();
			} else {
				ctrlTransaction = false;
			}
			Endereco endereco = (Endereco)entidade;
			
			sql.append("INSERT INTO BookCommerce.Endereco (Logradouro, numero, Bairro, Cep, Apelido, IdCidade) ");
			sql.append(" VALUES (?, ?, ?, ?, ?, ?); ");	
			connection.setAutoCommit(false);
			
			pst = connection.prepareStatement(sql.toString(), 
					Statement.RETURN_GENERATED_KEYS);
			int i = 1;
			pst.setString(i++, endereco.getLogradouro());
			pst.setString(i++, endereco.getNumero());
			pst.setString(i++, endereco.getBairro());
			pst.setString(i++, endereco.getCep());
			pst.setString(i++, endereco.getApelido());	
			pst.setInt(i++, endereco.getCidade().getId());		
			
			pst.executeUpdate();		
					
			ResultSet rs = pst.getGeneratedKeys();
			int idEnd=0;
			if(rs.next())
				idEnd = rs.getInt(1);
			endereco.setId(idEnd);
			
			if(ctrlTransaction) // commita apenas se não está na conexão de outra classe
				connection.commit();					
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();	
		}finally{
			if(ctrlTransaction){
				try {
					pst.close();
					if(ctrlTransaction) // fecha apenas se não tiver na conexao de outra classe
						connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}			
		}
	}

	
	@Override
	public void alterar(EntidadeDominio entidade) {
		// TODO Auto-generated method stub

	}

	/** 
	 * TODO Descri��o do M�todo
	 * @param entidade
	 * @return
	 * @see fai.dao.IDAO#consulta(fai.domain.EntidadeDominio)
	 */
	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) {
		// TODO Auto-generated method stub
		return null;
	}

}
