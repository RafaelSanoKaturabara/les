package bookcommerce.core.impl.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import bookcommerce.core.util.ConverteDate;
import bookcommerce.core.util.CriptografiaUtil;
import bookcommerce.dominio.CartaoCreditoTipo;
import bookcommerce.dominio.Cidade;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.Estado;
import bookcommerce.dominio.Pais;
import bookcommerce.dominio.Pessoa;
import bookcommerce.dominio.PessoaCartaoCredito;
import bookcommerce.dominio.PessoaEndereco;
import bookcommerce.dominio.PessoaGenero;
import bookcommerce.dominio.PessoaTipo;
import bookcommerce.dominio.Telefone;
import bookcommerce.dominio.TelefoneTipo;

public class PessoaDAO extends AbstractJdbcDAO {

	public PessoaDAO() {
		super("BookCommerce.Pessoa", "Id");
	}

	@Override
	public void salvar(EntidadeDominio entidade) throws SQLException {
		StringBuilder sql;
		ResultSet rs;
		openConnection();
		PreparedStatement pst=null;
		Pessoa pessoa = (Pessoa)entidade;		
		try {			
			connection.setAutoCommit(false);
			if(pessoa.getCartaoCreditoPadrao().getId() == null || pessoa.getCartaoCreditoPadrao().getId() == 0) { // Cadastrando pelo login?	
				try {
					// insert endereço
					new PessoaEnderecoDAO(connection).salvar(pessoa.getEnderecoPadrao());			
					
					//Salvando pessoa
					
					pst = null;
					sql = new StringBuilder();
					
					sql.append(" insert into BookCommerce.Pessoa ");
					sql.append(" (Nome, Sobrenome, DataNascimento, Cpf, Email, Senha, Observacoes, IdPessoaGenero, IdPessoaTipo, TelefoneCompleto, IdTelefoneTipo, Ativo) ");
					sql.append(" values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?); ");
					
					pst = connection.prepareStatement(sql.toString(), 
							Statement.RETURN_GENERATED_KEYS);					
					
					String senhaCriptografada = new CriptografiaUtil().encrypt(pessoa.getSenha());
					
					int i = 1;					
					pst.setString(i++, pessoa.getNome());
					pst.setString(i++, pessoa.getSobrenome());
					pst.setTimestamp(i++, ConverteDate.Convert(pessoa.getDataNascimento()));
					pst.setString(i++, pessoa.getCpf());
					pst.setString(i++, pessoa.getEmail());	
					pst.setString(i++, senhaCriptografada);	
					pst.setString(i++, pessoa.getObservacao().replace("\n", " "));		
					pst.setInt(i++, pessoa.getGenero().getId());	
					pst.setInt(i++, 1);	
					pst.setString(i++, pessoa.getTelefone().getNumeroCompleto());	
					pst.setInt(i++, pessoa.getTelefone().getTelefoneTipo().getId());	
					pst.setBoolean(i++, pessoa.getAtivo());						
					
					pst.executeUpdate();									
					rs = pst.getGeneratedKeys();
					int idPessoa = 0;
					if(rs.next())
						idPessoa = rs.getInt(1);
					pessoa.setId(idPessoa);		
													
					// insert pessoa endere�o
					sql = new StringBuilder();
					sql.append(" insert into BookCommerce.PessoaEndereco (IdPessoa, IdEndereco) ");
					sql.append(" values ( ?, ?); ");	
					
					i = 1;	
					pst = null;
					pst = connection.prepareStatement(sql.toString());	
					pst.setInt(i++, pessoa.getId());
					pst.setInt(i++, pessoa.getEnderecoPadrao().getId());			
					pst.executeUpdate();
					
					// salvando pessoa Cartão de crédito
					pessoa.getCartaoCreditoPadrao().setIdPessoa(pessoa.getId());
					new PessoaCartaoCreditoDAO(connection).salvar(pessoa.getCartaoCreditoPadrao());		

					connection.commit();					
				} catch (SQLException e) {
					try {
						connection.rollback();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
					e.printStackTrace();	
				}finally{
					if(ctrlTransaction){
						try {
							pst.close();
							if(ctrlTransaction)
								connection.close();
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}			
				}
			} else {
				
			}
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();			
		}finally{
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
	}

	@Override
	public void alterar(EntidadeDominio entidade) throws SQLException {
		openConnection();
		PreparedStatement pst=null;
		Pessoa pessoa = (Pessoa)entidade;		
		if(pessoa.getId() == null || pessoa.getId() == 0)
			return;
		try {
			connection.setAutoCommit(false);			
			if (pessoa.getSenha() != null && pessoa.getRepitaSenha() != null && pessoa.getId() != null && pessoa.getId() > 0) { // Está trocando de senha?
				String sql = " update BookCommerce.Pessoa " + 
						" set senha = ?" + 
						" where Id = ?";
				pst = connection.prepareStatement(sql.toString());				
				int i = 1;
				String senhaCriptografada = new CriptografiaUtil().encrypt(pessoa.getSenha());
				pst.setString(i++, senhaCriptografada);
				pst.setInt(i++, pessoa.getId());
			}else {
				String sql = " update BookCommerce.Pessoa " + 
						" set nome = ?" + 
						"	, Sobrenome = ?" + 
						"	, DataNascimento = ?" + 
						"	, Cpf = ?" + 
						"	, Email = ?" + 
						"	, Observacoes = ?" + 
						"	, IdCartaoPadrao = ?" + 
						"	, IdEnderecoPadrao = ?" + 
						"	, IdPessoaGenero = ?" + 
						"	, TelefoneCompleto = ?" + 
						"	, IdTelefoneTipo = ?	" + 
						"where Id = ?";
				pst = connection.prepareStatement(sql.toString());
				
				int i = 1;
				pst.setString(i++, pessoa.getNome());
				pst.setString(i++, pessoa.getSobrenome());
				pst.setTimestamp(i++, ConverteDate.Convert(pessoa.getDataNascimento()));
				pst.setString(i++, pessoa.getCpf());
				pst.setString(i++, pessoa.getEmail());
				pst.setString(i++, pessoa.getObservacao().replace("\n", " "));
				pst.setInt(i++, pessoa.getCartaoCreditoPadrao().getId());
				pst.setInt(i++, pessoa.getEnderecoPadrao().getId());
				pst.setInt(i++, pessoa.getGenero().getId());
				pst.setString(i++, pessoa.getTelefone().getNumeroCompleto());
				pst.setInt(i++, pessoa.getTelefone().getTelefoneTipo().getId());
				pst.setInt(i++, pessoa.getId());			
			}
			pst.executeUpdate();	
			connection.commit();
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();			
		}finally{
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) throws SQLException {
		PreparedStatement pst = null;	
		
		Pessoa pessoa = (Pessoa) entidade;

		boolean isLogin = ((pessoa.getId() == null || pessoa.getId() == 0 ) 
				&& (pessoa.getNome() == null || pessoa.getNome() == "") 
				&& (pessoa.getCpf() == null || pessoa.getCpf() == "") 
				&& (pessoa.getEmail() != null)
				&& pessoa.getRepitaSenha() == null
				&& pessoa.getSenha() != null);
		
		String sql=null;
		
		sql = "	select p.* " + 
				"	, pg.Id 'pg_Id' " + 
				"	, pg.Descricao 'pg_Descricao' " + 
				"	, e.Id 'pe_Id' " + 
				"	, e.Logradouro 'pe_Logradouro' " + 
				"	, e.numero 'pe_Numero' " + 
				"	-- , e.Bairro 'pe_Bairro' \n " + 
				"	, e.Cep 'pe_Cep' " + 
				"	, e.Apelido 'pe_Apelido' " + 
				"	, c.Id 'c_Id' " + 
				"	, c.Nome 'c_Nome' " + 
				"	, es.Id 'es_Id' " + 
				"	, es.Nome 'es_Nome'" + 
				"	, es.Uf 'es_Uf'" + 
				"	, pa.Id 'pa_Id'" + 
				"	, pa.Nome 'pa_Nome'" + 
				"	, pcc.Id 'pcc_Id'" + 
				"	, pcc.Numero 'pcc_Numero'" + 
				"	, pcc.NomeImpresso 'pcc_NomeImpresso'" + 
				"	, pcc.CodigoSeguranca 'pcc_CodigoSeguranca'" + 
				"	, pcc.IdPessoa 'pcc_IdPessoa'" + 
				"	, pcc.IdCartaoCreditoTipo 'pcc_IdCartaoCreditoTipo'" + 
				"	, pcc.Apelido 'pcc_Apelido'" + 
				"	, cct.Id 'cct_Id'" + 
				"	, cct.Descricao 'cct_Descricao' " + 
				" from BookCommerce.Pessoa p " + 
				"	join BookCommerce.PessoaGenero pg on p.IdPessoaGenero = pg.id " + 
				"	left join BookCommerce.PessoaEndereco pe on p.IdEnderecoPadrao = pe.IdEndereco " + 
				"	left join BookCommerce.Endereco e on pe.IdEndereco = e.Id" + 
				"	left join BookCommerce.Cidade c on e.IdCidade = c.Id" + 
				"	left join BookCommerce.Estado es on c.IdEstado = es.Id" + 
				"	left join BookCommerce.Pais pa on es.IdPais = pa.Id" + 
				"	left join BookCommerce.PessoaCartaoCredito pcc on p.IdCartaoPadrao = pcc.Id" + 
				"	left join BookCommerce.CartaoCreditoTipo cct on pcc.IdCartaoCreditoTipo = cct.id" +
				" where " +  
				" (? is null or p.id = ?) " + 
				"	and (? is null or p.Cpf = ?) " + 
				" 	and (? is null or p.Email = ?) ";
		if(isLogin)
			sql += " 	and (p.Email = ?) " +  
			" and p.Senha = ? ; "; 		
		try {
			openConnection();
			pst = connection.prepareStatement(sql);
			int i = 1;
			if(pessoa.getId() == null || pessoa.getId() == 0) {
				pst.setNull(i++, Types.INTEGER);
				pst.setNull(i++, Types.INTEGER);				
			} else {
				pst.setInt(i++, pessoa.getId());
				pst.setInt(i++, pessoa.getId());	
			}
			if(pessoa.getCpf() == null || pessoa.getCpf().equals("")) {
				pst.setNull(i++, Types.VARCHAR);
				pst.setNull(i++, Types.VARCHAR);				
			} else {
				pst.setString(i++, pessoa.getCpf());
				pst.setString(i++, pessoa.getCpf());	
			}
			if(pessoa.getEmail() == null || pessoa.getEmail().equals("")) {
				pst.setNull(i++, Types.VARCHAR);
				pst.setNull(i++, Types.VARCHAR);				
			} else {
				pst.setString(i++, pessoa.getEmail());
				pst.setString(i++, pessoa.getEmail());	
			}
			
			if(isLogin) {
				String senhaCriptografada = new CriptografiaUtil().encrypt(pessoa.getSenha());
				pst.setString(i++, pessoa.getEmail());
				pst.setString(i++, senhaCriptografada);
			}			
			
			ResultSet rs = pst.executeQuery();
			List<EntidadeDominio> pessoaList = new ArrayList<EntidadeDominio>();
			while (rs.next()) {
				Pessoa pe = new Pessoa();
				
				pe.setId(rs.getInt("Id"));
				pe.setNome(rs.getString("Nome"));
				pe.setSobrenome(rs.getString("Sobrenome"));
				pe.setDataNascimento(rs.getDate("DataNascimento"));
				pe.setCpf(rs.getString("Cpf"));
				pe.setEmail(rs.getString("Email"));
				//pe.setSenha(rs.getString("Senha"));
				pe.setObservacao(rs.getString("Observacoes").replace("\n", " "));
				pe.setAtivo(rs.getBoolean("Ativo"));	

				pe.setTelefone(new Telefone());
				pe.getTelefone().setTelefoneTipo(new TelefoneTipo());
				pe.getTelefone().getTelefoneTipo().setId(rs.getInt("IdTelefoneTipo"));
				pe.getTelefone().setNumeroCompleto(rs.getString("TelefoneCompleto"));
				
				pe.setGenero(new PessoaGenero());
				pe.getGenero().setId(rs.getInt("pg_Id"));
				pe.getGenero().setDescricao(rs.getString("pg_Descricao"));
				
				pe.setPessoaTipo(new PessoaTipo());
				pe.getPessoaTipo().setId(rs.getInt("IdPessoaTipo"));
				
				pe.setEnderecoPadrao(new PessoaEndereco());
				pe.getEnderecoPadrao().setId(rs.getInt("pe_Id"));
				pe.getEnderecoPadrao().setLogradouro(rs.getString("pe_Logradouro"));
				pe.getEnderecoPadrao().setNumero(rs.getString("pe_Numero"));
				pe.getEnderecoPadrao().setCep(rs.getString("pe_Cep"));
				pe.getEnderecoPadrao().setApelido(rs.getString("pe_Apelido"));
				
				pe.getEnderecoPadrao().setCidade(new Cidade());
				pe.getEnderecoPadrao().getCidade().setId(rs.getInt("c_Id"));
				pe.getEnderecoPadrao().getCidade().setNome(rs.getString("c_Nome"));
				
				pe.getEnderecoPadrao().getCidade().setEstado(new Estado());
				pe.getEnderecoPadrao().getCidade().getEstado().setId(rs.getInt("es_Id"));
				pe.getEnderecoPadrao().getCidade().getEstado().setNome(rs.getString("es_Nome"));
				pe.getEnderecoPadrao().getCidade().getEstado().setUf(rs.getString("es_Uf"));
				
				pe.getEnderecoPadrao().getCidade().getEstado().setPais(new Pais());
				pe.getEnderecoPadrao().getCidade().getEstado().getPais().setId(rs.getInt("pa_Id"));
				pe.getEnderecoPadrao().getCidade().getEstado().getPais().setNome(rs.getString("pa_Nome"));
				
				pe.setCartaoCreditoPadrao(new PessoaCartaoCredito());
				pe.getCartaoCreditoPadrao().setId(rs.getInt("pcc_Id"));
				pe.getCartaoCreditoPadrao().setNumero(rs.getString("pcc_Numero"));
				pe.getCartaoCreditoPadrao().setNomeImpresso(rs.getString("pcc_NomeImpresso"));
				pe.getCartaoCreditoPadrao().setCodigoSeguranca(rs.getString("pcc_CodigoSeguranca"));
				pe.getCartaoCreditoPadrao().setIdPessoa(rs.getInt("pcc_IdPessoa"));
				pe.getCartaoCreditoPadrao().setApelido(rs.getString("pcc_Apelido"));
				
				pe.getCartaoCreditoPadrao().setCartaoCreditoTipo(new CartaoCreditoTipo());
				pe.getCartaoCreditoPadrao().getCartaoCreditoTipo().setId(rs.getInt("cct_Id"));
				pe.getCartaoCreditoPadrao().getCartaoCreditoTipo().setDescricao(rs.getString("cct_Descricao"));	
										
//				pe.setCarrinho(new Carrinho());
//				pe.getCarrinho().setId(rs.getInt("ca_id"));
				
				pessoaList.add(pe);
			}
			return pessoaList;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
