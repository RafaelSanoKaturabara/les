package bookcommerce.core.impl.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import bookcommerce.dominio.Ano;
import bookcommerce.dominio.Autor;
import bookcommerce.dominio.Categoria;
import bookcommerce.dominio.Editora;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.GrupoPrecificacao;
import bookcommerce.dominio.Livro;

public class LivroDAO extends AbstractJdbcDAO {

	public LivroDAO() {
		super("BookCommerce.Livro", "Id");
		// TODO Auto-generated constructor stub
	}

	
	public void putEstoqueLivro(EntidadeDominio entidade) {
		PreparedStatement pst = null;		
		Livro livro = (Livro)entidade;
		String sql=null;
		//
		sql = "  select 	" + 
				" 	((select sum(quantidade) from BookCommerce.EntradaLivro where IdLivro = ? ) - SUM(ci.quantidade)) 'qtd_Estoque'  " + 
				" from BookCommerce.CarrinhoItem ci	" + 
				" 	join BookCommerce.Carrinho c on c.Id = ci.IdCarrinho " + 
				" where 	" + 
				" 	ci.Status = 1 " + 
				" 	and c.IdCarrinhoStatus <> 7" + 
				" 	and ci.IdLivro = ? " + 
				" group by 	ci.IdLivro  ";
	
		try {
			openConnection();
			pst = connection.prepareStatement(sql);
			int i = 1;
			pst.setInt(i++, livro.getId());
			pst.setInt(i++, livro.getId());
			
			ResultSet rs = pst.executeQuery();
			
			List<EntidadeDominio> livroList = new ArrayList<EntidadeDominio>();
			while (rs.next()) {
				Livro li = new Livro();
				li.setEstoque(rs.getInt("qtd_Estoque"));
				livroList.add(li);
			}
			if(livroList.size() == 1) {
				Livro liv = (Livro) livroList.get(0);
				livro.setEstoque(liv.getEstoque());				
			}
				
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	// retorno - true está válido / não existe
		//		false já existe
	public boolean validarExistenciaLivro(EntidadeDominio entidade) {
		PreparedStatement pst = null;		
		Livro livro = (Livro)entidade;
		String sql=null;
		//
		if(livro.getId() != null) // se tem id, j� deve ter cadastrado um cod barras
			return true;
		sql = " select * from BookCommerce.Livro where CodigoBarras = ?";
	
		try {
			openConnection();
			pst = connection.prepareStatement(sql);
			
			pst.setString(1, livro.getCodBarras());
			
			ResultSet rs = pst.executeQuery();
			
			List<EntidadeDominio> livroList = new ArrayList<EntidadeDominio>();
			while (rs.next()) {
				Livro li = new Livro();
				li.setId(rs.getInt("Id"));
				li.setTitulo(rs.getString("titulo"));
				livroList.add(li);
			}
			if(livroList.isEmpty())
				return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public void getAndPutAutorLivro(List<EntidadeDominio> livroList) {
		LivroAutorDAO livroAutorDAO = new LivroAutorDAO();
		livroAutorDAO.putAutorListInLivroList(livroList);
	}
	
	public void getAndPutCategoriaLivro(List<EntidadeDominio> livroList) {
		LivroCategoriaDAO livroCategoriaDAO = new LivroCategoriaDAO();
		livroCategoriaDAO.putCategorisListInLivroList(livroList);		
	}
	
	private void salvarLivroAutor(Livro livro) {
		openConnection();
		PreparedStatement pst=null;
		
		try {
			connection.setAutoCommit(false);			
					
			StringBuilder sql = new StringBuilder();
			sql.append(" delete BookCommerce.LivroAutor where IdLivro = ? ");	
			
			pst = connection.prepareStatement(sql.toString());
			
			pst.setInt(1, livro.getId());
			pst.executeUpdate();				
			connection.commit();
			
			if(livro.getAutorList() != null && !livro.getAutorList().isEmpty()) {
				for(Autor autor : livro.getAutorList()) {
					sql = new StringBuilder();
					sql.append(" insert into BookCommerce.LivroAutor (IdLivro, IdAutor) " + 
							" values (?,?); ");	
					
					pst = connection.prepareStatement(sql.toString());
					
					pst.setInt(1, livro.getId());
					pst.setInt(2, autor.getId());
					
					pst.executeUpdate();				
					connection.commit();
				}
			}
			
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();			
		}finally{
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void salvarLivroCategoria(Livro livro) {
		openConnection();
		PreparedStatement pst=null;
		
		try {
			connection.setAutoCommit(false);			
					
			StringBuilder sql = new StringBuilder();
			sql.append(" delete BookCommerce.LivroCategoria where IdLivro = ? ");	
			
			pst = connection.prepareStatement(sql.toString());
			
			pst.setInt(1, livro.getId());
			pst.executeUpdate();				
			connection.commit();
			
			if(livro.getCategoriaList() != null && !livro.getCategoriaList().isEmpty()) {
				for(Categoria categoria : livro.getCategoriaList()) {
					sql = new StringBuilder();
					sql.append(" insert into BookCommerce.LivroCategoria (IdLivro, IdCategoria) " + 
							" values (?,?); ");	
					
					pst = connection.prepareStatement(sql.toString());
					
					pst.setInt(1, livro.getId());
					pst.setInt(2, categoria.getId());
					
					pst.executeUpdate();				
					connection.commit();
				}
			}
			
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();			
		}finally{
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
	}
	
	@Override
	public void salvar(EntidadeDominio entidade) throws SQLException {
		openConnection();
		PreparedStatement pst=null;
		Livro livro = (Livro)entidade;		
		
		try {
			connection.setAutoCommit(false);			
					
			StringBuilder sql = new StringBuilder();
			sql.append(" insert into BookCommerce.Livro (CodigoBarras, Titulo, Edicao, ISBN, ");
			sql.append(" NumeroPaginas, Sinopse, Altura, Largura, Profundidade, Peso, ");		
			sql.append(" IdGrupoPrecificacao, IdAno, IdEditora, Ativo, preco) ");		
			sql.append(" values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");		
			
			pst = connection.prepareStatement(sql.toString(), 
					Statement.RETURN_GENERATED_KEYS);
			
			pst.setString(1, livro.getCodBarras());
			pst.setString(2, livro.getTitulo());
			pst.setInt(3, livro.getEdicao());
			pst.setString(4, livro.getIsbn());
			pst.setInt(5, livro.getQtdPaginas());
			pst.setString(6, livro.getSinopse());
			pst.setFloat(7, livro.getAltura());
			pst.setFloat(8, livro.getLargura());
			pst.setFloat(9, livro.getProfundidade());
			pst.setFloat(10, livro.getPeso());
			pst.setInt(11, livro.getGrupoPrecificacao().getId());
			pst.setInt(12, livro.getAno().getId());
			pst.setInt(13, livro.getEditora().getId());
			pst.setBoolean(14, livro.getAtivo());
			pst.setFloat(15, livro.getPreco());
			pst.executeUpdate();	
			
			ResultSet rs = pst.getGeneratedKeys();
			int id=0;
			if(rs.next())
				id = rs.getInt(1);
			livro.setId(id);
			salvarLivroCategoria(livro);
			salvarLivroAutor(livro);
			
			connection.commit();		
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();			
		}finally{
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void alterar(EntidadeDominio entidade) throws SQLException {
		openConnection();
		PreparedStatement pst=null;
		Livro livro = (Livro)entidade;		
		if(livro.getId() == null || livro.getId() == 0)
			return;
		try {
			connection.setAutoCommit(false);			
					
			StringBuilder sql = new StringBuilder();
			sql.append(" update BookCommerce.Livro set CodigoBarras = ?, Titulo = ?, ");
			sql.append(" Edicao = ?, ISBN = ?, NumeroPaginas = ?, Sinopse = ?, ");		
			sql.append(" Altura = ?, Largura = ?, Profundidade = ?, Peso = ?,  ");		
			sql.append(" IdGrupoPrecificacao = ?, IdAno = ?, IdEditora = ?, Ativo = ?, Preco = ? where id = ?; ");		
			
			pst = connection.prepareStatement(sql.toString());
			
			pst.setString(1, livro.getCodBarras());
			pst.setString(2, livro.getTitulo());
			pst.setInt(3, livro.getEdicao());
			pst.setString(4, livro.getIsbn());
			pst.setInt(5, livro.getQtdPaginas());
			pst.setString(6, livro.getSinopse());
			pst.setFloat(7, livro.getAltura());
			pst.setFloat(8, livro.getLargura());
			pst.setFloat(9, livro.getProfundidade());
			pst.setFloat(10, livro.getPeso());
			pst.setInt(11, livro.getGrupoPrecificacao().getId());
			pst.setInt(12, livro.getAno().getId());
			pst.setInt(13, livro.getEditora().getId());
			pst.setBoolean(14, livro.getAtivo());
			pst.setFloat(15, livro.getPreco());
			pst.setInt(16, livro.getId());
			//pst.setTimestamp(3, time);
			pst.executeUpdate();	
			
			//ResultSet rs = pst.getGeneratedKeys();
			//int id=0;
			//if(rs.next())
				//id = rs.getInt(1);
			//livro.setId(id);

			connection.commit();

			salvarLivroCategoria(livro);
			salvarLivroAutor(livro);
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();			
		}finally{
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) throws SQLException {
		PreparedStatement pst = null;
		
		Livro livro = (Livro)entidade;
		String sql=null;
		
		sql = "select l.* ,gp.Id as 'grupoPrecificacao_Id' ,gp.Nome as 'grupoPrecificacao_Nome', " + 
				"		gp.MargemVenda as 'grupoPrecificacao_MargemVenda',e.Id as 'editora_Id', " + 
				"		e.Nome as 'editora_Nome',a.Id as 'ano_Id',a.Ano as 'ano_Ano'  " + 
				"		from BookCommerce.livro l " + 
				"			join BookCommerce.GrupoPrecificacao gp on l.IdGrupoPrecificacao = gp.Id  		" + 
				"			join BookCommerce.Editora e on l.IdEditora = e.Id " + 
				"	 		join BookCommerce.Ano a on l.IdAno = a.Id  	" + 
				"       where ( ? is null or l.Ativo = ? )      "; 
		if 	(livro.getId() != null && livro.getId() > 0)	
			sql += "		 and l.id = ?  "	;		
		
		try {
			openConnection();
			pst = connection.prepareStatement(sql);
			int i = 1;
			
			if(livro.getAtivo() == false) {
				pst.setNull(i++, Types.BIT);
				pst.setNull(i++, Types.BIT);
			} else {
				pst.setBoolean(i++, livro.getAtivo());
				pst.setBoolean(i++, livro.getAtivo());
			}
			
			if(livro.getId() != null && livro.getId() > 0) { // se enviar nulo, coloca nulo na string para listar todos
				pst.setInt(i++, livro.getId());
				//pst.setString(2, "null");
			} 
			
			ResultSet rs = pst.executeQuery();
			List<EntidadeDominio> livroList = new ArrayList<EntidadeDominio>();
			while (rs.next()) {
				Livro li = new Livro();
				
				li.setId(rs.getInt("Id"));
				li.setCodBarras(rs.getString("CodigoBarras"));
				li.setTitulo(rs.getString("Titulo"));
				li.setEdicao(rs.getInt("Edicao"));
				li.setIsbn(rs.getString("ISBN"));
				li.setQtdPaginas(rs.getInt("NumeroPaginas"));
				li.setSinopse(rs.getString("Sinopse"));
				li.setAltura(rs.getFloat("Altura"));
				li.setLargura(rs.getFloat("Largura"));
				li.setProfundidade(rs.getFloat("Profundidade"));
				li.setPeso(rs.getFloat("Peso"));
				li.setAtivo(rs.getBoolean("Ativo"));
				li.setPreco(rs.getFloat("Preco"));
				li.setImg100(rs.getString("Img100"));
				li.setImg210(rs.getString("Img210"));
				
				li.setGrupoPrecificacao(new GrupoPrecificacao());
				li.getGrupoPrecificacao().setId(rs.getInt("grupoPrecificacao_Id"));
				li.getGrupoPrecificacao().setNome(rs.getString("grupoPrecificacao_Nome"));
				li.getGrupoPrecificacao().setMargemVenda(rs.getFloat("grupoPrecificacao_MargemVenda"));
				
				li.setAno(new Ano());
				li.getAno().setId(rs.getInt("ano_Id"));
				li.getAno().setAno(rs.getInt("ano_Ano"));
				
				li.setEditora(new Editora());
				li.getEditora().setId(rs.getInt("editora_Id"));
				li.getEditora().setNome(rs.getString("editora_Nome"));
						
				livroList.add(li);
			}
			getAndPutAutorLivro(livroList);
			getAndPutCategoriaLivro(livroList);
			
			if(livroList.size() == 1) { // pesquisou um livro em específico?
				new CarrinhoItemDAO().desabilitarCarrinhoItensAntigos();
				putEstoqueLivro(livroList.get(0));
			}
			
			return livroList;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
