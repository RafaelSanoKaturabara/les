package bookcommerce.core.impl.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import bookcommerce.core.util.ConverteDate;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.Entrada;
import bookcommerce.dominio.Fornecedor;

public class EntradaDAO extends AbstractJdbcDAO {

	public EntradaDAO(Connection connection) {
		super(connection, "BookCommerce.Entrada", "Id");
	}
	
	public EntradaDAO() {
		super("BookCommerce.Entrada", "Id");
	}

	@Override
	public void salvar(EntidadeDominio entidade) throws SQLException {
		PreparedStatement pst=null;
		StringBuilder sql = new StringBuilder();
		try {
			if(connection == null){
				openConnection();
			}else {
				ctrlTransaction = false;
			}
			Entrada entrada = (Entrada)entidade;
			
			List<EntidadeDominio> entEntradaList = consultar(entidade);
			
			if(entEntradaList.size() > 0) { // já existe uma entrado para esta nota fiscal
				entrada.setId(entEntradaList.get(0).getId());
				new EntradaDAO(connection).alterar(entidade);
				return;
			}
			
			sql.append(" insert into BookCommerce.Entrada (NumeroNotaFiscal, DataEntrada, IdFornecedor) ");
			sql.append(" values ( ?, ?, ? ); ");	
			
			connection.setAutoCommit(false);
			pst = connection.prepareStatement(sql.toString(), 
					Statement.RETURN_GENERATED_KEYS);
			
			int i = 1;
			pst.setString(i++, entrada.getNumeroNotaFiscal());
			pst.setTimestamp(i++, ConverteDate.Convert( entrada.getDataEntrada()));
			pst.setInt(i++, entrada.getFornecedor().getId());	
			
			pst.executeUpdate();		
					
			ResultSet rs = pst.getGeneratedKeys();
			int idEnd=0;
			if(rs.next())
				idEnd = rs.getInt(1);
			entrada.setId(idEnd);
			
			if(ctrlTransaction) // commita apenas se não está na conexão de outra classe
				connection.commit();					
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();	
		}finally{
			if(ctrlTransaction){
				try {
					if(pst != null)
						pst.close();
					if(ctrlTransaction)
						connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}			
		}		
	}

	@Override
	public void alterar(EntidadeDominio entidade) throws SQLException {
		if(connection == null) {
			openConnection();
			connection.setAutoCommit(false);
		} else {
			ctrlTransaction = false;
		}
		PreparedStatement pst=null;
		Entrada entrada = (Entrada)entidade;		
		if(entrada.getId() == null || entrada.getId() == 0)
			return;
		try {
			connection.setAutoCommit(false);			
			
			String sql = " update BookCommerce.Entrada " + 
					" set	NumeroNotaFiscal = ?" + 
					"	, DataEntrada = ?" + 
					"	, IdFornecedor = ?" + 
					" where " + 
					"	Id = ? ;  "; 
			pst = connection.prepareStatement(sql.toString());
			
			int i = 1;
			pst.setString(i++, entrada.getNumeroNotaFiscal());
			pst.setTimestamp(i++, ConverteDate.Convert(entrada.getDataEntrada()));
			pst.setInt(i++, entrada.getFornecedor().getId());
			pst.setInt(i++, entrada.getId());			
			
			pst.executeUpdate();			
			connection.commit();		
			
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();			
		}finally{
			if(ctrlTransaction){
				try {
					pst.close();
					if(ctrlTransaction)
						connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}		
	}

	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) throws SQLException {
		PreparedStatement pst = null;

		Entrada entrada = (Entrada) entidade;
		String sql = null;

		sql = " select e.*" + 
				"	, f.Nome 'f_Nome' " + 
				" from  BookCommerce.Entrada e" + 
				"	left join BookCommerce.Fornecedor f on e.IdFornecedor = f.Id" + 
				" where" + 
				"	( ? is null or e.Id = ?) " + 
				" 	and (? is null or e.NumeroNotaFiscal = ?) ";
		try {
			openConnection();
			pst = connection.prepareStatement(sql);
			int i = 1;
			if (entrada.getId() == null || entrada.getId() == 0) {
				pst.setNull(i++, Types.INTEGER);
				pst.setNull(i++, Types.INTEGER);
			} else {
				pst.setInt(i++, entrada.getId());
				pst.setInt(i++, entrada.getId());
			}
			
			if (entrada.getNumeroNotaFiscal() == null ||entrada.getNumeroNotaFiscal().equals("")) {
				pst.setNull(i++, Types.VARCHAR);
				pst.setNull(i++, Types.VARCHAR);
			} else {
				pst.setString(i++, entrada.getNumeroNotaFiscal());
				pst.setString(i++, entrada.getNumeroNotaFiscal());
			}

			ResultSet rs = pst.executeQuery();
			List<EntidadeDominio> entradaList = new ArrayList<EntidadeDominio>();
			while (rs.next()) {
				Entrada en = new Entrada();

				en.setId(rs.getInt("Id"));
				en.setNumeroNotaFiscal(rs.getString("NumeroNotaFiscal"));
				en.setDataEntrada(ConverteDate.Convert(rs.getDate("DataEntrada")));
				
				en.setFornecedor(new Fornecedor());
				en.getFornecedor().setId(rs.getInt("IdFornecedor"));
				en.getFornecedor().setNome(rs.getString("f_Nome"));
				
				entradaList.add(en);
			}
			return entradaList;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
