package bookcommerce.core.impl.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import bookcommerce.core.util.ConverteDate;
import bookcommerce.dominio.Carrinho;
import bookcommerce.dominio.CarrinhoCupom;
import bookcommerce.dominio.CarrinhoItem;
import bookcommerce.dominio.CarrinhoPagamento;
import bookcommerce.dominio.CarrinhoStatus;
import bookcommerce.dominio.Cupom;
import bookcommerce.dominio.Endereco;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.Pessoa;

public class CarrinhoDAO extends AbstractJdbcDAO {

	public CarrinhoDAO(Connection connection) {
		super(connection, "BookCommerce.Carrinho", "Id");
	}
	
	public CarrinhoDAO() {
		super("BookCommerce.Carrinho", "Id");
	}

	public void atualizarDataCarrinho(EntidadeDominio entidade) throws SQLException {
		if(connection == null) {
			openConnection();
			connection.setAutoCommit(false);
		}
		PreparedStatement pst=null;
		Carrinho carrinho = (Carrinho)entidade;		
		if(carrinho.getId() == null || carrinho.getId() == 0)
			return;
		try {
			connection.setAutoCommit(false);			
			
			String sql = " update BookCommerce.Carrinho set " + 
					" DataUltimaAlteracao = ?" + 
					" where " + 
					"	Id = ? ;  "; 
			pst = connection.prepareStatement(sql.toString());
			
			int i = 1;
			pst.setTimestamp(i++, ConverteDate.Convert(new Date()));
			pst.setInt(i++, carrinho.getId());
			pst.executeUpdate();			
			connection.commit();		
			
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();			
		}finally{
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void salvar(EntidadeDominio entidade) throws SQLException {
		openConnection();
		PreparedStatement pst=null;	
		Carrinho carrinho = (Carrinho)entidade;
		
		List<EntidadeDominio> carrinhoList = new ArrayList<EntidadeDominio>();
		// verificar se já existe este carrinho		
		if(carrinho.getId() != null) {
			alterar(entidade);
			return;// Não enviou o id do carrinho?
		}
		
		carrinhoList = consultar(entidade);		
		
		if(!carrinhoList.isEmpty()) { // Já existe o carrinho?
			// atualizar
			entidade.setId(carrinhoList.get(0).getId());
			alterar(entidade);
			return;
		} 

		try {
			connection.setAutoCommit(false);			
					
			StringBuilder sql = new StringBuilder();
			sql.append(" insert into BookCommerce.Carrinho (IdPessoa,  ");
			sql.append("  PrecoFrete, DataUltimaAlteracao, IdCarrinhoStatus, IdCupomDevolucao ) ");		
			sql.append(" values ( ?, ?, ?, ?, ? ) ; ");			
			
			pst = connection.prepareStatement(sql.toString(), 
					Statement.RETURN_GENERATED_KEYS);
			int i = 1;
			
			
			pst.setInt(i++, carrinho.getPessoa().getId());
			pst.setFloat(i++, carrinho.getPrecoFrete());
			pst.setTimestamp(i++, ConverteDate.Convert(carrinho.getDataUltimaAlteracao()));	
			pst.setInt(i++, carrinho.getCarrinhoStatus().getId());
			
			if(carrinho.getCupomDevolucao() == null || carrinho.getCupomDevolucao().getId() == null
					|| carrinho.getCupomDevolucao().getId() == 0)
				pst.setNull(i++, Types.INTEGER);
			else
				pst.setInt(i++, carrinho.getCarrinhoStatus().getId());
			pst.executeUpdate();	
			
			ResultSet rs = pst.getGeneratedKeys();
			int id=0;
			if(rs.next())
				id = rs.getInt(1);
			carrinho.setId(id);
			
			connection.commit();		
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();			
		}finally{
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void alterar(EntidadeDominio entidade) throws SQLException {
		if(connection == null) {
			openConnection();
			connection.setAutoCommit(false);
		}
		PreparedStatement pst=null;
		Carrinho carrinho = (Carrinho)entidade;		
		if(carrinho.getId() == null || carrinho.getId() == 0)
			return;
		try {
			connection.setAutoCommit(false);			
			
			String sql = " update BookCommerce.Carrinho set " + 
					" PrecoFrete = ? " + 
					" , DataUltimaAlteracao = ?" + 
					" , IdCarrinhoStatus = ? " +
					" , IdEndereco = ? " +
					" , IdCupomDevolucao = ? " +
					" where " + 
					"	Id = ? ;  "; 
			pst = connection.prepareStatement(sql.toString());
			
			int i = 1;
			pst.setFloat(i++, carrinho.getPrecoFrete());
			pst.setTimestamp(i++, ConverteDate.Convert(carrinho.getDataUltimaAlteracao()));
			// idCarrinho
			if(carrinho.getCarrinhoStatus() == null || carrinho.getCarrinhoStatus().getId() == null
					|| carrinho.getCarrinhoStatus().getId() == 0) 
				pst.setNull(i++, Types.INTEGER);				
			else	
				pst.setInt(i++, carrinho.getCarrinhoStatus().getId());
			// idEndereço
			if(carrinho.getEndereco() == null || carrinho.getEndereco().getId() == null || carrinho.getEndereco().getId() == 0)
				pst.setNull(i++, Types.INTEGER);
			else
				pst.setInt(i++, carrinho.getEndereco().getId());
			if(carrinho.getCupomDevolucao() == null || carrinho.getCupomDevolucao().getId() == null
					|| carrinho.getCupomDevolucao().getId() == 0)
				pst.setNull(i++, Types.INTEGER);
			else
				pst.setInt(i++, carrinho.getCupomDevolucao().getId());
			pst.setInt(i++, carrinho.getId());
			pst.executeUpdate();			
			connection.commit();		
			
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();			
		}finally{
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) throws SQLException {
		PreparedStatement pst = null;		
		Carrinho carrinho = (Carrinho)entidade;
		Boolean flgPegandoOrdemCompra = carrinho.getCarrinhoStatus() == null || carrinho.getCarrinhoStatus().getId() == null
				|| carrinho.getCarrinhoStatus().getId() == 0;
		String sql=null;
		
		sql = "  select c.*  " + 
				" 	, cs.Descricao 'cs_Descricao'" + 
				" 	, e.Apelido 'e_apelido'" + 
				" 	, p.Nome 'p_Nome'" +
				"	, p.Sobrenome 'p_Sobrenome'" +
				"	, cu.Codigo 'cu_Codigo'" +
				"   , cu.Status 'cu_Status'" +
				" from 	BookCommerce.Carrinho c " + 
				" 	join BookCommerce.CarrinhoStatus cs on c.IdCarrinhoStatus = cs.Id" + 
				" 	left join BookCommerce.Endereco e on c.IdEndereco = e.Id" + 
				" 	join BookCommerce.Pessoa p on c.IdPessoa = p.Id" +
				" 	left join BookCommerce.Cupom cu on c.IdCupomDevolucao = cu.Id " +
				" where " + 
				"	( ? is null or c.Id = ? ) " + 
				"	and ( ? is null or c.IdPessoa = ?) " +
				"   and ( ? is null or c.IdCarrinhoStatus = ?)  ";
		
		if(flgPegandoOrdemCompra) {
			sql += " and c.IdCarrinhoStatus <> 1 " +
					" order by c.Id desc ";
		}
		
		try {
			if(connection == null) {
				openConnection();
				connection.setAutoCommit(false);
			}
			pst = connection.prepareStatement(sql);
			
			int i = 1;
			if(carrinho.getId() == null || carrinho.getId() == 0) {
				pst.setNull(i++, Types.INTEGER);				
				pst.setNull(i++, Types.INTEGER);	
			} else {
				pst.setInt(i++, carrinho.getId());
				pst.setInt(i++, carrinho.getId());
			}
			if(carrinho.getPessoa() == null || carrinho.getPessoa().getId() == null
					|| carrinho.getPessoa().getId() == 0) {
				pst.setNull(i++, Types.INTEGER);				
				pst.setNull(i++, Types.INTEGER);	
			} else {
				pst.setInt(i++, carrinho.getPessoa().getId());
				pst.setInt(i++, carrinho.getPessoa().getId());
			}	
			if(carrinho.getCarrinhoStatus() == null || carrinho.getCarrinhoStatus().getId() == null
					|| carrinho.getCarrinhoStatus().getId() == 0) {
				pst.setNull(i++, Types.INTEGER);				
				pst.setNull(i++, Types.INTEGER);	
			} else {
				pst.setInt(i++, carrinho.getCarrinhoStatus().getId());
				pst.setInt(i++, carrinho.getCarrinhoStatus().getId());
			}	
					
			ResultSet rs = pst.executeQuery();
			List<EntidadeDominio> pessoaCarrinhoList = new ArrayList<EntidadeDominio>();
			while (rs.next()) {
				Carrinho ca = new Carrinho();
				
				ca.setId(rs.getInt("Id"));
				ca.setPessoa(new Pessoa());
				ca.getPessoa().setId(rs.getInt("IdPessoa"));
				ca.getPessoa().setNome(rs.getString("p_Nome"));
				ca.getPessoa().setSobrenome(rs.getString("p_Sobrenome"));
				ca.setDataUltimaAlteracao(ConverteDate.Convert(rs.getTimestamp("DataUltimaAlteracao")));
				ca.setPrecoFrete(rs.getFloat("PrecoFrete"));		
				
				ca.setCarrinhoStatus(new CarrinhoStatus());
				ca.getCarrinhoStatus().setId(rs.getInt("IdCarrinhoStatus"));
				ca.getCarrinhoStatus().setDescricao(rs.getString("cs_Descricao"));
				
				ca.setEndereco(new Endereco());
				ca.getEndereco().setId(rs.getInt("IdEndereco"));
				ca.getEndereco().setApelido(rs.getString("e_apelido"));
				
				ca.setCupomDevolucao(new Cupom());
				ca.getCupomDevolucao().setId(rs.getInt("IdCupomDevolucao"));
				ca.getCupomDevolucao().setCodigo(rs.getString("cu_Codigo"));
				ca.getCupomDevolucao().setStatus(rs.getBoolean("cu_Status"));
						
				pessoaCarrinhoList.add(ca);
			}
			
			if(pessoaCarrinhoList.size() == 1) {
				// inserindo carrinho item list
				// não sei fazer de outra forma
				Carrinho carrinhoWork = (Carrinho) pessoaCarrinhoList.get(0);
				CarrinhoItem carrinhoItem = new CarrinhoItem();
				carrinhoItem.setCarrinho(new Carrinho());
				carrinhoItem.getCarrinho().setId(carrinhoWork.getId());
				// transformando um por um
				List<EntidadeDominio> EntidadeDominioList = new CarrinhoItemDAO().consultar(carrinhoItem);
				List<CarrinhoItem> carrinhoItemList = new ArrayList<CarrinhoItem>();
				if(EntidadeDominioList.size()>0) {
					for(EntidadeDominio entidom : EntidadeDominioList) {
						carrinhoItemList.add((CarrinhoItem)entidom);
					}
				}				
				carrinhoWork.setCarrinhoItemList(carrinhoItemList);
				// obter e inserir carrinho cupom list
				CarrinhoCupom carrinhoCupom = new CarrinhoCupom();
				carrinhoCupom.setCarrinho(new Carrinho());
				carrinhoCupom.getCarrinho().setId(carrinhoWork.getId());
				List<EntidadeDominio> entidadeCarrinhoCupomList = new CarrinhoCupomDAO().consultar(carrinhoCupom);
				List<CarrinhoCupom> carrinhoCupomList = new ArrayList<CarrinhoCupom>();
				if(entidadeCarrinhoCupomList.size()>0) {
					for(EntidadeDominio entiCarrinhoCupom : entidadeCarrinhoCupomList) {
						carrinhoCupomList.add((CarrinhoCupom)entiCarrinhoCupom);
					}
				}
				carrinhoWork.setCarrinhoCupomList(carrinhoCupomList);
				// Obter o carrinhoPagamento list
				CarrinhoPagamento carrinhoPagamento = new CarrinhoPagamento();
				carrinhoPagamento.setCarrinho(new Carrinho());
				carrinhoPagamento.getCarrinho().setId(carrinhoWork.getId());
				List<EntidadeDominio> entidadeCarrinhoPagamentoList = new CarrinhoPagamentoDAO().consultar(carrinhoPagamento);
				List<CarrinhoPagamento> carrinhoPagamentoList = new ArrayList<CarrinhoPagamento>();
				if(entidadeCarrinhoPagamentoList.size() > 0) {
					for(EntidadeDominio entidadeCarrinhoPagamento : entidadeCarrinhoPagamentoList) {
						carrinhoPagamentoList.add((CarrinhoPagamento)entidadeCarrinhoPagamento);
					}
				}
				carrinhoWork.setCarrinhoPagamentoList(carrinhoPagamentoList);
			}
			return pessoaCarrinhoList;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
