package bookcommerce.core.impl.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import bookcommerce.core.util.ConverteDate;
import bookcommerce.dominio.CarrinhoCupom;
import bookcommerce.dominio.Cupom;
import bookcommerce.dominio.CupomDescontoTipo;
import bookcommerce.dominio.CupomTipo;
import bookcommerce.dominio.EntidadeDominio;

public class CarrinhoCupomDAO extends AbstractJdbcDAO {

	public CarrinhoCupomDAO(Connection connection) {
		super(connection, "BookCommerce.CarrinhoCupom", "Id");
	}
	
	public CarrinhoCupomDAO() {
		super("BookCommerce.CarrinhoCupom", "Id");
	}

	@Override
	public void salvar(EntidadeDominio entidade) throws SQLException {
		PreparedStatement pst = null;
		StringBuilder sql = new StringBuilder();
		try {
			if (connection == null) {
				openConnection();
			} else {
				ctrlTransaction = false;
			}
			CarrinhoCupom carrinhoCupom = (CarrinhoCupom) entidade;

			// obter o carrinho
			if(carrinhoCupom.getCarrinho().getId() == null || carrinhoCupom.getCarrinho().getId() == 0) {// Não enviou carrinho
				//carrinhoCupom.getCarrinho().getCarrinhoStatus().setId(1); // Tipo Carrinho
				new CarrinhoDAO().salvar(carrinhoCupom.getCarrinho());
			}
			
			sql.append(" insert into BookCommerce.CarrinhoCupom (IdCupom, IdCarrinho) ");
			sql.append(" VALUES ( ?, ? ); ");
			connection.setAutoCommit(false);

			pst = connection.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			int i = 1;
			pst.setInt(i++, carrinhoCupom.getCupom().getId());
			pst.setInt(i++, carrinhoCupom.getCarrinho().getId());

			pst.executeUpdate();

			ResultSet rs = pst.getGeneratedKeys();
			int idEnd = 0;
			if (rs.next())
				idEnd = rs.getInt(1);
			carrinhoCupom.setId(idEnd);

			if (ctrlTransaction) {// commita apenas se não está na conexão de outra classe		
				connection.commit();
			}
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			if (ctrlTransaction) {
				try {
					pst.close();
					if (ctrlTransaction)
						connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void alterar(EntidadeDominio entidade) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) throws SQLException {
		PreparedStatement pst = null;		
		CarrinhoCupom carrinhoCupom = (CarrinhoCupom)entidade;
				
		String sql=null;
		
		sql = " select cu.* " + 
				"	, ct.Id 'ct_Id'" + 
				"	, ct.Descricao 'ct_Descricao'" + 
				"	, cdt.Id 'cdt_Id'" + 
				"	, cdt.Descricao 'cdt_Descricao'" + 
				" from BookCommerce.CarrinhoCupom cc" + 
				"	join BookCommerce.Cupom cu on cc.IdCupom = cu.Id" + 
				"	join BookCommerce.CupomTipo ct on cu.IdCupomTipo = ct.Id" + 
				"	join BookCommerce.CupomDescontoTipo cdt on cu.IdCupomDescontoTipo = cdt.Id" + 
				" where " + 
				"	cc.IdCarrinho = ? ";
		
		try {
			if(connection == null) {
				openConnection();
				connection.setAutoCommit(false);
			}
			pst = connection.prepareStatement(sql);
			
			int i = 1;
			pst.setInt(i++, carrinhoCupom.getCarrinho().getId());			
			
			ResultSet rs = pst.executeQuery();
			List<EntidadeDominio> carrinhoCupomList = new ArrayList<EntidadeDominio>();
			while (rs.next()) {
				CarrinhoCupom cp = new CarrinhoCupom();
				cp.setCupom(new Cupom());
				cp.getCupom().setId(rs.getInt("Id"));
				cp.getCupom().setCodigo(rs.getString("Codigo"));
				cp.getCupom().setValorDesconto(rs.getFloat("ValorDesconto"));
				cp.getCupom().setValidade(ConverteDate.Convert(rs.getDate("Validade")));
				cp.getCupom().setStatus(rs.getBoolean("Status"));
				
				cp.getCupom().setCupomTipo(new CupomTipo());
				cp.getCupom().getCupomTipo().setId(rs.getInt("ct_Id"));
				cp.getCupom().getCupomTipo().setDescricao(rs.getString("ct_Descricao"));
				
				cp.getCupom().setCupomDescontoTipo(new CupomDescontoTipo());
				cp.getCupom().getCupomDescontoTipo().setId(rs.getInt("cdt_Id"));
				cp.getCupom().getCupomDescontoTipo().setDescricao(rs.getString("cdt_Descricao"));		
						
				carrinhoCupomList.add(cp);
			}		
			return carrinhoCupomList;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
