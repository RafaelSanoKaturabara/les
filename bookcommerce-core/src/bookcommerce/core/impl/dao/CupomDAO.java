package bookcommerce.core.impl.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import bookcommerce.core.util.ConverteDate;
import bookcommerce.dominio.Cupom;
import bookcommerce.dominio.CupomDescontoTipo;
import bookcommerce.dominio.CupomTipo;
import bookcommerce.dominio.EntidadeDominio;

public class CupomDAO extends AbstractJdbcDAO {

	public CupomDAO(Connection connection) {
		super(connection, "BookCommerce.Cupom", "Id");
	}
	
	public CupomDAO() {
		super("BookCommerce.Cupom", "Id");
	}

	@Override
	public void salvar(EntidadeDominio entidade) throws SQLException {
		openConnection();
		PreparedStatement pst=null;	
		Cupom cupom = (Cupom)entidade;
		
		// verificar se já existe este carrinho		
		if(cupom.getId() != null) {
			alterar(entidade);
			return;// Não enviou o id do carrinho?
		}
		
		try {
			connection.setAutoCommit(false);			
					
			StringBuilder sql = new StringBuilder();
			sql.append(" insert into BookCommerce.Cupom (codigo, IdCupomTipo, IdCupomDescontoTipo, ValorDesconto, Status)  ");
			sql.append(" values ( ?, ?, ?, ?, ? ) ; ");			
			
			pst = connection.prepareStatement(sql.toString(), 
					Statement.RETURN_GENERATED_KEYS);
			int i = 1;			
			
			pst.setString(i++, cupom.getCodigo());
			pst.setFloat(i++, cupom.getCupomTipo().getId());
			pst.setInt(i++, cupom.getCupomDescontoTipo().getId());	
			pst.setFloat(i++, cupom.getValorDesconto());
			pst.setBoolean(i++, cupom.isStatus());
						
			pst.executeUpdate();	
			
			ResultSet rs = pst.getGeneratedKeys();
			int id=0;
			if(rs.next())
				id = rs.getInt(1);
			cupom.setId(id);
			
			connection.commit();		
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();			
		}finally{
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void alterar(EntidadeDominio entidade) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) throws SQLException {
		PreparedStatement pst = null;		
		Cupom cupom = (Cupom)entidade;
				
		String sql=null;

		sql = " select c.* " + 
				"	, ct.Id 'ct_Id'" + 
				"	, ct.Descricao 'dt_Descricao'" + 
				"	, cdt.Id 'cdt_Id'" + 
				"	, cdt.Descricao 'cdt_Descticao' " + 
				" from BookCommerce.Cupom c " + 
				"	join BookCommerce.CupomTipo ct on c.IdCupomTipo = ct.Id" + 
				"	join BookCommerce.CupomDescontoTipo cdt on c.IdCupomDescontoTipo = cdt.Id" + 
				" where " + 
				"	c.Status = 1" + 
				"	and ( ? is null or c.Codigo = ?) ";

		try {
			if(connection == null) {
				openConnection();
				connection.setAutoCommit(false);
			}
			pst = connection.prepareStatement(sql);
			
			int i = 1;
			if(cupom.getCodigo() == null) {
				pst.setNull(i++, Types.VARCHAR);				
				pst.setNull(i++, Types.VARCHAR);	
			} else {
				pst.setString(i++, cupom.getCodigo());
				pst.setString(i++, cupom.getCodigo());
			}			
			
			ResultSet rs = pst.executeQuery();
			List<EntidadeDominio> cupomList = new ArrayList<EntidadeDominio>();
			while (rs.next()) {
				Cupom pci = new Cupom();
				
				pci.setId(rs.getInt("Id"));
				pci.setCodigo(rs.getString("Codigo"));
				pci.setValorDesconto(rs.getFloat("ValorDesconto"));
				pci.setValidade(ConverteDate.Convert( rs.getDate("Validade")));
				pci.setStatus(rs.getBoolean("Status"));
				// Cupom Tipo
				pci.setCupomTipo(new CupomTipo());
				pci.getCupomTipo().setId(rs.getInt("ct_Id"));
				pci.getCupomTipo().setDescricao(rs.getString("dt_Descricao"));
				// Livro
				pci.setCupomDescontoTipo(new CupomDescontoTipo());
				pci.getCupomDescontoTipo().setId(rs.getInt("cdt_Id"));
				pci.getCupomDescontoTipo().setDescricao(rs.getString("cdt_Descticao"));
						
				cupomList.add(pci);
			}
			return cupomList;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
