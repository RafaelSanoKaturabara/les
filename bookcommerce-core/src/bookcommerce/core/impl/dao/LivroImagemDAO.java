package bookcommerce.core.impl.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.LivroImagem;

public class LivroImagemDAO extends AbstractJdbcDAO {

	public LivroImagemDAO() {
		super("BookCommerce.LivroImagem", "Id");
		// TODO Auto-generated constructor stub
	}

	@Override
	public void salvar(EntidadeDominio entidade) throws SQLException {
		openConnection();
		PreparedStatement pst=null;
		LivroImagem livroImagem = (LivroImagem)entidade;		
		
		try {
			connection.setAutoCommit(false);			
					
			StringBuilder sql = new StringBuilder();
			sql.append(" insert into BookCommerce.LivroImagem (IdLivro, Nome, Imagem )");
			sql.append(" values ( ?, ?, ?);");		
			
			pst = connection.prepareStatement(sql.toString(), 
					Statement.RETURN_GENERATED_KEYS);
			
			pst.setInt(1, livroImagem.getIdLivro());
			pst.setString(2, livroImagem.getNome());
			pst.setString(3, livroImagem.getImagem());			
			pst.executeUpdate();	
			
			ResultSet rs = pst.getGeneratedKeys();
			int id=0;
			if(rs.next())
				id = rs.getInt(1);
			livroImagem.setId(id);
			
			connection.commit();		
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();			
		}finally{
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void alterar(EntidadeDominio entidade) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) throws SQLException {
		PreparedStatement pst = null;		
		LivroImagem livroImagem = (LivroImagem)entidade;
		String sql=null;
		
		String IdLivro; 
		if(livroImagem.getIdLivro() > 0){
			IdLivro = Integer.toString(livroImagem.getIdLivro());
		} else
			IdLivro = "null";
		
		sql = " select * "
				+ " from BookCommerce.LivroImagem "
				+ " where "+ IdLivro + " is null or IdLivro = " + IdLivro
				+ ";";	
		try {
			openConnection();
			pst = connection.prepareStatement(sql);		
			ResultSet rs = pst.executeQuery();
			List<EntidadeDominio> livroImagemList = new ArrayList<EntidadeDominio>();
			while (rs.next()) {
				LivroImagem c = new LivroImagem();
				c.setId(rs.getInt("Id"));
				c.setIdLivro(rs.getInt("IdLivro"));
				c.setImagem(rs.getString("Imagem"));
				c.setNome(rs.getString("Nome"));
				livroImagemList.add(c);
			}
			return livroImagemList;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
