package bookcommerce.core.impl.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bookcommerce.dominio.Ano;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.GrupoPrecificacao;

public class AnoDAO extends AbstractJdbcDAO{

	public AnoDAO() {
		super("BookCommerce.Ano", "id");
		// TODO Auto-generated constructor stub
	}

	@Override
	public void salvar(EntidadeDominio entidade) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void alterar(EntidadeDominio entidade) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) throws SQLException {
		PreparedStatement pst = null;		
		Ano ano = (Ano)entidade;
		String sql=null;
		
		String Id; 
		if(ano.getId() != null && ano.getId() > 0){
			Id = Integer.toString(ano.getId());
		} else
			Id = "null";
		
		sql = " select * "
				+ " from BookCommerce.Ano "
				+ " where "+ Id + " is null or Id = " + Id
				+ " order by ano desc ;";	
		try {
			openConnection();
			pst = connection.prepareStatement(sql);		
			ResultSet rs = pst.executeQuery();
			List<EntidadeDominio> anoList = new ArrayList<EntidadeDominio>();
			while (rs.next()) {
				GrupoPrecificacao c = new GrupoPrecificacao();
				c.setId(rs.getInt("Id"));
				c.setNome(rs.getString("Ano"));
				anoList.add(c);
			}
			return anoList;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
