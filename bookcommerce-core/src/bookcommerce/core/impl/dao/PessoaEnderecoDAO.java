package bookcommerce.core.impl.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import bookcommerce.dominio.CartaoCreditoTipo;
import bookcommerce.dominio.Cidade;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.Estado;
import bookcommerce.dominio.Pais;
import bookcommerce.dominio.Pessoa;
import bookcommerce.dominio.PessoaCartaoCredito;
import bookcommerce.dominio.PessoaEndereco;
import bookcommerce.dominio.PessoaGenero;
import bookcommerce.dominio.PessoaTipo;
import bookcommerce.dominio.Telefone;
import bookcommerce.dominio.TelefoneTipo;

public class PessoaEnderecoDAO extends AbstractJdbcDAO {

	public PessoaEnderecoDAO() {
		super("BookCommerce.PessoaEndereco", "Id");
		// TODO Auto-generated constructor stub
	}

	public PessoaEnderecoDAO(Connection connection) {
		super(connection, "BookCommerce.PessoaEndereco", "Id");
		// TODO Auto-generated constructor stub
	}

	@Override
	public void salvar(EntidadeDominio entidade) throws SQLException {
		PreparedStatement pst = null;
		StringBuilder sql = new StringBuilder();
		try {
			if (connection == null) {
				openConnection();
			} else {
				ctrlTransaction = false;
			}
			PessoaEndereco pessoaEndereco = (PessoaEndereco) entidade;

			sql.append("INSERT INTO BookCommerce.Endereco (Logradouro, numero, Bairro, Cep, Apelido, IdCidade) ");
			sql.append(" VALUES (?, ?, ?, ?, ?, ?); ");
			connection.setAutoCommit(false);

			pst = connection.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			int i = 1;
			pst.setString(i++, pessoaEndereco.getLogradouro());
			pst.setString(i++, pessoaEndereco.getNumero());
			pst.setString(i++, pessoaEndereco.getBairro());
			pst.setString(i++, pessoaEndereco.getCep());
			pst.setString(i++, pessoaEndereco.getApelido());
			pst.setInt(i++, pessoaEndereco.getCidade().getId());

			pst.executeUpdate();

			ResultSet rs = pst.getGeneratedKeys();
			int idEnd = 0;
			if (rs.next())
				idEnd = rs.getInt(1);
			pessoaEndereco.setId(idEnd);

			if (ctrlTransaction) {// commita apenas se não está na conexão de outra classe			
				sql = new StringBuilder();
				sql.append(" insert into BookCommerce.PessoaEndereco (IdPessoa, IdEndereco) " + 
						" values (? ,?); ");
				connection.setAutoCommit(false);

				pst = connection.prepareStatement(sql.toString());
				int j = 1;
				pst.setInt(j++, pessoaEndereco.getIdPessoa());
				pst.setInt(j++, pessoaEndereco.getId());

				pst.executeUpdate();

//				rs = pst.getGeneratedKeys();
//				int idEnd = 0;
//				if (rs.next())
//					idEnd = rs.getInt(1);
//				pessoaEndereco.setId(idEnd);
//				
				
				connection.commit();
			}
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			if (ctrlTransaction) {
				try {
					pst.close();
					if (ctrlTransaction)
						connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void alterar(EntidadeDominio entidade) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) throws SQLException {
		PreparedStatement pst = null;

		PessoaEndereco pessoaEndereco = (PessoaEndereco) entidade;
		String sql = null;

		sql = "	select pe.* " 
				+ "	, e.*" + "	, c.Id 'c_Id'" 
				+ "	, c.Nome 'c_Nome'" 
				+ "	, es.Id 'es_Id'"
				+ "	, es.Nome 'es_Nome'" 
				+ " , es.Uf 'es_Uf' " 
				+ "	, p.Id 'p_Id'" 
				+ "	, p.Nome 'p_Nome' "
				+ "	, pes.IdCartaoPadrao 'pes_IdCartaoPadrao'\r\n" 
				+ "	, pes.IdEnderecoPadrao 'pes_IdEnderecoPadrao'"
				+ "	from BookCommerce.PessoaEndereco pe" 
				+ "	join BookCommerce.Endereco e on pe.IdEndereco = e.Id"
				+ "	join BookCommerce.Cidade c on e.IdCidade = c.Id"
				+ "	join BookCommerce.Estado es on c.IdEstado = es.Id"
				+ "	join BookCommerce.Pais p on es.IdPais = p.Id" 
				+ "	join BookCommerce.Pessoa pes on pe.IdPessoa = pes.Id "
				+ " where " 
				+ "	( ? is null or pe.IdPessoa = ?) ;";
		try {
			openConnection();
			pst = connection.prepareStatement(sql);
			int i = 1;
			if (pessoaEndereco.getIdPessoa() == 0) {
				pst.setNull(i++, Types.INTEGER);
				pst.setNull(i++, Types.INTEGER);
			} else {
				pst.setInt(i++, pessoaEndereco.getIdPessoa());
				pst.setInt(i++, pessoaEndereco.getIdPessoa());
			}

			ResultSet rs = pst.executeQuery();
			List<EntidadeDominio> pessoaEnderecoList = new ArrayList<EntidadeDominio>();
			while (rs.next()) {
				PessoaEndereco pe = new PessoaEndereco();

				pe.setId(rs.getInt("Id"));
				pe.setLogradouro(rs.getString("Logradouro"));
				pe.setNumero(rs.getString("numero"));
				pe.setApelido(rs.getString("Apelido"));
				pe.setCep(rs.getString("Cep"));
				
				pe.setPessoa(new Pessoa());
				pe.getPessoa().setCartaoCreditoPadrao(new PessoaCartaoCredito());
				pe.getPessoa().getCartaoCreditoPadrao().setId(rs.getInt("pes_IdCartaoPadrao"));
				
				pe.getPessoa().setEnderecoPadrao(new PessoaEndereco());
				pe.getPessoa().getEnderecoPadrao().setId(rs.getInt("pes_IdEnderecoPadrao"));

				pe.setCidade(new Cidade());
				pe.getCidade().setId(rs.getInt("c_Id"));
				pe.getCidade().setNome(rs.getString("c_Nome"));

				pe.getCidade().setEstado(new Estado());
				pe.getCidade().getEstado().setId(rs.getInt("es_Id"));
				pe.getCidade().getEstado().setNome(rs.getString("es_Nome"));
				pe.getCidade().getEstado().setUf(rs.getString("es_Uf"));

				pe.getCidade().getEstado().setPais(new Pais());
				pe.getCidade().getEstado().getPais().setId(rs.getInt("p_Id"));
				pe.getCidade().getEstado().getPais().setNome(rs.getString("p_Nome"));

				pessoaEnderecoList.add(pe);
			}
			return pessoaEnderecoList;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
