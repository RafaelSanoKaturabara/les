package bookcommerce.core.impl.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bookcommerce.dominio.Autor;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.GrupoPrecificacao;

public class AutorDAO extends AbstractJdbcDAO{

	public AutorDAO() {
		super("BookCommerce.Autor", "id");
	}

	@Override
	public void salvar(EntidadeDominio entidade) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void alterar(EntidadeDominio entidade) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) throws SQLException {
		PreparedStatement pst = null;		
		Autor autor = (Autor)entidade;
		String sql=null;
		
		String Id; 
		if(autor.getId() != null && autor.getId() > 0){
			Id = Integer.toString(autor.getId());
		} else
			Id = "null";
		
		sql = " select * "
				+ " from BookCommerce.Autor "
				+ " where "+ Id + " is null or Id = " + Id
				+ ";";	
		try {
			openConnection();
			pst = connection.prepareStatement(sql);		
			ResultSet rs = pst.executeQuery();
			List<EntidadeDominio> autorList = new ArrayList<EntidadeDominio>();
			while (rs.next()) {
				GrupoPrecificacao c = new GrupoPrecificacao();
				c.setId(rs.getInt("Id"));
				c.setNome(rs.getString("Nome"));
				autorList.add(c);
			}
			return autorList;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
