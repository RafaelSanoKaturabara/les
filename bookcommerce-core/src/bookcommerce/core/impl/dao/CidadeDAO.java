package bookcommerce.core.impl.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import bookcommerce.dominio.Cidade;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.Livro;
import bookcommerce.dominio.LivroImagem;

public class CidadeDAO extends AbstractJdbcDAO {

	public CidadeDAO(Connection connection) {
		super(connection, "BookCommerce.Cidade", "Id");
	}
	
	public CidadeDAO() {
		super("BookCommerce.Cidade", "Id");
	}

	@Override
	public void salvar(EntidadeDominio entidade) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void alterar(EntidadeDominio entidade) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) throws SQLException {
		PreparedStatement pst = null;		
		Cidade cidade = (Cidade)entidade;
		String sql=null;

		sql = " select c.* "
				+ " from BookCommerce.Cidade c "
				+ " where ( ? is null or c.IdEstado = ?) ;";	
		try {
			openConnection();
			pst = connection.prepareStatement(sql);	
			int i = 1;
			if(cidade.getEstado() == null || cidade.getEstado().getId() == 0) {
				pst.setNull(i++, Types.INTEGER);
				pst.setNull(i++, Types.INTEGER);
			}else {
				pst.setInt(i++, cidade.getEstado().getId());
				pst.setInt(i++, cidade.getEstado().getId());
			}			
			
			ResultSet rs = pst.executeQuery();
			List<EntidadeDominio> cidadeList = new ArrayList<EntidadeDominio>();
			while (rs.next()) {
				Cidade c = new Cidade();
				c.setId(rs.getInt("Id"));
				c.setNome(rs.getString("Nome"));
				cidadeList.add(c);
			}
			return cidadeList;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
