package bookcommerce.core.impl.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bookcommerce.dominio.Categoria;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.EstatisticaCategoria;

public class EstatisticaCategoriaDAO extends AbstractJdbcDAO {

	public EstatisticaCategoriaDAO(Connection connection) {
		super(connection, "", "");
	}
	
	public EstatisticaCategoriaDAO() {
		super( "", "");
	}

	@Override
	public void salvar(EntidadeDominio entidade) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void alterar(EntidadeDominio entidade) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) throws SQLException {
		PreparedStatement pst = null;		
		//EstatisticaCategoria estatisticaCategoria = (EstatisticaCategoria)entidade;
				
		String sql=null;
		
		sql = " select " + 
				"	sum(ci.Quantidade) 'qtde_vendida'" + 
				"	, c.Id 'c_Id'" + 
				"	, c.Nome 'c_Nome'" + 
				" from BookCommerce.CarrinhoItem ci" + 
				"	join BookCommerce.Livro l on ci.IdLivro = l.Id" + 
				"	join BookCommerce.LivroCategoria lc on l.Id = lc.IdLivro" + 
				"	join BookCommerce.Categoria c on lc.IdCategoria = c.Id" + 
				"	join BookCommerce.Carrinho ca on ci.IdCarrinho = ca.Id" + 
				" where " + 
				"	ca.IdCarrinhoStatus <> 7" + 
				"	and ca.IdCarrinhoStatus <> 1" + 
				" group by " + 
				"	c.Id" + 
				"	, c.Nome ";
		
		try {
			if(connection == null) {
				openConnection();
				connection.setAutoCommit(false);
			}
			pst = connection.prepareStatement(sql);
						
			ResultSet rs = pst.executeQuery();
			List<EntidadeDominio> estatisticaCategoriaList = new ArrayList<EntidadeDominio>();
			while (rs.next()) {
				EstatisticaCategoria ec = new EstatisticaCategoria();
				ec.setQuantidadeVendida(rs.getInt("qtde_vendida"));
				
				ec.setCategoria(new Categoria());
				ec.getCategoria().setId(rs.getInt("c_Id"));
				ec.getCategoria().setNome(rs.getString("c_Nome"));	
						
				estatisticaCategoriaList.add(ec);
			}		
			return estatisticaCategoriaList;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
