package bookcommerce.core.impl.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bookcommerce.dominio.Ano;
import bookcommerce.dominio.Autor;
import bookcommerce.dominio.Categoria;
import bookcommerce.dominio.Editora;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.GrupoPrecificacao;
import bookcommerce.dominio.Livro;
import bookcommerce.dominio.PesquisaLivro;

public class PesquisaLivroDAO extends AbstractJdbcDAO {
	
	private void verificarMaisParametros(PesquisaLivro pesquisaLivro, List<EntidadeDominio> livroList) {
		boolean flgEncontrou;
		Livro livro = new Livro();
		List<EntidadeDominio> livroToRemoveList = new ArrayList<EntidadeDominio>();
		if(pesquisaLivro != null && pesquisaLivro.getParamList() != null && livroList != null && livroList.size() > 0) {
			// contains
			for (String strPesquisa : pesquisaLivro.getParamList()) {
				for(EntidadeDominio livroWork : livroList ) {
					flgEncontrou = false;
					livro = (Livro)livroWork;
					if(livro.getTitulo().toLowerCase().contains(strPesquisa)) { // encontrou a string igual
						flgEncontrou = true;
					}
					if(livro.getCodBarras().toLowerCase().contains(strPesquisa)) { // encontrou a string igual
						flgEncontrou = true;
					}
					if(livro.getEditora() != null && livro.getEditora().getNome().toLowerCase().contains(strPesquisa)) { // encontrou a string igual
						flgEncontrou = true;
					}		
					// loop para verificar autor
					if(livro.getAutorList() != null && livro.getAutorList().size() > 0) {
						for(Autor autor : livro.getAutorList()) {
							if(autor.getNome().toLowerCase().contains(strPesquisa)) { // encontrou a string igual
								flgEncontrou = true;
							}
						}
					}
					// loop para verificar a categoria
					if(livro.getCategoriaList() != null && livro.getCategoriaList().size() > 0) {
						for(Categoria categoria : livro.getCategoriaList()) {
							if(categoria.getNome().toLowerCase().contains(strPesquisa)) { // encontrou a string igual
								flgEncontrou = true;
							}
						}
					}
					if(!flgEncontrou) { // não encontrou?
						livroToRemoveList.add(livroWork);
					}
				}						
			}
			// remover os livros
			if(livroToRemoveList.size() > 0) {
				for(EntidadeDominio entidadeDominio : livroToRemoveList) {
					livroList.remove(entidadeDominio);
				}
			}
		}
		//return livroList;
	}

	public PesquisaLivroDAO(Connection connection) {
		super(connection, "", "");
	}
	
	public PesquisaLivroDAO() {
		super("", "");
	}

	@Override
	public void salvar(EntidadeDominio entidade) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void alterar(EntidadeDominio entidade) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) throws SQLException {
		PreparedStatement pst = null;
		
		PesquisaLivro pesquisaLivro = (PesquisaLivro)entidade;
		String sql=null;
		
		sql = " select distinct\r\n" + 
				"	l.* \r\n" + 
				"	, gp.Id as 'grupoPrecificacao_Id' \r\n" + 
				"	, gp.Nome as 'grupoPrecificacao_Nome'\r\n" + 
				"	, gp.MargemVenda as 'grupoPrecificacao_MargemVenda'\r\n" + 
				"	, e.Id as 'editora_Id'\r\n" + 
				"	, e.Nome as 'editora_Nome'\r\n" + 
				"	, a.Id as 'ano_Id'\r\n" + 
				"	, a.Ano as 'ano_Ano'  		\r\n" + 
				"from BookCommerce.livro l 			\r\n" + 
				"	join BookCommerce.GrupoPrecificacao gp on l.IdGrupoPrecificacao = gp.Id \r\n" + 
				"	join BookCommerce.Editora e on l.IdEditora = e.Id 	 		\r\n" + 
				"	join BookCommerce.Ano a on l.IdAno = a.Id  	       \r\n" + 
				"	left join BookCommerce.LivroCategoria lc on l.Id = lc.IdLivro\r\n" + 
				"	join BookCommerce.Categoria c on lc.IdCategoria = c.Id\r\n" + 
				"	left join BookCommerce.LivroAutor la on l.Id = la.IdLivro\r\n" + 
				"	join BookCommerce.Autor au on la.IdAutor = au.Id\r\n" + 
				"where \r\n" + 
				" l.Ativo = 1 and (" +
				"	l.CodigoBarras like '%@PARAM%' \r\n" + 
				"	OR l.Titulo like '%@PARAM%'\r\n" + 
				"	OR l.ISBN like '%@PARAM%'\r\n" + 
				"	OR e.Nome like '%@PARAM%'\r\n" + 
				"	OR c.Nome like '%@PARAM%'\r\n" + 
				"	OR au.Nome like '%@PARAM%'     ) "; 
		sql = sql.replaceAll("@PARAM", pesquisaLivro.getParamList().get(0));		
		try {
			openConnection();
			pst = connection.prepareStatement(sql);
			ResultSet rs = pst.executeQuery();
			List<EntidadeDominio> livroList = new ArrayList<EntidadeDominio>();
			while (rs.next()) {
				Livro li = new Livro();
				
				li.setId(rs.getInt("Id"));
				li.setCodBarras(rs.getString("CodigoBarras"));
				li.setTitulo(rs.getString("Titulo"));
				li.setEdicao(rs.getInt("Edicao"));
				li.setIsbn(rs.getString("ISBN"));
				li.setQtdPaginas(rs.getInt("NumeroPaginas"));
				li.setSinopse(rs.getString("Sinopse"));
				li.setAltura(rs.getFloat("Altura"));
				li.setLargura(rs.getFloat("Largura"));
				li.setProfundidade(rs.getFloat("Profundidade"));
				li.setPeso(rs.getFloat("Peso"));
				li.setAtivo(rs.getBoolean("Ativo"));
				li.setPreco(rs.getFloat("Preco"));
				li.setImg100(rs.getString("Img100"));
				li.setImg210(rs.getString("Img210"));
				
				li.setGrupoPrecificacao(new GrupoPrecificacao());
				li.getGrupoPrecificacao().setId(rs.getInt("grupoPrecificacao_Id"));
				li.getGrupoPrecificacao().setNome(rs.getString("grupoPrecificacao_Nome"));
				li.getGrupoPrecificacao().setMargemVenda(rs.getFloat("grupoPrecificacao_MargemVenda"));
				
				li.setAno(new Ano());
				li.getAno().setId(rs.getInt("ano_Id"));
				li.getAno().setAno(rs.getInt("ano_Ano"));
				
				li.setEditora(new Editora());
				li.getEditora().setId(rs.getInt("editora_Id"));
				li.getEditora().setNome(rs.getString("editora_Nome"));
						
				livroList.add(li);
			}
			LivroDAO livroDAO = new LivroDAO();
			livroDAO.getAndPutAutorLivro(livroList);
			livroDAO.getAndPutCategoriaLivro(livroList);
			
			verificarMaisParametros(pesquisaLivro, livroList);
//			
//			if(livroList.size() == 1) { // pesquisou um livro em específico?
//				new CarrinhoItemDAO().desabilitarCarrinhoItensAntigos();
//				putEstoqueLivro(livroList.get(0));
//			}
			
			return livroList;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
