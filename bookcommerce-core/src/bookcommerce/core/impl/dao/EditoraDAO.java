package bookcommerce.core.impl.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bookcommerce.dominio.Editora;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.GrupoPrecificacao;

public class EditoraDAO extends AbstractJdbcDAO{

	public EditoraDAO() {
		super("BookCommerce.Editora", "id");
		// TODO Auto-generated constructor stub
	}

	@Override
	public void salvar(EntidadeDominio entidade) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void alterar(EntidadeDominio entidade) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) throws SQLException {
		PreparedStatement pst = null;		
		Editora editora = (Editora)entidade;
		String sql=null;
		
		String Id; 
		if(editora.getId() != null && editora.getId() > 0){
			Id = Integer.toString(editora.getId());
		} else
			Id = "null";
		
		sql = " select * "
				+ " from BookCommerce.Editora "
				+ " where "+ Id + " is null or Id = " + Id
				+ ";";	
		try {
			openConnection();
			pst = connection.prepareStatement(sql);		
			ResultSet rs = pst.executeQuery();
			List<EntidadeDominio> editoraList = new ArrayList<EntidadeDominio>();
			while (rs.next()) {
				GrupoPrecificacao c = new GrupoPrecificacao();
				c.setId(rs.getInt("Id"));
				c.setNome(rs.getString("Nome"));
				editoraList.add(c);
			}
			return editoraList;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
