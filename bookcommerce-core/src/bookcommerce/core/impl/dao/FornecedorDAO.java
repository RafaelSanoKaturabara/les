
package bookcommerce.core.impl.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.Fornecedor;

public class FornecedorDAO extends AbstractJdbcDAO {
	
	public FornecedorDAO() {
		super("BookCommerce.Fornecedor", "Id");		
	}
	
	public FornecedorDAO(Connection connection) {
		super(connection, "BookCommerce.Fornecedor", "Id");
	}
	
	public void salvar(EntidadeDominio entidade) {
		
	}
	
	@Override
	public void alterar(EntidadeDominio entidade) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) {
		PreparedStatement pst = null;

		//Entrada entrada = (Entrada) entidade;
		String sql = null;

		sql = " select f.* " + 
				"from BookCommerce.Fornecedor f ";
		try {
			openConnection();
			pst = connection.prepareStatement(sql);
//			int i = 1;
//			if (entrada.getId() == null || entrada.getId() == 0) {
//				pst.setNull(i++, Types.INTEGER);
//				pst.setNull(i++, Types.INTEGER);
//			} else {
//				pst.setInt(i++, entrada.getId());
//				pst.setInt(i++, entrada.getId());
//			}

			ResultSet rs = pst.executeQuery();
			List<EntidadeDominio> fornecedorList = new ArrayList<EntidadeDominio>();
			while (rs.next()) {
				Fornecedor fo = new Fornecedor();

				fo.setId(rs.getInt("Id"));
				fo.setNome(rs.getString("Nome"));
				fo.setCnpj(rs.getString("Cnpj"));
				
				fornecedorList.add(fo);
			}
			return fornecedorList;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
