package bookcommerce.core.impl.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bookcommerce.dominio.Categoria;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.Livro;
import bookcommerce.dominio.LivroCategoria;

public class LivroCategoriaDAO extends AbstractJdbcDAO {

	public LivroCategoriaDAO() {
		super("BookCommerce.LivroCategoria", "IdLivro, IdCategoria");
		// TODO Auto-generated constructor stub
	}

	public void putCategorisListInLivroList(List<EntidadeDominio> entidade) {
		PreparedStatement pst = null;	
		if(entidade == null)
			return;	
		List<Categoria> categoriaList = new ArrayList<Categoria>();
		List<LivroCategoria> livroCategoriaList = new ArrayList<LivroCategoria>();
		
		String sql=null;
		// Obtendo lista de Categorias
		sql = " select * from BookCommerce.Categoria ";	
		try {
			openConnection();
			pst = connection.prepareStatement(sql);						
			ResultSet rs = pst.executeQuery();			
			while (rs.next()) {
				Categoria categoria = new Categoria();
				categoria.setId(rs.getInt("Id"));
				categoria.setNome(rs.getString("Nome"));
				categoriaList.add(categoria);
			}
			if(categoriaList.isEmpty())
				return;
					
		} catch (SQLException e) {
			e.printStackTrace();
		}
		// neste ponto, possui a lista de Todos os autores cadastrados
		sql = " select * from BookCommerce.LivroCategoria ";
		try {
			openConnection();
			pst = connection.prepareStatement(sql);						
			ResultSet rs = pst.executeQuery();			
			while (rs.next()) {
				LivroCategoria livroCategoria = new LivroCategoria();
				livroCategoria.setIdLivro(rs.getInt("IdLivro"));
				livroCategoria.setIdCategoria(rs.getInt("IdCategoria"));
				livroCategoriaList.add(livroCategoria);
			}
			if(livroCategoriaList.isEmpty())
				return;
					
		} catch (SQLException e) {
			e.printStackTrace();
		}
		// neste ponto, possu� a lista de LivroAutor
		for(LivroCategoria livroCategoria : livroCategoriaList) { // loop para inserir os autores no livroAutor
			for(Categoria categoria : categoriaList) {
				if(livroCategoria.getIdCategoria() == categoria.getId())
					livroCategoria.setCategoria(categoria);
			}
		}
		// neste ponto, possui LivroAutorList com obj autor completo
		for(EntidadeDominio livroEntidade : entidade) {
			Livro livro = (Livro)livroEntidade;
			List<Categoria> cList = new ArrayList<Categoria>();
			for(LivroCategoria livroCategoria : livroCategoriaList) {
				if(livro.getId() == livroCategoria.getIdLivro())
					cList.add(livroCategoria.getCategoria());
			}
			livro.setCategoriaList(cList);
		}
	}
	@Override
	public void salvar(EntidadeDominio entidade) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void alterar(EntidadeDominio entidade) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}
	public void excluir(EntidadeDominio entidade) {
		Livro livro = (Livro)entidade;
		openConnection();
		PreparedStatement pst=null;
		
		try {
			connection.setAutoCommit(false);			
					
			StringBuilder sql = new StringBuilder();
			sql.append(" delete BookCommerce.LivroCategoria where IdLivro = ? ");	
			
			pst = connection.prepareStatement(sql.toString());
			
			pst.setInt(1, livro.getId());
			pst.executeUpdate();				
			connection.commit();		
			
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();			
		}finally{
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}	
	}
}
