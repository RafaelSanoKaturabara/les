package bookcommerce.core.impl.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import bookcommerce.dominio.Carrinho;
import bookcommerce.dominio.CarrinhoPagamento;
import bookcommerce.dominio.CarrinhoPagamentoStatus;
import bookcommerce.dominio.CarrinhoStatus;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.PessoaCartaoCredito;

public class CarrinhoPagamentoDAO extends AbstractJdbcDAO {

	public CarrinhoPagamentoDAO(Connection connection) {
		super(connection, "BookCommerce.CarrinhoPagamento", "Id");
	}
	
	public CarrinhoPagamentoDAO() {
		super("BookCommerce.CarrinhoPagamento", "Id");
	}

	@Override
	public void salvar(EntidadeDominio entidade) throws SQLException {
		PreparedStatement pst = null;
		StringBuilder sql = new StringBuilder();
		try {
			if (connection == null) {
				openConnection();
			} else {
				ctrlTransaction = false;
			}
			CarrinhoPagamento carrinhoPagamento = (CarrinhoPagamento) entidade;

			// obter o carrinho
			if(carrinhoPagamento.getCarrinho().getId() == null || carrinhoPagamento.getCarrinho().getId() == 0) {// Não enviou carrinho
				carrinhoPagamento.getCarrinho().setCarrinhoStatus(new CarrinhoStatus());
				carrinhoPagamento.getCarrinho().getCarrinhoStatus().setId(1); // Tipo Carrinho
				new CarrinhoDAO().salvar(carrinhoPagamento.getCarrinho());
			}
			
			sql.append(" insert into BookCommerce.CarrinhoPagamento (IdCarrinho, Valor, IdPessoaCartao) ");
			sql.append(" values ( ?, ?, ? ); ");
			connection.setAutoCommit(false);

			pst = connection.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			int i = 1;
			pst.setInt(i++, carrinhoPagamento.getCarrinho().getId());
			pst.setFloat(i++, carrinhoPagamento.getValor());
			pst.setInt(i++, carrinhoPagamento.getPessoaCartaoCredito().getId());

			pst.executeUpdate();

			ResultSet rs = pst.getGeneratedKeys();
			int idEnd = 0;
			if (rs.next())
				idEnd = rs.getInt(1);
			carrinhoPagamento.setId(idEnd);

			if (ctrlTransaction) {// commita apenas se não está na conexão de outra classe		
				connection.commit();
			}
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			if (ctrlTransaction) {
				try {
					pst.close();
					if (ctrlTransaction)
						connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void alterar(EntidadeDominio entidade) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) throws SQLException {
		PreparedStatement pst = null;		
		CarrinhoPagamento carrinhoPagamento = (CarrinhoPagamento)entidade;
				
		String sql=null;
		
		sql = " select cp.*" + 
				"	, pcc.Apelido 'pcc_Apelido'" + 
				"	, cps.Descricao 'cps_Descricao'" + 
				" from BookCommerce.CarrinhoPagamento cp" + 
				"	join BookCommerce.Carrinho c on cp.IdCarrinho = c.Id" + 
				"	join BookCommerce.PessoaCartaoCredito pcc on cp.IdPessoaCartao = pcc.Id" + 
				"	left join BookCommerce.CarrinhoPagamentoStatus cps on cp.IdCarrinhoPagamentoStatus = cps.Id" + 
				" where " + 
				"	c.Id = ? ";
		
		try {
			if(connection == null) {
				openConnection();
				connection.setAutoCommit(false);
			}
			pst = connection.prepareStatement(sql);
			
			int i = 1;
			pst.setInt(i++, carrinhoPagamento.getCarrinho().getId());			
			
			ResultSet rs = pst.executeQuery();
			List<EntidadeDominio> carrinhoPagamentoList = new ArrayList<EntidadeDominio>();
			while (rs.next()) {
				CarrinhoPagamento cp = new CarrinhoPagamento();
				cp.setId(rs.getInt("Id"));
				cp.setValor(rs.getFloat("Valor"));
				cp.setCodigoPagamentoExterno(rs.getString("CodigoAutorizacaoExterno"));
				
				cp.setCarrinho(new Carrinho());
				cp.getCarrinho().setId(rs.getInt("IdCarrinho"));
				
				cp.setPessoaCartaoCredito(new PessoaCartaoCredito());
				cp.getPessoaCartaoCredito().setId(rs.getInt("IdPessoaCartao"));
				cp.getPessoaCartaoCredito().setApelido(rs.getString("pcc_Apelido"));
				
				cp.setCarrinhoPagamentoStatus(new CarrinhoPagamentoStatus());
				cp.getCarrinhoPagamentoStatus().setId(rs.getInt("IdCarrinhoPagamentoStatus"));
				cp.getCarrinhoPagamentoStatus().setDescricao(rs.getString("cps_Descricao"));	
						
				carrinhoPagamentoList.add(cp);
			}		
			return carrinhoPagamentoList;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
