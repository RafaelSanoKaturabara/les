package bookcommerce.core.impl.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bookcommerce.dominio.Autor;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.Livro;
import bookcommerce.dominio.LivroAutor;

public class LivroAutorDAO extends AbstractJdbcDAO{	
	public LivroAutorDAO() {
		super("BookCommerce.LivroAutor", "IdLivro, IdAutor");
		// TODO Auto-generated constructor stub
	}
	
	public void putAutorListInLivroList(List<EntidadeDominio> entidade) {
		PreparedStatement pst = null;	
		if(entidade == null)
			return;	
		List<Autor> autorList = new ArrayList<Autor>();
		List<LivroAutor> livroAutorList = new ArrayList<LivroAutor>();
		
		String sql=null;
		// Obtendo lista de Autores
		sql = " select * from BookCommerce.Autor ";	
		try {
			openConnection();
			pst = connection.prepareStatement(sql);						
			ResultSet rs = pst.executeQuery();			
			while (rs.next()) {
				Autor autor = new Autor();
				autor.setId(rs.getInt("Id"));
				autor.setNome(rs.getString("Nome"));
				autorList.add(autor);
			}
			if(autorList.isEmpty())
				return;
					
		} catch (SQLException e) {
			e.printStackTrace();
		}
		// neste ponto, possui a lista de Todos os autores cadastrados
		sql = " select * from BookCommerce.LivroAutor ";
		try {
			openConnection();
			pst = connection.prepareStatement(sql);						
			ResultSet rs = pst.executeQuery();			
			while (rs.next()) {
				LivroAutor livroAutor = new LivroAutor();
				livroAutor.setIdLivro(rs.getInt("IdLivro"));
				livroAutor.setIdAutor(rs.getInt("IdAutor"));
				livroAutorList.add(livroAutor);
			}
			if(livroAutorList.isEmpty())
				return;
					
		} catch (SQLException e) {
			e.printStackTrace();
		}
		// neste ponto, possu� a lista de LivroAutor
		for(LivroAutor livroAutor : livroAutorList) { // loop para inserir os autores no livroAutor
			for(Autor autor : autorList) {
				if(livroAutor.getIdAutor() == autor.getId())
					livroAutor.setAutor(autor);
			}
		}
		// neste ponto, possui LivroAutorList com obj autor completo
		for(EntidadeDominio livroEntidade : entidade) {
			Livro livro = (Livro)livroEntidade;
			List<Autor> aList = new ArrayList<Autor>();
			for(LivroAutor livroAutor : livroAutorList) {
				if(livro.getId() == livroAutor.getIdLivro())
					aList.add(livroAutor.getAutor());
			}
			livro.setAutorList(aList);
		}
	}

	@Override
	public void salvar(EntidadeDominio entidade) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void alterar(EntidadeDominio entidade) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) throws SQLException {

		return null;
	}
	
	public void excluir(EntidadeDominio entidade) {
		Livro livro = (Livro)entidade;
		openConnection();
		PreparedStatement pst=null;
		
		try {
			connection.setAutoCommit(false);			
					
			StringBuilder sql = new StringBuilder();
			sql.append(" delete BookCommerce.LivroAutor where IdLivro = ? ");	
			
			pst = connection.prepareStatement(sql.toString());
			
			pst.setInt(1, livro.getId());
			pst.executeUpdate();				
			connection.commit();
						
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();			
		}finally{
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
