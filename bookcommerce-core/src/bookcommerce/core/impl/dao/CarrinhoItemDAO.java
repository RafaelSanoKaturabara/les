package bookcommerce.core.impl.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import bookcommerce.dominio.Ano;
import bookcommerce.dominio.Carrinho;
import bookcommerce.dominio.Editora;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.Livro;
import bookcommerce.dominio.CarrinhoItem;
import bookcommerce.dominio.CarrinhoStatus;

public class CarrinhoItemDAO extends AbstractJdbcDAO {

	public CarrinhoItemDAO(Connection connection) {
		super(connection, "BookCommerce.CarrinhoItem", "Id");
	}

	public CarrinhoItemDAO() {
		super("BookCommerce.CarrinhoItem", "Id");
	}
	
	public void desabilitarCarrinhoItensAntigos() {
		PreparedStatement pst=null;
		try {
			openConnection();
			connection.setAutoCommit(false);			
			
			String sql = " update BookCommerce.CarrinhoItem set Status = 0" + 
					" where Id in " + 
					"(select ci.Id from BookCommerce.CarrinhoItem ci" + 
					"	join BookCommerce.Carrinho c on ci.IdCarrinho = c.Id" + 
					" where " + 
					"	c.IdCarrinhoStatus = 1 and" + 
					"	DATEDIFF(minute, c.DataUltimaAlteracao, getdate()) > 30 )  "; 
			pst = connection.prepareStatement(sql.toString());				
			
			pst.executeUpdate();			
			connection.commit();		
			
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();			
		}finally{
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void salvar(EntidadeDominio entidade) throws SQLException {
		openConnection();
		PreparedStatement pst=null;	
		CarrinhoItem carrinhoItem = (CarrinhoItem)entidade;
		// obter o carrinho
		if(carrinhoItem.getCarrinho().getId() == null || carrinhoItem.getCarrinho().getId() == 0) {// Não enviou carrinho
			carrinhoItem.getCarrinho().setCarrinhoStatus(new CarrinhoStatus());
			carrinhoItem.getCarrinho().getCarrinhoStatus().setId(1); // Aguardando pagamento
			new CarrinhoDAO().salvar(carrinhoItem.getCarrinho());
		}
		
		// verificar se já existe este item no carrinho		
		List<EntidadeDominio> pessoaCarrinhoItemList = consultar(entidade);
		if(!pessoaCarrinhoItemList.isEmpty()) { // Já existe o livro no carrinho?
			// atualizar
			CarrinhoItem pciWork = (CarrinhoItem) pessoaCarrinhoItemList.get(0);
			entidade.setId(pessoaCarrinhoItemList.get(0).getId());
			carrinhoItem.setQuantidade(pciWork.getQuantidade() + carrinhoItem.getQuantidade());
			alterar(entidade);
			return;
		} 

		try {
			connection.setAutoCommit(false);			
					
			StringBuilder sql = new StringBuilder();
			sql.append(" insert into BookCommerce.CarrinhoItem (IdLivro,  ");
			sql.append("  PrecoItem, Quantidade, Status, IdCarrinho) ");		
			sql.append(" values ( ?, ?, ?, ?, ? ) ; ");			
			
			pst = connection.prepareStatement(sql.toString(), 
					Statement.RETURN_GENERATED_KEYS);
			int i = 1;
			pst.setInt(i++, carrinhoItem.getLivro().getId());
			pst.setFloat(i++, carrinhoItem.getPrecoItem());
			pst.setInt(i++, carrinhoItem.getQuantidade());
			pst.setBoolean(i++, carrinhoItem.getStatus());
			pst.setInt(i++, carrinhoItem.getCarrinho().getId());
			
			pst.executeUpdate();	
			
			ResultSet rs = pst.getGeneratedKeys();
			int id=0;
			if(rs.next())
				id = rs.getInt(1);
			carrinhoItem.setId(id);
			
			connection.commit();		
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();			
		}finally{
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
	}

	@Override
	public void alterar(EntidadeDominio entidade) throws SQLException {
		if(connection == null) {
			openConnection();
			connection.setAutoCommit(false);
		}
		PreparedStatement pst=null;
		CarrinhoItem pessoaCarrinhoItem = (CarrinhoItem)entidade;		
		if(pessoaCarrinhoItem.getId() == null || pessoaCarrinhoItem.getId() == 0)
			return;
		try {
			connection.setAutoCommit(false);			
			
			String sql = " update BookCommerce.CarrinhoItem set " + 
					" IdLivro = ? " + 
					" , PrecoItem = ?" + 
					" , Quantidade = ?" + 
					" , Status = ?  " + 
					" where " + 
					"	Id = ? ;  "; 
			pst = connection.prepareStatement(sql.toString());
			
			int i = 1;
			pst.setInt(i++, pessoaCarrinhoItem.getLivro().getId());
			pst.setFloat(i++, pessoaCarrinhoItem.getPrecoItem());
			pst.setInt(i++, pessoaCarrinhoItem.getQuantidade());
			pst.setBoolean(i++, pessoaCarrinhoItem.getStatus());
			pst.setInt(i++, pessoaCarrinhoItem.getId());			
			
			pst.executeUpdate();			
			connection.commit();		
			
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();			
		}finally{
			try {
				pst.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) throws SQLException {
		PreparedStatement pst = null;		
		CarrinhoItem carrinhoItem = (CarrinhoItem)entidade;
		boolean flgConsultandoParaSalvar = 
				(carrinhoItem.getId() == null || carrinhoItem.getId() == 0) 
				&& (carrinhoItem.getCarrinho() != null && carrinhoItem.getCarrinho().getId() != null
						&& carrinhoItem.getCarrinho().getId() > 0)
				&& (carrinhoItem.getLivro() != null && carrinhoItem.getLivro().getId() != null
						&& carrinhoItem.getLivro().getId() > 0);
				
		String sql=null;
		
		sql = " select ci.*   " +
				" 	, l.Id 'l_Id'  " +
				"	, l.Titulo 'l_Titulo' " +
				" 	, l.Edicao 'l_Edicao' " +
				" 	, l.Sinopse 'l_Sinopse' " +
				" 	, l.Preco 'l_Preco' " +
				" 	, l.img100 'l_img100' " +
				" 	, a.Ano 'a_Ano' " +
				"   , e.Id 'e_Id' " +
				" 	, e.Nome 'e_nome' " +
				" from BookCommerce.CarrinhoItem ci  " +
				" 	join BookCommerce.Livro l on ci.IdLivro = l.Id " +
				" 	join BookCommerce.Ano a on l.IdAno = a.Id " +
				" 	join BookCommerce.Editora e on l.IdEditora = e.Id " +
				" where	(? is null or ci.Id = ?) and"
				+ " ( ? is null or ci.IdCarrinho = ? ) ";
		
		if(flgConsultandoParaSalvar)
			sql += "	and (? is null or ci.IdLivro = ?) ;  "; 
		
		try {
			if(connection == null) {
				openConnection();
				connection.setAutoCommit(false);
			}
			pst = connection.prepareStatement(sql);
			
			int i = 1;
			if(carrinhoItem.getId() == null || carrinhoItem.getId() == 0) {
				pst.setNull(i++, Types.INTEGER);				
				pst.setNull(i++, Types.INTEGER);	
			} else {
				pst.setInt(i++, carrinhoItem.getId());
				pst.setInt(i++, carrinhoItem.getId());
			}
			if(carrinhoItem.getCarrinho() == null || carrinhoItem.getCarrinho().getId() == null 
					|| carrinhoItem.getCarrinho().getId() == 0) {
				pst.setNull(i++, Types.INTEGER);				
				pst.setNull(i++, Types.INTEGER);	
			} else {
				pst.setInt(i++, carrinhoItem.getCarrinho().getId());
				pst.setInt(i++, carrinhoItem.getCarrinho().getId());
			}
			
			if(flgConsultandoParaSalvar) {
				if(carrinhoItem.getLivro() == null || carrinhoItem.getLivro().getId() == null
						|| carrinhoItem.getLivro().getId() == 0) {
					pst.setNull(i++, Types.INTEGER);		
					pst.setNull(i++, Types.INTEGER);			
				} else {
					pst.setInt(i++, carrinhoItem.getLivro().getId());
					pst.setInt(i++, carrinhoItem.getLivro().getId());
				}
//				if(pessoaCarrinhoItem.getPessoa() == null || pessoaCarrinhoItem.getPessoa().getId() == null
//						|| pessoaCarrinhoItem.getPessoa().getId() == 0) {
//					pst.setNull(i++, Types.INTEGER);	
//					pst.setNull(i++, Types.INTEGER);				
//				} else {
//					pst.setInt(i++, pessoaCarrinhoItem.getPessoa().getId());
//					pst.setInt(i++, pessoaCarrinhoItem.getPessoa().getId());
//				}				
			}			
			
			ResultSet rs = pst.executeQuery();
			List<EntidadeDominio> pessoaCarrinhoItemList = new ArrayList<EntidadeDominio>();
			while (rs.next()) {
				CarrinhoItem pci = new CarrinhoItem();
				
				pci.setId(rs.getInt("Id"));
				pci.setPrecoItem(rs.getFloat("PrecoItem"));
				pci.setQuantidade(rs.getInt("Quantidade"));
				pci.setStatus(rs.getBoolean("Status"));
				// Pessoas
				pci.setCarrinho(new Carrinho());
				pci.getCarrinho().setId(rs.getInt("IdCarrinho"));
				// Livro
				pci.setLivro(new Livro());
				pci.getLivro().setId(rs.getInt("IdLivro"));
				pci.getLivro().setTitulo(rs.getString("l_Titulo"));
				pci.getLivro().setEdicao(rs.getInt("l_Edicao"));
				pci.getLivro().setSinopse(rs.getString("l_Sinopse"));
				pci.getLivro().setPreco(rs.getFloat("l_Preco"));
				pci.getLivro().setImg100(rs.getString("l_img100"));
				
				pci.getLivro().setAno(new Ano());
				pci.getLivro().getAno().setAno(rs.getInt("a_Ano"));
				
				pci.getLivro().setEditora(new Editora());
				pci.getLivro().getEditora().setId(rs.getInt("e_Id"));
				pci.getLivro().getEditora().setNome(rs.getString("e_nome"));
						
				pessoaCarrinhoItemList.add(pci);
			}
			return pessoaCarrinhoItemList;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
