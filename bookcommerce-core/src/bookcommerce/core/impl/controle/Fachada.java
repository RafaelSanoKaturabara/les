package bookcommerce.core.impl.controle;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import bookcommerce.core.IDAO;
import bookcommerce.core.IFachada;
import bookcommerce.core.IStrategy;
import bookcommerce.core.aplicacao.Resultado;
import bookcommerce.core.impl.dao.*;
import bookcommerce.core.impl.negocio.*;
import bookcommerce.dominio.*;

public class Fachada implements IFachada {

	/** 
	 * Mapa de DAOS, será indexado pelo nome da entidade 
	 * O valor � uma instância do DAO para uma dada entidade; 
	 */
	private Map<String, IDAO> daos;
	
	/**
	 * Mapa para conter as regras de neg�cio de todas operações por entidade;
	 * O valor é um mapa que de regras de negócio indexado pela operação
	 */
	private Map<String, Map<String, List<IStrategy>>> rns;
	
	private Resultado resultado;
	
	
	public Fachada(){
		/* Int�nciando o Map de DAOS */
		daos = new HashMap<String, IDAO>();
		/* Int�nciando o Map de Regras de Neg�cio */
		rns = new HashMap<String, Map<String, List<IStrategy>>>();
		
		/* Criando inst�ncias dos DAOs a serem utilizados*/
		FornecedorDAO forDAO = new FornecedorDAO();
		ClienteDAO cliDAO = new ClienteDAO();
		ProdutoDAO proDAO = new ProdutoDAO();
		GrupoPrecificacaoDAO GrupoPrecificacaoDAO = new GrupoPrecificacaoDAO();
		AnoDAO anoDAO = new AnoDAO();
		AutorDAO autorDAO = new AutorDAO();
		CategoriaDAO categoriaDAO = new CategoriaDAO();
		EditoraDAO editoraDAO = new EditoraDAO();
		LivroDAO livroDAO = new LivroDAO();
		LivroImagemDAO livroImagemDAO = new LivroImagemDAO();
		PessoaDAO pessoaDAO = new PessoaDAO();
		PessoaEnderecoDAO pessoaEnderecoDAO = new PessoaEnderecoDAO();
		PessoaCartaoCreditoDAO pessoaCartaoCreditoDAO = new PessoaCartaoCreditoDAO();
		CidadeDAO cidadeDAO = new CidadeDAO();
		CarrinhoItemDAO carrinhoItemDAO = new CarrinhoItemDAO();
		CarrinhoDAO carrinhoDAO = new CarrinhoDAO();
		CarrinhoCupomDAO carrinhoCupomDAO = new CarrinhoCupomDAO();
		CarrinhoPagamentoDAO carrinhoPagamentoDAO = new CarrinhoPagamentoDAO();
		EntradaDAO entradaDAO = new EntradaDAO();
		EntradaLivroDAO entradaLivroDAO = new EntradaLivroDAO();
		EstatisticaCategoriaDAO estatisticaCategoriaDAO = new EstatisticaCategoriaDAO();
		EstatisticaLivroVendidoMesDAO estatisticaLivroVendidoMesDAO = new EstatisticaLivroVendidoMesDAO();
		LogDAO logDAO = new LogDAO();
		PesquisaLivroDAO pesquisaLivroDAO = new PesquisaLivroDAO();
		
		/* Adicionando cada dao no MAP indexando pelo nome da classe */
		daos.put(Fornecedor.class.getName(), forDAO);		
		daos.put(Cliente.class.getName(), cliDAO);		
		daos.put(Produto.class.getName(), proDAO);	
		daos.put(GrupoPrecificacao.class.getName(), GrupoPrecificacaoDAO);
		daos.put(Ano.class.getName(), anoDAO);
		daos.put(Autor.class.getName(), autorDAO);
		daos.put(Categoria.class.getName(), categoriaDAO);
		daos.put(Editora.class.getName(), editoraDAO);
		daos.put(Livro.class.getName(), livroDAO);
		daos.put(LivroImagem.class.getName(), livroImagemDAO);
		daos.put(Pessoa.class.getName(), pessoaDAO);		
		daos.put(PessoaEndereco.class.getName(), pessoaEnderecoDAO);	
		daos.put(PessoaCartaoCredito.class.getName(), pessoaCartaoCreditoDAO);	
		daos.put(Cidade.class.getName(), cidadeDAO);	
		daos.put(CarrinhoItem.class.getName(), carrinhoItemDAO);	
		daos.put(Carrinho.class.getName(), carrinhoDAO);	
		daos.put(CarrinhoCupom.class.getName(), carrinhoCupomDAO);	
		daos.put(CarrinhoPagamento.class.getName(), carrinhoPagamentoDAO);	
		daos.put(Entrada.class.getName(), entradaDAO);	
		daos.put(EntradaLivro.class.getName(), entradaLivroDAO);	
		daos.put(EstatisticaCategoria.class.getName(), estatisticaCategoriaDAO);
		daos.put(EstatisticaLivroVendidoMes.class.getName(), estatisticaLivroVendidoMesDAO);
		daos.put(Log.class.getName(), logDAO);
		daos.put(PesquisaLivro.class.getName(), pesquisaLivroDAO);
		
		/* Criando inst�ncias de regras de neg�cio a serem utilizados*/		
		ValidadorDadosObrigatoriosFornecedor vrDadosObrigatoriosFornecedor = new ValidadorDadosObrigatoriosFornecedor();
		ValidadorCnpj vCnpj = new ValidadorCnpj();
		ComplementarDtCadastro cDtCadastro = new ComplementarDtCadastro();
		ValidadorCpf vCpf = new ValidadorCpf();
		ValidadorQtdProduto vQtd = new ValidadorQtdProduto();		
		// instancia da regra de negocio de livro
		ValidarExistenciaLivro vExistenciaLivro = new ValidarExistenciaLivro(); 
		ValidarDadosObrigatoriosLivro VDadosObrigatoriosLivro = new ValidarDadosObrigatoriosLivro();
		// Inst�ncia de regras de neg�cio da Pessoa
		ValidarDadosObrigatoriosPessoa ValidarDadosObrigatoriosPessoa = new ValidarDadosObrigatoriosPessoa();
		ValidarSenhasIguais validarSenhasIguais = new ValidarSenhasIguais();
		ValidarExistenciaPessoa vExistenciaPessoa = new ValidarExistenciaPessoa(); 
		ValidadorExistenciaPessoaEmail vExistenciaPessoaEmail = new ValidadorExistenciaPessoaEmail();
		ValidarSenhaForte validarSenhaForte = new ValidarSenhaForte();
		ValidarCarrinhoCupom vCarrinhoCupom = new ValidarCarrinhoCupom();
		ValidarCarrinhoItemEstoque vCarrinhoItemEstoque = new ValidarCarrinhoItemEstoque();
		AtualizarDataCarrinho atualizarDataCarrinho = new AtualizarDataCarrinho();
		GerarCupomCarrinho gerarCupomCarrinho = new GerarCupomCarrinho();
		/* Criando uma lista para conter as regras de neg�cio de fornencedor
		 * quando a opera��o for salvar
		 */
		List<IStrategy> rnsSalvarFornecedor = new ArrayList<IStrategy>();
		List<IStrategy> rnsSalvarLivro = new ArrayList<IStrategy>();
		List<IStrategy> rnsSalvarPessoa = new ArrayList<IStrategy>();
		List<IStrategy> rnsAlterarPessoa = new ArrayList<IStrategy>();
		List<IStrategy> rnsSalvarCarrinhoCupom = new ArrayList<IStrategy>();
		List<IStrategy> rnsAlterarCarrinhoItem = new ArrayList<IStrategy>();
		List<IStrategy> rnsAlterarCarrinho = new ArrayList<IStrategy>();
		
		/* Adicionando as regras a serem utilizadas na opera��o salvar do fornecedor*/
		rnsSalvarFornecedor.add(vrDadosObrigatoriosFornecedor);
		rnsSalvarFornecedor.add(vCnpj);
		rnsSalvarFornecedor.add(cDtCadastro);		
		// Adicionando as regras para Livro
		rnsSalvarLivro.add(vExistenciaLivro);
		rnsSalvarLivro.add(VDadosObrigatoriosLivro);
		// Adicionando regras para Pessoa
		rnsSalvarPessoa.add(ValidarDadosObrigatoriosPessoa);
		rnsSalvarPessoa.add(validarSenhasIguais);
		rnsSalvarPessoa.add(vCpf);
		rnsSalvarPessoa.add(vExistenciaPessoa);
		rnsSalvarPessoa.add(vExistenciaPessoaEmail);
		rnsSalvarPessoa.add(validarSenhaForte);
		rnsAlterarPessoa.add(validarSenhaForte);
		rnsAlterarPessoa.add(validarSenhasIguais);
		// Adicionando regras para CarrinhoCupom
		rnsSalvarCarrinhoCupom.add(vCarrinhoCupom);
		// Carrinho Item
		rnsAlterarCarrinhoItem.add(atualizarDataCarrinho);	
		rnsAlterarCarrinhoItem.add(vCarrinhoItemEstoque);	
		
		// carrinho
		rnsAlterarCarrinho.add(gerarCupomCarrinho);
		
		/* Cria o mapa que poder� conter todas as listas de regras de neg�cio espec�fica 
		 * por opera��o  do fornecedor
		 */
		Map<String, List<IStrategy>> rnsFornecedor = new HashMap<String, List<IStrategy>>();
		Map<String, List<IStrategy>> rnsLivro = new HashMap<String, List<IStrategy>>();
		Map<String, List<IStrategy>> rnsPessoa = new HashMap<String, List<IStrategy>>();
		Map<String, List<IStrategy>> rnsCarrinhoCupom = new HashMap<String, List<IStrategy>>();
		Map<String, List<IStrategy>> rnsCarrinhoItem = new HashMap<String, List<IStrategy>>();
		Map<String, List<IStrategy>> rnsCarrinho = new HashMap<String, List<IStrategy>>();
		
		/*
		 * Adiciona a listra de regras na opera��o salvar no mapa do fornecedor (lista criada na linha 70)
		 */
		rnsFornecedor.put("SALVAR", rnsSalvarFornecedor);	
		rnsLivro.put("SALVAR", rnsSalvarLivro);	
		rnsLivro.put("ALTERAR", rnsSalvarLivro);	
		rnsPessoa.put("SALVAR", rnsSalvarPessoa);	
		rnsPessoa.put("ALTERAR", rnsAlterarPessoa);
		rnsCarrinhoCupom.put("SALVAR", rnsSalvarCarrinhoCupom);
		rnsCarrinhoItem.put("ALTERAR", rnsAlterarCarrinhoItem);
		rnsCarrinho.put("ALTERAR", rnsAlterarCarrinho);
		/* Adiciona o mapa(criado na linha 79) com as regras indexadas pelas opera��es no mapa geral indexado 
		 * pelo nome da entidade
		 */
		rns.put(Fornecedor.class.getName(), rnsFornecedor);
		rns.put(Livro.class.getName(), rnsLivro);
		rns.put(Pessoa.class.getName(), rnsPessoa);
		rns.put(CarrinhoCupom.class.getName(), rnsCarrinhoCupom);
		rns.put(CarrinhoItem.class.getName(), rnsCarrinhoItem);
		rns.put(Carrinho.class.getName(), rnsCarrinho);
		
		/* Criando uma lista para conter as regras de negócio de cliente
		 * quando a operação for salvar
		 */
		List<IStrategy> rnsSalvarCliente = new ArrayList<IStrategy>();
		/* Adicionando as regras a serem utilizadas na operação salvar do cliente */
		rnsSalvarCliente.add(cDtCadastro);		
		rnsSalvarCliente.add(vCpf);
		
		/* Cria o mapa que poder� conter todas as listas de regras de neg�cio espec�fica 
		 * por opera��o do cliente
		 */
		Map<String, List<IStrategy>> rnsCliente = new HashMap<String, List<IStrategy>>();
		/*
		 * Adiciona a listra de regras na opera��o salvar no mapa do cliente (lista criada na linha 93)
		 */
		rnsCliente.put("SALVAR", rnsSalvarCliente);		
		/* Adiciona o mapa(criado na linha 101) com as regras indexadas pelas opera��es no mapa geral indexado 
		 * pelo nome da entidade. Observe que este mapa (rns) � o mesmo utilizado na linha 88.
		 */
		rns.put(Cliente.class.getName(), rnsCliente);
		
		/* Criando uma lista para conter as regras de neg�cio de produto
		 * quando a opera��o for salvar
		 */
		List<IStrategy> rnsSalvarProduto = new ArrayList<IStrategy>();
		/* Adicionando as regras a serem utilizadas na opera��o salvar do produto */
		rnsSalvarProduto.add(cDtCadastro);		
		rnsSalvarProduto.add(vQtd);
		
		/* Criando uma lista para conter as regras de neg�cio de produto
		 * quando a opera��o for alterar
		 */
		List<IStrategy> rnsAlterarProduto = new ArrayList<IStrategy>();
		/* Adicionando as regras a serem utilizadas na opera��o alterar do produto */
		rnsAlterarProduto.add(vQtd);
		
		/* Cria o mapa que poder� conter todas as listas de regras de neg�cio espec�fica 
		 * por opera��o do produto
		 */
		Map<String, List<IStrategy>> rnsProduto = new HashMap<String, List<IStrategy>>();
		/*
		 * Adiciona a listra de regras na opera��o salvar no mapa do produto (lista criada na linha 114)
		 */
		rnsProduto.put("SALVAR", rnsSalvarProduto);
		/*
		 * Adiciona a listra de regras na opera��o alterar no mapa do produto (lista criada na linha 122)
		 */
		rnsProduto.put("ALTERAR", rnsAlterarProduto);
		
		/* Adiciona o mapa(criado na linha 129) com as regras indexadas pelas opera��es no mapa geral indexado 
		 * pelo nome da entidade. Observe que este mapa (rns) � o mesmo utilizado na linha 88.
		 */
		rns.put(Produto.class.getName(), rnsProduto);		
	}
	
	
	@Override
	public Resultado salvar(EntidadeDominio entidade) {
		resultado = new Resultado();
		String nmClasse = entidade.getClass().getName();	
		String msg = executarRegras(entidade, "SALVAR");
		
		if(msg == null){
			IDAO dao = daos.get(nmClasse);
			try {
				dao.salvar(entidade);
				List<EntidadeDominio> entidades = new ArrayList<EntidadeDominio>();
				entidades.add(entidade);
				resultado.setEntidades(entidades);
			} catch (SQLException e) {
				e.printStackTrace();
				resultado.setMsg("Não foi possível realizar o registro!");
			}
		}else{
			resultado.setMsg(msg);
		}
		return resultado;
	}

	@Override
	public Resultado alterar(EntidadeDominio entidade) {
		resultado = new Resultado();
		String nmClasse = entidade.getClass().getName();	
		String msg = executarRegras(entidade, "ALTERAR");
	
		if(msg == null){
			IDAO dao = daos.get(nmClasse);
			try {
				dao.alterar(entidade);
				List<EntidadeDominio> entidades = new ArrayList<EntidadeDominio>();
				entidades.add(entidade);
				resultado.setEntidades(entidades);
			} catch (SQLException e) {
				e.printStackTrace();
				resultado.setMsg("Não foi possível realizar o registro!");
			}
		}else{
			resultado.setMsg(msg);
		}
		return resultado;
	}

	@Override
	public Resultado excluir(EntidadeDominio entidade) {
		resultado = new Resultado();
		String nmClasse = entidade.getClass().getName();	
		
		String msg = executarRegras(entidade, "EXCLUIR");
		
		if(msg == null){
			IDAO dao = daos.get(nmClasse);
			try {
				dao.excluir(entidade);
				List<EntidadeDominio> entidades = new ArrayList<EntidadeDominio>();
				entidades.add(entidade);
				resultado.setEntidades(entidades);
			} catch (SQLException e) {
				e.printStackTrace();
				resultado.setMsg("Nâo foi possível realizar a exlusão!");
			}
		}else{
			resultado.setMsg(msg);
		}
		return resultado;
	}

	@Override
	public Resultado consultar(EntidadeDominio entidade) {
		resultado = new Resultado();
		String nmClasse = entidade.getClass().getName();	
		
		String msg = executarRegras(entidade, "CONSULTAR");
		
		if(msg == null){
			IDAO dao = daos.get(nmClasse);
			try {
				
				resultado.setEntidades(dao.consultar(entidade));
			} catch (SQLException e) {
				e.printStackTrace();
				resultado.setMsg("Nâo foi possível realizar a consulta!");
			}
		}else{
			resultado.setMsg(msg);
		}
		return resultado;
	}
	
	public Resultado visualizar(EntidadeDominio entidade) {
		resultado = new Resultado();
		resultado.setEntidades(new ArrayList<EntidadeDominio>(1));
		resultado.getEntidades().add(entidade);		
		return resultado;
	}
	
	private String executarRegras(EntidadeDominio entidade, String operacao){
		String nmClasse = entidade.getClass().getName();		
		StringBuilder msg = new StringBuilder();
		
		Map<String, List<IStrategy>> regrasOperacao = rns.get(nmClasse);
		
		if(regrasOperacao != null){
			List<IStrategy> regras = regrasOperacao.get(operacao);
			
			if(regras != null){
				for(IStrategy s: regras){			
					String m = s.processar(entidade);			
					
					if(m != null){
						msg.append(m);
						msg.append("\n");
					}			
				}	
			}			
		}
		if(msg.length()>0)
			return msg.toString();
		else
			return null;
	}
}
