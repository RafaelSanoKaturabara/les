package bookcommerce.core.impl.negocio;

import bookcommerce.core.IStrategy;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.Pessoa;

public class ValidarDadosObrigatoriosPessoa implements IStrategy {
	@Override
	public String processar(EntidadeDominio entidade) {
		String inicioRespostaPessoa = "Campos do cadastro faltando: ";
		String respostaOut = "";
		String resposta = "";
		if(entidade instanceof Pessoa){
			Pessoa pessoa = (Pessoa)entidade;		
			if (pessoa.getSenha() != null && pessoa.getRepitaSenha() != null && pessoa.getId() != null && pessoa.getId() > 0) { // Está trocando de senha?
				return null;
			}
			// Validando pessoa
			if(pessoa.getNome() == null || pessoa.getNome() == "")
				resposta += "nome, ";		
			if(pessoa.getSobrenome() == null || pessoa.getSobrenome() == "")
				resposta += "sobrenome, ";		
			if(pessoa.getEmail() == null || pessoa.getEmail() == "")
				resposta += "e-mail, ";		
			if(pessoa.getSenha() == null || pessoa.getSenha() == "")
				resposta += "senha, ";				
			if(pessoa.getCpf() == null || pessoa.getCpf() == "")
				resposta += "cpf, ";
			if(pessoa.getTelefone() == null || pessoa.getTelefone().getNumeroCompleto() == null || pessoa.getTelefone().getNumeroCompleto() == "")
				resposta += "telefone, ";		
			if(pessoa.getDataNascimento() == null)
				resposta += "dataNascimento, ";		
			if(pessoa.getGenero() == null || pessoa.getGenero().getId() == null || pessoa.getGenero().getId() == 0)
				resposta += "genero, ";	 
			if(resposta != "")
				respostaOut += inicioRespostaPessoa + resposta + "\n";
			// Limpado para verificar o endereço
			inicioRespostaPessoa = "Campos do endereço faltando: ";
			resposta = "";
			
			// Validando Endere�o
			if(pessoa.getEnderecoPadrao() == null) { // Sem nenhum campo de endere�o preenchido?
				respostaOut += "Por favor, clique o \"+\" ao lado do endereço para adicionar.\n";
			} else {
				if(pessoa.getEnderecoPadrao().getApelido() == null || pessoa.getEnderecoPadrao().getApelido() == "")
					resposta += "apelido, ";
				if(pessoa.getEnderecoPadrao().getLogradouro() == null || pessoa.getEnderecoPadrao().getLogradouro() == "")
					resposta += "logradouro, ";
				if(pessoa.getEnderecoPadrao().getNumero() == null || pessoa.getEnderecoPadrao().getNumero() == "")
					resposta += "número, ";
				if(pessoa.getEnderecoPadrao().getCep() == null || pessoa.getEnderecoPadrao().getCep() == "")
					resposta += "cep, ";
//				if(pessoa.getEnderecoPadrao().getComplemento() == null || pessoa.getEnderecoPadrao().getComplemento() == "")
//					resposta += "complemento, ";
//				if(pessoa.getEnderecoPadrao().getBairro() == null || pessoa.getEnderecoPadrao().getBairro() == "")
//					resposta += "bairro, ";
				if(pessoa.getEnderecoPadrao().getCidade() == null || pessoa.getEnderecoPadrao().getCidade().getId() == null 
						|| pessoa.getEnderecoPadrao().getCidade().getId() == 0)
					resposta += "cidade, ";	
				if(pessoa.getEnderecoPadrao().getResidenciaTipo() == null || pessoa.getEnderecoPadrao().getResidenciaTipo().getId() == null 
						|| pessoa.getEnderecoPadrao().getResidenciaTipo().getId() == 0)
					resposta += "tipo de residência, ";	
				if(pessoa.getEnderecoPadrao().getLogradouroTipo() == null || pessoa.getEnderecoPadrao().getLogradouroTipo().getId() == null 
						|| pessoa.getEnderecoPadrao().getLogradouroTipo().getId() == 0)
					resposta += "tipo de logradouro, ";	
				
				if(resposta != "") // Houve erro?
					respostaOut += inicioRespostaPessoa + resposta + "\n";				
			}
			// Limpado para verificar o cart�o
			inicioRespostaPessoa = "Campos do cartão faltando: ";
			resposta = "";
			// Validando cart�o
			if(pessoa.getCartaoCreditoPadrao() == null) {
				respostaOut += "Por favor, clique o \"+\" ao lado do cartão para adicionar.\n";
			} else {
				if(pessoa.getCartaoCreditoPadrao().getApelido() == null || pessoa.getCartaoCreditoPadrao().getApelido() == "")
					resposta += "apelido, ";
				if(pessoa.getCartaoCreditoPadrao().getNumero() == null || pessoa.getCartaoCreditoPadrao().getNumero() == "")
					resposta += "numero, ";
				if(pessoa.getCartaoCreditoPadrao().getNomeImpresso() == null || pessoa.getCartaoCreditoPadrao().getNomeImpresso() == "")
					resposta += "nome, ";
				if(pessoa.getCartaoCreditoPadrao().getCodigoSeguranca() == null || pessoa.getCartaoCreditoPadrao().getCodigoSeguranca() == "")
					resposta += "código de segurança, ";
				if(pessoa.getCartaoCreditoPadrao().getCartaoCreditoTipo() == null || pessoa.getCartaoCreditoPadrao().getCartaoCreditoTipo().getId() == null 
						|| pessoa.getCartaoCreditoPadrao().getCartaoCreditoTipo().getId() == 0)
					resposta += "bandeira, ";	
				
				if(resposta != "") // Houve erro?
					respostaOut += inicioRespostaPessoa + resposta + "\n";
			}
			if(respostaOut == "")
				return null;
			return respostaOut;					
		}else{
			return "Deve ser registrado um cliente!";
		}
	}
}
