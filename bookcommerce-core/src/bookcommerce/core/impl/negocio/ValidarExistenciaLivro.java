package bookcommerce.core.impl.negocio;

import bookcommerce.core.IStrategy;
import bookcommerce.core.impl.dao.LivroDAO;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.Livro;

public class ValidarExistenciaLivro implements IStrategy {

	@Override
	public String processar(EntidadeDominio entidade) {
		Livro livro = (Livro)entidade;
		if(livro.getCodBarras() != null && livro.getCodBarras() != "") {
			LivroDAO livroDAO = new LivroDAO();
			if(!livroDAO.validarExistenciaLivro(livro))
				return "Já possui um livro com este Código de barras.";
		}				
		return null; // ok
	}
}
