package bookcommerce.core.impl.negocio;

import java.sql.SQLException;
import java.util.List;

import bookcommerce.core.IStrategy;
import bookcommerce.core.impl.dao.CupomDAO;
import bookcommerce.dominio.CarrinhoCupom;
import bookcommerce.dominio.Cupom;
import bookcommerce.dominio.EntidadeDominio;

public class ValidarCarrinhoCupom implements IStrategy {

	@Override
	public String processar(EntidadeDominio entidade) {
		if(entidade instanceof CarrinhoCupom){
			List<EntidadeDominio> entidadeCupomList = null;
			CarrinhoCupom carrinhoCupom = (CarrinhoCupom)entidade;	
			try {
				entidadeCupomList = new CupomDAO().consultar(carrinhoCupom.getCupom());
			} catch (SQLException e){
				e.printStackTrace();
			}
			if(entidadeCupomList == null || entidadeCupomList.size() == 0) {
				return "Cupom não encontrado ou já utilizado.";			
			}
			carrinhoCupom.setCupom((Cupom)entidadeCupomList.get(0));
		}else{
			return "Deve ser registrado um carrinhoCupom!";
		}
		return null;
	}
}
