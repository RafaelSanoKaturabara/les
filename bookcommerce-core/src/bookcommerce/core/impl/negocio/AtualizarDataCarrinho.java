package bookcommerce.core.impl.negocio;

import java.sql.SQLException;

import bookcommerce.core.IStrategy;
import bookcommerce.core.impl.dao.CarrinhoDAO;
import bookcommerce.dominio.Carrinho;
import bookcommerce.dominio.CarrinhoItem;
import bookcommerce.dominio.EntidadeDominio;

public class AtualizarDataCarrinho implements IStrategy {

	@Override
	public String processar(EntidadeDominio entidade) {
		CarrinhoDAO carrinhoDAO = new CarrinhoDAO();
		try {
			CarrinhoItem carrinhoItem = (CarrinhoItem)entidade;
			Carrinho carrinho = new Carrinho();
			carrinho.setId(carrinhoItem.getCarrinho().getId());
			carrinhoDAO.atualizarDataCarrinho(carrinho);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "Não foi possível atualizar a data da ultima modificação do carrinho";
		}
		return null;
	}
}
