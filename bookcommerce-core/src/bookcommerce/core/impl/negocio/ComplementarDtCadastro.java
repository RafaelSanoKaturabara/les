package bookcommerce.core.impl.negocio;

import java.util.Date;

import bookcommerce.core.IStrategy;
import bookcommerce.dominio.EntidadeDominio;

public class ComplementarDtCadastro implements IStrategy {

	@Override
	public String processar(EntidadeDominio entidade) {		
		
		
		if(entidade !=null){
			Date data = new Date();		
			entidade.setDtCadastro(data);
		}else{
			return "Entidade: nula!";
		}
		
		
		
		return null;
	}

}
