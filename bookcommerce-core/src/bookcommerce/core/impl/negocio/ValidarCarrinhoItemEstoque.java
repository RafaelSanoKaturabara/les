package bookcommerce.core.impl.negocio;

import java.sql.SQLException;
import java.util.List;

import bookcommerce.core.IStrategy;
import bookcommerce.core.impl.dao.CarrinhoItemDAO;
import bookcommerce.core.impl.dao.LivroDAO;
import bookcommerce.dominio.CarrinhoItem;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.Livro;

public class ValidarCarrinhoItemEstoque implements IStrategy {

	@Override
	public String processar(EntidadeDominio entidade) {
		if(entidade instanceof CarrinhoItem){
			List<EntidadeDominio> entidadeLivroList = null;
			CarrinhoItem carrinhoItem = (CarrinhoItem)entidade;	
			int qtdeEstoque = 0, qtdeItemNoCarrinho = 0;
			try {
				// obtendo o estoque
				Livro livroWork = new Livro();
				livroWork.setId(carrinhoItem.getLivro().getId());
				entidadeLivroList = new LivroDAO().consultar(livroWork);		 // deve conter o estoque aqui
				Livro livroWorkEstoque = new Livro();
				if(entidadeLivroList.size() > 0) {
					livroWorkEstoque = (Livro)entidadeLivroList.get(0);
					qtdeEstoque = livroWorkEstoque.getEstoque();				// temos o estoque do produto considerando o proprio carrinho
				}
				// obtendo a quantidade o produto no próprio carrinho
				CarrinhoItemDAO carrinhoItemDAO = new CarrinhoItemDAO();
				List<EntidadeDominio> carrinhoItemList = carrinhoItemDAO.consultar(entidade); // carrinho id/ livro id
				if(carrinhoItemList.size() > 0) {
					CarrinhoItem ci = (CarrinhoItem)carrinhoItemList.get(0);
					qtdeItemNoCarrinho = ci.getQuantidade();
				}
				
				if(qtdeEstoque < carrinhoItem.getQuantidade() - qtdeItemNoCarrinho)
					return "Quantidade insulficiente no estoque (" + (qtdeEstoque + qtdeItemNoCarrinho) + ")";
			} catch (SQLException e){
				e.printStackTrace();
			}
//			if(entidadeCarrinhoItemList != null && entidadeCarrinhoItemList.size() > 0) {
//				
//			} else {
//				return "Erro interno ao obter o estoque.";		
//			}
		}else{
			return "Deve ser registrado um carrinho item!";
		}
		return null;
	}
}
