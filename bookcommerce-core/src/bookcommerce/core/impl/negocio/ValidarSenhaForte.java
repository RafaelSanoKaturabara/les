package bookcommerce.core.impl.negocio;

import bookcommerce.core.IStrategy;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.Pessoa;

public class ValidarSenhaForte implements IStrategy {
	@Override
	public String processar(EntidadeDominio entidade) {
		if(entidade instanceof Pessoa){
			Pessoa motorista = (Pessoa)entidade;
			if(motorista.getSenha() == null)
				return null;
			char[] arraySenha = motorista.getSenha().toCharArray();
			boolean temMaiuscula = false,  temMimuscula = false,temNumero = false, temCaracterEspecial = false, temMaisQueOito = false;
			for (char c : arraySenha) {
				if(Character.isLowerCase(c)) {
					temMimuscula = true;	//Significa que a senha tem pelo menos uma letra
					break;
				}
			}
			for (char c : arraySenha) {
				if(Character.isUpperCase(c)) {
					temMaiuscula = true;	//Significa que a senha tem pelo menos uma letra
					break;
				}
			}
			for (char c : arraySenha) {
				if(Character.isDigit(c)) {
					temNumero = true;	//Significa que a senha tem pelo menos um número
					break;
				}
			}
			for (char c : arraySenha) {
				if (!Character.isDigit(c) && !Character.isLetter(c)) {
					temCaracterEspecial = true;	//Significa que a senha tem pelo menos um caracter especial
					break;
				}
			}
			if(arraySenha.length > 8) {
				temMaisQueOito = true;
			}
				
			if(!temMaiuscula|| !temMimuscula || !temNumero || !temCaracterEspecial || !temMaisQueOito)
				return "Senha inválida! Ela deve conter no mínimo 8 caracteres, números, letras maiúsculas, minúsculas e caractere especial!";
		}else{
			return "Senha não pode ser validada, pois entidade não é um motorista!";
		}
		return null;
	}
}
