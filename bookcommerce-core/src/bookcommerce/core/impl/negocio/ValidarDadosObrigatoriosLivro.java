package bookcommerce.core.impl.negocio;

import bookcommerce.core.IStrategy;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.Livro;

public class ValidarDadosObrigatoriosLivro implements IStrategy {

	@Override
	public String processar(EntidadeDominio entidade) {		
		if(entidade instanceof Livro){
			Livro livro = (Livro)entidade;			
			if(livro.getCodBarras() == null || livro.getCodBarras() == "" || 
					livro.getTitulo() == null || livro.getTitulo() == "" ||
					livro.getIsbn() == null || livro.getIsbn() == "" ||
					livro.getSinopse() == null || livro.getSinopse() == "" ||
					livro.getAltura() <= 0 || livro.getLargura() <= 0 ||
					livro.getPeso() <= 0 || livro.getProfundidade() <= 0
					){
				return "Todos os campos s�o obrigat�rios e n�o podem ser valores 0 ou negativos.";
			}			
		}else{
			return "Deve ser registrado um livro!";
		}
		return null;
	}
}

