package bookcommerce.core.impl.negocio;

import java.sql.SQLException;
import java.util.List;

import bookcommerce.core.IStrategy;
import bookcommerce.core.impl.dao.PessoaDAO;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.Pessoa;

public class ValidadorExistenciaPessoaEmail implements IStrategy {
	@Override
	public String processar(EntidadeDominio entidade) {
		Pessoa pessoa = (Pessoa)entidade;
		if(pessoa.getCpf() != null && pessoa.getCpf() != "") {
			PessoaDAO pessoaDAO = new PessoaDAO();
			try {
				Pessoa pe = new Pessoa();
				pe.setEmail(pessoa.getEmail());
				List<EntidadeDominio> pessoaList = pessoaDAO.consultar(pe);
				if(pessoaList.size() > 0)
					return "Já possui uma pessoa com este e-mail.";
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}				
		return null; // ok
	}
}
