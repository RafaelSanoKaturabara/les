package bookcommerce.core.impl.negocio;

import java.sql.SQLException;
import java.util.List;
import java.util.Random;

import bookcommerce.core.IStrategy;
import bookcommerce.core.impl.dao.CupomDAO;
import bookcommerce.dominio.Carrinho;
import bookcommerce.dominio.Cupom;
import bookcommerce.dominio.CupomDescontoTipo;
import bookcommerce.dominio.CupomTipo;
import bookcommerce.dominio.EntidadeDominio;

public class GerarCupomCarrinho implements IStrategy {

	private static String getRandomPass(int len){
		char[] chart ={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
		char[] senha= new char[len];
		int chartLenght = chart.length;
		Random rdm = new Random();
		for (int x=0; x<len; x++)
			senha[x] = chart[rdm.nextInt(chartLenght)];
		return new String(senha);
	}
	
	@Override
	public String processar(EntidadeDominio entidade) {
		if(entidade instanceof Carrinho){
			Carrinho carrinho = (Carrinho)entidade;			
			if(carrinho.getCarrinhoStatus().getId() != 7) // não é carrinho de aprovação de troca?
				return null;			
			//List<EntidadeDominio> entidadeLivroList = null;
			List<EntidadeDominio> entCupomList = null;
			String codigoCupom = "";
			CupomDAO cupomDAO = new CupomDAO();
			Cupom cupomWork = new Cupom();
			try {
				do {
					codigoCupom = getRandomPass(10);
					cupomWork.setCodigo(codigoCupom);
					entCupomList = cupomDAO.consultar(cupomWork);
				} while (entCupomList.size() > 0);
				// buscando o total da compra
				cupomWork.setValorDesconto(35.25f);
				cupomWork.setCupomTipo(new CupomTipo());
				cupomWork.getCupomTipo().setId(3);
				cupomWork.setCupomDescontoTipo(new CupomDescontoTipo());
				cupomWork.getCupomDescontoTipo().setId(2);
				cupomWork.setStatus(true);
				
				// salvando o cupom no carrinho
				cupomDAO.salvar(cupomWork);
				carrinho.setCupomDevolucao(new Cupom());
				carrinho.getCupomDevolucao().setId(cupomWork.getId());
				
			} catch (SQLException e){
				e.printStackTrace();
			}
//			if(entidadeCarrinhoItemList != null && entidadeCarrinhoItemList.size() > 0) {
//				
//			} else {
//				return "Erro interno ao obter o estoque.";		
//			}
		}else{
			return "Deve ser registrado um carrinho item!";
		}
		return null;
	}

}
