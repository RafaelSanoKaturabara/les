package bookcommerce.core.impl.negocio;

import bookcommerce.core.IStrategy;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.Pessoa;

public class ValidadorCpf implements IStrategy {
	@Override
	public String processar(EntidadeDominio entidade) {
		if(entidade instanceof Pessoa){
			Pessoa p = (Pessoa)entidade;
			if (p.getSenha() != null && p.getRepitaSenha() != null && p.getId() != null && p.getId() > 0) { // Está trocando de senha?
				return null;
			}
			if(p.getCpf().length() < 9){
				return "CPF deve conter 14 digitos!";
			}
		}else{
			return "CPF não pode ser validado, pois entidade não é um cliente!";
		}
		return null;
	}
}
