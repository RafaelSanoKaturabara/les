package bookcommerce.core.impl.negocio;

import bookcommerce.core.IStrategy;
import bookcommerce.dominio.EntidadeDominio;
import bookcommerce.dominio.Pessoa;

public class ValidarSenhasIguais implements IStrategy {
	@Override
	public String processar(EntidadeDominio entidade) {
		if(entidade instanceof Pessoa){
			Pessoa pessoa = (Pessoa)entidade;
			if(pessoa.getSenha() == null)
				return null;
			if(!pessoa.getSenha().equals(pessoa.getRepitaSenha()))
				return "Digite novamente duas senhas iguais.";
		}else{
			return "Deve ser registrado um livro!";
		}
		return null;
	}
}
