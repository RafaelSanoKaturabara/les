package bookcommerce.core;

import bookcommerce.dominio.EntidadeDominio;


public interface IStrategy 
{
	public String processar(EntidadeDominio entidade);	
}
