
package bookcommerce.core.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;



public class ConverteDate {

	public static String converteDateString(Date dtData){  
	   SimpleDateFormat formatBra = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");		  	     
	   return (formatBra.format(dtData)); 
	} 
		
	public static Date converteStringDate(String data) {   
        if (data == null || data.equals(""))  
            return null;  

        Date date = null;  
        try {  
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");  
            date = (java.util.Date)formatter.parse(data);  
            return date;
        } catch (ParseException e) {              
            e.printStackTrace();
        }  
        return null;
	}
	
	public static Timestamp Convert(Date date) {
		if(date == null)
			return null;
		Timestamp timestamp = new Timestamp(date.getTime());		
		return timestamp;		
	}
	
	public static Date Convert(java.sql.Date sqlDate) {
		if(sqlDate == null)
			return null;
		Date utilDate = new Date(sqlDate.getTime());	
		return utilDate;		
	}
	
	public static Date Convert(Timestamp timestamp) {
		if(timestamp == null)
			return null;
		Date utilDate = new Date(timestamp.getTime());		
		return utilDate;		
	}
}
