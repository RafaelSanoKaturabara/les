package bookcommerce.core.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class CriptografiaUtil {

	  // gerando um hash code simples
      public String encrypt(final String msgString) {
	  MessageDigest algorithm;
	  String senha = "";
		try {
			algorithm = MessageDigest.getInstance("SHA-256");
			byte messageDigest[] = algorithm.digest(msgString.getBytes("UTF-8"));
	    	   
	    	  StringBuilder hexString = new StringBuilder();
	    	  for (byte b : messageDigest) {
	    	    hexString.append(String.format("%02X", 0xFF & b));
	    	  }
	    	  senha = hexString.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}    	  
		return senha;
      } 
}
