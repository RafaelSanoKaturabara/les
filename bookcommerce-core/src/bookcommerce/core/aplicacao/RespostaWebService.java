package bookcommerce.core.aplicacao;

public class RespostaWebService extends EntidadeAplicacao{
	private int codigoResposta;
	private boolean flgResposta;
	private String mensagemResposta;
	private String errosString;
	private Resultado resultado;
	
	public int getCodigoResposta() {
		return codigoResposta;
	}
	public void setCodigoResposta(int codigoResposta) {
		this.codigoResposta = codigoResposta;
	}
	public boolean isFlgResposta() {
		return flgResposta;
	}
	public void setFlgResposta(boolean flgResposta) {
		this.flgResposta = flgResposta;
	}
	public String getMensagemResposta() {
		return mensagemResposta;
	}
	public void setMensagemResposta(String mensagemResposta) {
		this.mensagemResposta = mensagemResposta;
	}
	public Resultado getResultado() {
		return resultado;
	}
	public void setResultado(Resultado resultado) {
		this.resultado = resultado;
	}
	
	public RespostaWebService(int codigo, String mensagem, Boolean flag, Resultado resultado) {
		codigoResposta = codigo;
		flgResposta = flag;
		mensagemResposta = mensagem;
		this.resultado = resultado;
	}
	
	public RespostaWebService(int codigo, String mensagem, String mensagemResposta, Boolean flag, Resultado resultado) {
		codigoResposta = codigo;
		flgResposta = flag;
		mensagemResposta = mensagem;
		this.resultado = resultado;
		this.mensagemResposta = mensagemResposta;
	}
	
	public RespostaWebService(int codigo, String mensagem, String mensagemResposta, Boolean flag) {
		codigoResposta = codigo;
		flgResposta = flag;
		mensagemResposta = mensagem;
		this.mensagemResposta = mensagemResposta;
	}
	
	public RespostaWebService() {
		
	}
	public String getErrosString() {
		return errosString;
	}
	public void setErrosString(String errosString) {
		this.errosString = errosString;
	}
}
